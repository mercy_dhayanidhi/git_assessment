package org.tupeloproject.cassandra.context;

import org.apache.cassandra.service.EmbeddedCassandraService;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.tupeloproject.kernel.context.AbstractTripleContextTest;
import org.tupeloproject.kernel.context.TripleContext;

public class CassandraContextTripleTest extends AbstractTripleContextTest {
	private static CassandraContext cc;

	@BeforeClass
	public static void setupCassandra() throws Exception {
		EmbeddedCassandraService cassandra = new EmbeddedCassandraService();
		cassandra.start();
	}

	@Before
	public void setupContext() throws Exception {
		cc = new CassandraContext();
		try {
			cc.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
		cc.initialize();
	}

	@After
	public void destroyContext() throws Exception {
		cc.destroy();
	}

	@Override
	protected TripleContext getContext() throws Exception {
		if (cc == null) {
			setupCassandra();
		}
		setupContext();
		return cc;
	}
}
