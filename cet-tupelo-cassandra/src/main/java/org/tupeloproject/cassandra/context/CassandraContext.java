package org.tupeloproject.cassandra.context;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.cassandra.thrift.CfDef;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.KeyRange;
import org.apache.cassandra.thrift.KsDef;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.scale7.cassandra.pelops.Bytes;
import org.scale7.cassandra.pelops.Cluster;
import org.scale7.cassandra.pelops.IConnection;
import org.scale7.cassandra.pelops.KeyspaceManager;
import org.scale7.cassandra.pelops.Mutator;
import org.scale7.cassandra.pelops.Pelops;
import org.scale7.cassandra.pelops.Selector;
import org.scale7.cassandra.pelops.exceptions.PelopsException;
import org.scale7.cassandra.pelops.pool.CommonsBackedPool.Policy;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.context.AbstractTripleContext;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.PagingTable;
import org.tupeloproject.util.ListTable;
import org.tupeloproject.util.Tuple;
import org.tupeloproject.util.Variable;

public class CassandraContext extends AbstractTripleContext {
    private static Log log              = LogFactory.getLog(CassandraContext.class);

    private int        batchSize        = 100;
    private String     host             = "localhost";                              //$NON-NLS-1$
    private int        port             = 9160;
    private int        replication      = 1;
    private String     pool             = "tupelo";                                 //$NON-NLS-1$
    private String     keyspace         = "tupelo";                                 //$NON-NLS-1$
    private int        initTimeout      = 300000;                                   //5 * 60 * 1000;                            // 5 minutes
    private int        maxTries         = 5;
    private int        operationTimeout = 60000;                                    //5 * 60 * 1000;                            // 5 minutes
    private boolean    autoDiscovery    = false;
    private int        consistencyLevel = ConsistencyLevel.ONE.getValue();

    private boolean    isOpen           = false;

    public CassandraContext() {
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getReplication() {
        return replication;
    }

    public void setReplication(int replication) {
        this.replication = replication;
    }

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public int getInitTimeout() {
        return initTimeout;
    }

    public void setInitTimeout(int initTimeout) {
        this.initTimeout = initTimeout;
    }

    public int getMaxTries() {
        return maxTries;
    }

    public void setMaxTries(int maxTries) {
        this.maxTries = maxTries;
    }

    public int getOperationTimeout() {
        return operationTimeout;
    }

    public void setOperationTimeout(int operationTimeout) {
        this.operationTimeout = operationTimeout;
    }

    public boolean isAutoDiscovery() {
        return autoDiscovery;
    }

    public void setAutoDiscovery(boolean autoDiscovery) {
        this.autoDiscovery = autoDiscovery;
    }

    public int getConsistencyLevel() {
        return consistencyLevel;
    }

    public void setConsistencyLevel(int consistencyLevel) {
        if (ConsistencyLevel.findByValue(consistencyLevel) != null) {
            this.consistencyLevel = consistencyLevel;
        }
    }

    public void setConsistencyLevel(ConsistencyLevel consistencyLevel) {
        this.consistencyLevel = consistencyLevel.getValue();
    }

    // ----------------------------------------------------------------------
    // CONTEXT
    // ----------------------------------------------------------------------

    @Override
    public void open() throws OperatorException {
        // init the connection pool
        Cluster cluster = new Cluster(host, new IConnection.Config(port, true, operationTimeout), autoDiscovery);
        Policy policy = new Policy(cluster);
        policy.setMaxWaitForConnection(3 * operationTimeout);
        Pelops.addPool(pool, cluster, keyspace, policy, null);
        isOpen = true;
    }

    @Override
    public void close() throws OperatorException {
        if (isOpen) {
            Pelops.removePool(pool);
        }
        isOpen = false;
    }

    @Override
    public void initialize() throws OperatorException {
        if (isOpen) {
            close();
            isOpen = false;
        }
        List<CfDef> columns = new ArrayList<CfDef>();

        CfDef col = new CfDef(keyspace, "subject"); //$NON-NLS-1$
        col.setColumn_type("Super"); //$NON-NLS-1$
        col.setComparator_type("org.apache.cassandra.db.marshal.UTF8Type"); //$NON-NLS-1$
        col.setComment("Triples sorted by subject.");
        columns.add(col);

        col = new CfDef(keyspace, "predicate"); //$NON-NLS-1$
        col.setColumn_type("Super"); //$NON-NLS-1$
        col.setComparator_type("org.apache.cassandra.db.marshal.UTF8Type"); //$NON-NLS-1$
        col.setComment("Triples sorted by predicate.");
        columns.add(col);

        col = new CfDef(keyspace, "object"); //$NON-NLS-1$
        col.setColumn_type("Super"); //$NON-NLS-1$
        col.setComparator_type("org.apache.cassandra.db.marshal.UTF8Type"); //$NON-NLS-1$
        col.setComment("Triples sorted by object.");
        columns.add(col);

        col = new CfDef(keyspace, "blob"); //$NON-NLS-1$
        col.setColumn_type("Standard"); //$NON-NLS-1$
        col.setComparator_type("org.apache.cassandra.db.marshal.UTF8Type"); //$NON-NLS-1$
        col.setComment("Blobs associated with subject.");
        columns.add(col);

        Cluster cluster = new Cluster(host, new IConnection.Config(port, true, initTimeout), autoDiscovery);
        for (int attempt = 0;; attempt++ ) {
            KeyspaceManager km = new KeyspaceManager(cluster, initTimeout);
            try {
                km.addKeyspace(new KsDef(keyspace, KeyspaceManager.KSDEF_STRATEGY_SIMPLE, replication, columns));
                return;
            } catch (InvalidRequestException e) {
                log.info(String.format("Failed to execute iniitalize, try %d.", attempt), e);
                if (attempt > maxTries) {
                    throw (new OperatorException("Could not perform initialize.", e));
                }
            } catch (Exception e) {
                throw (new OperatorException("Could not perform initialize.", e));
            }
        }
    }

    @Override
    public void destroy() throws OperatorException {
        if (isOpen) {
            close();
            isOpen = false;
        }

        Cluster cluster = new Cluster(host, new IConnection.Config(port, true, initTimeout), autoDiscovery);
        KeyspaceManager km = new KeyspaceManager(cluster, initTimeout);

        try {
            km.dropKeyspace(keyspace);
        } catch (InvalidRequestException e) {
            if (!e.getWhy().equals("Keyspace does not exist.")) {
                throw (new OperatorException("Could not remove keyspace", e));
            }
            log.info("keyspace did not exist, is ok.", e);
        } catch (Exception e) {
            throw (new OperatorException("Could not remove keyspace", e));
        }
    }

    @Override
    public void sync() throws OperatorException {
    }

    // ----------------------------------------------------------------------
    // TRIPLE OPERATIONS
    // ----------------------------------------------------------------------       

    public void perform(TripleWriter tw) throws OperatorException {
        if (!isOpen) {
            open();
        }
        for (int attempt = 0;; attempt++ ) {
            try {
                Mutator mutator = Pelops.createMutator(pool);
                for (Triple t : tw.getToAdd() ) {
                    String s = t.getSubject().toNTriples();
                    String p = t.getPredicate().toNTriples();
                    String o = t.getObject().toNTriples();
                    mutator.writeSubColumn("subject", s, p, mutator.newColumn(o)); //$NON-NLS-1$
                    mutator.writeSubColumn("predicate", p, o, mutator.newColumn(s)); //$NON-NLS-1$
                    mutator.writeSubColumn("object", o, s, mutator.newColumn(p)); //$NON-NLS-1$
                }
                for (Triple t : tw.getToRemove() ) {
                    String s = t.getSubject().toNTriples();
                    String p = t.getPredicate().toNTriples();
                    String o = t.getObject().toNTriples();
                    mutator.deleteSubColumn("subject", s, p, o); //$NON-NLS-1$
                    mutator.deleteSubColumn("predicate", p, o, s); //$NON-NLS-1$
                    mutator.deleteSubColumn("object", o, s, p); //$NON-NLS-1$
                }
                mutator.execute(ConsistencyLevel.findByValue(consistencyLevel));
                return;
            } catch (Exception exc) {
                log.info(String.format("Failed to execute triplewriter, try %d.", attempt), exc);
                if (attempt > maxTries) {
                    throw (new OperatorException("Could not perform triplewriter.", exc));
                }
            }
        }
    }

    public void perform(Unifier uf) throws OperatorException {
        ListTable<Resource> result = new ListTable<Resource>();
        result.setColumnNames(new Tuple<String>(uf.getColumnNames()));

        // create list of all open vars
        HashMap<String, Resource> vars = new HashMap<String, Resource>();
        for (org.tupeloproject.rdf.query.Pattern p : uf.getPatterns() ) {
            if (!p.get(0).isBound()) {
                vars.put(p.get(0).getName(), null);
            }
            if (!p.get(1).isBound()) {
                vars.put(p.get(1).getName(), null);
            }
            if (!p.get(2).isBound()) {
                vars.put(p.get(2).getName(), null);
            }
        }

        // call worker
        unify(uf, 0, vars, result);

        // fix paging
        if (uf.hasPaging()) {
            uf.setResult(new PagingTable(uf, result));
        } else {
            uf.setResult(result);
        }
    }

    private void unify(Unifier uf, int idx, Map<String, Resource> vars, ListTable<Resource> table) throws OperatorException {
        if (idx >= uf.getPatterns().size()) {
            Tuple<Resource> result = new Tuple<Resource>(uf.getColumnNames().size());
            for (int i = 0; i < result.size(); i++ ) {
                String name = uf.getColumnNames().get(i);
                if (vars.containsKey(name)) {
                    result.set(i, vars.get(name));
                }
            }
            table.addRow(result);
            return;
        }

        org.tupeloproject.rdf.query.Pattern p = uf.getPatterns().get(idx);
        TripleMatcher tm = new TripleMatcher();

        // get pattern
        Variable<Resource> subj = p.get(0);
        if (subj.isBound()) {
            tm.setSubject(subj.getValue());
        } else {
            tm.setSubject(vars.get(subj.getName()));
        }
        Variable<Resource> pred = p.get(1);
        if (pred.isBound()) {
            tm.setPredicate(pred.getValue());
        } else {
            tm.setPredicate(vars.get(pred.getName()));
        }
        Variable<Resource> obj = p.get(2);
        if (obj.isBound()) {
            tm.setObject(obj.getValue());
        } else {
            tm.setObject(vars.get(obj.getName()));
        }

        // execute
        if (((tm.getSubject() == null) || tm.getSubject().isUri()) && ((tm.getPredicate() == null) || tm.getPredicate().isUri())) {
            perform(tm);
        }

        // make sure we got something
        if ((tm.getResult() != null) && tm.getResult().size() > 0) {
            // fill in vars
            for (Triple t : tm.getResult() ) {
                Map<String, Resource> clone = new HashMap<String, Resource>();
                clone.putAll(vars);
                if (!subj.isBound()) {
                    clone.put(subj.getName(), t.getSubject());
                }
                if (!pred.isBound()) {
                    clone.put(pred.getName(), t.getPredicate());
                }
                if (!obj.isBound()) {
                    clone.put(obj.getName(), t.getObject());
                }
                unify(uf, idx + 1, clone, table);
            }
        } else if (p.isOptional()) {
            unify(uf, idx + 1, vars, table);
        }
    }

    public void perform(TripleMatcher tm) throws OperatorException {
        if (!isOpen) {
            open();
        }

        for (int attempt = 0;; attempt++ ) {
            try {
                // start empty
                Selector selector = Pelops.createSelector(pool);
                Set<Triple> result = new HashSet<Triple>();

                if ((tm.getSubject() != null) && !((tm.getPredicate() == null) && (tm.getObject() != null))) {
                    // get a batch
                    SlicePredicate slice;
                    if (tm.getPredicate() == null) {
                        slice = Selector.newColumnsPredicateAll(false);
                    } else {
                        slice = Selector.newColumnsPredicate(tm.getPredicate().toNTriples());
                    }
                    List<SuperColumn> supercols = selector.getSuperColumnsFromRow("subject", tm.getSubject().toNTriples(), slice, ConsistencyLevel.findByValue(consistencyLevel)); //$NON-NLS-1$

                    // get triples
                    for (SuperColumn sc : supercols ) {
                        Resource predicate = Resource.fromNTriples(Bytes.fromByteArray(sc.getName()).toUTF8());
                        for (Column col : sc.columns ) {
                            Resource object = Resource.fromNTriples(Bytes.fromByteArray(col.getName()).toUTF8());
                            if ((tm.getObject() == null) || (tm.getObject().equals(object))) {
                                result.add(new Triple(tm.getSubject(), predicate, object));
                            }
                        }
                    }

                } else if ((tm.getPredicate() != null) && (tm.getSubject() == null)) {
                    // get a batch
                    SlicePredicate slice;
                    if (tm.getObject() == null) {
                        slice = Selector.newColumnsPredicateAll(false);
                    } else {
                        slice = Selector.newColumnsPredicate(tm.getObject().toNTriples());
                    }
                    List<SuperColumn> supercols = selector.getSuperColumnsFromRow("predicate", tm.getPredicate().toNTriples(), slice, ConsistencyLevel.findByValue(consistencyLevel)); //$NON-NLS-1$

                    // get triples
                    for (SuperColumn sc : supercols ) {
                        Resource object = Resource.fromNTriples(Bytes.fromByteArray(sc.getName()).toUTF8());
                        for (Column col : sc.columns ) {
                            Resource subject = Resource.fromNTriples(Bytes.fromByteArray(col.getName()).toUTF8());
                            result.add(new Triple(subject, tm.getPredicate(), object));
                        }
                    }

                } else if ((tm.getObject() != null) && (tm.getPredicate() == null)) {
                    // get a batch
                    SlicePredicate slice;
                    if (tm.getSubject() == null) {
                        slice = Selector.newColumnsPredicateAll(false);
                    } else {
                        slice = Selector.newColumnsPredicate(tm.getSubject().toNTriples());
                    }
                    String obj = tm.getObject().toNTriples();
                    List<SuperColumn> supercols = selector.getSuperColumnsFromRow("object", obj, slice, ConsistencyLevel.findByValue(consistencyLevel)); //$NON-NLS-1$

                    // get triples
                    for (SuperColumn sc : supercols ) {
                        Resource subject = Resource.fromNTriples(Bytes.fromByteArray(sc.getName()).toUTF8());
                        for (Column col : sc.columns ) {
                            Resource predicate = Resource.fromNTriples(Bytes.fromByteArray(col.getName()).toUTF8());
                            result.add(new Triple(subject, predicate, tm.getObject()));
                        }
                    }

                } else {
                    tm.setIterator(new CassandraIterator());
                    return;
                }

                tm.setIterator(result);
                return;
            } catch (Exception exc) {
                log.info(String.format("Failed to execute triplematcher, try %d (%s).", attempt, exc.getMessage()), exc);
                if (attempt > maxTries) {
                    throw (new OperatorException("Could not perform triplematcher.", exc));
                }
            }
        }
    }

    public void perform(TripleIterator ti) throws OperatorException {
        if (!isOpen) {
            open();
        }
        ti.setIterator(new CassandraIterator());
    }

    // ----------------------------------------------------------------------
    // RESULT ITERATOR
    // ----------------------------------------------------------------------

    private class CassandraIterator implements Iterator<Triple> {
        private Bytes        EMPTY   = Bytes.fromUTF8("");     //$NON-NLS-1$
        private Bytes        lastkey = EMPTY;
        private List<Triple> triples = new ArrayList<Triple>();
        private int          index   = 0;

        public boolean hasNext() {
            if (index < triples.size()) {
                return true;
            }

            // start empty
            triples.clear();
            index = 0;
            Bytes last = lastkey;

            for (int attempt = 0;; attempt++ ) {
                try {
                    // get a batch
                    Selector selector = Pelops.createSelector(pool);
                    KeyRange range = Selector.newKeyRange(lastkey, EMPTY, batchSize);
                    Map<Bytes, List<SuperColumn>> map = selector.getSuperColumnsFromRows("subject", range, false, ConsistencyLevel.findByValue(consistencyLevel)); //$NON-NLS-1$

                    // get triples
                    for (Entry<Bytes, List<SuperColumn>> entry : map.entrySet() ) {
                        if (entry.getKey().equals(lastkey)) {
                            continue;
                        }
                        last = entry.getKey();
                        Resource subject = Resource.fromNTriples(entry.getKey().toUTF8());
                        for (SuperColumn sc : entry.getValue() ) {
                            Resource predicate = Resource.fromNTriples(Bytes.fromByteArray(sc.getName()).toUTF8());
                            for (Column col : sc.columns ) {
                                Resource object = Resource.fromNTriples(Bytes.fromByteArray(col.getName()).toUTF8());
                                triples.add(new Triple(subject, predicate, object));
                            }
                        }
                    }

                    lastkey = last;
                    return (triples.size() > 0);
                } catch (ParseException exc) {
                    log.error("Error fetching triples.", exc);
                    return false;
                } catch (PelopsException exc) {
                    log.info(String.format("Failed to execute tripleiterator, try %d.", attempt), exc);
                    if (attempt > maxTries) {
                        log.error("Failed to execute tripleiterator", exc);
                        return false;
                    }
                }
            }

        }

        public Triple next() {
            if ((index >= triples.size()) && !hasNext()) {
                return null;
            }
            return triples.get(index++);
        }

        public void remove() {
        }

    }
}
