package edu.illinois.ncsa.cet.persistence.jena;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.hp.hpl.jena.db.DBConnection;
import com.hp.hpl.jena.db.IDBConnection;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;

import edu.illinois.ncsa.cet.jena.beans.BeanFactory;
import edu.illinois.ncsa.cet.jena.beans.BeanUtil;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;

public class JenaBeanModule extends AbstractModule {
    private static Logger        log             = LoggerFactory.getLogger(JenaBeanModule.class);
    private String               persistenceName = null;

    private ThreadLocal<Session> session         = new ThreadLocal<Session>();

    public JenaBeanModule(String persistenceName) {
        this.persistenceName = persistenceName;
    }

    @Override
    protected void configure() {
        bind(AnnotationBeanUtil.class).to(JenaAnnotationBeanUtil.class);
        bind(CollectionBeanUtil.class).to(JenaCollectionBeanUtil.class);
        bind(DatasetBeanUtil.class).to(JenaDatasetBeanUtil.class);
        bind(PersonBeanUtil.class).to(JenaPersonBeanUtil.class);
        bind(TagBeanUtil.class).to(JenaTagBeanUtil.class);
        bind(TagEventBeanUtil.class).to(JenaTagEventBeanUtil.class);

        bind(UnitOfWork.class).to(JenaUnitOfWork.class);
    }

    @Provides
    BeanUtil getBeanUtil(BeanFactory factory) {
        return new BeanUtil(factory);
    }

    @Provides
    Session getSession(BeanFactory factory) {
        if ((session.get() == null) || session.get().wasComitted()) {
            session.set(factory.openSession());
        }
        return session.get();
    }

    @Provides
    @Singleton
    BeanFactory getBeanFactory() {
        if ((persistenceName == null) || persistenceName.isEmpty() || persistenceName.equalsIgnoreCase("memory")) {
            return new BeanFactory(ModelFactory.createDefaultModel());
        }

        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> resources = cl.getResources("META-INF/" + persistenceName + ".properties");
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                InputStream is = url.openStream();
                try {
                    Properties props = new Properties();
                    props.load(is);

                    if (props.containsKey("DB")) {
                        Class.forName(props.getProperty("DB_CLASS"));

                        // Create database connection
                        IDBConnection conn = new DBConnection(props.getProperty("DB_URL"), props.getProperty("DB_USER"), props.getProperty("DB_PASSWD"), props.getProperty("DB"));
                        ModelMaker maker = ModelFactory.createModelRDBMaker(conn);

                        // create or open the default model
                        Model model = maker.createDefaultModel();
                        return new BeanFactory(model);
                    }
                } catch (Exception e) {
                    log.error("Could not find conext with url " + url);
                } finally {
                    is.close();
                }
            }
            log.error("Could not find conext with name " + persistenceName);
        } catch (Exception e) {
            log.error("Could not load conext from xmlfile " + persistenceName, e);
        }

        Model model = ModelFactory.createDefaultModel();
        return new BeanFactory(model);
        //        return null;
    }
}
