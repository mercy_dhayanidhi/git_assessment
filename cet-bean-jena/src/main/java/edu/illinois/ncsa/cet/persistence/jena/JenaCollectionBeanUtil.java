package edu.illinois.ncsa.cet.persistence.jena;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;

public class JenaCollectionBeanUtil extends JenaBeanUtil<CollectionBean> implements CollectionBeanUtil {

    @Inject
    protected JenaCollectionBeanUtil(Session session, Provider<CollectionBean> creator) {
        super(session, creator);
    }
}
