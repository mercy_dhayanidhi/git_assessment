package edu.illinois.ncsa.cet.persistence.jena;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class JenaTagEventBeanUtil extends JenaBeanUtil<TagEventBean> implements TagEventBeanUtil {

    @Inject
    protected JenaTagEventBeanUtil(Session session, Provider<TagEventBean> creator) {
        super(session, creator);
    }
}
