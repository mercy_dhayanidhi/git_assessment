package edu.illinois.ncsa.cet.persistence.jena;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;

public class JenaPersonBeanUtil extends JenaBeanUtil<PersonBean> implements PersonBeanUtil {

    @Inject
    protected JenaPersonBeanUtil(Session session, Provider<PersonBean> creator) {
        super(session, creator);
    }

    //    public PersonBean getAnonymous() {
    //        if (anonymous == null) {
    //            anonymous = find(PersonBean.class, ANONYMOUS);
    //            if (anonymous == null) {
    //                anonymous = new PersonBean();
    //                anonymous.setId(ANONYMOUS);
    //                anonymous.setName("Anonymous");
    //            }
    //        }
    //        return anonymous;
    //    }

}
