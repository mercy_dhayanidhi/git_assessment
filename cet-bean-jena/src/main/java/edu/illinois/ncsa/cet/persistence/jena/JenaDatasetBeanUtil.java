package edu.illinois.ncsa.cet.persistence.jena;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.RDF;

import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.jena.beans.BeanException;
import edu.illinois.ncsa.cet.jena.beans.BeanInfo;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public class JenaDatasetBeanUtil extends JenaBeanUtil<DatasetBean> implements DatasetBeanUtil {
    private static Logger log = LoggerFactory.getLogger(JenaDatasetBeanUtil.class);

    @Inject
    protected JenaDatasetBeanUtil(Session session, Provider<DatasetBean> creator) {
        super(session, creator);
    }

    @Override
    public List<DatasetBean> getOrderedList(String orderBy, boolean desc, int offset, int limit) throws PersistenceException {
        StringBuilder query = new StringBuilder();

        Resource type;
        try {
            type = BeanInfo.get(DatasetBean.class).getType();
        } catch (BeanException e) {
            log.debug("Could not get dataset type.", e);
            throw (new PersistenceException("Could not get dataset type", e));
        }

        query.append("SELECT ?s \n");
        query.append("WHERE {\n");
        query.append("  ?s <" + RDF.type.toString() + "> <" + type.toString() + "> .\n");

        if ("title".equals(orderBy)) {
            query.append("  ?s <" + DC.title + "> ?o . \n");
        } else {
            throw (new PersistenceException("Don't know how to order by " + orderBy));
        }

        query.append("} \n");

        if (orderBy != null) {
            if (desc) {
                query.append("ORDER BY DESC(?o) \n");
            } else {
                query.append("ORDER BY ASC(?o) \n");
            }
        }

        query.append("OFFSET " + offset + " \n");
        query.append("LIMIT " + limit + " \n");

        Query q = QueryFactory.create(query.toString());
        QueryExecution qe = QueryExecutionFactory.create(q, session.getBeanFactory().getModel());
        ResultSet rs = qe.execSelect();

        ArrayList<DatasetBean> result = new ArrayList<DatasetBean>();
        while (rs.hasNext()) {
            QuerySolution qs = rs.next();
            result.add(load(qs.getResource("?s")));
        }

        qe.close();
        return result;
    }

    @Override
    public Set<DatasetBean> getByCreator(PersonBean person) throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }
}
