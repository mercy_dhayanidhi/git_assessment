package edu.illinois.ncsa.cet.persistence.jena;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import edu.illinois.ncsa.cet.jena.beans.BeanException;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;

public class JenaUnitOfWork implements UnitOfWork {
    private static Logger log = LoggerFactory.getLogger(JenaUnitOfWork.class);

    private final Session session;

    @Inject
    protected JenaUnitOfWork(Session session) {
        this.session = session;
    }

    @Override
    public void begin() throws PersistenceException {
        if (session.hasTransaction() && !session.wasComitted()) {
            throw (new PersistenceException("Can only have one unit of work open."));
        }
        try {
            if (session.hasTransaction()) {
                session.reuseSession();
            } else {
                session.beginTransaction();
            }
        } catch (BeanException e) {
            log.debug("Error starting transaction.", e);
            throw (new PersistenceException("Error starting transaction", e));
        }
    }

    @Override
    public void end() throws PersistenceException {
        if (!session.hasTransaction()) {
            throw (new PersistenceException("Can not end unit of work if not began."));
        }
        try {
            session.commit();
        } catch (BeanException e) {
            log.debug("Error comitting transaction.", e);
            throw (new PersistenceException("Error comitting transaction", e));
        }
    }

}
