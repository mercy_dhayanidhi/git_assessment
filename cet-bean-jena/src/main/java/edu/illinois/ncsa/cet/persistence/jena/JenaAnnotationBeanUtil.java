package edu.illinois.ncsa.cet.persistence.jena;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.AnnotationBean;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;

public class JenaAnnotationBeanUtil extends JenaBeanUtil<AnnotationBean> implements AnnotationBeanUtil {

    @Inject
    protected JenaAnnotationBeanUtil(Session session, Provider<AnnotationBean> creator) {
        super(session, creator);
    }
}
