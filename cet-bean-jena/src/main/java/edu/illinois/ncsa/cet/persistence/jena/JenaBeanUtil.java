package edu.illinois.ncsa.cet.persistence.jena;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.RDF;

import edu.illinois.ncsa.cet.jena.beans.BeanException;
import edu.illinois.ncsa.cet.jena.beans.BeanInfo;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.BeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public class JenaBeanUtil<T> implements BeanUtil<T> {
    private static Logger       log = LoggerFactory.getLogger(JenaBeanUtil.class);

    protected final Provider<T> creator;

    protected final Session     session;

    protected JenaBeanUtil(Session session, Provider<T> creator) {
        this.session = session;
        this.creator = creator;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<T> getBeanClass() {
        return (Class<T>) create().getClass();
    }

    @Override
    public T create() {
        return creator.get();
    }

    @Override
    public int count() throws PersistenceException {
        try {
            return session.getBeanFactory().getModel().listResourcesWithProperty(RDF.type, BeanInfo.get(getBeanClass()).getType()).toList().size();
        } catch (BeanException e) {
            log.debug("Could not get beancount.", e);
            throw (new PersistenceException("Could not get beancount", e));
        }
    }

    @Override
    public Collection<String> getAllIds() throws PersistenceException {
        edu.illinois.ncsa.cet.jena.beans.BeanUtil bu = new edu.illinois.ncsa.cet.jena.beans.BeanUtil(session.getBeanFactory());
        try {
            return bu.getAllIds(getBeanClass());
        } catch (Exception e) {
            log.debug("Could not get beans.", e);
            throw (new PersistenceException("Could not get beans", e));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<T> loadAll() throws PersistenceException {
        edu.illinois.ncsa.cet.jena.beans.BeanUtil bu = new edu.illinois.ncsa.cet.jena.beans.BeanUtil(session.getBeanFactory());
        try {
            return (Collection<T>) bu.getAll(getBeanClass());
        } catch (Exception e) {
            log.debug("Could not get beans.", e);
            throw (new PersistenceException("Could not get beans", e));
        }
    }

    @Override
    public Collection<T> loadAll(boolean includeDeleted) throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public T load(String id) throws PersistenceException {
        return load(ResourceFactory.createResource(id));
    }

    protected T load(Resource id) throws PersistenceException {
        try {
            return session.load(getBeanClass(), id);
        } catch (BeanException e) {
            log.debug("Could not get bean.", e);
            throw (new PersistenceException("Could not get bean", e));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T save(T object) throws PersistenceException {
        if (!session.hasTransaction()) {
            throw (new PersistenceException("No unit of work started."));
        }
        try {
            return (T) session.merge(object);
        } catch (BeanException e) {
            log.debug("Could not save bean.", e);
            throw (new PersistenceException("Could not save bean", e));
        }
    }
}
