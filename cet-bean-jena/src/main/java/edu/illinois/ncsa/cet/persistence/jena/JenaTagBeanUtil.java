package edu.illinois.ncsa.cet.persistence.jena;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import edu.illinois.ncsa.cet.bean.AbstractBean;
import edu.illinois.ncsa.cet.bean.AnnotatedBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;
import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.jena.beans.BeanException;
import edu.illinois.ncsa.cet.jena.beans.Session;
import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class JenaTagBeanUtil extends JenaBeanUtil<TagBean> implements TagBeanUtil {
    @Inject
    private Provider<TagEventBeanUtil> tagEventProvider;

    @Inject
    protected JenaTagBeanUtil(Session session, Provider<TagBean> creator) {
        super(session, creator);
    }

    @Override
    public Collection<AbstractBean> getTagged(String word) throws PersistenceException {
        String query = "";
        query += "SELECT ?tb ?object WHERE {\n";
        query += "  ?tb <http://www.holygoat.co.uk/owl/redwood/0.1/tags/name> \"" + word + "\" .\n";
        query += "  ?tb <http://www.holygoat.co.uk/owl/redwood/0.1/tags/taggedWithTag> ?object .\n";
        query += "}\n";

        Query q = QueryFactory.create(query);
        QueryExecution qe = QueryExecutionFactory.create(q, session.getBeanFactory().getModel());
        ResultSet rs = qe.execSelect();

        Set<AbstractBean> result = new HashSet<AbstractBean>();
        while (rs.hasNext()) {
            QuerySolution qs = rs.next();
            try {
                result.add((AbstractBean) session.load(null, qs.getResource("?s")));
            } catch (BeanException e) {
                throw (new PersistenceException("Could not load tagged object.", e));
            }
        }

        qe.close();
        return result;
    }

    @Override
    public Collection<String> getTags() throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addTags(AnnotatedBean toBeTagged, PersonBean creator, String... tags) throws PersistenceException {
        TagEventBeanUtil tebu = tagEventProvider.get();

        TagEventBean teb = tebu.create();
        teb.setCreator(creator);
        teb.setTagEventDate(new Date());

        for (String tag : tags ) {
            TagBean t = create();
            t.setTag(tag);
            teb.addTag(save(t));
        }

        toBeTagged.getTagEvents().add(tebu.save(teb));
    }
}
