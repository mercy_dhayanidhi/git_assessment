package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;

import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;

public class JpaUnitOfWork implements UnitOfWork {
    private final EntityManager em;

    @Inject
    protected JpaUnitOfWork(EntityManager em) {
        this.em = em;
    }

    @Override
    public void begin() throws PersistenceException {
        if (em.getTransaction().isActive()) {
            throw (new PersistenceException("Can only have one unit of work open."));
        }
        em.getTransaction().begin();
    }

    @Override
    public void end() throws PersistenceException {
        if (!em.getTransaction().isActive()) {
            throw (new PersistenceException("Can not end unit of work if not began."));
        }
        em.getTransaction().commit();
        em.clear();
    }
}
