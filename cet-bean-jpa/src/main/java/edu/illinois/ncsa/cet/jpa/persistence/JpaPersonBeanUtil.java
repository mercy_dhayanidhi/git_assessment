package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;

public class JpaPersonBeanUtil extends JpaBeanUtil<PersonBean> implements PersonBeanUtil {

    @Inject
    protected JpaPersonBeanUtil(EntityManager em, Provider<PersonBean> creator) {
        super(em, creator);
    }

}
