package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.AnnotationBean;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;

public class JpaAnnotationBeanUtil extends JpaBeanUtil<AnnotationBean> implements AnnotationBeanUtil {

    @Inject
    protected JpaAnnotationBeanUtil(EntityManager em, Provider<AnnotationBean> creator) {
        super(em, creator);
    }

}
