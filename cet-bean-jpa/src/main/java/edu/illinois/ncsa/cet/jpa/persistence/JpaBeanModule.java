package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.ejb.Ejb3Configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import edu.illinois.ncsa.cet.bean.AnnotationBean;
import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;
import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;

public class JpaBeanModule extends AbstractModule {

    private String                     persistenceName = null;

    private ThreadLocal<EntityManager> entityManager   = new ThreadLocal<EntityManager>();

    public JpaBeanModule(String persistenceName) {
        this.persistenceName = persistenceName;
    }

    @Override
    protected void configure() {
        bind(DatasetBeanUtil.class).to(JpaDatasetBeanUtil.class);
        bind(CollectionBeanUtil.class).to(JpaCollectionBeanUtil.class);
        bind(PersonBeanUtil.class).to(JpaPersonBeanUtil.class);
        bind(TagEventBeanUtil.class).to(JpaTagEventBeanUtil.class);
        bind(TagBeanUtil.class).to(JpaTagBeanUtil.class);
        bind(AnnotationBeanUtil.class).to(JpaAnnotationBeanUtil.class);

        bind(UnitOfWork.class).to(JpaUnitOfWork.class);
    }

    @Provides
    EntityManager getEntityManager(EntityManagerFactory factory) {
        if ((entityManager.get() == null) || !entityManager.get().isOpen()) {
            entityManager.set(factory.createEntityManager());
        }
        return entityManager.get();
    }

    @Provides
    @Singleton
    EntityManagerFactory getEntityManagerFactory() {
        // create configuration and load persistence.xml
        Ejb3Configuration cfg = new Ejb3Configuration();
        Ejb3Configuration configured = cfg.configure(persistenceName, null);

        // add mappings
        configured.addAnnotatedClass(DatasetBean.class);
        configured.addAnnotatedClass(CollectionBean.class);
        configured.addAnnotatedClass(PersonBean.class);
        configured.addAnnotatedClass(TagEventBean.class);
        configured.addAnnotatedClass(TagBean.class);
        configured.addAnnotatedClass(TagBeanUtil.class);
        configured.addAnnotatedClass(AnnotationBean.class);

        // create factory
        return configured != null ? configured.buildEntityManagerFactory() : null;
        //
        //        return Persistence.createEntityManagerFactory(persistenceName);
    }
}
