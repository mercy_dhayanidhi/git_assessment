package edu.illinois.ncsa.cet.jpa.persistence;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public class JpaDatasetBeanUtil extends JpaBeanUtil<DatasetBean> implements DatasetBeanUtil {

    @Inject
    protected JpaDatasetBeanUtil(EntityManager em, Provider<DatasetBean> creator) {
        super(em, creator);
    }

    @Override
    public List<DatasetBean> getOrderedList(String orderBy, boolean desc, int offset, int limit) throws PersistenceException {
        String query = "SELECT db FROM DatasetBean db ORDER BY ";

        if ("title".equalsIgnoreCase(orderBy)) {
            query += "title ";
            if (desc) {
                query += " DESC ";
            }
        } else {
            throw (new PersistenceException("Don't know how to order by " + orderBy));
        }
        TypedQuery<DatasetBean> q = em.createQuery(query, DatasetBean.class);
        if (offset > 0) {
            q.setFirstResult(offset);
        }
        if (limit > 0) {
            q.setMaxResults(limit);
        }

        return q.getResultList();
    }

    @Override
    public Set<DatasetBean> getByCreator(PersonBean person) throws PersistenceException {
        return new HashSet<DatasetBean>(em.createQuery("SELECT db FROM DatasetBean db WHERE db.creator = '" + person.getId() + "'", DatasetBean.class).getResultList());
    }
}
