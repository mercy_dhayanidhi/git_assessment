package edu.illinois.ncsa.cet.jpa.persistence;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.AbstractBean;
import edu.illinois.ncsa.cet.bean.AnnotatedBean;
import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;
import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class JpaTagBeanUtil extends JpaBeanUtil<TagBean> implements TagBeanUtil {
    @Inject
    private Provider<TagEventBeanUtil> tagEventProvider;

    @Inject
    protected JpaTagBeanUtil(EntityManager em, Provider<TagBean> creator) {
        super(em, creator);
    }

    @Override
    public Collection<AbstractBean> getTagged(String word) throws PersistenceException {
        return new HashSet<AbstractBean>(em.createQuery("SELECT db FROM DatasetBean db JOIN db.tagEvents teb JOIN teb.tags tb WHERE tb.tag = '" + word + "'", DatasetBean.class).getResultList());
    }

    @Override
    public Collection<String> getTags() throws PersistenceException {
        return new HashSet<String>(em.createQuery("SELECT x.tag FROM  TagBean x GROUP BY x.tag", String.class).getResultList());
    }

    @Override
    public void addTags(AnnotatedBean toBeTagged, PersonBean creator, String... tags) throws PersistenceException {
        TagEventBeanUtil tebu = tagEventProvider.get();

        TagEventBean teb = tebu.create();
        teb.setCreator(creator);
        teb.setTagEventDate(new Date());

        for (String tag : tags ) {
            TagBean t = create();
            t.setTag(tag);
            teb.addTag(save(t));
        }

        toBeTagged.getTagEvents().add(tebu.save(teb));
    }
}
