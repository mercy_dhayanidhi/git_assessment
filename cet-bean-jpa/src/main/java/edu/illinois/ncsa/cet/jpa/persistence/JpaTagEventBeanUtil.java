package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class JpaTagEventBeanUtil extends JpaBeanUtil<TagEventBean> implements TagEventBeanUtil {

    @Inject
    protected JpaTagEventBeanUtil(EntityManager em, Provider<TagEventBean> creator) {
        super(em, creator);
    }
}
