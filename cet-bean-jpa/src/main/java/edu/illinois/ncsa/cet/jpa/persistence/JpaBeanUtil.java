package edu.illinois.ncsa.cet.jpa.persistence;

import java.util.Collection;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;

import edu.illinois.ncsa.cet.persistence.BeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public abstract class JpaBeanUtil<T> implements BeanUtil<T> {
    private static Logger         log = LoggerFactory.getLogger(JpaBeanUtil.class);

    protected final Provider<T>   creator;

    protected final EntityManager em;

    protected JpaBeanUtil(EntityManager em, Provider<T> creator) {
        this.em = em;
        this.creator = creator;
    }

    public void clear() {
        em.clear();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<T> getBeanClass() {
        return (Class<T>) create().getClass();
    }

    @Override
    public T create() {
        return creator.get();
    }

    @Override
    public int count() {
        Long result = em.createQuery("SELECT COUNT(x) FROM " + getBeanClass().getSimpleName() + " x", Long.class).getSingleResult();
        return result.intValue();
    }

    @Override
    public Collection<String> getAllIds() throws PersistenceException {
        String q = "SELECT x.id FROM " + getBeanClass().getSimpleName() + " x";
        try {
            return em.createQuery(q, String.class).getResultList();
        } catch (Exception e) {
            throw (new PersistenceException("Error loading bean ids.", e));
        }
    }

    @Override
    public Collection<T> loadAll() throws PersistenceException {
        return loadAll(false);
    }

    @Override
    public Collection<T> loadAll(boolean includeDeleted) throws PersistenceException {
        String q = "SELECT x FROM " + getBeanClass().getSimpleName() + " x";
        //        if (!includeDeleted) {
        //            q += " WHERE x.deleted=false";
        //        }
        try {
            return em.createQuery(q, getBeanClass()).getResultList();
        } catch (Exception e) {
            throw (new PersistenceException("Error loading beans.", e));
        }
    }

    @Override
    public T load(String id) {
        return em.find(getBeanClass(), id);
    }

    @Override
    public T save(T object) throws PersistenceException {
        if (object == null) {
            throw (new PersistenceException("Can not save null object."));
        }
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
                T result = em.merge(object);
                em.getTransaction().commit();
                return result;
            } else {
                return em.merge(object);
            }
        } catch (Exception e) {
            throw (new PersistenceException("Error saving bean.", e));
        }
    }
}
