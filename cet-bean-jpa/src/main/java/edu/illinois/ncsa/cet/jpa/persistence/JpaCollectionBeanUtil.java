package edu.illinois.ncsa.cet.jpa.persistence;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;

public class JpaCollectionBeanUtil extends JpaBeanUtil<CollectionBean> implements CollectionBeanUtil {

    @Inject
    protected JpaCollectionBeanUtil(EntityManager em, Provider<CollectionBean> creator) {
        super(em, creator);
    }

}
