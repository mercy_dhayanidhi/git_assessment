package org.tupeloproject.rdf;

import java.io.Serializable;
import java.text.ParseException;

import org.tupeloproject.kernel.OperatorException;

public class Triple implements Serializable, Comparable<Triple> {
    private static final long serialVersionUID = 1L;

    private Resource          subject;
    private Resource          predicate;
    private Resource          object;

    /**
     * @deprecated use new Triple
     */
    public static Triple create(Object s, Object p, Object o) {
        return new Triple(s, p, o);
    }

    public Triple() {
    }

    public Triple(Object s, Object p, Object o) {
        subject = Resource.fromObject(s);
        predicate = Resource.fromObject(p);
        object = Resource.fromObject(o);
    }

    public Triple(Object s, Object p, Object o, Object type) {
        subject = Resource.fromObject(s);
        predicate = Resource.fromObject(p);
        object = Resource.literal(o.toString(), type.toString());
    }

    public Resource getSubject() {
        return subject;
    }

    public void setSubject(Resource subject) {
        this.subject = subject;
    }

    public Resource getPredicate() {
        return predicate;
    }

    public void setPredicate(Resource predicate) {
        this.predicate = predicate;
    }

    public Resource getObject() {
        return object;
    }

    public void setObject(Resource object) {
        this.object = object;
    }

    /**
     * Produce a string representation of this triple in N-Triples
     * format. This is mostly useful for debugging.
     */
    public synchronized String toString() {
        Resource s = getSubject();
        Resource p = getPredicate();
        Resource o = getObject();
        return s.toNTriples() + " " + p.toNTriples() + " " + o.toNTriples() + " .";
    }

    @Override
    public int compareTo(Triple o) {
        return toString().compareTo(o.toString());
    }

    public static Triple parseTriple(String triple) throws OperatorException {
        String[] x = triple.split(" ");
        if (x.length != 4) {
            throw (new OperatorException("Invalid triple string."));
        }
        try {
            return new Triple(Resource.fromNTriples(x[0]), Resource.fromNTriples(x[1]), Resource.fromNTriples(x[2]));
        } catch (ParseException e) {
            throw (new OperatorException("Invalid triple data.", e));
        }
    }

    /**
     * Based on {@link Triple#compareTo(Object o)}.
     */
    public boolean equals(Object other) {
        if (other instanceof Triple) {
            return this.toString().equals(other.toString());
        } else {
            return false;
        }
    }

    /**
     * Based on subject, predicate, object, and datatype.
     */
    public synchronized int hashCode() {
        return toString().hashCode();
    }

}
