/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/**
 * CET base vocabulary terms. This includes types such as cet:Artifact
 * as well as properties such as cet:causedBy.
 */
public class Cet {
    public static final String NS = "http://cet.ncsa.uiuc.edu/2007/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    /**
     * @deprecated use uriRef
     */
    public static Resource cet(String suffix) {
        return uriRef(suffix);
    }

    /**
     * General class to represent any octet stream.
     */
    public static final UriRef BLOB_TYPE            = uriRef("Blob");

    /**
     * Type to represent any CETBean
     */
    public static final UriRef CET_BEAN_TYPE        = uriRef("CetBean");

    /**
     * Any data item used or usable as input and output to a
     * scientific work process, including observations, initial conditions,
     * processed data, analysis results, intermediate data, etc.
     */
    public static final UriRef DATASET              = uriRef("Dataset");

    public static final UriRef ANNOTATION           = uriRef("Annotation");

    /**
     * Any simple or complex occurrence, including processes, communications,
     * experiments, etc.
     */
    public static final UriRef EVENT                = uriRef("Event");

    /**
     * General Property to associate Event, Tag, Annotation or Provenance
     * statement to any Resource. E.g. an Editing Event
     * can be associated to Artifact using it.
     */
    public static final UriRef HAS_RESOURCE         = uriRef("hasResource");

    /**
     * The previous version of a resource
     */
    public static final UriRef HAS_PREVIOUS_VERSION = uriRef("hasPreviousVersion");

    /**
     * The latest version of a Resource
     */
    public static final UriRef HAS_LATEST_VERSION   = uriRef("hasLatestVersion");

    /**
     * property holding resource of latest tag event
     */
    public static final UriRef HAS_LATEST_TAG_EVENT = uriRef("hasLatestTagEvent");

    /**
     * The version number of a resource
     */
    public static final UriRef HAS_VERSION_NUMBER   = uriRef("hasVersionNumber");

    public static final UriRef HAS_ANNOTATION       = uriRef("hasAnnotation");
    /**
     * A part of a resource
     */
    public static final UriRef HAS_CONSTITUENT      = uriRef("hasConstituent");
    /**
     * A causal relationship between events
     */
    public static final UriRef CAUSED_BY            = uriRef("causedBy");

    /**
     */
    public static final UriRef HAS_BLOB             = uriRef("hasBlob");
}