/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/**
 */

/**
 * Richard Newman's <a href="http://www.holygoat.co.uk/projects/tags/">tagging
 * ontology</a>
 */
public class Tags {
    public static final String NS = "http://www.holygoat.co.uk/owl/redwood/0.1/tags/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    /**
     * A Tagging which has precisely one associated resource, and one associated
     * tag object. Subclass of tags:Tagging.
     */
    public static final UriRef RESTRICTED_TAGGING   = uriRef("RestrictedTagging");

    /**
     * A reified class which defines an instance of a tagging by an agent of a
     * resource with one or more tags.
     */
    public static final UriRef TAGGING_EVENT        = uriRef("Tagging");

    /**
     * A natural-language concept which is used to annotate another resource. A
     * Class defining a Tag object.
     */
    public static final UriRef TAG_OBJECT           = uriRef("Tag");

    /**
     * The object is a Tag which plays a role in the subject Tagging. In other
     * words links
     * a Tagging event object to the Tag object. Domain is tags:Tagging and
     * range is tags:Tag.
     */
    public static final UriRef HAS_TAG_OBJECT       = uriRef("associatedTag");

    /**
     * The two tags are asserted to be equivalent --- that is, that whenever one
     * is associated with a resource, the other
     * tag can be logically inferred to also be associated. Be very careful with
     * this.
     * I'm not sure if this should be a subproperty of owl:sameAs. Domain is
     * tags:Tag and range is tags:Tag.
     * Currently in testing stage.
     */
    public static final UriRef IS_EQUIVALENT_TO_TAG = uriRef("equivalentTag");

    /**
     * Indicates that the subject tag applies to the object resource. This does
     * not assert by who, when, or why the tagging occurred.
     * For that information, use a reified Tagging resource. Domain is tags:Tag.
     * It is owl:inverseOf tags:taggedWithTag.
     */
    public static final UriRef IS_TAG_OF            = uriRef("isTagOf");

    /**
     * The name of a tag. Note that we can't relate this to skos:prefLabel
     * because we cannot guarantee that tags have unique labels in a given
     * conceptual scheme. Or can we?
     * It is subPropertyOf dc:title. Domain is tags:Tag.
     */
    public static final UriRef HAS_TAG_TITLE        = uriRef("name");

    /**
     * The two tags are asserted as being related. This might be symmetric, but
     * it certainly isn't transitive.
     * Domain and range both are tags:Tag.
     */
    public static final UriRef IS_RELATED_TO_TAG    = uriRef("relatedTag");

    /**
     * The relationship between a resource and a Tagging (basically a predicate
     * between resource object and Tagging event).
     * Range is tags:Tagging object.
     * Note, of course, that this allows us to tag tags and taggings
     * themselves...
     */
    public static final UriRef HAS_TAGGING_EVENT    = uriRef("tag");

    /**
     * The object plays the role of the tagger in the subject Tagging. Domain is
     * tags:Tagging and range is foaf:Agent.
     */
    public static final UriRef TAGGED_BY            = uriRef("taggedBy");

    /**
     * The date and/or time at which the tagging event occurred in other words
     * The subject Tagging occurred at the subject time and date.
     * Domain is tags:Tagging. It is subPropertyOf dc:date.
     */
    public static final UriRef TAGGED_ON            = uriRef("taggedOn");

    /**
     * The object is a resource which plays a role in the subject Tagging.
     * domain is tags:Tagging. Currently in testing stage. I am not sure about
     * its purpose. Is it inverse of tags:tag?
     */
    public static final UriRef TAGGED_RESOURCE      = uriRef("taggedResource");

    /**
     * Indicates that the subject has been tagged with the object tag.
     * This does not assert by who, when, or why the tagging occurred. For that
     * information, use a reified Tagging resource.
     * Range is tags:Tag. It is owl:inverseOf tags:isTagOf.
     */
    public static final UriRef TAGGED_WITH_TAG      = uriRef("taggedWithTag");

}
