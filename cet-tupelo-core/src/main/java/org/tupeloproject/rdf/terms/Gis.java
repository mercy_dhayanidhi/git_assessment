/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/**
 * vocabulary for GIS related objects and properties.
 */
public class Gis {
    public static final String NS = "tag:tupeloproject.org,2006:/2.0/gis/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    static final public UriRef HAS_ADDRESS         = uriRef("hasAddress");

    /**
     * gml:_Feature can also use generic predicate gml:where instead of specific
     * predicate like following
     */
    static final public UriRef HAS_GEO_POINT       = uriRef("hasGeoPoint");

    //A GeoTag is composition of Tag and GeoPoint. Helps to plot a point on map. Point contains atleast Longitude and latitude required to plot it on Map.
    //Tag is required to label that point. Concept of GeoTag borrowed from flicker.
    static final public UriRef GEO_TAG             = uriRef("GeoTag");

    static final public UriRef HAS_GEO_TAG         = uriRef("hasGeoTag");

    //Type of address e.g. work home
    static final public UriRef ADDRESS_TYPE        = uriRef("addressType");

    /**
     * A blob object can be associated with location object using following
     * predicate. e.g. Location of building (or any geographic
     * feature) can be specified using its street address with a blob of
     * KML file providing spatial coordinates or satellite image of building or
     * can be just pic of building.
     */
    static final public UriRef HAS_GEOSPATIAL_BLOB = uriRef("hasGeoSpatialBlob");
}