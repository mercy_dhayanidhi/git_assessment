package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

public class Tupelo {
    public static final String NS = "tag:tupeloproject.org,2006:/2.0/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }
}
