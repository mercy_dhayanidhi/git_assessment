/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/**
 * Vocabulary for describing file attributes
 * 
 * @see org.tupeloproject.kernel.impl.FileContext
 */
public class Files {
    public static final String NS = "tag:tupeloproject.org,2006:/2.0/files/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    /**
     * Folder object. A Folder object can have at the most one parent (root
     * folder doesn't have one).
     * Can have one or several children.
     */
    public static final UriRef FOLDER_TYPE       = uriRef("Folder");

    /**
     * A file. A file object cannot have any child. Has exactly one parent. In
     * case of versioning
     * a file object can hold set of File Version object.
     */
    public static final UriRef FILE_TYPE         = uriRef("File");

    /**
     * Represents a version of File object
     */
    public static final UriRef FILE_VERSION_TYPE = uriRef("File_Version");

    /**
     * path of file or folder
     */
    public static final UriRef PATH              = uriRef("path");

    /**
     * Can this file be read?
     */
    public static final UriRef CAN_READ          = uriRef("canRead");
    /**
     * Can this file be written?
     */
    public static final UriRef CAN_WRITE         = uriRef("canWrite");
    /**
     * The file name.
     * 
     * @deprecated use {@link org.tupeloproject.rdf.terms.Rdfs#LABEL}
     */
    @Deprecated
    public static final UriRef HAS_NAME          = uriRef("hasName");
    /**
     * The parent of the file.
     */
    public static final UriRef HAS_PARENT        = uriRef("hasParent");
    /**
     * Is this file a directory?
     */
    public static final UriRef IS_DIRECTORY      = uriRef("isDirectory");
    /**
     * A child of this directory.
     */
    public static final UriRef HAS_CHILD         = uriRef("hasChild");
    /**
     * Is this a file (as opposed to a directory?)
     */
    public static final UriRef IS_FILE           = uriRef("isFile");
    /**
     * Is this file hidden?
     */
    public static final UriRef IS_HIDDEN         = uriRef("isHidden");
    /**
     * When was this file last modified?
     */
    public static final UriRef LAST_MODIFIED     = uriRef("lastModified");
    /**
     * What is the length of the file in bytes?
     */
    public static final UriRef LENGTH            = uriRef("length");
}
