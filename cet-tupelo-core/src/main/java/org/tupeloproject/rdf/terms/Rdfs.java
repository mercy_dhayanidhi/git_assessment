/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/** a subset of the RDF Schema vocabulary */
public class Rdfs {
    public static final String NS = "http://www.w3.org/2000/01/rdf-schema#";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    /**
     * Superclass of everything including rdfs:Class. Equivalent to owl:Thing in
     * OWL Full.
     */
    public static final UriRef RESOURCE_TYPE                 = uriRef("Resource");

    /**
     * The class of classes. All Object Types have rdf:type of rdfs:Class.
     * It is subClassOf rdfs:Resource.
     */
    public static final UriRef CLASS_TYPE                    = uriRef("Class");

    /**
     * The class of literal values, eg. textual strings and integers. It is
     * subClassOf rdfs:Resource.
     */
    public static final UriRef LITERAL                       = uriRef("Literal");

    /**
     * The class of RDF containers. It is subClassOf rdfs:Resource.
     */
    public static final UriRef CONTAINER                     = uriRef("Container");

    /**
     * The class of RDF datatypes. It is subClassOf rdfs:Class.
     */
    public static final UriRef DATATYPE                      = uriRef("Datatype");

    /**
     * The class of container membership properties, rdf:_1, rdf:_2, ...,
     * all of which are sub-properties of 'member'. It is subClassOf
     * rdf:Property.
     */
    public static final UriRef CONTAINER_MEMBERSHIP_PROPERTY = uriRef("CONTAINER_MEMBERSHIP_PROPERTY");

    /**
     * The subject is a subclass of a class.
     */
    public static final UriRef SUB_CLASS_OF                  = uriRef("subClassOf");

    /**
     * The subject is a subproperty of a property.
     */
    public static final UriRef SUB_PROPERTY_OF               = uriRef("subPropertyOf");

    /**
     * A member of the subject resource.
     */
    public static final UriRef MEMBER                        = uriRef("member");

    /** A human-readable name for the subject. Type of Annotation Property. */
    public static final UriRef LABEL                         = uriRef("label");

    /** A description of the subject resource. Type of Annotation Property. */
    public static final UriRef COMMENT                       = uriRef("comment");

    /**
     * Further information about the subject resource. Type of Annotation
     * Property.
     */
    public static final UriRef SEE_ALSO                      = uriRef("seeAlso");

    /**
     * The definition of the subject resource. Type of Annotation Property.
     */
    public static final UriRef IS_DEFINED_BY                 = uriRef("isDefinedBy");

    /**
     * A domain of the subject property
     */
    public static final UriRef DOMAIN                        = uriRef("domain");

    /**
     * A range of the subject property
     */
    public static final UriRef RANGE                         = uriRef("range");
}