/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/**
 * Dublin Core Elements
 */
public class Dc {
    public static final String NS = "http://purl.org/dc/elements/1.1/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    /** dc:contributor {@value} */
    public static final UriRef CONTRIBUTOR = uriRef("contributor");
    /** dc:coverage {@value} */
    public static final UriRef COVERAGE    = uriRef("coverage");
    /** dc:creator {@value} */
    public static final UriRef CREATOR     = uriRef("creator");
    /** dc:date {@value} */
    public static final UriRef DATE        = uriRef("date");
    /** dc:description {@value} */
    public static final UriRef DESCRIPTION = uriRef("description");
    /** dc:format {@value} */
    public static final UriRef FORMAT      = uriRef("format");
    /** dc:identifier {@value} */
    public static final UriRef IDENTIFIER  = uriRef("identifier");
    /** dc:language {@value} */
    public static final UriRef LANGUAGE    = uriRef("language");
    /** dc:publisher {@value} */
    public static final UriRef PUBLISHER   = uriRef("publisher");
    /** dc:relation {@value} */
    public static final UriRef RELATION    = uriRef("relation");
    /** dc:rights {@value} */
    public static final UriRef RIGHTS      = uriRef("rights");
    /** dc:source {@value} */
    public static final UriRef SOURCE      = uriRef("source");
    /** dc:subject {@value} */
    public static final UriRef SUBJECT     = uriRef("subject");
    /** dc:title {@value} */
    public static final UriRef TITLE       = uriRef("title");

    /** dc:type {@value} */
    /** this predicate can be use to describe dcmitype object **/
    public static final UriRef TYPE        = uriRef("type");
}
