/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.terms;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;

/** Dublin core DcmiType */
public class DcmiType {
    public static final String NS = "http://purl.org/dc/dcmitype/";

    public static UriRef uriRef(String suffix) {
        return Resource.uriRef(NS + suffix);
    }

    public static final UriRef COLLECTION           = uriRef("Collection");

    public static final UriRef DATASET              = uriRef("Dataset");

    public static final UriRef EVENT                = uriRef("Event");

    public static final UriRef IMAGE                = uriRef("Image");

    public static final UriRef INTERACTIVE_RESOURCE = uriRef("InteractiveResource");

    public static final UriRef SERVICE              = uriRef("Service");

    public static final UriRef SOFTWARE             = uriRef("Software");

    public static final UriRef SOUND                = uriRef("Sound");

    public static final UriRef TEXT                 = uriRef("Text");

    public static final UriRef PHYSICAL_OBJECT      = uriRef("PhysicalObject");

    public static final UriRef STILL_IMAGE          = uriRef("Software");

    public static final UriRef MOVING_IMAGE         = uriRef("MovingImage");
}
