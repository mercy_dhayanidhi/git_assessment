package org.tupeloproject.rdf.xml;

import static org.tupeloproject.rdf.Resource.uriRef;

import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.tupeloproject.rdf.Literal;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.util.UriFactory;
import org.tupeloproject.util.Xml;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class XmlRdfHandler extends DefaultHandler2 {
    private static final String RDF             = Rdf.uriRef("").getString();
    private static final String RDF_RDF         = Rdf.uriRef("RDF").getString();
    private static final String RDF_DESCRIPTION = Rdf.uriRef("Description").getString();
    private static final String RDF_ABOUT       = Rdf.uriRef("about").getString();
    private static final String RDF_RESOURCE    = Rdf.uriRef("resource").getString();
    private static final String RDF_NODE_ID     = Rdf.uriRef("nodeID").getString();
    private static final String RDF_ID          = Rdf.uriRef("ID").getString();
    private static final String RDF_LI          = Rdf.uriRef("li").getString();
    private static final String RDF_TYPE        = Rdf.uriRef("type").getString();

    private Stack<Frame>        stack           = new Stack<Frame>();
    private StringWriter        pcdata          = null;
    private StringWriter        xmlLiteral      = null;
    private int                 literalLevel    = 0;                                    // level in XMLLiteral
    private int                 bn              = 0;

    private Set<Triple>         triples         = new HashSet<Triple>();

    public Set<Triple> getTriples() {
        return triples;
    }

    public void startElement(String ns, String name, String qn, Attributes attrs) throws SAXException {
        if (literalLevel > 0) { // this isn't RDF; we're in an XMLLiteral
            literalLevel++;
            // now produce an equivalent start tag
            xmlLiteralStart(xmlLiteral, ns, qn, attrs);
        } else { // we're in RDF
            Frame frame = new Frame();
            String uri = ns + name;
            frame.lang = attrs.getValue("xml:lang");
            String baseString = attrs.getValue("xml:base");
            if (baseString != null) {
                try {
                    frame.base = new URI(baseString);
                } catch (URISyntaxException x) {
                    throw new SAXException("malformed xml:base URI \"" + baseString + "\"");
                }
            }
            if (expectSubject(stack)) {
                if (!uri.equals(RDF_RDF)) {
                    // get this resource's ID
                    getSubjectNode(frame, stack, attrs);
                    // we have the subject
                    if (!uri.equals(RDF_DESCRIPTION)) {
                        // this is a typed node, so assert the type
                        yield(frame.node, Rdf.TYPE, uriRef(uri));
                    }
                    // now process attribute-specified predicates
                    for (int i = 0; i < attrs.getLength(); i++ ) {
                        String aQn = attrs.getQName(i);
                        String aUri = attrs.getURI(i) + attrs.getLocalName(i);
                        String aVal = attrs.getValue(i);
                        if (aUri.startsWith(RDF) || aQn.startsWith("xml:")) {
                            // skip
                        } else {
                            yield(frame.node, uriRef(aUri), Resource.literal(aVal));
                        }
                    }
                    // is this node the value of some enclosing predicate?
                    if (inPredicate(stack)) {
                        // is the value of the predicate a collection?
                        if (isCollectionItem(stack)) {
                            Frame ppFrame = parentPredicateFrame(stack);
                            ppFrame.collection.add(frame.node);
                        } else { // not a collection
                            // this subject is the value of its enclosing predicate
                            yield(ancestorSubject(stack), parentPredicate(stack), frame.node);
                        }
                    }
                }
                // do not accumulate pcdata
                pcdata = null;
            } else { // expect predicate
                frame.node = uriRef(uri);
                frame.isPredicate = true;
                // handle reification
                String reification = attrs.getValue(RDF, "ID");
                if (reification != null) {
                    frame.reification = resolve(reification, stack);
                }
                // handle container items
                if (uri.equals(RDF_LI)) {
                    Frame asf = ancestorSubjectFrame(stack);
                    frame.node = uriRef(RDF + "_" + asf.li);
                    asf.li++;
                }
                // parse attrs to see if the value of this pred is a uriref
                Resource object = getObjectNode(stack, attrs);
                if (object != null) {
                    yield(ancestorSubject(stack), frame.node, object, frame.reification);
                } else {
                    // this predicate encloses pcdata, prepare to accumulate
                    pcdata = new StringWriter();
                }
                // handle rdf:parseType="resource"
                String parseType = attrs.getValue(RDF, "parseType");
                if ("Resource".equals(parseType)) {
                    object = object == null ? blankNode() : object;
                    yield(ancestorSubject(stack), frame.node, object, frame.reification);
                    // perform surgery on the current frame
                    frame.node = object;
                    frame.isSubject = true;
                } else if ("Collection".equals(parseType)) {
                    frame.isCollection = true;
                    frame.collection = new LinkedList<Resource>();
                    frame.collectionHead = new Triple(ancestorSubject(stack), frame.node, blankNode());
                    pcdata = null;
                } else if ("Literal".equals(parseType)) {
                    literalLevel = 1; // enter into a literal
                    xmlLiteral = new StringWriter();
                    // which means we shouldn't accumulate pcdata!
                    pcdata = null;
                } else {
                    // handle datatype
                    frame.datatype = attrs.getValue(RDF, "datatype");
                }
                // now handle property attributes (if we do this, then this
                // must be an empty element)
                object = null;
                for (int i = 0; i < attrs.getLength(); i++ ) {
                    String aQn = attrs.getQName(i);
                    String aUri = attrs.getURI(i) + attrs.getLocalName(i);
                    String aVal = attrs.getValue(i);
                    if ((!aUri.equals(RDF_TYPE) && aUri.startsWith(RDF)) || aQn.startsWith("xml:")) {
                        // ignore
                    } else {
                        if (object == null) {
                            object = blankNode();
                            yield(ancestorSubject(stack), frame.node, object);
                        }
                        if (aUri.equals(RDF_TYPE)) {
                            yield(object, Rdf.TYPE, uriRef(aVal));
                        } else {
                            Literal value = withLanguageTag(Resource.literal(aVal), stack);
                            yield(object, uriRef(aUri), value);
                        }
                    }
                }
                // if we had to generate a node to hold properties specified
                // as attributes, then expect an empty element and therefore
                // don't record pcdata
                if (object != null) {
                    pcdata = null;
                }
            }
            // finally, push the frame for use in subsequent callbacks
            stack.push(frame);
        }
    }

    public void endElement(String ns, String name, String qn) throws SAXException {
        if (literalLevel > 0) { // this isn't RDF; we're in an XMLLiteral
            literalLevel--;
            if (literalLevel > 0) {
                xmlLiteralEnd(xmlLiteral, qn);
            }
        } else { // this is RDF
            if (inPredicate(stack)) {
                Frame ppFrame = parentPredicateFrame(stack);
                // this is a predicate closing
                if (xmlLiteral != null) { // it was an XMLLiteral
                    Literal value = Resource.literal(xmlLiteral.toString());
                    value.setDatatype(Rdf.uriRef("XMLLiteral"));
                    yield(ancestorSubject(stack), parentPredicate(stack), value);
                    xmlLiteral = null;
                } else if (pcdata != null) { // we have an RDF literal
                    Literal value = withLanguageTag(Resource.literal(pcdata.toString()), stack);
                    // deal with datatype
                    String datatype = ppFrame.datatype;
                    if (datatype != null) {
                        value.setDatatype(uriRef(datatype));
                    }
                    // deal with reification
                    Resource reification = ppFrame.reification;
                    yield(ancestorSubject(stack), ppFrame.node, value, reification);
                    // no longer collect pcdata
                    pcdata = null;
                } else if (ppFrame.isCollection) { // deal with collections
                    if (ppFrame.collection.size() == 0) {
                        // in this case, the value of this property is rdf:nil
                        yield(ppFrame.collectionHead.getSubject(), ppFrame.collectionHead.getPredicate(), Rdf.NIL);
                    } else {
                        yield(ppFrame.collectionHead);
                        Resource prevNode = null;
                        Resource node = ppFrame.collectionHead.getObject();
                        for (Resource item : ppFrame.collection ) {
                            if (prevNode != null) {
                                yield(prevNode, Rdf.REST, node);
                            }
                            yield(node, Rdf.FIRST, item);
                            prevNode = node;
                            node = blankNode();
                        }
                        yield(prevNode, Rdf.REST, Rdf.NIL);
                    }
                }
            }
            stack.pop();
        }
    }

    public void characters(char[] chars, int start, int len) throws SAXException {
        if (literalLevel > 0) { // this isn't RDF; we're in an XMLLiteral
            xmlLiteral.write(Xml.escape(new String(chars, start, len)));
        } else if (pcdata != null) { // we're in RDF, collecting an attribute value
            // accumulate char data
            for (int i = start; i < start + len; i++ ) {
                pcdata.write(chars[i]);
            }
        }
    }

    public void processingInstruction(String target, String data) throws SAXException {
        if (literalLevel > 0) {
            xmlLiteral.write("<?" + target + " " + data + "?>");
        }
    }

    public void comment(char[] chars, int start, int len) throws SAXException {
        if (literalLevel > 0) {
            xmlLiteral.write("<!--");
            xmlLiteral.write(new String(chars, start, len));
            xmlLiteral.write("-->");
        }
    }

    private Frame getParent(Stack<Frame> stack) {
        Frame parent = null;
        Frame child = null;
        for (Frame frame : stack ) {
            parent = child;
            child = frame;
        }
        return parent;
    }

    // get the most-specific langauge tag in scope
    private String getLanguage(Stack<Frame> stack) {
        String lang = "";
        for (Frame frame : stack ) {
            if (frame.lang != null && !lang.startsWith(frame.lang)) {
                lang = frame.lang;
            }
        }
        return lang;
    }

    // get the xml:base in scope
    private URI getBase(Stack<Frame> stack) {
        URI base = null;
        for (Frame frame : stack ) {
            if (frame.base != null) {
                base = frame.base;
            }
        }
        return base;
    }

    // is our parent a predicate?
    private boolean inPredicate(Stack<Frame> stack) {
        boolean ip = false;
        for (Frame frame : stack ) {
            ip = frame.isPredicate;
        }
        return ip;
    }

    // do we expect to encouter a subject (rather than a predicate?)
    private boolean expectSubject(Stack<Frame> stack) {
        boolean es = true;
        for (Frame frame : stack ) {
            es = !frame.isSubject;
        }
        return es;
    }

    // if we're in a predicate, get its frame
    private Frame parentPredicateFrame(Stack<Frame> stack) throws SAXException {
        if (inPredicate(stack)) {
            Frame predicateFrame = null;
            for (Frame frame : stack ) {
                if (frame.isPredicate) {
                    predicateFrame = frame;
                }
            }
            return predicateFrame;
        } else {
            throw new SAXException("internal parser error: cannot find enclosing predicate");
        }
    }

    // get the uriRef of the predicate we're in
    private Resource parentPredicate(Stack<Frame> stack) throws SAXException {
        Frame ppFrame = parentPredicateFrame(stack);
        return ppFrame != null ? ppFrame.node : null;
    }

    // get the datatype (if any) of the predicate we're in
    private String datatype(Stack<Frame> stack) throws SAXException {
        Frame f = parentPredicateFrame(stack);
        return f != null ? f.datatype : null;
    }

    // get the nearest ancestor subject frame
    private Frame ancestorSubjectFrame(Stack<Frame> stack) throws SAXException {
        Frame subjectFrame = null;
        for (Frame frame : stack ) {
            if (frame.isSubject) {
                subjectFrame = frame;
            }
        }
        return subjectFrame;
    }

    // get the nearest ancestor subject
    private Resource ancestorSubject(Stack<Frame> stack) throws SAXException {
        Frame subjectFrame = ancestorSubjectFrame(stack);
        return subjectFrame != null ? subjectFrame.node : null;
    }

    // if we're looking at a subject, is it an item in a Collection?
    private boolean isCollectionItem(Stack<Frame> stack) throws SAXException {
        if (inPredicate(stack)) {
            Frame predicateFrame = parentPredicateFrame(stack);
            return predicateFrame != null && predicateFrame.isCollection;
        } else {
            return false;
        }
    }

    // blank node generation
    private Resource blankNode() {
        return Resource.blankNode("b" + (bn++));
    }

    private Resource blankNode(String s) {
        return Resource.blankNode(s);
    }

    // resolve relative uri's against the in-scope xml:base URI
    private Resource resolve(String uriString, Stack<Frame> stack) throws SAXException {
        URI uri = UriFactory.getFactory().newUri(uriString);
        if (uri.isAbsolute()) {
            return uriRef(uri);
        } else {
            URI base = getBase(stack);
            if (base == null) {
                throw new SAXException("can't resolve relative URI reference \"" + uriString + "\", no xml:base in scope");
            }
            return uriRef(base.resolve(uriString));
        }
    }

    // the complicated logic to determine the subject uriref
    private void getSubjectNode(Frame frame, Stack<Frame> stack, Attributes attrs) throws SAXException {
        String about = attrs.getValue(RDF, "about");
        if (about != null) {
            frame.node = resolve(about, stack);
        }
        String nodeId = attrs.getValue(RDF, "nodeID");
        if (nodeId != null) {
            if (frame.node != null) {
                throw (new SAXException("ambiguous use of rdf:nodeID"));
            }
            frame.node = blankNode(nodeId);
        }
        String rdfId = attrs.getValue(RDF, "ID");
        if (rdfId != null) {
            if (frame.node != null) {
                throw (new SAXException("ambiguous use of rdf:ID"));
            }
            frame.node = resolve(rdfId, stack);
        }
        if (frame.node == null) {
            frame.node = blankNode();
        }
        frame.isSubject = true;
    }

    // the complicated logic to deal with attributes with rdf:resource, nodeID attrs
    private Resource getObjectNode(Stack<Frame> stack, Attributes attrs) throws SAXException {
        Resource node = null;
        String resource = attrs.getValue(RDF, "resource");
        if (resource != null) {
            node = resolve(resource, stack);
        }
        String nodeId = attrs.getValue(RDF, "nodeID");
        if (nodeId != null) {
            if (node != null) {
                throw (new SAXException("ambiguous use of rdf:nodeID"));
            }
            node = blankNode(nodeId);
        }
        return node;
    }

    // here we're in a literal so we have to produce reasonably-canonical XML
    // representation of this start tag
    private void xmlLiteralStart(StringWriter out, String ns, String qn, Attributes attrs) {
        out.write("<" + qn);
        Map<String, String> pfxMap = new HashMap<String, String>();
        for (int i = -1; i < attrs.getLength(); i++ ) {
            String aQn, aNs;
            if (i < 0) {
                aQn = qn;
                aNs = ns;
            } else {
                aQn = attrs.getQName(i);
                aNs = attrs.getURI(i);
            }
            if (!"".equals(aNs)) {
                String pfx = aQn.replaceFirst(":.*", "");
                pfxMap.put(pfx, aNs);
            }
        }
        for (Map.Entry<String, String> pfxMapping : pfxMap.entrySet() ) {
            out.write(" xmlns:" + pfxMapping.getKey() + "=\"" + pfxMapping.getValue() + "\"");
        }
        for (int i = 0; i < attrs.getLength(); i++ ) {
            String aQn = attrs.getQName(i);
            String aVal = attrs.getValue(i);
            out.write(" " + aQn + "=\"" + Xml.escape(aVal) + "\"");
        }
        out.write(">");
    }

    // produce a reasonably canonical end tag
    private void xmlLiteralEnd(StringWriter out, String qn) {
        out.write("</" + qn + ">");
    }

    // if a language tag is in scope, apply it to the literal
    private Literal withLanguageTag(Literal l, Stack<Frame> stack) {
        String lang = getLanguage(stack);
        if (!"".equals(lang)) {
            l.setLanguageTag(lang);
        }
        return l;
    }

    // produce a triple for the handler
    private void yield(Object s, Object p, Object o) {
        triples.add(new Triple(s, p, o));
    }

    // produce a triple for the handler
    private void yield(Triple t) {
        triples.add(t);
    }

    // produce a (possibly) reified triple
    private void yield(Object s, Object p, Object o, Resource reified) {
        yield(s, p, o);
        if (reified != null) {
            yield(reified, Rdf.TYPE, Rdf.STATEMENT);
            yield(reified, Rdf.SUBJECT, s);
            yield(reified, Rdf.PREDICATE, p);
            yield(reified, Rdf.OBJECT, o);
        }
    }

    // a stack frame represents a nesting level in the RDF/XML document
    class Frame {
        public Resource       node           = null; // the subject/object
        public String         lang           = null; // the language tag
        public URI            base           = null; // the xml:base
        public String         datatype       = null; // a predicate's datatype
        public Resource       reification    = null; // when reifying, the triple's uriRef
        public List<Resource> collection     = null; // for parseType=Collection, the items
        public Triple         collectionHead = null; // for parseType=Collection, the head triple
        public boolean        isSubject      = false; // is there a subject at this frame
        public boolean        isPredicate    = false; // is there a predicate at this frame
        public boolean        isCollection   = false; // is the predicate at this frame a collection
        public int            li             = 1;
    }
}
