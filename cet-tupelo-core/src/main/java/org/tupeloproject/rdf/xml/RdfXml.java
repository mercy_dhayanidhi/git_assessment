/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Set;

import org.tupeloproject.rdf.Triple;
import org.tupeloproject.util.Iterators;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Provides static facilities for dealing with RDF/XML serilization and
 * deserialization
 */
public class RdfXml {
    static RdfXmlWriter writer = null;

    /**
     * Parse RDF/XML data
     * 
     * @param is
     *            the input source
     * @return the triples
     * @throws IOException
     */
    public static Set<Triple> parse(InputSource is) throws IOException {
        try {
            RdfXmlParser parser = new RdfXmlParser();
            return parser.parse(is);
        } catch (SAXException x) {
            throw (new IOException("parser error", x));
        }
    }

    /**
     * Parse RDF/XML data
     * 
     * @param is
     *            the input source
     * @return the triples
     * @throws IOException
     */
    public static Set<Triple> parse(InputStream is) throws IOException {
        return parse(new InputSource(is));
    }

    /**
     * Parse RDF/XML data
     * 
     * @param r
     *            the input source
     * @return the triples
     * @throws IOException
     */
    public static Set<Triple> parse(Reader r) throws IOException {
        return parse(new InputSource(r));
    }

    /**
     * Write triples to an output stream
     * 
     * @param triples
     *            the triples
     * @param os
     *            the output stream
     * @throws IOException
     */
    public static void write(Set<Triple> triples, OutputStream os) throws IOException {
        if (writer == null) {
            writer = new RdfXmlWriter();
        }
        writer.write(triples, os);
    }

    /**
     * Write triples to an output stream
     * 
     * @param triples
     *            the triples
     * @param os
     *            the output stream
     * @throws IOException
     */
    public static void write(Iterable<Triple> triples, OutputStream os) throws IOException {
        write(Iterators.asSet(triples.iterator()), os);
    }
}
