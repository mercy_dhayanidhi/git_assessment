/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.Writer;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.rdf.Literal;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.util.Bag;
import org.tupeloproject.util.HashBag;
import org.tupeloproject.util.QNames;
import org.tupeloproject.util.Xml;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

/**
 * RdfXmlWriter
 */
public class RdfXmlWriter {
    private static Document xmlLiteralTransform = null;

    private static Document getXmlLiteralTransform() {
        if (xmlLiteralTransform == null) {
            // this is a string instead of a resource to insulate Tupelo from funky classloaders
            String xmlLiteralXsl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"\n" + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" + "\n" + "    <!-- copy input -->\n" + "    <xsl:template\n" + "        match=\"*|@*|comment()|processing-instruction()|text()\">\n" + "        <xsl:copy>\n" + "            <xsl:apply-templates\n" + "                select=\"*|@*|comment()|processing-instruction()|text()\"/>\n" + "        </xsl:copy>\n" + "    </xsl:template>\n" + "\n" + "    <!-- except for xmlliterals -->\n" + "    <xsl:template match=\"*[@rdf:datatype='http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral']/text()\">\n" + "      <xsl:value-of disable-output-escaping=\"yes\" select=\".\"/>\n" + "    </xsl:template>\n" + "\n" + "</xsl:stylesheet>";
            try {
                xmlLiteralTransform = Xml.parse(new StringReader(xmlLiteralXsl));
            } catch (Exception e) {
                // we can't handle XML literals! something bad is happening
                return Xml.newDocument();
            }
        }
        return xmlLiteralTransform;
    }

    public RdfXmlWriter() {
    }

    public void write(Set<Triple> triples, OutputStream os) throws IOException {
        write(triples, new PrintWriter(new OutputStreamWriter(os, "UTF-8")));
    }

    public void write(Set<Triple> triples, Writer w) throws IOException {
        // this is the document we're going to create
        Document doc = Xml.newDocument();
        Element rdf = doc.createElementNS(Rdf.NS, "rdf:RDF");
        doc.appendChild(rdf);
        // the set of all subjects
        Set<Resource> subjects = new TreeSet<Resource>();
        // for each blank node, its id
        int bnId = 1;
        Map<Resource, String> bNodes = new HashMap<Resource, String>();
        for (Triple t : triples ) {
            subjects.add(t.getSubject());
            if (t.getSubject().isBlank() && bNodes.get(t.getSubject()) == null) {
                bNodes.put(t.getSubject(), "b" + (bnId++));
            }
            if (t.getPredicate().isBlank() && bNodes.get(t.getPredicate()) == null) {
                bNodes.put(t.getPredicate(), "b" + (bnId++));
            }
            if (t.getObject().isBlank() && bNodes.get(t.getObject()) == null) {
                bNodes.put(t.getObject(), "b" + (bnId++));
            }
        }
        // for each subject, a subject element associated with it
        Map<Resource, Element> descriptionBySubject = new HashMap<Resource, Element>();
        // for each object, a predicate element associated with it
        Map<Resource, Element> predicateByObject = new HashMap<Resource, Element>();
        //  for each object, the number of times it is used as an object
        Bag<Resource> objects = new HashBag<Resource>();
        // a namespace prefix for each namespace URI that is used
        try {
            // stuff all the triples in a context
            TripleContext c = new MemoryContext();
            c.addTriples(triples);
            // for each subject, write a description
            for (Resource subjectNode : subjects ) {
                TripleMatcher tm = new TripleMatcher();
                tm.setSubject(subjectNode);
                c.perform(tm);
                Set<Triple> orderedTriples = new TreeSet<Triple>();
                Resource rdfType = null;
                for (Triple t : tm ) {
                    if (Rdf.TYPE.equals(t.getPredicate())) {
                        rdfType = t.getObject();
                    }
                    orderedTriples.add(t);
                }
                Element rdfDescription = null;
                // create an rdf:description element to hold the description
                rdfDescription = doc.createElementNS(Rdf.NS, "rdf:Description");
                // it's an rdf:Description, so can't skip the child representing the rdf:type assertion
                boolean skipType = false;
                // if it's a typed object, use the type as the element name to hold the description
                if (rdfType != null) {
                    // attempt to create a qname for the uri
                    QName pQn = QNames.uriToQName(rdfType.getUri());
                    // now make an element for it
                    try {
                        rdfDescription = doc.createElementNS(pQn.getNamespaceURI(), pQn.getLocalPart());
                        // now we can skip the child representing the rdf:type assertion
                        skipType = true;
                    } catch (DOMException x) {
                        // in this case, the rdf:type cannot be represented as a QName.
                        // fall through and use rdf:Description instead
                    }
                }
                // generate the rdf:nodeId or rdf:about, whichever is relevant
                String bn = bNodes.get(subjectNode);
                if (bn != null) {
                    rdfDescription.setAttributeNS(Rdf.NS, "rdf:nodeID", bn);
                } else {
                    rdfDescription.setAttributeNS(Rdf.NS, "rdf:about", subjectNode.getString());
                }
                // now do the attributes
                for (Triple triple : orderedTriples ) {
                    if (skipType && triple.getPredicate().equals(Rdf.TYPE) && triple.getObject().equals(rdfType)) {
                        // skip
                    } else {
                        // attempt to create a qname for the uri
                        QName pQn = QNames.uriToQName(triple.getPredicate().getUri());
                        // now make an element for it
                        Element predicate = doc.createElementNS(pQn.getNamespaceURI(), pQn.getLocalPart());
                        // if it's a literal, add the text child
                        // the object
                        Resource object = triple.getObject();
                        if (object.isLiteral()) {
                            Literal literal = (Literal) object;
                            if (literal.isTypedLiteral()) {
                                predicate.setAttributeNS(Rdf.NS, "rdf:datatype", literal.getDatatype().getString());
                            } else if (literal.hasLanguageTag()) {
                                predicate.setAttribute("xml:lang", literal.getLanguageTag());
                            }
                            // include the literal value as a text node
                            Text textNode = doc.createTextNode(literal.getString());
                            predicate.appendChild(textNode);
                        } else { // it's another resource
                            // add to its ref count
                            objects.add(object);
                            // generate the rdf:nodeId or rdf:resource, whichever is relevant
                            bn = bNodes.get(object);
                            if (bn != null) {
                                predicate.setAttributeNS(Rdf.NS, "rdf:nodeID", bn);
                            } else {
                                predicate.setAttributeNS(Rdf.NS, "rdf:resource", object.getString());
                            }
                            // this may replace an existing entry, which is OK
                            predicateByObject.put(object, predicate);
                        }
                        // tack the predicate on to the enclosing rdf:Description element
                        rdfDescription.appendChild(predicate);
                    }
                }
                descriptionBySubject.put(subjectNode, rdfDescription);
            }
            // now produce a nesting structure
            for (Resource subject : subjects ) {
                Element description = descriptionBySubject.get(subject);
                Element predicate = predicateByObject.get(subject);
                if (predicate != null) {
                    try {
                        predicate.appendChild(description);
                        predicate.removeAttributeNS(Rdf.NS, "resource");
                        predicate.removeAttributeNS(Rdf.NS, "nodeID");
                        // if this is a blank node that's only used as an object once,
                        // toast its nodeID
                        if (subject.isBlank() && objects.getCardinality(subject) == 1) {
                            description.removeAttributeNS(Rdf.NS, "nodeID");
                        }
                    } catch (DOMException x) {
                        // this is probably a hierarchy exception because
                        // we have a circular relationship. back off
                        rdf.appendChild(description);
                    }
                } else {
                    rdf.appendChild(description);
                }
            }
            generateNamespacePrefixes(doc);
            // done. write the document
            //Xml.write(doc, w);
            Xml.transform(new DOMSource(doc), new DOMSource(getXmlLiteralTransform()), new StreamResult(w), true);
        } catch (OperatorException x) {
            IOException e = new IOException("internal error");
            e.initCause(x);
            throw e;
        } catch (TransformerException x) {
            IOException e = new IOException("internal error");
            e.initCause(x);
            throw e;
        } catch (ParseException e) {
            IOException x = new IOException("bad RDF predicate");
            x.initCause(e);
            throw x;
        }
    }

    void generateNamespacePrefixes(Document doc) {
        Map<String, String> p = new HashMap<String, String>();
        p.put("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
        p.put("http://www.w3.org/2001/XMLSchema#", "xsd");
        p.put("http://www.w3.org/2000/01/rdf-schema#", "rdfs");
        p.put("http://purl.org/dc/elements/1.1/", "dc");
        p.put("http://purl.org/dc/terms/", "dcterms");
        p.put("http://purl.org/dc/dcmitype/", "dctypes");
        p.put("http://www.w3.org/2002/07/owl#", "owl");
        p.put("http://xmlns.com/foaf/0.1/", "foaf");
        p.put("http://www.holygoat.co.uk/owl/redwood/0.1/tags/", "tags");
        p.put("http://cet.ncsa.uiuc.edu/2007/", "cet");
        p.put("http://www.opengis.net/gml/", "gml");
        p.put("http://cet.ncsa.uiuc.edu/2008/2/20/Data.owl#", "ds");
        p.put("http://www.w3.org/2006/time#", "time");
        generateNamespacePrefixes(doc, doc.getDocumentElement(), p, new int[] { 1 });
    }

    void generateNamespacePrefixes(Document doc, Node node, Map<String, String> prefixes, int[] count) {
        Element root = doc.getDocumentElement();
        String localName = node.getLocalName();
        if (localName != null && !localName.contains(":")) {
            String namespaceUri = node.getNamespaceURI();
            String prefix = prefixes.get(namespaceUri);
            if (prefix == null) {
                if (count[0] <= 26) {
                    prefix = " abcdefghijklmnopqrstuvwxyz".substring(count[0], count[0] + 1);
                } else {
                    prefix = "ns" + count[0];
                }
                count[0]++;
                prefixes.put(namespaceUri, prefix);
            }
            root.setAttribute("xmlns:" + prefix, namespaceUri);
            node.setPrefix(prefix);
        }
        for (Node child : Xml.toList(node.getChildNodes()) ) {
            generateNamespacePrefixes(doc, child, prefixes, count);
        }
    }
}
