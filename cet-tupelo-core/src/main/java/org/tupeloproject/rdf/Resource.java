package org.tupeloproject.rdf;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tupeloproject.rdf.terms.Xsd;
import org.tupeloproject.util.Iso8601;
import org.tupeloproject.util.TagUriFactory;
import org.tupeloproject.util.UnicodeTranscoder;

/**
 * Represents a resource in RDF. Resources include URI references,
 * literals, and blank nodes. Resources are immutable; their setters
 * work, but only once, after which they throw
 * IllegalArgumentException.
 */
public abstract class Resource implements Comparable<Resource>, Serializable {
    private static final long serialVersionUID = 1L;

    // manage hash code computation state
    private boolean           hashed           = false;
    private int               hashCode;

    /**
     * Create a new unique URI reference using TagUriFactory.
     * 
     * @return the new UriRef
     */
    public static UriRef uriRef() {
        return uriRef(TagUriFactory.getFactory().newUri());
    }

    /**
     * create a URI reference.
     * 
     * @param uri
     *            the uri. must be absolute!
     * @return the UriRef
     */
    public static UriRef uriRef(URI uri) {
        return new UriRef(uri);
    }

    /**
     * create a URI reference.
     * 
     * @param uri
     *            a string consisting of the uri. must be absolute!
     * @return the UriRef
     */
    public static UriRef uriRef(String uri) {
        return new UriRef(uri);
    }

    /**
     * Create a plain literal
     * 
     * @param value
     *            the literal
     * @return the Literal
     */
    public static Literal literal(String value) {
        return new Literal(value);
    }

    /**
     * Create a typed literal
     * 
     * @param value
     *            the literal
     * @param datatype
     *            a URI reference for the datatype
     * @return the Literal
     */
    public static Literal literal(String value, UriRef datatype) {
        return new Literal(value, datatype);
    }

    /**
     * Create a typed literal
     * 
     * @param value
     *            the literal
     * @param datatype
     *            a URI reference for the datatype
     * @return the Literal
     */
    public static Literal literal(String value, String datatype) {
        return literal(value, uriRef(datatype));
    }

    /**
     * Create a typed literal
     * 
     * @param value
     *            the literal
     * @param datatype
     *            a URI reference for the datatype
     * @return the Literal
     */
    public static Literal literal(String value, URI datatype) {
        return literal(value, uriRef(datatype));
    }

    /**
     * Create a plain literal with a language tag
     * 
     * @param value
     *            the literal value
     * @param languageTag
     *            the language tag
     * @return the Literal
     */
    public static Literal plainLiteral(String value, String languageTag) {
        Literal l = literal(value);
        l.setLanguageTag(languageTag);
        return l;
    }

    /**
     * Create an integer typed literal.
     * 
     * @param value
     *            the value
     * @return the literal representing the integer
     */
    public static Literal literal(int value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a long typed literal
     * 
     * @param value
     *            the value
     * @return the literal representing the long
     */
    public static Literal literal(long value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a float typed literal
     * 
     * @param value
     *            the value
     * @return the literal representing the float
     */
    public static Literal literal(float value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a double typed literal
     * 
     * @param value
     *            the value
     * @return the literal representing the double
     */
    public static Literal literal(double value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a boolean typed literal
     * 
     * @param value
     *            the value
     * @return the literal representing the boolean
     */
    public static Literal literal(boolean value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a Date typed literal
     * 
     * @param value
     *            the value
     * @return the literal representing the date
     */
    public static Literal literal(Date value) {
        return (Literal) fromObject(value);
    }

    /**
     * Create a blank node
     * 
     * @param localId
     *            the local id of the blank node
     * @return a locally-scoped blank node
     */
    public static BlankNode blankNode(String localId) {
        return new BlankNode(localId);
    }

    /**
     * Create a blank node
     * 
     * @param localId
     *            the local id of the blank node
     * @param scopeId
     *            the scope id
     * @return a globally-scoped blank node
     */
    public static BlankNode blankNode(String localId, UriRef scopeId) {
        return new BlankNode(localId, scopeId);
    }

    /**
     * Create a blank node
     * 
     * @param localId
     *            the local id of the blank node
     * @param scopeId
     *            a string consisting of the scope id (must be an absolute uri)
     * @return a globally-scoped blank node
     */
    public static BlankNode blankNode(String localId, String scopeId) {
        return new BlankNode(localId, uriRef(scopeId));
    }

    /**
     * Create a blank node
     * 
     * @param localId
     *            the local id of the blank node
     * @param scopeId
     *            an absolute URI for the scope id
     * @return a globally-scoped blank node
     */
    public static BlankNode blankNode(String localId, URI scopeId) {
        return new BlankNode(localId, uriRef(scopeId));
    }

    /**
     * Create a resource, which is either a UriRef or a BlankNode depending
     * on whether the argument is an absolute URI.
     * 
     * @param description
     *            the identifier
     * @return the Resouce
     */
    public static Resource resource(String description) {
        try {
            URI theUri = new URI(description);
            if (theUri.isAbsolute()) {
                return uriRef(theUri);
            }
        } catch (URISyntaxException x) {
            // fall through
        }
        return blankNode(description);
    }

    /**
     * Return the resource's string value
     * 
     * @throws UnsupportedOperationException
     *             if there is no string value
     * @return the string representation
     */
    public String getString() {
        throw new UnsupportedOperationException("node does not have String representation");
    }

    /**
     * A synonym for getString
     * 
     * @return the string value
     */
    public String toString() {
        try {
            return getString();
        } catch (UnsupportedOperationException e) {
            return super.toString();
        }
    }

    // URI references

    /**
     * Is this node a URI reference?
     * 
     * @return is this node a URI reference?
     */
    public boolean isUri() {
        return false;
    }

    /**
     * Return the node's URI.
     * 
     * @throws UnsupportedOperationException
     *             if this is not a uri reference
     * @return the node's uri
     */
    public URI getUri() {
        throw new UnsupportedOperationException("node does not have a URI");
    }

    // blank nodes

    /**
     * Is this node a blank node?
     * 
     * @return is this node a blank node?
     */
    public boolean isBlank() {
        return false;
    }

    /**
     * Is this blank node scoped?
     * 
     * @throws UnsupportedOperationException
     *             if this is not a blank node
     * @return is this blank node scoped?
     */
    public boolean isScoped() {
        throw new UnsupportedOperationException("not a blank node");
    }

    /**
     * Get the scope id if this is blank node
     * 
     * @throws UnsupportedOperationException
     *             if this is not a blank node
     * @return the scope id if this is blank node
     */
    public UriRef getScopeId() {
        throw new UnsupportedOperationException("not a blank node");
    }

    /**
     * Set the scope id if this is blank node
     * 
     * @param scopeId
     *            the scope id
     * @throws UnsupportedOperationException
     *             if this is not a blank node
     */
    public void setScopeId(UriRef scopeId) {
        throw new UnsupportedOperationException("not a blank node");
    }

    /**
     * Get the local ID if this is a blank node.
     * 
     * @throws UnsupportedOperationException
     *             if this is not a blank node
     * @return the local ID if this is a blank node.
     */
    public String getLocalId() {
        throw new UnsupportedOperationException("not a blank node");
    }

    // literals

    /**
     * Is this node a literal?
     * 
     * @return if it's a literal
     */
    public boolean isLiteral() {
        return false;
    }

    /**
     * Is this resource a typed literal?
     * 
     * @return if it's a typed literal
     */
    public boolean isTypedLiteral() {
        return false;
    }

    /**
     * Return the node's datatype
     * 
     * @throws UnsupportedOperationException
     *             if this isn't a typed literal
     * @return the node's datatype
     */
    public UriRef getDatatype() {
        throw new UnsupportedOperationException("not a typed literal");
    }

    /**
     * Does this Resource have a language tag? Only true for
     * some literals.
     * 
     * @return if this Resource has a language tag
     */
    public boolean hasLanguageTag() {
        return false;
    }

    /**
     * If this resource is a plain literal with a language tag,
     * return the language tag
     * 
     * @return the language tag
     */
    public String getLanguageTag() {
        throw new UnsupportedOperationException("not a literal");
    }

    /**
     * Compare to another node. Nodes are ordered in the following way:
     * <ol>
     * <li>null comes first</li>
     * <li>URI nodes are ordered according to java.net.URI's implementation of
     * compareTo.</li>
     * <li>literals are ordered according to java.net.String's implementation of
     * compareTo.</li>
     * <li>typed literals are ordered by value first, then by datatype URI.</li>
     * <li>mixed-type comparisons are ordered by String representation.</li>
     * </ol>
     * Blank nodes are compared using a different algorithm.
     * 
     * @see BlankNode#compareTo(Resource)
     * @param other
     *            the other Resource
     * @return the comparison
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public int compareTo(Resource other) {
        if (other == this) {
            return 0;
        }
        /* null comes first */
        if (other == null) {
            return 1; // was -1 in Tupelo <=2.5 (see TUP-481)
        }
        /* order by URI's */
        if (isUri() && other.isUri()) {
            URI thisUri = getUri();
            URI thatUri = other.getUri();
            if (thisUri == thatUri) {
                return 0;
            } else {
                return thisUri.compareTo(thatUri);
            }
        } else if (isUri() && other.isLiteral()) {
            return -1;
        } else if (isLiteral() && other.isUri()) {
            return 1;
        } else if (isLiteral() && other.isLiteral()) { /* order literals by value */
            int litComp = getString().compareTo(other.getString());
            if (litComp == 0) { // the lexical forms are identical
                if (isTypedLiteral() && other.isTypedLiteral()) {
                    // if they're both typed, compare the dataype uri's
                    return getDatatype().compareTo(other.getDatatype());
                } else if (isTypedLiteral() && !other.isTypedLiteral()) {
                    // if this is typed and the other isn't, the other's first
                    return 1;
                } else if (!isTypedLiteral() && other.isTypedLiteral()) {
                    // and vice versa
                    return -1;
                } else {
                    // neither is typed, so these are the same literal
                    return 0;
                }
            } else {
                // if they're both typed and map to java types, use that as
                // the basis for comparison
                if (isTypedLiteral() && other.isTypedLiteral() && getDatatype().equals(other.getDatatype())) {
                    Object thisObject = asObject();
                    if (!(thisObject instanceof Resource)) {
                        int nonLitComp = ((Comparable) thisObject).compareTo(other.asObject());
                        if (nonLitComp == 0) {
                            // non-identical strings, but same value. these are
                            // still not the same literal, so return their lexical
                            // order
                            return litComp;
                        } else {
                            return nonLitComp;
                        }
                    }
                }
                return litComp;
            }
        } else if (isBlank()) {
            if (this == other) {
                return 0;
            } else if (other.isBlank()) {
                BlankNode otherBlank = (BlankNode) other;
                if (isScoped() && otherBlank.isScoped()) {
                    int compareScope = getScopeId().compareTo(otherBlank.getScopeId());
                    if (compareScope == 0) {
                        return getLocalId().compareTo(otherBlank.getLocalId());
                    } else {
                        return compareScope;
                    }
                } else {
                    // if just one of the nodes is scoped, order them as scoped/not-scoped
                    if (isScoped()) { // this is scoped, but other isn't
                        return -1;
                    } else if (otherBlank.isScoped()) { // other is scoped, but this isn't
                        return 1;
                    } else {
                        throw new IllegalArgumentException("can't compare two non-scoped blank nodes");
                    }
                }
            } else {
                // non-blank nodes precede blank nodes
                return -1;
            }
        } else if (other.isBlank()) {
            // non-blank nodes precede blank ones
            return 1;
        } else {
            return getString().compareTo(other.getString());
        }
    }

    /**
     * Uses the same implementation as compareTo.
     */
    public boolean equals(Object other) {
        try {
            Resource otherResource = (Resource) other;
            return compareTo(otherResource) == 0;
        } catch (ClassCastException x) {
            return false;
        }
    }

    public int hashCode() {
        if (!hashed) {
            int hash = 17;
            hash = (31 * hash) + (isUri() ? getUri().hashCode() : 0);
            hash = (31 * hash) + (isLiteral() ? getString().hashCode() : 0);
            hash = (31 * hash) + (isTypedLiteral() ? getDatatype().hashCode() : 0);
            hash = (31 * hash) + (hasLanguageTag() ? getLanguageTag().hashCode() : 0);
            hashCode = hash;
            hashed = true;
        }
        return hashCode;
    }

    /**
     * Return an
     * <a href="http://www.w3.org/2001/sw/RDFCore/ntriples/">N-Triples</a>
     * serialization of this Resource.
     * 
     * @return an
     *         <a
     *         href="http://www.w3.org/2001/sw/RDFCore/ntriples/">N-Triples</a>
     *         serialization of this Resource.
     */
    public abstract String toNTriples();

    public static Resource fromNTriples(String x) throws ParseException {
        if (x.startsWith("<")) { //$NON-NLS-1$
            return Resource.uriRef(x.substring(1, x.length() - 1));
        } else if (x.startsWith("_:")) { //$NON-NLS-1$
            return Resource.blankNode(x.substring(2));
        } else {
            Matcher m = Pattern.compile("\"(.*)\"\\^\\^\\<(.*)\\>").matcher(x); //$NON-NLS-1$
            if (m.find()) {
                return Resource.literal(UnicodeTranscoder.decode(m.group(1)), m.group(2));
            }
            m = Pattern.compile("\"(.*)\"\\@(.*)").matcher(x); //$NON-NLS-1$
            if (m.find()) {
                return Resource.plainLiteral(UnicodeTranscoder.decode(m.group(1)), m.group(2));
            }
        }
        return Resource.literal(UnicodeTranscoder.decode(x.substring(1, x.length() - 1)));
    }

    public Object asObject() {
        return toObject();
    }

    public abstract Object toObject();

    /**
     * Provides an mapping between (some kinds of) Java objects
     * and {@link Resource}, by mapping some Java types to some
     * <a href="http://www.w3.org/TR/xmlschema-2/">XML Schema datatypes</a>.
     * Here are the type mappings:
     * <table border="1">
     * <tr>
     * <th>Java</th>
     * <th>Resource type</th>
     * <th>XSD type</th>
     * </tr>
     * <tr>
     * <td>{@link URI}</td>
     * <td>{@link UriRef}</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>{@link String}</td>
     * <td>{@link Literal}</td>
     * <td></td>
     * </tr>
     * <tr>
     * <td>{@link Integer}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#int">xsd:int</a></td>
     * </tr>
     * <tr>
     * <td>{@link Long}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#long">xsd:long</a></td>
     * </tr>
     * <tr>
     * <td>{@link Float}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#float">xsd:float</a></td>
     * </tr>
     * <tr>
     * <td>{@link Double}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#double">xsd:double</a></td>
     * </tr>
     * <tr>
     * <td>{@link Date}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#dateTime">xsd:dateTime</a>
     * </td>
     * </tr>
     * <tr>
     * <td>{@link Boolean}</td>
     * <td>{@link Literal}</td>
     * <td><a href="http://www.w3.org/TR/xmlschema-2/#boolean">xsd:boolean</a></td>
     * </tr>
     * </table>
     */
    public static Resource fromObject(Object object) {
        if (object == null) {
            return null;

        } else if (object instanceof Resource) {
            return (Resource) object;

        } else if (object instanceof URI) {
            return Resource.uriRef((URI) object);

        } else if (object instanceof String) {
            return Resource.literal((String) object);

        } else if (object instanceof Integer) {
            return Resource.literal(object.toString(), Xsd.INT);

        } else if (object instanceof Float) {
            return Resource.literal(object.toString(), Xsd.FLOAT);

        } else if (object instanceof Double) {
            return Resource.literal(object.toString(), Xsd.DOUBLE);

        } else if (object instanceof Long) {
            return Resource.literal(object.toString(), Xsd.LONG);

        } else if (object instanceof Boolean) {
            return Resource.literal(object.toString(), Xsd.BOOLEAN);

        } else if (object instanceof Date) {
            return Resource.literal(Iso8601.date2String((Date) object), Xsd.DATE_TIME);

        } else {
            throw new IllegalArgumentException("unable to convert object " + object + " to RDF");
        }
    }
}