/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf;

import java.net.URI;
import java.util.regex.Pattern;

import org.tupeloproject.util.UriFactory;

/**
 * Represents a URI reference in RDF.
 * 
 * @author Joe Futrelle
 */
public class UriRef extends Resource {
    private static final long serialVersionUID = 1L;

    private static Pattern    _schemeId        = Pattern.compile("^[a-zA-Z][a-zA-Z0-9+.-]*:.*$");

    private String            uriString        = null;

    /** For setter injection */
    public UriRef() {
        super();
    }

    /**
     * Create a UriRef using the given URI.
     * 
     * @param uri
     *            the uri
     */
    public UriRef(URI uri) {
        setUriString(uri.toString());
    }

    /**
     * Create a UriRef using the given URI.
     * The URI must in the form of a valid absolute URI.
     * 
     * @param uri
     *            the uri. must be a valid absolute URI!
     */
    public UriRef(String uri) {
        validateUriString(uri);
        setUriString(uri);
    }

    private void validateUriString(String us) {
        // this is relatively expensive but it catches errors early
        if (!_schemeId.matcher(us).matches()) {
            throw new IllegalArgumentException("RDF does not permit relative URI refs: " + us);
        }
    }

    public String getUriString() {
        return uriString;
    }

    public void setUriString(String uriString) {
        this.uriString = uriString;
    }

    /**
     * Get the uri.
     * 
     * @return the uri
     */
    public URI getUri() {
        return UriFactory.getFactory().newUri(getUriString());
    }

    /**
     * Set the uri.
     * 
     * @param value
     *            the new uri
     */
    public void setUri(URI value) {
        if (uriString != null) {
            throw new IllegalArgumentException("UriRefs are immutable");
        }
        if (!value.isAbsolute()) {
            throw new IllegalArgumentException("RDF does not permit relative URI refs: " + value);
        }
        uriString = value.toString();
    }

    public boolean isUri() {
        return true;
    }

    public String getString() {
        return uriString;
    }

    @Override
    public String toNTriples() {
        return "<" + uriString + ">";
    }

    @Override
    public Object toObject() {
        return getUri();
    }
}
