/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf;

import java.net.URI;

import org.tupeloproject.util.SecureHashMinter;

/**
 * Represents a blank node in an RDF graph. Blank nodes do not have
 * globally unique ID's; rather, their identity is scoped to a
 * particular RDF graph. Since this makes blank nodes non-portable
 * between graphs, this class provides means of "globalizing" blank
 * node ID's by allowing applications to associate a blank node with a
 * "scope" URI identifying the graph. Unless a scope id is explicitly
 * set on a blank node instance, it will have a default scope id which
 * is unique per-VM-instance.
 * 
 * @author Joe Futrelle
 */
public class BlankNode extends Resource {
    private static final long serialVersionUID = 1L;

    private static UriRef     defaultScopeId   = Resource.uriRef();

    private UriRef            scopeId;
    private String            localId;

    /**
     * Construct a blank node.
     * The default scope id is a freshly-minted tag URI.
     * The default local id is a freshly-minted secure hash digest, prefixed
     * with an alpha character
     */
    public BlankNode() {
        setLocalId("blank_" + SecureHashMinter.getMinter().mint());
    }

    /**
     * Create a locally-scoped blank node.
     * 
     * @param localId
     *            the local id
     */
    public BlankNode(String localId) {
        setLocalId(localId);
        setScopeId(defaultScopeId);
    }

    /**
     * Create a globally-scoped blank node.
     * 
     * @param localId
     *            the local id
     * @param scopeId
     *            the scope id
     */
    public BlankNode(String localId, UriRef scopeId) {
        setLocalId(localId);
        setScopeId(scopeId);
    }

    /**
     * Create a globally-scoped blank node.
     * 
     * @param localId
     *            the local id
     * @param scopeId
     *            the scope id
     */
    public BlankNode(String localId, URI scopeId) {
        setLocalId(localId);
        setScopeId(uriRef(scopeId));
    }

    /**
     * Is this id a blank node?
     */
    public boolean isBlank() {
        return true;
    }

    /**
     * If this blank node is globally scoped, return a URI consisting
     * of the scope URI followed immediately by the local id. If it's
     * locally scoped, return the local id.
     */
    public String getString() {
        if (isScoped()) {
            return getScopeId() + getLocalId();
        } else {
            return getLocalId() + ""; // trick to convert null to "null"
        }
    }

    /**
     * Get the localId.
     * 
     * @return the localId
     */
    public String getLocalId() {
        return localId;
    }

    /**
     * Set the localId.
     * 
     * @param value
     *            the new localId
     */
    public void setLocalId(String value) {
        localId = value;
    }

    /**
     * Get the scopeId.
     * 
     * @return the scopeId
     */
    public UriRef getScopeId() {
        return scopeId;
    }

    /**
     * Set the scopeId.
     * 
     * @param value
     *            the new scopeId
     */
    public void setScopeId(UriRef value) {
        scopeId = value;
    }

    /**
     * Is this blank node globally scoped?
     */
    public boolean isScoped() {
        return getScopeId() != null;
    }

    /**
     * Blank nodes make poor keys unless they're scoped, because two
     * blank nodes from different graphs may have the same local id.
     * Application code MUST NOT use blank nodes as keys unless they
     * are scoped or the application completely controls the creation
     * and management of the BlankNode instances.
     */
    public int hashCode() {
        int hash = super.hashCode(); // this to avoid accidental collisions
        hash = (31 * hash) + (isScoped() ? getScopeId().hashCode() : 0);
        hash = (31 * hash) + (getLocalId() != null ? getLocalId().hashCode() : 0);
        return hash;
    }

    @Override
    public String toNTriples() {
        return "_:" + localId;
    }

    @Override
    public Object toObject() {
        return getString();
    }
}
