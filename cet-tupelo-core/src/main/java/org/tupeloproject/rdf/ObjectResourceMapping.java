package org.tupeloproject.rdf;

public class ObjectResourceMapping {
    public static Object object(Resource resource) {
        return resource.toObject();
    }

    public static Resource resource(Object object) {
        return Resource.fromObject(object);
    }
}
