/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf;

import java.net.URI;
import java.text.ParseException;
import java.util.regex.Pattern;

import org.tupeloproject.rdf.terms.Xsd;
import org.tupeloproject.util.Iso8601;
import org.tupeloproject.util.UnicodeTranscoder;

/**
 * Represents a literal in RDF.
 * 
 * @author Joe Futrelle
 */
public class Literal extends Resource {
    private static final long serialVersionUID  = 1L;

    private Pattern           languageTagSyntax = null;

    private String            string;
    private String            languageTag;
    private UriRef            datatype          = null;

    /**
     * For setter injection.
     */
    public Literal() {
        super();
    }

    /**
     * Create the literal using the given string.
     * 
     * @param string
     *            the string value of the literal.
     */
    public Literal(String string) {
        setString(string);
    }

    /**
     * Create the literal using the given string and data type.
     * 
     * @param string
     *            the string value of the literal.
     * @param datatype
     *            the datatype of the literal
     */
    public Literal(String string, UriRef datatype) {
        setString(string);
        setDatatype(datatype);
    }

    /**
     * Create the literal using the given string and data type.
     * 
     * @param string
     *            the string value of the literal.
     * @param datatype
     *            the datatype of the literal
     */
    public Literal(String string, URI datatype) {
        setString(string);
        setDatatype(uriRef(datatype));
    }

    /**
     * Create the literal using the given string and data type.
     * 
     * @param string
     *            the string value of the literal.
     * @param datatype
     *            the datatype of the literal
     */
    public Literal(String string, String datatype) {
        setString(string);
        setDatatype(uriRef(datatype));
    }

    /**
     * Is this a literal?
     */
    public boolean isLiteral() {
        return true;
    }

    /**
     * Is this a typed literal?
     */
    public boolean isTypedLiteral() {
        return datatype != null;
    }

    /**
     * Get the string.
     * 
     * @return the string
     */
    public String getString() {
        return string;
    }

    /**
     * Set the string.
     * 
     * @param value
     *            the new string
     */
    public void setString(String value) {
        if (string != null) {
            throw new IllegalArgumentException("literals are immutable");
        }
        string = value;
    }

    /**
     * Get the datatype.
     * 
     * @return the datatype
     */
    public UriRef getDatatype() {
        return datatype;
    }

    /**
     * Set the datatype.
     * 
     * @param value
     *            the new datatype
     */
    public void setDatatype(UriRef value) {
        if (datatype != null) {
            throw new IllegalArgumentException("literals are immutable");
        }
        datatype = value;
    }

    /**
     * Return if this literal has a language tag
     * 
     * @return if this literal has a language tag
     */
    public boolean hasLanguageTag() {
        return languageTag != null;
    }

    /**
     * Return the language tag
     * 
     * @return the language tag
     */
    public String getLanguageTag() {
        return languageTag;
    }

    /**
     * Set the language tag
     * 
     * @param languageTag
     *            the language tag
     * @throws IllegalArgumentException
     *             if the language tag is invalid or has
     *             already been set
     */
    public void setLanguageTag(String languageTag) {
        if (this.languageTag != null) {
            throw new IllegalArgumentException("literals are immutable");
        }
        if (languageTagSyntax == null) {
            languageTagSyntax = Pattern.compile("[a-zA-Z]+(-[a-zA-Z0-9]+)*");
        }
        if (!languageTagSyntax.matcher(languageTag).matches()) {
            throw new IllegalArgumentException("invalid language tag " + languageTag);
        }
        this.languageTag = languageTag;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Literal)) {
            return false;
        }
        Literal lit = (Literal) obj;
        return lit.string.equals(string);
    }

    @Override
    public String toNTriples() {
        String escapedQuoted = "\"" + UnicodeTranscoder.encode(string) + "\"";
        if (isTypedLiteral()) {
            return escapedQuoted + "^^<" + getDatatype() + ">";
        } else if (hasLanguageTag()) {
            return escapedQuoted + "@" + getLanguageTag();
        } else {
            return escapedQuoted;
        }
    }

    @Override
    public Object toObject() {
        if (!isTypedLiteral()) {
            return string;
        }

        if (Xsd.STRING.equals(datatype)) {
            return string;

        } else if (Xsd.INT.equals(datatype)) {
            return Integer.parseInt(string);

        } else if (Xsd.uriRef("integer").equals(datatype)) {
            return Integer.parseInt(string);

        } else if (Xsd.POSITIVE_INTEGER.equals(datatype)) {
            return Integer.parseInt(string);

        } else if (Xsd.FLOAT.equals(datatype)) {
            return Float.parseFloat(string);

        } else if (Xsd.DOUBLE.equals(datatype)) {
            return Double.parseDouble(string);

        } else if (Xsd.LONG.equals(datatype)) {
            return Long.parseLong(string);

        } else if (Xsd.BOOLEAN.equals(datatype)) {
            return Boolean.parseBoolean(string);

        } else if (Xsd.DATE_TIME.equals(datatype)) {
            try {
                return Iso8601.string2Date(string).getTime();
            } catch (ParseException x) {
                return string;
            }

        } else {
            // unknown xsd type, return String
            return string;
        }
    }
}
