/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.query;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * SimpleQuery is an abstract representation of an RDF query having some of the features of
 * a SPARQL query.
 */
public class SimpleQuery {

    List<String> columnNames = new LinkedList<String>();
    LinkedList<Pattern> patterns = new LinkedList<Pattern>();

    /**
     * Get the patterns.
     *
     * @return the patterns
     */
    public LinkedList<Pattern> getPatterns() {
	return patterns;
    }

    /**
     * Set the patterns.
     *
     * @param value the new patterns
     */
    public void setPatterns(LinkedList<Pattern> value) {
	patterns = value;
    }

    /**
     * Add a pattern.
     * @param pattern the pattern
     */
    public void addPattern(Pattern pattern) {
	getPatterns().add(pattern);
    }

    /**
     * Add a pattern
     * @param s a variable name or Node for the subject
     * @param p a variable name or Node for the predicate
     * @param o a variable name or Node for the object
     * @param optional is the pattern optional?
     */
    public void addPattern(Object s, Object p, Object o, boolean optional) {
        Pattern pattern = new Pattern(s,p,o);
        pattern.setOptional(optional);
        getPatterns().add(pattern);
    }

    /**
     * Add a pattern
     * @param s a variable name or Node for the subject
     * @param p a variable name or Node for the predicate
     * @param o a variable name or Node for the object
     */
    public void addPattern(Object s, Object p, Object o) {
        addPattern(s,p,o,false);
    }

    /**
     * Get the columnNames.
     *
     * @return the columnNames
     */
    public List<String> getColumnNames() {
	return columnNames;
    }

    /**
     * Set the columnNames.
     *
     * @param value the new columnNames
     */
    public void setColumnNames(List<String> value) {
	columnNames = value;
    }

    /**
     * Set the column names
     * @param value the column names
     */
    public void setColumnNames(String... value) {
	columnNames = Arrays.asList(value);
    }

    /**
     * Add a column name
     * @param value the column name
     */
    public void addColumnName(String value) {
	getColumnNames().add(value);
    }

}