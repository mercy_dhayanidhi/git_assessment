/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.query;

import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.util.ListTable;
import org.tupeloproject.util.Table;
import org.tupeloproject.util.Tables;
import org.tupeloproject.util.Tuple;

/**
 * PagingTable consumes a table and produces a subset based on a query's orderBy/limit/offset parameters
 */
public class PagingTable implements Table<Resource> {
    Table<Resource> table;
    OrderableQuery query;

    public PagingTable(OrderableQuery query, Table<Resource> table) {
        this.query = query;
        this.table = table;
    }

    int compareEvenIfNull(Resource a, Resource b) {
	if(a == null) {
	    if(b == null) { return 0; }
	    return -1; // was effectively 1 in Tupelo <=2.5 (see TUP-481)
	} else {
	    return a.compareTo(b);
	}
    }
    
    /**
     * Get an iterator over the rows in this table.
     */
    public Iterator<Tuple<Resource>> iterator() {
        long limit = query.getLimit();
        long offset = query.getOffset();
        Comparator<Tuple<Resource>> comparator = new Comparator<Tuple<Resource>>() {
            public int compare(Tuple<Resource> a, Tuple<Resource> b) {
                int cmp = 0;
                for(OrderBy ob : query.getOrderBy()) {
                    int ci = Tables.getColumnIndex(table,ob.getName());
                    boolean asc = ob.isAscending();
                    int tc = compareEvenIfNull(a.get(ci),b.get(ci));
                    if(tc != 0) {
                        cmp = asc ? tc : (0 - tc);
                        break;
                    }
                }
                return cmp;
            }
        };
        SortedSet<Tuple<Resource>> ss = new TreeSet<Tuple<Resource>>(comparator);
        for(Tuple<Resource> row : table) {
            ss.add(row);
        }
        ListTable<Resource> result = new ListTable<Resource>();
        int i = 0;
        for(Tuple<Resource> row : ss) {
            i++;
            if(limit != OrderableQuery.UNLIMITED && i > offset + limit) {
                break;
            } else if(i > offset) {
                result.addRow(row);
            }
        }
        return result.iterator();
    }

    /**
     * Get the column names for this table.
     *
     * @return the column names
     */
    public Tuple<String> getColumnNames() {
        return table.getColumnNames();
    }
}
