/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.query;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.util.Tuple;
import org.tupeloproject.util.Variable;

/**
 * A {@link Tuple} of {@link Variable}s. This can be used to represent
 * patterns in a query as well as templates for producing graphs.
 * 
 * @author Joe Futrelle
 */
public class Pattern extends Tuple<Variable<Resource>> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * Is this pattern optional? In some contexts, this affects whether or
     * not it is considered to match. By default, patterns are not optional.
     * 
     * @return whether or not the pattern is optional
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * Set whether or not this pattern is optional.
     * In some contexts, this affects whether or
     * not it is considered to match.
     * 
     * @param optional
     *            whether or not it is optional
     */
    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    boolean optional = false;

    /**
     * Create an empty pattern
     */
    public Pattern() {
    }

    /**
     * Create a pattern from the given terms. If a term is a String,
     * it's treated as a variable name. If it's a Resource, it's treated
     * as a variable value. If it's a Variable, it's simply copied
     * into the pattern.
     * 
     * @param terms
     *            the terms in the pattern
     */
    public Pattern(Object... terms) {
        if (terms == null) {
            throw new IllegalArgumentException("no terms");
        }
        setOrder(terms.length);
        for (int i = 0; i < terms.length; i++ ) {
            Object o = terms[i];
            if (o instanceof String) {
                String s = (String) o;
                set(i, new Variable<Resource>(s));
            } else if (o instanceof Resource) {
                set(i, new Variable<Resource>(null, (Resource) o));
            } else if (o instanceof Variable) {
                set(i, (Variable) o);
            } else {
                throw new IllegalArgumentException("malformed pattern for \"" + o + "\"");
            }
        }
    }

    public Tuple<String> getNames() {
        Tuple<String> result = new Tuple<String>(getOrder());
        for (int i = 0; i < getOrder(); i++ ) {
            result.set(i, get(i).getName());
        }
        return result;
    }

    public Tuple<Resource> getValues() {
        Tuple<Resource> result = new Tuple<Resource>(getOrder());
        for (int i = 0; i < getOrder(); i++ ) {
            result.set(i, get(i).getValue());
        }
        return result;
    }

    /** Copy the pattern */
    public Object clone() {
        Pattern p = new Pattern();
        int order = getOrder();
        p.setOrder(order);
        for (int i = 0; i < order; i++ ) {
            Variable<Resource> vc = new Variable<Resource>();
            vc.setName(get(i).getName());
            vc.setValue(get(i).getValue());
            p.set(i, vc);
        }
        p.setOptional(isOptional());
        return p;
    }

    /**
     * Does the tuple match the pattern? This method does not respect
     * the optional flag.
     * 
     * @param tuple
     *            the tuple
     * @return if it matches
     */
    public boolean matches(Tuple<Resource> tuple) {
        for (int i = 0; i < size(); i++ ) {
            Variable<Resource> var = get(i);
            if (var.isBound() && !var.getValue().equals(tuple.get(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * is the given pattern identical to this one, except with
     * fewer bound variables?
     */
    public boolean hasAncestor(Pattern other) {
        // in practice these are both going to be 3-patterns
        if (this.size() != other.size()) {
            return false;
        }
        //
        for (int i = 0; i < size(); i++ ) {
            Variable<Resource> a = get(i);
            Variable<Resource> b = other.get(i);
            if (a.isBound()) {
                if (b.isBound() && !a.getValue().equals(b.getValue())) {
                    return false;
                }
                // fall through if b matches, or is ubound
            } else if (b.isBound()) {
                return false; // a is unbound but b is bound, bad
            }
        }
        return true;
    }

    /**
     * Convenience for get(0).getValue()
     * 
     * @return
     */
    public Resource getSubject() {
        return get(0).getValue();
    }

    /**
     * Convenience for get(1).getValue()
     * 
     * @return
     */
    public Resource getPredicate() {
        return get(1).getValue();
    }

    /**
     * Convenience for get(2).getValue()
     */
    public Resource getObject() {
        return get(2).getValue();
    }
}
