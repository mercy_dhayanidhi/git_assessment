/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf.query;

import java.util.LinkedList;
import java.util.List;

/**
 * OrderableQuery
 */
public class OrderableQuery extends SimpleQuery {
    public static final long UNLIMITED = -1;
    List<OrderBy>            orderBy;
    long                     limit     = UNLIMITED;
    long                     offset    = 0;

    public List<OrderBy> getOrderBy() {
        if (orderBy == null) {
            orderBy = new LinkedList<OrderBy>();
        }
        return orderBy;
    }

    public void setOrderBy(List<OrderBy> orderBy) {
        this.orderBy = orderBy;
    }

    public void addOrderBy(OrderBy orderBy) {
        getOrderBy().add(orderBy);
    }

    public void addOrderBy(String column, boolean ascending) {
        OrderBy ob = new OrderBy();
        ob.setName(column);
        ob.setAscending(ascending);
        getOrderBy().add(ob);
    }

    public void addOrderBy(String column) {
        OrderBy ob = new OrderBy();
        ob.setName(column);
        getOrderBy().add(ob);
    }

    public void addOrderByDesc(String column) {
        addOrderBy(column, false);
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public void setNoLimit() {
        setLimit(UNLIMITED);
    }

    public boolean hasNoLimit() {
        return getLimit() == UNLIMITED;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    // do we have paging parameters that need handling?
    public boolean hasPaging() {
        return !hasNoLimit() || getOffset() > 0 || getOrderBy().size() > 0;
    }
}
