package org.tupeloproject.kernel;

import java.util.ArrayList;
import java.util.List;

public class MultipleOperatorException extends OperatorException {
    private static final long serialVersionUID = 1L;
    
    private List<Throwable> exceptions = new ArrayList<Throwable>();

    public MultipleOperatorException() {
        super();
    }

    public MultipleOperatorException(String msg) {
        super(msg);
    }

    public MultipleOperatorException(Throwable thr) {
        super();
        addException(thr);
    }

    public  MultipleOperatorException(String msg, Throwable thr) {
        super(msg);
        addException(thr);
    }

    public void addException(Throwable thr) {
        addException(thr);
    }
    
    public List<Throwable> getExceptions() {
        return exceptions;
    }

    @Override
    public Throwable getCause() {
        return exceptions.get(0);
    }

}
