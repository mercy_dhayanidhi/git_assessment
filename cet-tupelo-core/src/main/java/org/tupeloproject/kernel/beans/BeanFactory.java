package org.tupeloproject.kernel.beans;

import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.util.Minter;
import org.tupeloproject.util.UriRefMinter;

public class BeanFactory {
    private TripleContext    context;
    private Cache            cache;
    private Minter<Resource> minter;

    public BeanFactory() {
        this(null, null, null);
    }

    public BeanFactory(TripleContext context) {
        this(context, null, null);
    }

    public BeanFactory(TripleContext context, Cache cache) {
        this(context, cache, null);
    }

    public BeanFactory(TripleContext context, Minter<Resource> minter) {
        this(context, null, minter);
    }

    public BeanFactory(TripleContext context, Cache cache, Minter<Resource> minter) {
        this.context = context;
        this.cache = cache;
        if (minter != null) {
            this.minter = minter;
        } else {
            this.minter = new UriRefMinter();
        }
    }

    public Session openSession() {
        return new Session(this);
    }

    public Minter<Resource> getMinter() {
        return minter;
    }

    public void setMinter(Minter<Resource> minter) {
        this.minter = minter;
    }

    public TripleContext getContext() {
        return context;
    }

    public void setContext(TripleContext context) {
        this.context = context;
    }

    // ----------------------------------------------------------------------
    // CACHE OPERATIONS
    // ----------------------------------------------------------------------

    public Cache getCache() {
        return cache;
    }

    public void setCache(Cache cache) {
        this.cache = cache;
    }

    public void evict(Resource subject) throws BeanException {
        // TODO implement
        throw (new BeanException("Not implemented."));
    }

    public void evict(Object object) throws BeanException {
        // TODO implement
        throw (new BeanException("Not implemented."));
    }
}