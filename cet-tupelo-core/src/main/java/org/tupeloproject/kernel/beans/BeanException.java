package org.tupeloproject.kernel.beans;

import org.tupeloproject.kernel.OperatorException;

public class BeanException extends OperatorException {
    private static final long serialVersionUID = 1L;

    public BeanException() {
    }

    public BeanException(String msg) {
        super(msg);
    }

    public BeanException(String msg, Throwable thr) {
        super(msg, thr);
    }

    public BeanException(Throwable thr) {
        super(thr);
    }
}