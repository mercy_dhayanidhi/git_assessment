package org.tupeloproject.kernel.beans;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tupeloproject.kernel.beans.Cache.TripleObject;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public interface Cache extends Map<Resource, TripleObject> {
    TripleObject put(Resource key, TripleObject object);

    TripleObject put(Resource key, Object object);

    TripleObject put(Resource key, Object object, Set<Triple> triples);

    Resource firstKey(Object object);

    static public class TripleObject {
        public Set<Triple> triples;
        public Object      object;

        public TripleObject(Object object) {
            this(object, new HashSet<Triple>());
        }

        public TripleObject(Object object, Set<Triple> triples) {
            this.object = object;
            this.triples = triples;
        }
    }
}