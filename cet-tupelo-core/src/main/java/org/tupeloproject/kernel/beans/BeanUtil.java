package org.tupeloproject.kernel.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.rdf.terms.Tupelo;
import org.tupeloproject.util.Tuple;

public class BeanUtil {
    private static Log  log = LogFactory.getLog(BeanUtil.class);

    private BeanFactory beanFactory;

    public BeanUtil() {
        this(null);
    }

    public BeanUtil(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    // ----------------------------------------------------------------------
    // GET CLASS/ID
    // ----------------------------------------------------------------------

    public Class<?> getClass(Resource id) throws BeanException {
        // find all rdf types
        TripleMatcher tm = new TripleMatcher();
        tm.setSubject(id);
        tm.setPredicate(Rdf.TYPE);
        try {
            beanFactory.getContext().perform(tm);
        } catch (OperatorException e) {
            throw (new BeanException("Could not get type.", e));
        }

        // find the first (random) class
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if (cl == null) {
            cl = BeanUtil.class.getClassLoader();
        }
        String autoclass = Tupelo.uriRef("beans/class/").getString();
        try {
            for (Triple t : tm ) {
                try {
                    return BeanInfo.get(t.getObject()).getBeanClass();
                } catch (BeanException e) {
                    log.debug(e);
                }
                if (t.getObject().getString().startsWith(autoclass)) {
                    try {
                        Class<?> clz = cl.loadClass(t.getObject().getString().substring(autoclass.length()));
                        return BeanInfo.get(clz).getBeanClass();
                    } catch (Exception e) {
                        log.debug(e);
                    }
                }
            }
        } finally {
            try {
                tm.close();
            } catch (IOException e) {
                throw (new BeanException("Could not close triplematcher", e));
            }
        }

        throw (new BeanException(String.format("Could not find information for id %s", id)));
    }

    public Resource getId(Object object) throws BeanException {
        BeanInfo bi = BeanInfo.get(object.getClass());
        try {
            if ((bi.getSubject() != null) && (bi.getSubject().get(object) != null)) {
                return Resource.uriRef(bi.getSubject().get(object).toString());
            } else {
                return Resource.uriRef();
            }
        } catch (IllegalAccessException e) {
            throw (new BeanException(String.format("Could not access subject field %s.", bi.getSubject()), e));
        }
    }

    // ----------------------------------------------------------------------
    // GET ALL
    // ----------------------------------------------------------------------

    /**
     * Based on a collection of ids this will return the collection of Beans
     * with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<? extends Object> get(Class<?> clz, Collection<String> ids) throws BeanException {
        return get(clz, new ArrayList<String>(ids), false);
    }

    /**
     * Based on a collection of ids this will return the collection of Beans
     * with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> get(Class<?> clz, Collection<String> ids, boolean getDeleted) throws BeanException {
        return get(clz, new ArrayList<String>(ids), getDeleted);
    }

    /**
     * Based on a list of ids this will return the ordered beans with those ids.
     * This will only return a list of items that are not deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> get(Class<?> clz, List<String> ids) throws BeanException {
        return get(clz, ids, false);
    }

    /**
     * Based on a list of ids this will return the ordered beans with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> get(Class<?> clz, List<String> ids, boolean getDeleted) throws BeanException {
        List<Object> result = new ArrayList<Object>();
        Session sess = getBeanFactory().openSession();
        sess.beginTransaction();
        for (String id : ids ) {
            Object obj = sess.load(clz, Resource.uriRef(id), getDeleted);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Return a collection of all the beans found for the given class. This
     * could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> getAll(Class<?> clz) throws Exception {
        return getAll(clz, false, 0, -1);
    }

    /**
     * Return a collection of all the beans found of this specific type. This
     * could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans of this type.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> getAll(Class<?> clz, boolean getDeleted) throws BeanException {
        return getAll(clz, getDeleted, 0, -1);
    }

    /**
     * Returns a list of beans based on the given class. This will only return
     * beans marked as not deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param offset
     *            the starting point in the list of beans.
     * @param limit
     *            the maximum number of beans to return.
     * @return list of all beans of the given class.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> getAll(Class<?> clz, long offset, long limit) throws BeanException {
        return getAll(clz, false, offset, limit);
    }

    /**
     * Fetch all beans of a given rdf type with paging parameters.
     * 
     * @param getDeleted
     * @param offset
     * @param limit
     * @return
     * @throws Exception
     */
    public List<?> getAll(Class<?> clz, boolean getDeleted, long offset, long limit) throws BeanException {
        return get(clz, getAllIds(clz, getDeleted, offset, limit), getDeleted);
    }

    /**
     * Return a collection of all ids of the beans found of this specific type.
     * This could be a large list. This will only return the ids of those that
     * are not marked as deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @return list of all ids of Beans of this type.
     * @throws BeanException
     *             throws a BeanException if the list could not be retrieved.
     */
    public List<String> getAllIds(Class<?> clz) throws BeanException {
        return getAllIds(clz, false, 0, -1);
    }

    /**
     * Return a collection of all ids of the beans found of this specific type.
     * This could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all ids of Beans of this type.
     * @throws BeanException
     *             throws a BeanException if the list could not be retrieved.
     */
    public List<String> getAllIds(Class<?> clz, boolean getDeleted) throws BeanException {
        return getAllIds(clz, getDeleted, 0, -1);
    }

    /**
     * Return a collection of all ids of the beans found of this specific type.
     * This could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all ids of Beans of this type.
     * @throws BeanException
     *             throws a BeanException if the list could not be retrieved.
     */
    public List<String> getAllIds(Class<?> clz, boolean getDeleted, long offset, long limit) throws BeanException {
        Unifier u = new Unifier();
        u.setColumnNames("s"); //$NON-NLS-1$
        u.addPattern("s", Rdf.TYPE, BeanInfo.get(clz).getType()); //$NON-NLS-1$
        // arbitrary order, makes paging consistent but is otherwise meaningless
        u.addOrderBy("s"); //$NON-NLS-1$        
        u.setOffset(offset);
        if (limit > 0) {
            u.setLimit(limit);
        }

        try {
            beanFactory.getContext().perform(u);
        } catch (OperatorException e) {
            throw (new BeanException("Could not get list of beans.", e));
        }

        List<String> result = new ArrayList<String>();
        for (Tuple<Resource> row : u.getResult() ) {
            result.add(row.get(0).toString());
        }
        return result;
    }
}