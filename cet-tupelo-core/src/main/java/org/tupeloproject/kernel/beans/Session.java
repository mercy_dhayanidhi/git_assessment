package org.tupeloproject.kernel.beans;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.BeanInfo.FieldInfo;
import org.tupeloproject.kernel.beans.Cache.TripleObject;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.DcTerms;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.rdf.terms.Tupelo;
import org.tupeloproject.util.Tuple;

/**
 * The main runtime interface between a Java application and a Context. This is
 * the central API class abstracting the notion of a persistence service. This
 * is loosely modeled after the Session interface in Hibernate.
 * 
 * The lifecycle of a Session is bounded by the beginning and end of a logical
 * transaction.
 * 
 * The main function of the Session is to offer create, read and delete
 * operations for instances of mapped entity classes. Instances may exist in one
 * of three states:
 * 
 * transient: never persistent, not associated with the Session<br>
 * persistent: associated with a the Session<br>
 * detached: previously persistent, not associated with the Session<br>
 * 
 * Transient instances may be made persistent by calling save(). Persistent
 * instances may be made transient by calling delete(). Any instance returned by
 * a load() method is persistent. The state of a transient or detached instance
 * may also be made persistent as a new persistent instance by calling merge().
 * 
 * It is not intended that implementors be threadsafe. Instead each
 * thread/transaction should obtain its own instance from a BeanFactory.
 * 
 * A typical transaction should use the following idiom:
 * 
 * <pre>
 * Session sess = factory.getSession();
 * try {
 *     sess.beginTransaction();
 *     //do some work
 *     sess.commit();
 * } catch (Exception e) {
 *     sess.rollback();
 *     throw e;
 * }
 * </pre>
 * 
 * If the Session throws an exception, the transaction must be rolled back and
 * the session discarded. The internal state of the Session might not be
 * consistent with the database after the exception occurs.
 * 
 * @author Rob Kooper
 * @see BeanFactory
 * 
 */
public class Session {
    private static final Resource STORAGE_TYPE_MAP_ENTRY = Tupelo.uriRef("beans/storageTypeMapEntry"); //$NON-NLS-1$
    private static final Resource STORAGE_TYPE_MAP_KEY   = Tupelo.uriRef("beans/storageTypeMapKey");  //$NON-NLS-1$
    private static final Resource STORAGE_TYPE_MAP_VALUE = Tupelo.uriRef("beans/storageTypeMapValue"); //$NON-NLS-1$

    private static Log            log                    = LogFactory.getLog(Session.class);
    private static final String   RDF_IDX                = Rdf.NS + "_";                              //$NON-NLS-1$
    private static int            MAXPATTERN             = 50;
    private static Method         isInitializedMethod    = null;

    private Cache                 sessionCache;
    private Set<Resource>         deletes;
    private final BeanFactory     beanFactory;
    private final BeanUtil        beanUtil;
    private TripleWriter          tw;
    private boolean               comitted;
    private boolean               rollback;

    /**
     * do a quick check to see if there is any data when reading
     * set/list/collection/map, instead of always using a proxy.
     */
    private boolean               checkNullCollection    = false;

    static {
        try {
            isInitializedMethod = UninitializedProxy.class.getMethod("isInitialized");
        } catch (SecurityException e) {
            log.warn("Could not find isInitialized method for UninitializedProxy", e);
        } catch (NoSuchMethodException e) {
            log.warn("Could not find isInitialized method for UninitializedProxy", e);
        }
    }

    public Session(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
        this.beanUtil = new BeanUtil(beanFactory);
        this.sessionCache = null;
        this.deletes = null;
        this.comitted = false;
        this.rollback = false;
    }

    /**
     * Should a check be done and set/list/collection/map be set to null if no
     * data is found. This requires a check of the context, which could be
     * expensive.
     * 
     * @return true if a check for no data needs to be done.
     */
    public boolean isCheckNullCollection() {
        return checkNullCollection;
    }

    /**
     * Should a check be done and set/list/collection/map be set to null if no
     * data is found. This requires a check of the context, which could be
     * expensive.
     * 
     * @param checkNullCollection
     *            true if a check for no data needs to be done.
     */
    public void setCheckNullCollection(boolean checkNullCollection) {
        this.checkNullCollection = checkNullCollection;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    // ----------------------------------------------------------------------
    // TRANSACTION
    // ----------------------------------------------------------------------

    /**
     * @deprecated session should be used only once.
     */
    public void reuseSession() {
        if ((sessionCache != null) && !comitted) {
            log.info("Session had transaction but not comitted.");
        }
        rollback = false;
        comitted = false;
        deletes = new HashSet<Resource>();
        tw = new TripleWriter();
    }

    public boolean hasTransaction() {
        return (deletes != null);
    }

    /**
     * Begin a unit of work.
     */
    public void beginTransaction() throws BeanException {
        if (hasTransaction()) {
            throw (new BeanException("Can only start a transaction once."));
        }
        if (sessionCache == null) {
            sessionCache = new MapCache();
        }
        deletes = new HashSet<Resource>();
        tw = new TripleWriter();
    }

    /**
     * This method will commit the underlying transaction and end the unit of
     * work.
     * 
     * @throws BeanException
     */
    public void commit() throws BeanException {
        if (!hasTransaction()) {
            log.info("Trying to commit a transaction that was not started.");
            beginTransaction();
        }
        if (comitted) {
            throw (new BeanException("Session is already comitted."));
        }
        comitted = true;

        writeTriples();
    }

    /**
     * Check if this transaction was successfully committed.
     * 
     * @return True if the transaction was (unequivocally) committed; false
     *         otherwise.
     */
    public boolean wasComitted() {
        return comitted;
    }

    /**
     * Force the underlying transaction to roll back.
     * 
     * @throws BeanException
     */
    public void rollBack() throws BeanException {
        if (!comitted) {
            throw (new BeanException("Session is not yet comitted."));
        }
        if (rollback) {
            throw (new BeanException("Session is already rolled back."));
        }

        TripleWriter rb = new TripleWriter();
        rb.setToRemove(tw.getToAdd());
        rb.setToAdd(tw.getToRemove());
        try {
            beanFactory.getContext().perform(rb);
            rollback = true;
        } catch (OperatorException e) {
            throw (new BeanException("Could not roll back triples, context is in unknown state.", e));
        }
    }

    /**
     * Was this transaction rolled back.
     * 
     * @return True if the transaction was rolled back; false otherwise.
     */
    public boolean wasRolledBack() {
        return rollback;
    }

    /**
     * Remove this instance from the session cache. Changes to the instance will
     * not be synchronized with the context.
     * 
     * @param bean
     * @throws BeanException
     */
    public void evict(Object bean) throws BeanException {
        // FIXME implement
        throw (new BeanException("NOT YET IMPLEMENTED."));
    }

    public void close() throws BeanException {
        if (hasTransaction() && !comitted) {
            log.info("Session had transaction but not comitted.", new Exception());
        }
        sessionCache = null;
        deletes = null;
        tw = null;
    }

    // ----------------------------------------------------------------------
    // DELETE BEAN
    // ----------------------------------------------------------------------
    /**
     * Remove a persistent instance from the context. The argument may be an
     * instance associated with the receiving Session or a transient instance
     * with an identifier associated with existing persistent state. This
     * operation will NOT cascades to associated instances.
     * 
     * @param object
     *            the instance to be removed
     * @throws BeanException
     */
    public void delete(Object object) throws BeanException {
        if (!hasTransaction()) {
            beginTransaction();
        }
        deletes.add(beanUtil.getId(object));
    }

    // ----------------------------------------------------------------------
    // SAVE/MERGE/UPDATE BEAN
    // ----------------------------------------------------------------------

    /**
     * Persist the given transient instance, first assigning a generated
     * identifier. (Or using the current value of the identifier property if the
     * assigned generator is used.) This operation cascades to associated
     * instances.
     * 
     * @param object
     *            a transient instance of a persistent class
     * @return the generated identifier
     * @throws BeanException
     *             a BeanException is thrown if the object already exists in the
     *             session.
     */
    public Resource save(Object object) throws BeanException {
        return save(object, null);
    }

    /**
     * Persist the given transient instance, using the give id. This operation
     * cascades to associated instances.
     * 
     * @param object
     *            a transient instance of a persistent class
     * @param id
     *            the id of the object to save
     * @return the generated identifier
     * @throws BeanException
     *             a BeanException is thrown if the object already exists in the
     *             session.
     */
    @SuppressWarnings("unchecked")
    public Resource save(Object object, Resource id) throws BeanException {
        if (object == null) {
            return null;
        }
        if (!hasTransaction()) {
            beginTransaction();
        }

        // check if saved already
        Resource sessionid = sessionCache.firstKey(object);
        if (sessionid != null) {
            return sessionid;
        }

        // get bean info for object, throws exception if non found (such as for String)
        BeanInfo bi = BeanInfo.get(object.getClass());

        // find generate id for bean
        if (id == null) {
            id = beanUtil.getId(object);
        }

        // save bean
        sessionCache.put(id, object);
        log.trace("SAVING : " + id);

        // now save all associated instances
        for (FieldInfo fi : bi.getFields() ) {
            Object o;
            try {
                o = fi.getField().get(object);
            } catch (IllegalArgumentException e) {
                throw (new BeanException("Could not read field " + fi.getField(), e));
            } catch (IllegalAccessException e) {
                throw (new BeanException("Could not read field " + fi.getField(), e));
            }

            // null means?
            if (o == null) {
                continue;
            }

            // check for UnitializedProxy and ignore anything that was not fetched yet
            if (o instanceof UninitializedProxy) {
                if (!((UninitializedProxy) o).isInitialized()) {
                    continue;
                }
            }

            // check for array
            if (fi.getField().getType().isArray()) {
                for (Object x : ((Object[]) o) ) {
                    if (BeanInfo.isBean(x.getClass())) {
                        save(x, null);
                    }
                }
                continue;
            }

            // check for map
            if (Map.class.isAssignableFrom(fi.getField().getType())) {
                for (Entry<Object, Object> entry : ((Map<Object, Object>) o).entrySet() ) {
                    if (BeanInfo.isBean(entry.getKey().getClass())) {
                        save(entry.getKey(), null);
                    }
                    if (BeanInfo.isBean(entry.getValue().getClass())) {
                        save(entry.getValue(), null);
                    }
                }
                continue;
            }

            // check for collection
            if (Collection.class.isAssignableFrom(fi.getField().getType())) {
                for (Object x : ((Collection<Object>) o) ) {
                    if (BeanInfo.isBean(x.getClass())) {
                        save(x, null);
                    }
                }
                continue;
            }

            // check for enum
            if (fi.getField().getType().isEnum()) {
                continue;
            }

            // check for bean
            if (BeanInfo.isBean(o.getClass())) {
                save(o, null);
            }
        }

        return id;
    }

    /**
     * Copy the state of the given object onto the persistent object with the
     * same identifier. If there is no persistent instance currently associated
     * with the session, it will be loaded. Return the persistent instance. The
     * given instance does not become associated with the session. This
     * operation cascades to associated instances.
     * 
     * @param object
     *            a detached instance with state to be copied
     * @return an updated persistent instance
     * @throws BeanException
     */
    public Object merge(Object bean) throws BeanException {
        return load(bean.getClass(), save(bean));
    }

    /**
     * Update the persistent instance with the identifier of the given detached
     * instance. If there is a persistent instance with the same identifier, an
     * exception is thrown. This operation cascades to associated instances if
     * the association is mapped with cascade="save-update". Parameters: object
     * - a detached instance containing updated state Throws: HibernateException
     * 
     * @param object
     * @throws BeanException
     */
    public void update(Object object) throws BeanException {
        // FIXME implement
        throw (new BeanException("NOT YET IMPLEMENTED."));
    }

    // ----------------------------------------------------------------------
    // (RE)LOAD BEAN
    // ----------------------------------------------------------------------

    /**
     * Re-read the state of the given instance from the underlying context. It
     * is inadvisable to use this to implement long-running sessions that span
     * many business tasks. This method is, however, useful in certain special
     * circumstances. For example where a event is fired indicating an external
     * change to the object. This will refresh all instances associated with the
     * object as well.
     * 
     * @param object
     *            a persistent or detached instance
     * @throws BeanException
     */
    public void refresh(Object object) throws BeanException {
        // get the id of the object
        Resource id = beanUtil.getId(object);

        // make sure it is cached
        if ((sessionCache != null) && sessionCache.containsKey(id)) {
            load(object.getClass(), id, false, true, new HashMap<Resource, Object>());
            return;
        }
        if ((beanFactory.getCache() != null) && beanFactory.getCache().containsKey(id)) {
            load(object.getClass(), id, false, true, new HashMap<Resource, Object>());
            return;
        }
        throw (new BeanException("Object is not cached, no need to refresh."));
    }

    /**
     * Return the persistent instance of the given entity class with the given
     * identifier. (If the instance is already associated with the session,
     * return that instance.)
     */
    public <T> T load(Class<? extends T> clz, Resource id) throws BeanException {
        return load(clz, id, false);
    }

    /**
     * Return the persistent instance of the given entity class with the given
     * identifier. (If the instance is already associated with the session,
     * return that instance.) This will not return an instance that has been
     * marked as deleted.
     */
    @SuppressWarnings("unchecked")
    public <T> T load(Class<? extends T> clz, Resource id, boolean getDeleted) throws BeanException {
        Object o = load(clz, id, getDeleted, false, new HashMap<Resource, Object>());
        if (clz == null) {
            return (T) o;
        } else if (o == null) {
            return (T) o;
        } else if (clz.isInstance(o)) {
            return (T) o;
        } else {
            throw (new BeanException("Can not convert object, which is type " + o.getClass() + " to " + clz));
        }
    }

    // ----------------------------------------------------------------------
    // CREATE OBJECT FROM CONTEXT
    // ----------------------------------------------------------------------
    /**
     * Actual code to load the bean and all sub-beans from the context.
     */
    protected Object load(Class<?> clz, Resource id, boolean getDeleted, boolean refresh, Map<Resource, Object> done) throws BeanException {
        log.trace(String.format("[%s] : Reading bean.", id));

        // make sure we have a session cache, no need for transaction yet.
        if (sessionCache == null) {
            sessionCache = new MapCache();
        }

        // return previous fetched
        if (done.containsKey(id)) {
            return done.get(id);
        }

        // check cache for object
        TripleContext context = beanFactory.getContext();
        if (!refresh && sessionCache.containsKey(id)) {
            log.trace("Found " + id + " in session cache.");
            Object o = sessionCache.get(id).object;
            done.put(id, o);
            return o;
        }
        if (!refresh && (beanFactory.getCache() != null) && beanFactory.getCache().containsKey(id)) {
            log.trace("Found " + id + " in secondary cache.");
            context = new MemoryContext();
            try {
                TripleWriter tw = new TripleWriter();
                tw.addAll(beanFactory.getCache().get(id).triples);
                context.perform(tw);
            } catch (OperatorException e) {
                log.warn("Could not copy old triples.", e);
                context = beanFactory.getContext();
            }
        }

        // find class if not associated
        if ((clz == null) || Modifier.isAbstract(clz.getModifiers())) {
            clz = beanUtil.getClass(id);
        }

        // bean is not in cache now create the bean
        BeanInfo bi = BeanInfo.get(clz);
        Object result;
        try {
            result = bi.getBeanClass().newInstance();
        } catch (InstantiationException e) {
            throw (new BeanException(String.format("Could not create bean for %s.", id), e));
        } catch (IllegalAccessException e) {
            throw (new BeanException(String.format("Could not create bean for %s.", id), e));
        }
        done.put(id, result);
        Set<Triple> triples = new HashSet<Triple>();

        // Create the unifier
        Map<String, FieldInfo> fields = new HashMap<String, FieldInfo>();
        Unifier uf = new Unifier();

        // limit to beans of correct type
        uf.addPattern(id, Rdf.TYPE, bi.getType());

        // check for deleted beans
        if (!getDeleted) {
            uf.addPattern(id, DcTerms.IS_REPLACED_BY, "replacedBy", true); //$NON-NLS-1$
        }
        uf.addColumnName("replacedBy"); //$NON-NLS-1$

        // recursively process all the fields.
        int idx = 0;
        for (FieldInfo fi : bi.getFields() ) {
            // check for array
            if (fi.getField().getType().isArray()) {
                try {
                    List<Object> list = readList(fi, id, getDeleted, refresh, done, triples, context);
                    if (list != null) {
                        Object o = Array.newInstance(fi.getField().getType().getComponentType(), list.size());
                        for (int i = 0; i < list.size(); i++ ) {
                            Array.set(o, i, list.get(i));
                        }
                        fi.getField().set(result, o);
                    } else {
                        fi.getField().set(result, null);
                    }
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                }
                continue;
            }

            // check for map
            if (Map.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readMap(fi, id, getDeleted, refresh, done, triples, context));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy map.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy map.", fi.getField()), e));
                }
                continue;
            }

            // check for list
            if (List.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readList(fi, id, getDeleted, refresh, done, triples, context));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy list.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy list.", fi.getField()), e));
                }
                continue;
            }

            // check for set
            if (Set.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readSet(fi, id, getDeleted, refresh, done, triples, context));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy set.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy set.", fi.getField()), e));
                }
                continue;
            }

            // check for collection
            if (Collection.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readCollection(fi, id, getDeleted, refresh, done, triples, context));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                }
                continue;
            }

            // fetch the rest
            String name = Integer.toHexString(idx++);
            if (fi.isInverse()) {
                uf.addPattern(name, fi.getPredicate(), id, true);
            } else {
                uf.addPattern(id, fi.getPredicate(), name, true);
            }
            uf.addColumnName(name);
            fields.put(name, fi);
        }

        // run the unifier
        try {
            context.perform(uf);
        } catch (OperatorException e1) {
            throw (new BeanException("Could not read information about the bean.", e1));
        }

        // fill in the bean
        List<String> cols = uf.getColumnNames();
        Map<String, Set<Resource>> seen = new HashMap<String, Set<Resource>>();
        for (Tuple<Resource> row : uf.getResult() ) {
            // check deleted
            if (!getDeleted && Rdf.NIL.equals(row.get(0))) {
                return null;
            }

            // set fields
            idx = 0;
            for (String name : cols ) {
                Resource r = row.get(idx++);
                if (r != null) {
                    FieldInfo fi = fields.get(name);
                    Set<Resource> rs = seen.get(name);
                    if (rs == null) {
                        rs = new HashSet<Resource>();
                        seen.put(name, rs);
                    } else {
                        if (rs.contains(r)) {
                            // ignore
                            continue;
                        } else if (!Map.class.isAssignableFrom(fi.getField().getType())) {
                            log.info(String.format("[%s] : Found additional values for %s.", id, fields.get(name).getField()));
                        }
                    }
                    rs.add(r);

                    // store cached value
                    if (fi.isInverse()) {
                        triples.add(new Triple(r, fi.getPredicate(), id));
                    } else {
                        triples.add(new Triple(id, fi.getPredicate(), r));
                    }

                    // set field to the new value
                    try {
                        readField(id, result, fi, r, getDeleted, refresh, done, triples, context);
                    } catch (IllegalAccessException e) {
                        log.warn(String.format("[%s] : Could not set property for name %s.", id, fields.get(name)), e);
                    }

                }
            }
        }

        // set the subject, overwriting anything that was there.
        if (bi.getSubject() != null) {
            Field subject = bi.getSubject();
            try {
                if (subject.getType().isAssignableFrom(String.class)) {
                    subject.set(result, id.getString());
                } else if (subject.getType().isAssignableFrom(URI.class)) {
                    subject.set(result, id.getUri());
                } else if (subject.getType().isAssignableFrom(Resource.class)) {
                    subject.set(result, id);
                } else {
                    throw (new BeanException("Could not set subject field."));
                }
            } catch (IllegalArgumentException e) {
                throw (new BeanException("Could not set subject field.", e));
            } catch (IllegalAccessException e) {
                throw (new BeanException("Could not set subject field.", e));
            }
        }

        // store object in cache with all triples
        sessionCache.put(id, result, triples);

        return result;
    }

    /**
     * Based on the value read from the context set the value for the
     * corresponding field. This could result in getting more data being read
     * from the context in case of another bean, map, list or collection.
     * 
     * @param id
     *            the id of bean that is being populated.
     * @param obj
     *            the bean that is being populated.
     * @param field
     *            the field whose variable is to be set.
     * @param val
     *            the value for the field as retrieved from the context.
     * @param getDeleted
     *            used when getting other beans.
     * @param cache
     *            used when getting other beans.
     * @throws OperatorException
     *             throws OperatorException if the item could not be read from
     *             the context.
     * @throws OperatorException
     *             throws IllegalAccessException if the field could not be
     *             modified.
     */
    private void readField(Resource id, Object obj, FieldInfo fi, Resource val, boolean getDeleted, boolean refresh, Map<Resource, Object> done, Set<Triple> triples, TripleContext context) throws BeanException, IllegalAccessException {
        // check for map
        if (Map.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING MAP FOR " + fi.getField()));
        }

        // check for list
        if (List.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING LIST FOR " + fi.getField()));
        }

        // check for collection
        if (Collection.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING COLLECTION FOR " + fi.getField()));
        }

        // check for enum
        if (fi.getField().getType().isEnum()) {
            loadEnum(fi, obj, val);
            return;
        }

        // check for bean
        Object result = loadObject(fi.getField().getType(), val, getDeleted, refresh, done);
        fi.getField().set(obj, result);
    }

    /**
     * Converts the resource to the appropriate enum value.
     * 
     * @param fi
     *            the field whose value is to be read.
     * @param obj
     *            the object whose field is to be read.
     * @param v
     *            the resource to be converted.
     * @throws IllegalAccessException
     *             throws IllegalAccessException if the map could not be read
     *             from the field.
     */
    private void loadEnum(FieldInfo fi, Object obj, Resource v) throws IllegalAccessException {
        String s = v.toString();

        for (Object o : fi.getField().getType().getEnumConstants() ) {
            if (o.toString().equals(s)) {
                fi.getField().set(obj, o);
                return;
            }
        }
    }

    /**
     * Return the java object mapped from the given resource. This will try and
     * load a bean if the id is a URI and clz is not Resource.
     * 
     * @param id
     *            the resource to be mapped.
     * @param clz
     *            the desired class for the object, can be null.
     * @param getDeleted
     *            should deleted beans be considered?
     * @param cache
     *            cached objects
     * @return the object either as a bean or just by converting the resource to
     *         a java object.
     * @throws BeanException
     */
    protected Object loadObject(Class<?> clz, Resource id, boolean getDeleted, boolean refresh, Map<Resource, Object> done) throws BeanException {
        if (!id.isUri() || ((clz != null) && (Resource.class.isAssignableFrom(clz)))) {
            return id.asObject();
        }
        return load(clz, id, getDeleted, refresh, done);
    }

    // ----------------------------------------------------------------------
    // SAVE SESSION
    // ----------------------------------------------------------------------
    private void writeTriples() throws BeanException {
        // delete beans
        for (Resource x : deletes ) {
            tw.add(x, DcTerms.IS_REPLACED_BY, Rdf.NIL);
        }

        // delete old triples
        Unifier uf = new Unifier();
        Map<String, FieldResource> names = new HashMap<String, FieldResource>();
        for (Resource id : sessionCache.keySet() ) {
            TripleObject to = sessionCache.get(id);
            BeanInfo bi = BeanInfo.get(to.object.getClass());

            // remove all known triples
            if (to.triples.isEmpty()) {
                for (FieldInfo fi : bi.getFields() ) {
                    uf = updateUnifier(id, fi, uf, names, tw);
                }

                // add to unifier
            } else {
                tw.removeAll(to.triples);
            }

            // write new triples
            to.triples.clear();
            to.triples.add(new Triple(id, Rdf.TYPE, bi.getType()));
            for (FieldInfo fi : bi.getFields() ) {
                try {
                    writeField(id, fi, to);
                } catch (IllegalAccessException e) {
                    log.warn(String.format("[%s] : Could not write property for name %s.", id, fi.getField()), e);
                }
            }
            tw.addAll(to.triples);
        }

        // run unifier to get missing values
        try {
            beanFactory.getContext().perform(uf);
        } catch (OperatorException e) {
            throw (new BeanException("Could not retrieve existing values from context.", e));
        }
        List<String> cols = uf.getColumnNames();
        for (Tuple<Resource> row : uf.getResult() ) {
            int idx = 0;
            for (String name : cols ) {
                Resource r = row.get(idx++);
                if (r != null) {
                    FieldResource fr = names.get(name);
                    if (fr.fi.isInverse()) {
                        tw.remove(r, fr.fi.getPredicate(), fr.subj);
                    } else {
                        tw.remove(fr.subj, fr.fi.getPredicate(), r);
                    }
                }
            }
        }

        // write all new values and remove old values
        try {
            beanFactory.getContext().perform(tw);
        } catch (OperatorException e) {
            throw (new BeanException("Could not write session to context.", e));
        }

        // add new values to secondary cache
        if (beanFactory.getCache() != null) {
            for (Resource key : sessionCache.keySet() ) {
                beanFactory.getCache().put(key, sessionCache.get(key));
            }
        }
    }

    private Unifier updateUnifier(Resource id, FieldInfo fi, Unifier uf, Map<String, FieldResource> names, TripleWriter tw) throws BeanException {
        if (uf.getPatterns().size() > MAXPATTERN) {
            try {
                beanFactory.getContext().perform(uf);
            } catch (OperatorException e) {
                throw (new BeanException("Could not retrieve existing values from context.", e));
            }
            List<String> cols = uf.getColumnNames();
            for (Tuple<Resource> row : uf.getResult() ) {
                int idx = 0;
                for (String name : cols ) {
                    Resource r = row.get(idx++);
                    if (r != null) {
                        FieldResource fr = names.get(name);
                        if (fr.fi.isInverse()) {
                            tw.remove(r, fr.fi.getPredicate(), fr.subj);
                        } else {
                            tw.remove(fr.subj, fr.fi.getPredicate(), r);
                        }
                    }
                }
            }
            names.clear();
            uf = new Unifier();
        }

        String name = Integer.toHexString(names.size());
        if (fi.isInverse()) {
            uf.addPattern(name, fi.getPredicate(), id, true);
        } else {
            uf.addPattern(id, fi.getPredicate(), name, true);
        }
        uf.addColumnName(name);
        names.put(name, new FieldResource(fi, id));
        return uf;
    }

    private void writeField(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        // don't write this field
        if (fi.isReadOnly()) {
            return;
        }

        // no data means no triples
        Object o = fi.getField().get(to.object);
        if (o == null) {
            return;
        }

        // check for UnitializedProxy and ignore anything that was not fetched yet
        if (o instanceof UninitializedProxy) {
            if (!((UninitializedProxy) o).isInitialized()) {
                return;
            }
        }

        // check for array
        if (fi.getField().getType().isArray()) {
            writeArray(id, fi, to);
            return;
        }

        // check for map
        if (Map.class.isAssignableFrom(fi.getField().getType())) {
            writeMap(id, fi, to);
            return;
        }

        // check for list
        if (List.class.isAssignableFrom(fi.getField().getType())) {
            writeList(id, fi, to);
            return;
        }

        // check for set
        if (Set.class.isAssignableFrom(fi.getField().getType())) {
            writeSet(id, fi, to);
            return;
        }

        // check for collection
        if (Collection.class.isAssignableFrom(fi.getField().getType())) {
            writeCollection(id, fi, to);
            return;
        }

        // check for enum
        if (fi.getField().getType().isEnum()) {
            writeEnum(id, fi, to);
            return;
        }

        // try writing it as a bean or literal
        writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.triples);
    }

    // TODO JavaDoc
    // TODO can map be inverse
    @SuppressWarnings("unchecked")
    private void writeMap(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalArgumentException, IllegalAccessException {
        Map<Object, Object> objects = (Map<Object, Object>) fi.getField().get(to.object);
        for (Entry<Object, Object> entry : objects.entrySet() ) {
            Resource mapid = beanFactory.getMinter().mint();

            to.triples.add(new Triple(id, fi.getPredicate(), mapid));
            to.triples.add(new Triple(mapid, Rdf.TYPE, STORAGE_TYPE_MAP_ENTRY));

            writeObject(mapid, STORAGE_TYPE_MAP_KEY, entry.getKey(), false, to.triples);
            writeObject(mapid, STORAGE_TYPE_MAP_VALUE, entry.getValue(), false, to.triples);

        }
    }

    // TODO JavaDoc
    private void writeArray(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        // create the list
        Resource lstitem = beanFactory.getMinter().mint();
        to.triples.add(new Triple(id, fi.getPredicate(), lstitem));
        to.triples.add(new Triple(lstitem, Rdf.TYPE, Rdf.SEQ));

        // add all items to the list
        int idx = 1;
        Object[] objects = (Object[]) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(lstitem, Resource.uriRef(RDF_IDX + idx), o, false, to.triples);
            idx++;
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeList(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        // create the list
        Resource lstitem = beanFactory.getMinter().mint();
        to.triples.add(new Triple(id, fi.getPredicate(), lstitem));
        to.triples.add(new Triple(lstitem, Rdf.TYPE, Rdf.SEQ));

        // add all items to the list
        int idx = 1;
        List<Object> objects = (List<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(lstitem, Resource.uriRef(RDF_IDX + idx), o, false, to.triples);
            idx++;
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeSet(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        Collection<Object> objects = (Collection<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.triples);
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeCollection(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        Collection<Object> objects = (Collection<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.triples);
        }
    }

    // TODO JavaDoc
    private void writeEnum(Resource id, FieldInfo fi, TripleObject to) throws BeanException, IllegalAccessException {
        String value = fi.getField().get(to.object).toString();
        writeObject(id, fi.getPredicate(), value, fi.isInverse(), to.triples);
    }

    /**
     * Either object is a bean in which case the ID is associated, or the object
     * is written as a literal. If the object is a bean it is assumed the bean
     * is written by this session or already exists.
     * 
     * @throws BeanException
     */
    private void writeObject(Resource id, Resource pred, Object o, boolean inverse, Set<Triple> triples) throws BeanException {
        if (o == null) {
            return;
        }
        if (BeanInfo.isBean(o.getClass())) {
            Resource oid = sessionCache.firstKey(o);
            if (oid == null) {
                oid = beanUtil.getId(o);
            }
            if (inverse) {
                triples.add(new Triple(oid, pred, id));
            } else {
                triples.add(new Triple(id, pred, oid));
            }
        } else {
            triples.add(new Triple(id, pred, Resource.fromObject(o)));
        }
    }

    // ----------------------------------------------------------------------
    // PROXY CLASSES
    // ----------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    private List<Object> readList(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Triple> triples, final TripleContext context) {
        if (checkNullCollection) {
            Unifier uf = new Unifier();
            uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
            uf.addPattern("x", Rdf.TYPE, Rdf.SEQ); //$NON-NLS-1$
            uf.addPattern("x", "p", "v"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setColumnNames("x", "p", "v"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setLimit(1);

            try {
                context.perform(uf);
            } catch (OperatorException e) {
                log.error("Could not fetch data for list.", e);
            }
            if (!uf.getResult().iterator().hasNext()) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private List<Object> list = null;

            private void fetchData() {
                if (list != null) {
                    return;
                }
                list = new ArrayList<Object>();

                Unifier uf = new Unifier();
                uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
                uf.addPattern("x", Rdf.TYPE, Rdf.SEQ); //$NON-NLS-1$
                uf.addPattern("x", "p", "v"); //$NON-NLS-1$ //$NON-NLS-2$
                uf.setColumnNames("x", "p", "v"); //$NON-NLS-1$ //$NON-NLS-2$

                try {
                    context.perform(uf);
                } catch (OperatorException e) {
                    log.error("Could not fetch data for list.", e);
                }

                for (Tuple<Resource> row : uf.getResult() ) {
                    triples.add(new Triple(id, fi.getPredicate(), row.get(0)));
                    triples.add(new Triple(row.get(0), Rdf.TYPE, Rdf.SEQ));
                    triples.add(new Triple(row.get(0), row.get(1), row.get(2)));

                    if ((row.get(2) != null) && row.get(1).toString().startsWith(RDF_IDX)) {
                        // make sure there is enough space in the array
                        int idx = Integer.parseInt(row.get(1).toString().substring(RDF_IDX.length())) - 1;
                        while (list.size() <= idx) {
                            list.add(null);
                        }

                        // get the object, either mapped from a resource or the bean
                        try {
                            Object o = loadObject(fi.getValue(), row.get(2), getDeleted, refresh, done);
                            if (o != null) {
                                // set the index in the array to the new value
                                if (list.get(idx) != null) {
                                    log.info(String.format("[%s] : Replacing %d value in array for field %s.", id, idx, fi.getField()));
                                }
                                list.set(idx, o);
                            } else {
                                log.warn("loadObject returned null " + row.get(2));
                            }
                        } catch (BeanException e) {
                            log.warn("Could not fetch data for item " + row.get(2), e);
                        }
                    }
                }
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (list != null);
                }
                fetchData();
                return method.invoke(list, args);
            }
        };

        return (List<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { List.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Collection<Object> readCollection(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Triple> triples, final TripleContext context) {
        if (checkNullCollection) {
            Unifier uf = new Unifier();
            if (fi.isInverse()) {
                uf.addPattern("x", fi.getPredicate(), id); //$NON-NLS-1$
            } else {
                uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
            }
            uf.setColumnNames("x"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setLimit(1);

            try {
                context.perform(uf);
            } catch (OperatorException e) {
                log.error("Could not fetch data for collection.", e);
            }
            if (!uf.getResult().iterator().hasNext()) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private List<Object> list = null;

            private void fetchData() {
                if (list != null) {
                    return;
                }
                list = new ArrayList<Object>();

                Unifier uf = new Unifier();
                if (fi.isInverse()) {
                    uf.addPattern("x", fi.getPredicate(), id); //$NON-NLS-1$
                } else {
                    uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
                }
                uf.setColumnNames("x"); //$NON-NLS-1$ //$NON-NLS-2$

                try {
                    context.perform(uf);
                } catch (OperatorException e) {
                    log.error("Could not fetch data for collection.", e);
                }

                for (Tuple<Resource> row : uf.getResult() ) {
                    if (fi.isInverse()) {
                        triples.add(new Triple(row.get(0), fi.getPredicate(), id));
                    } else {
                        triples.add(new Triple(id, fi.getPredicate(), row.get(0)));
                    }

                    // get the object, either mapped from a resource or the bean
                    try {
                        Object o = loadObject(fi.getValue(), row.get(0), getDeleted, refresh, done);
                        if (o != null) {
                            list.add(o);
                        } else {
                            log.warn("loadObject returned null " + row.get(0));
                        }
                    } catch (BeanException e) {
                        log.warn("Could not fetch data for item " + row.get(0), e);
                    }
                }
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (list != null);
                }
                fetchData();
                return method.invoke(list, args);
            }
        };

        return (Collection<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Collection.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Set<Object> readSet(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Triple> triples, final TripleContext context) {
        if (checkNullCollection) {
            Unifier uf = new Unifier();
            if (fi.isInverse()) {
                uf.addPattern("x", fi.getPredicate(), id); //$NON-NLS-1$
            } else {
                uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
            }
            uf.setColumnNames("x"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setLimit(1);

            try {
                context.perform(uf);
            } catch (OperatorException e) {
                log.error("Could not fetch data for set.", e);
            }
            if (!uf.getResult().iterator().hasNext()) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private Set<Object> set = null;

            private void fetchData() {
                if (set != null) {
                    return;
                }
                set = new HashSet<Object>();

                Unifier uf = new Unifier();
                if (fi.isInverse()) {
                    uf.addPattern("x", fi.getPredicate(), id); //$NON-NLS-1$
                } else {
                    uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
                }
                uf.setColumnNames("x"); //$NON-NLS-1$ //$NON-NLS-2$

                try {
                    context.perform(uf);
                } catch (OperatorException e) {
                    log.error("Could not fetch data for set.", e);
                }

                for (Tuple<Resource> row : uf.getResult() ) {
                    if (fi.isInverse()) {
                        triples.add(new Triple(row.get(0), fi.getPredicate(), id));
                    } else {
                        triples.add(new Triple(id, fi.getPredicate(), row.get(0)));
                    }

                    // get the object, either mapped from a resource or the bean
                    try {
                        Object o = loadObject(fi.getValue(), row.get(0), getDeleted, refresh, done);
                        if (o != null) {
                            set.add(o);
                        } else {
                            log.warn("loadObject returned null " + row.get(0));
                        }
                    } catch (BeanException e) {
                        log.warn("Could not fetch data for item " + row.get(0), e);
                    }
                }
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (set != null);
                }
                fetchData();
                return method.invoke(set, args);
            }
        };

        return (Set<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Set.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Map<Object, Object> readMap(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Triple> triples, final TripleContext context) {
        if (checkNullCollection) {
            Unifier uf = new Unifier();
            uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
            uf.addPattern("x", Rdf.TYPE, STORAGE_TYPE_MAP_ENTRY); //$NON-NLS-1$
            uf.addPattern("x", STORAGE_TYPE_MAP_KEY, "key"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.addPattern("x", STORAGE_TYPE_MAP_VALUE, "value"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setColumnNames("x", "key", "value"); //$NON-NLS-1$ //$NON-NLS-2$
            uf.setLimit(1);

            try {
                context.perform(uf);
            } catch (OperatorException e) {
                log.error("Could not fetch data for map.", e);
            }
            if (!uf.getResult().iterator().hasNext()) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private Map<Object, Object> map = null;

            private void fetchData() {
                if (map != null) {
                    return;
                }
                map = new HashMap<Object, Object>();

                Unifier uf = new Unifier();
                uf.addPattern(id, fi.getPredicate(), "x"); //$NON-NLS-1$
                uf.addPattern("x", Rdf.TYPE, STORAGE_TYPE_MAP_ENTRY); //$NON-NLS-1$
                uf.addPattern("x", STORAGE_TYPE_MAP_KEY, "key"); //$NON-NLS-1$ //$NON-NLS-2$
                uf.addPattern("x", STORAGE_TYPE_MAP_VALUE, "value"); //$NON-NLS-1$ //$NON-NLS-2$
                uf.setColumnNames("x", "key", "value"); //$NON-NLS-1$ //$NON-NLS-2$

                try {
                    context.perform(uf);
                } catch (OperatorException e) {
                    log.error("Could not fetch data for map.", e);
                }

                for (Tuple<Resource> row : uf.getResult() ) {
                    triples.add(new Triple(id, fi.getPredicate(), row.get(0)));
                    triples.add(new Triple(row.get(0), Rdf.TYPE, STORAGE_TYPE_MAP_ENTRY));
                    triples.add(new Triple(row.get(0), STORAGE_TYPE_MAP_KEY, row.get(1)));
                    triples.add(new Triple(row.get(0), STORAGE_TYPE_MAP_VALUE, row.get(2)));

                    try {
                        Object key = loadObject(fi.getKey(), row.get(1), getDeleted, refresh, done);
                        Object value = loadObject(fi.getValue(), row.get(2), getDeleted, refresh, done);
                        if (key == null) {
                            log.warn("loadObject returned null " + row.get(1));
                        } else if (value == null) {
                            log.warn("loadObject returned null " + row.get(2));
                        } else {
                            map.put(key, value);
                        }
                    } catch (BeanException e) {
                        log.warn("Could not fetch data for item " + row.get(1) + " or " + row.get(2), e);
                    }

                }
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (map != null);
                }
                fetchData();
                return method.invoke(map, args);
            }
        };

        return (Map<Object, Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Map.class, UninitializedProxy.class }, handler);
    }

    // ----------------------------------------------------------------------
    // HELPER CLASSES
    // ----------------------------------------------------------------------
    class FieldResource {
        public FieldInfo fi;
        public Resource  subj;

        public FieldResource(FieldInfo fi, Resource subj) {
            this.fi = fi;
            this.subj = subj;
        }
    }

    public interface UninitializedProxy {
        /**
         * Simple check to see if a proxy has been initialized (i.e. fetched the
         * data from the backend). When saving the system can ignore this field
         * when trying to save the bean.
         * 
         * @return true if the proxy has been initialized, false otherwise.
         */
        boolean isInitialized();
    }
}
