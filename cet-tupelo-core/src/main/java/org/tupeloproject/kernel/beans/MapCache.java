package org.tupeloproject.kernel.beans;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.tupeloproject.kernel.beans.Cache.TripleObject;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public class MapCache extends HashMap<Resource, TripleObject> implements Cache {
    private static final long serialVersionUID = 1L;

    @Override
    public TripleObject put(Resource key, Object object) {
        return put(key, new TripleObject(object));
    }

    @Override
    public TripleObject put(Resource key, Object object, Set<Triple> triples) {
        return put(key, new TripleObject(object, triples));
    }

    @Override
    public Resource firstKey(Object object) {
        for (Entry<Resource, TripleObject> entry : entrySet() ) {
            if (entry.getValue().object == object) {
                return entry.getKey();
            }
        }
        return null;
    }
}