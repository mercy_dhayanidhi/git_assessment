package org.tupeloproject.kernel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.context.BlobContext;
import org.tupeloproject.kernel.context.TripleBlobContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public class Contexts {
    private static Log           log               = LogFactory.getLog(Contexts.class);
    private static final Pattern nonCommentPattern = Pattern.compile("^([^#]+)");

    private Contexts() {
    }

    //
    // CONTEXT COPIER
    //

    public static void copyContext(TripleContext src, TripleContext dest) throws OperatorException {
        TripleIterator ti = new TripleIterator();
        src.perform(ti);

        TripleWriter tw = null;
        int count = 0;
        for (Triple t : ti ) {
            if (tw == null) {
                tw = new TripleWriter();
            }
            tw.add(t);
            if (tw.getToAdd().size() > 1000) {
                count += tw.getToAdd().size();
                dest.perform(tw);
                tw = null;
            }
        }
        if (tw != null) {
            count += tw.getToAdd().size();
            dest.perform(tw);
            tw = null;
        }
        log.info(String.format("Copied %d triples between contexts.", count));
    }

    public static void copyContext(BlobContext src, BlobContext dest) throws OperatorException {
        BlobIterator bi = new BlobIterator();
        src.perform(bi);

        int count = 0;
        long bytes = 0;
        for (Resource x : bi ) {
            BlobFetcher bf = new BlobFetcher();
            bf.setSubject(x);
            src.perform(bf);

            BlobWriter bw = new BlobWriter();
            bw.setSubject(x);
            bw.setInputStream(bf.getInputStream());
            dest.perform(bw);

            count++;
            bytes += bw.getSize();
        }
        log.info(String.format("Copied %d blobs (%d bytes) between contexts.", count, bytes));
    }

    public static void copyContext(TripleBlobContext src, TripleBlobContext dest) throws OperatorException {
        copyContext((TripleContext) src, (TripleContext) dest);
        copyContext((BlobContext) src, (BlobContext) dest);
    }

    //
    // CONTEXT CREATOR
    //

    public static void createContext(Properties props) throws OperatorException {
        BlobContext bc = null;
        TripleContext tc = null;

        // get classloader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if (cl == null) {
            cl = Contexts.class.getClassLoader();
        }

        if (props.containsKey("blobcontext.type")) {
            try {
                bc = createBlobContext(cl, props.getProperty("blobcontext.type"));
            } catch (Exception e) {
                throw (new OperatorException("Could not create blobcontext.", e));
            }
        }
        if (props.containsKey("triplecontext.type")) {
            try {
                bc = createBlobContext(cl, props.getProperty("triplecontext.type"));
            } catch (Exception e) {
                throw (new OperatorException("Could not create blobcontext.", e));
            }
        }
    }

    private static TripleContext createTripleContext(ClassLoader cl, String name) throws Exception {
        Enumeration<URL> resources = cl.getResources("META-INF/services/" + TripleContext.class.getName());
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            InputStream is = url.openStream();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    Matcher m = nonCommentPattern.matcher(line);
                    if (m.find()) {
                        String classname = m.group().trim();
                        if (classname.equals(name) || classname.endsWith(name) || classname.endsWith(name + "Context")) {
                            return (TripleContext) cl.loadClass(classname).newInstance();
                        }
                    }
                }
            } finally {
                is.close();
            }
        }
        throw (new IOException("No TripleContext found with name " + name));
    }

    private static BlobContext createBlobContext(ClassLoader cl, String name) throws Exception {
        Enumeration<URL> resources = cl.getResources("META-INF/services/" + BlobContext.class.getName());
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            InputStream is = url.openStream();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    Matcher m = nonCommentPattern.matcher(line);
                    if (m.find()) {
                        String classname = m.group().trim();
                        if (classname.equals(name) || classname.endsWith(name) || classname.endsWith(name + "Context")) {
                            return (BlobContext) cl.loadClass(classname).newInstance();
                        }
                    }
                }
            } finally {
                is.close();
            }
        }
        throw (new IOException("No BlobContext found with name " + name));
    }
}
