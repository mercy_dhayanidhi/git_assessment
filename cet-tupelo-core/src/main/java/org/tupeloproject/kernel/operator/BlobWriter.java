/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import org.tupeloproject.rdf.Resource;

/**
 * Writes data to a blob.
 * 
 * @author Joe Futrelle
 */
public class BlobWriter implements Operator, Closeable {
    private static final long serialVersionUID = 1L;

    private Resource          subject;
    private InputStream       inputStream;
    private long              size;

    /**
     * Create a blob operator without a URI set
     */
    public BlobWriter() {
    }

    /**
     * Create a blob operator
     * 
     * @param subject
     *            a UriRef identifying the blob
     */
    public BlobWriter(Resource subject) {
        this.subject = subject;
    }

    public Resource getSubject() {
        return subject;
    }

    public void setSubject(Resource subject) {
        this.subject = subject;
    }

    /**
     * Get the the input stream to read blob data from
     * 
     * @return the the input stream to read blob data from
     */
    public InputStream getInputStream() {
        return inputStream;
    }

    /**
     * Set the the input stream to read blob data from.
     * 
     * @param value
     *            the new the input stream to read blob data from
     */
    public void setInputStream(InputStream value) {
        inputStream = value;
    }

    /**
     * Get how many bytes were read during perform.
     * 
     * @return how many bytes were read during perform
     */
    public long getSize() {
        return size;
    }

    /**
     * Set how many bytes were read during perform.
     * 
     * @param value
     *            the new how many bytes were read during perform
     */
    public void setSize(long value) {
        size = value;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
