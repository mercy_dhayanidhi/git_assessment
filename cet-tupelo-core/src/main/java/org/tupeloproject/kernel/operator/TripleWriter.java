/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.tupeloproject.rdf.Triple;

/**
 * Use this operator to add and remove {@link Triple Triples} to and from
 * a {@link Context}.
 * 
 * @author Rob Kooper
 */
public class TripleWriter implements Operator {
    private static final long serialVersionUID = 1L;

    private Set<Triple>       toAdd            = new HashSet<Triple>();
    private Set<Triple>       toRemove         = new HashSet<Triple>();

    /**
     * Add a triple.
     * 
     * @param triple
     *            the triple
     * @return this for chaning
     */
    public TripleWriter add(Triple triple) {
        toAdd.add(triple);
        toRemove.remove(triple);
        return this;
    }

    /**
     * Remove a triple.
     * 
     * @param triple
     *            the triple to remove
     * @return this for chaning
     */
    public TripleWriter remove(Triple triple) {
        toAdd.remove(triple);
        toRemove.add(triple);
        return this;
    }

    /**
     * Add a triple.
     * 
     * @param s
     *            the subject
     * @param p
     *            the predicate
     * @param o
     *            the object
     * @return this for chaining
     */
    public TripleWriter add(Object s, Object p, Object o) {
        return add(new Triple(s, p, o));
    }

    /**
     * Remove a triple.
     * 
     * @param s
     *            the subject
     * @param p
     *            the predicate
     * @param o
     *            the object
     * @return this for chaining
     */
    public TripleWriter remove(Object s, Object p, Object o) {
        return remove(new Triple(s, p, o));
    }

    /**
     * Add a set of triples.
     * The default implementation calls {@link #add(Triple)} repeatedly.
     * 
     * @param triples
     *            the triples
     * @return this (for chaining)
     */
    public TripleWriter addAll(Collection<Triple> triples) {
        for (Triple triple : triples ) {
            add(triple);
        }
        return this;
    }

    /**
     * Remove a set of triples.
     * The default implementation calls {@link #remove(Triple)} repeatedly.
     * 
     * @param triples
     *            the triples
     * @return this (for chaining)
     */
    public TripleWriter removeAll(Collection<Triple> triples) {
        for (Triple triple : triples ) {
            remove(triple);
        }
        return this;
    }

    /**
     * Get the the triples to add.
     * 
     * @return the the triples to add
     */
    public Set<Triple> getToAdd() {
        return toAdd;
    }

    /**
     * Set the the triples to add.
     * 
     * @param value
     *            the new the triples to add
     */
    public void setToAdd(Set<Triple> value) {
        toAdd = value;
    }

    /**
     * Get the the triples to remove.
     * 
     * @return the the triples to remove
     */
    public Set<Triple> getToRemove() {
        return toRemove;
    }

    /**
     * Set the the triples to remove.
     * 
     * @param value
     *            the new the triples to remove
     */
    public void setToRemove(Set<Triple> value) {
        toRemove = value;
    }
}
