/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.util.Iterators;

/**
 * This operation fetches subsets of triples matching the given
 * pattern.
 * 
 * @author Rob Kooper
 */
public class TripleMatcher implements Iterator<Triple>, Iterable<Triple>, Closeable, Operator {
    private static final long serialVersionUID = 1L;

    private Resource          subject;
    private Resource          predicate;
    private Resource          object;
    private Iterator<Triple>  iterator;
    private Set<Triple>       result;

    public TripleMatcher() {
    }

    public Resource getSubject() {
        return subject;
    }

    public void setSubject(Resource subject) {
        this.subject = subject;
    }

    public Resource getPredicate() {
        return predicate;
    }

    public void setPredicate(Resource predicate) {
        this.predicate = predicate;
    }

    public Resource getObject() {
        return object;
    }

    public void setObject(Resource object) {
        this.object = object;
    }

    public void setIterator(Iterator<Triple> iterator) {
        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.iterator = iterator;
        this.result = null;
    }

    public void setIterator(Iterable<Triple> iterable) {
        setIterator(iterable.iterator());
    }

    public Iterator<Triple> getIterator() {
        if (result != null) {
            return result.iterator();
        } else {
            return iterator;
        }
    }

    @Override
    public Iterator<Triple> iterator() {
        return getIterator();
    }

    @Override
    public boolean hasNext() {
        if (iterator == null) {
            return false;
        }
        return iterator.hasNext();
    }

    @Override
    public Triple next() {
        if (iterator == null) {
            return null;
        }
        return iterator.next();
    }

    @Override
    public void remove() {
        if (iterator != null) {
            iterator.remove();
        }
    }

    @Override
    public void close() throws IOException {
        if ((iterator != null) && (iterator instanceof Closeable)) {
            ((Closeable) iterator).close();
        }
    }

    // ---
    // CONVIENENCE FUNCTIONS
    // ---

    public void match(Resource subject, Resource predicate, Resource object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public Set<Resource> subjects() {
        Set<Resource> set = new HashSet<Resource>();
        for (Triple t : getResult() ) {
            set.add(t.getSubject());
        }
        return set;
    }

    public Set<Resource> objects() {
        Set<Resource> set = new HashSet<Resource>();
        for (Triple t : getResult() ) {
            set.add(t.getObject());
        }
        return set;
    }

    public Set<Triple> getResult() {
        if (result == null) {
            if (iterator != null) {
                result = Iterators.asSet(iterator);
                try {
                    close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                iterator = null;
            } else {
                result = new HashSet<Triple>();
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("TM : ");
        if (subject == null) {
            sb.append("?s ");
        } else {
            sb.append(subject.toNTriples()).append(" ");
        }
        if (predicate == null) {
            sb.append("?p ");
        } else {
            sb.append(predicate.toNTriples()).append(" ");
        }
        if (object == null) {
            sb.append("?o");
        } else {
            sb.append(object.toNTriples());
        }
        return sb.toString();
    }
}
