/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.util.List;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;
import org.tupeloproject.rdf.query.OrderableQuery;
import org.tupeloproject.rdf.query.Pattern;
import org.tupeloproject.util.Table;
import org.tupeloproject.util.Tables;

/**
 * Unifies a List of {@link Pattern}s with triple space to produce a
 * result {@link Table}. "Unification" means finding sets of triples
 * that match the patterns and for which same-named variable bindings
 * have identical values. A typical query of this sort represented in
 * SPARQL is the following:
 * <p>
 * 
 * <pre>
 * SELECT ?child ?grandparent
 * WHERE {
 *  { ?child &lt;ns:hasParent&gt; ?parent . }
 *  { ?parent &lt;ns:hasParent&gt; ?grandparent . }
 * }
 * </pre>
 * <p>
 * Which would be expressed against this API as follows (assuming the "ns"
 * method generates {@link UriRef}s with some prefix):
 * <p>
 * 
 * <pre>
 * Unifier u = context.getUnifier();
 * u.setColumnNames(&quot;child&quot;, &quot;grandparent&quot;);
 * u.addPattern(&quot;child&quot;, ns(&quot;hasParent&quot;), &quot;parent&quot;);
 * u.addPattern(&quot;parent&quot;, ns(&quot;hasParent&quot;), &quot;grandparent&quot;);
 * context.perform(u);
 * Table&lt;Resource&gt; result = u.getResult();
 * </pre>
 * 
 * @author Joe Futrelle
 */
public class Unifier extends OrderableQuery implements Operator {
    private static final long serialVersionUID = 1L;

    private Table<Resource>   result           = null;

    public Unifier() {
    }

    /**
     * Set the column names
     * 
     * @param columnNames
     *            the column names
     */
    public Unifier(String... columnNames) {
        setColumnNames(columnNames);
    }

    /**
     * Set the column names
     * 
     * @param columnNames
     *            the column names
     */
    public Unifier(List<String> columnNames) {
        setColumnNames(columnNames);
    }

    /**
     * A convenience method to retrieve one of the columns of the result table.
     * Note that this will discard information in the other columns.
     * 
     * @param ix
     *            the index of the column to get
     * @return the values in that column
     */
    public List<Resource> getColumn(int ix) {
        return Tables.getColumn(getResult(), ix);
    }

    /**
     * A convenience method to retrieve the first column of the result table.
     * Note that this will discard information in the other columns.
     * 
     * @return the values in that column
     */
    public List<Resource> getFirstColumn() {
        return getColumn(0);
    }

    /**
     * A convenience method to retrieve one of the columns of the result table.
     * Note that this will discard information in the other columns.
     * 
     * @param name
     *            the name of the column to retrieve
     * @return the values in that column
     */

    public List<Resource> getColumn(String name) {
        return Tables.getColumn(getResult(), name);
    }

    /**
     * Get the the result of the unification.
     * 
     * @return the the result of the unification
     */
    public Table<Resource> getResult() {
        return result;
    }

    /**
     * Set the the result of the unification.
     * 
     * @param value
     *            the new the result of the unification
     */
    public void setResult(Table<Resource> value) {
        result = value;
    }
}
