/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.util.LinkedList;
import java.util.Set;

import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.Pattern;

/**
 * Unifies a {@link Context} with a List of {@link Pattern}s using a
 * {@link Unifier}, then projects the resulting
 * {@link org.tupeloproject.util.Table} through
 * second List of Patterns via a
 * {@link org.tupeloproject.rdf.query.TableProjector}, to produce a
 * set of {@link Triple}s.
 * <p>
 * This can be used to transform RDF graphs in many different ways, which is
 * useful for translating between ontologies.
 * 
 * @author Joe Futrelle
 */
public class Transformer implements Operator {
    private static final long   serialVersionUID = 1L;

    private LinkedList<Pattern> inPatterns       = new LinkedList<Pattern>();
    private LinkedList<Pattern> outPatterns      = new LinkedList<Pattern>();
    private Set<Triple>         result;

    /**
     * Add a pattern to the unifier.
     * 
     * @param inPattern
     *            the new unification pattern
     * @see Pattern
     * @see Unifier
     */
    public void addInPattern(Pattern inPattern) {
        getInPatterns().add(inPattern);
    }

    /**
     * Add a pattern to the unifier.
     * 
     * @param s
     *            a variable name or Resource for the subject
     * @param p
     *            a variable name or Resource for the predicate
     * @param o
     *            a variable name or Resource for the object
     * @param optional
     *            is the pattern optional?
     * @see Pattern
     * @see Unifier
     */
    public void addInPattern(Object s, Object p, Object o, boolean optional) {
        Pattern pattern = new Pattern(s, p, o);
        pattern.setOptional(optional);
        getInPatterns().add(pattern);
    }

    /**
     * Add a pattern to the unifier.
     * 
     * @param s
     *            a variable name or Resource for the subject
     * @param p
     *            a variable name or Resource for the predicate
     * @param o
     *            a variable name or Resource for the object
     * @see Pattern
     * @see Unifier
     */
    public void addInPattern(Object s, Object p, Object o) {
        addInPattern(s, p, o, false);
    }

    /**
     * Add a pattern to the projector.
     * 
     * @param outPattern
     *            the new projection pattern.
     * @see org.tupeloproject.rdf.query.PatternProjector
     */
    public void addOutPattern(Pattern outPattern) {
        getOutPatterns().add(outPattern);
    }

    /**
     * Add a pattern to the projector.
     * 
     * @param s
     *            a variable name or Resource for the subject
     * @param p
     *            a variable name or Resource for the predicate
     * @param o
     *            a variable name or Resource for the object
     * @see org.tupeloproject.rdf.query.PatternProjector
     */
    public void addOutPattern(Object s, Object p, Object o) {
        getOutPatterns().add(new Pattern(s, p, o));
    }

    /**
     * Get the inPatterns.
     * 
     * @return the inPatterns
     */
    public LinkedList<Pattern> getInPatterns() {
        return inPatterns;
    }

    /**
     * Set the inPatterns.
     * 
     * @param value
     *            the new inPatterns
     */
    public void setInPatterns(LinkedList<Pattern> value) {
        inPatterns = value;
    }

    /**
     * Get the outPatterns.
     * 
     * @return the outPatterns
     */
    public LinkedList<Pattern> getOutPatterns() {
        return outPatterns;
    }

    /**
     * Set the outPatterns.
     * 
     * @param value
     *            the new outPatterns
     */
    public void setOutPatterns(LinkedList<Pattern> value) {
        outPatterns = value;
    }

    /**
     * Get the result.
     * 
     * @return the result
     */
    public Set<Triple> getResult() {
        return result;
    }

    /**
     * Set the result.
     * 
     * @param value
     *            the new result
     */
    public void setResult(Set<Triple> value) {
        result = value;
    }
}
