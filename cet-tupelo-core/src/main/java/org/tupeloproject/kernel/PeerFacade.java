/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.beans.BeanFactory;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.context.Context;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.rdf.terms.Tupelo;
import org.tupeloproject.rdf.xml.RdfXml;

/**
 * PeerFacade fronts a Context and provides methods for storing and
 * retrieving serialized Context instances (their configuration, not their
 * contents)
 * in the delegate Context. These are called "peers" because the mechanisms
 * in this facade provide a way to build networks of Contexts.
 * <P>
 * Note: An instance of PeerFacade will always return the same instance of a
 * peer once loaded. To get different instance, create different instances of
 * PeerFacade.
 */
public class PeerFacade {
    private static Resource CONTEXT_TYPE         = Tupelo.uriRef("contexts/Context");
    private static Resource DEFAULT_CONTEXT_TYPE = Tupelo.uriRef("contexts/DefaultContext");

    private static Log      log                  = LogFactory.getLog(PeerFacade.class);

    private BeanFactory     bf;

    public TripleContext getContext() {
        return bf.getContext();
    }

    public void setContext(TripleContext context) {
        this.bf = new BeanFactory(context);
    }

    /**
     * Save the configuration of the given peer into the delegate Context
     * 
     * @param id
     *            the id to assign to the peer
     * @param peer
     *            the peer
     * @param other
     *            any other triples to save along with the peer itself
     * @throws Exception
     *             on failure
     */
    public void savePeer(Context peer, Resource id, Triple... other) throws OperatorException {
        Session session = bf.openSession();
        session.save(peer, id);
        session.commit();

        Set<Triple> yetMore = new HashSet<Triple>(Arrays.asList(other));
        yetMore.add(new Triple(id, Rdf.TYPE, CONTEXT_TYPE));
        getContext().addTriples(yetMore.toArray(new Triple[yetMore.size()]));
    }

    /**
     * Save the configuration of the given peer into the delegate Context as
     * the default peer.
     * 
     * @param peer
     *            the peer
     * @param other
     *            any other triples to save along with the peer
     * @throws Exception
     *             on failure
     */
    public void saveDefaultPeer(Context peer, Triple... other) throws OperatorException {
        // if there's already a default peer, delete it
        TripleWriter tw = new TripleWriter();
        tw.removeAll(getContext().match(null, Rdf.TYPE, DEFAULT_CONTEXT_TYPE));
        getContext().perform(tw);

        // now generate an id for the default peer
        Resource id = Resource.uriRef();
        Set<Triple> yetMore = new HashSet<Triple>(Arrays.asList(other));
        // add the assertion that this is the default peer
        yetMore.add(new Triple(id, Rdf.TYPE, DEFAULT_CONTEXT_TYPE));
        // go.
        savePeer(peer, id, (Triple[]) yetMore.toArray(new Triple[] {}));
    }

    /**
     * Get a reference to the default peer.
     * 
     * @return the default peer
     * @throws NotFoundException
     *             if there is no default peer
     * @throws OperatorException
     *             on other failure
     */
    public Resource getDefaultPeer() throws OperatorException {
        log.debug("matching on " + DEFAULT_CONTEXT_TYPE);
        for (Triple t : getContext().match(null, Rdf.TYPE, DEFAULT_CONTEXT_TYPE) ) {
            return t.getSubject();
        }
        log.debug("no default peer");
        throw new NotFoundException("no default peer");
    }

    /**
     * Load the default peer
     * 
     * @return the default peer
     * @throws NotFoundException
     *             if there is no default peer
     * @throws OperatorException
     *             on other failure
     */
    public Context loadDefaultPeer() throws OperatorException {
        return loadPeer(getDefaultPeer());
    }

    /**
     * List all the peers in the delegate Context
     * 
     * @return the ids of all the peers in the delegate Context
     * @throws OperatorException
     *             if the query fails
     */
    public Set<Resource> listPeers() throws OperatorException {
        TripleMatcher tm = new TripleMatcher();
        tm.match(null, Rdf.TYPE, CONTEXT_TYPE);
        getContext().perform(tm);
        return tm.subjects();
    }

    /**
     * Load a peer. Blindly trusts the client that the id
     * identifies one.
     * 
     * @param id
     *            the id of the peer.
     * @return the peer
     * @throws Exception
     *             if querying or instantiation fails
     */
    public Context loadPeer(Resource id) throws OperatorException {
        return (Context) bf.openSession().load(null, id);
    }

    public static Set<Triple> toTriples(Context peer) throws OperatorException {
        MemoryContext mc = new MemoryContext();
        PeerFacade pf = new PeerFacade();
        pf.setContext(mc);
        pf.saveDefaultPeer(peer);
        return mc.getTriples();
    }

    public static Context fromTriples(Set<Triple> triples) throws OperatorException {
        MemoryContext mc = new MemoryContext();
        mc.addTriples(triples);
        PeerFacade pf = new PeerFacade();
        pf.setContext(mc);
        return pf.loadDefaultPeer();
    }

    public static void toXml(Context peer, OutputStream os) throws OperatorException, IOException {
        RdfXml.write(toTriples(peer), os);
    }

    public static Context fromXml(InputStream is) throws OperatorException, IOException {
        return fromTriples(RdfXml.parse(is));
    }
}