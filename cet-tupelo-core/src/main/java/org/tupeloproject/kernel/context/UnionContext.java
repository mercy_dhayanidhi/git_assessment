/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.MultipleOperatorException;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.util.Iterators;

/**
 * This context assumes that triple/blob space is the union of what is
 * reported by all its children, considered together. Read operations
 * are attempted against all children and results, if any, are
 * merged. If an operation fails against some, but not all, of the
 * children, the failure is ignored. Each write operations is
 * attempted against each child in turn until a child operation
 * succeeds.
 * 
 * @author Rob Kooper
 */
public class UnionContext extends AbstractTripleBlobContext {
    private static Log    log      = LogFactory.getLog(UnionContext.class);
    private List<Context> contexts = new ArrayList<Context>();

    /**
     * @deprecated use addContext
     */
    public void addChild(Context context) throws OperatorException {
        addContext(context);
    }

    public void addContext(Context context) throws OperatorException {
        if (contexts.contains(context)) {
            throw (new OperatorException("Already contains this context."));
        }
        contexts.add(context);
    }

    public void removeContext(Context context) {
        contexts.remove(context);
    }

    public List<Context> getContexts() {
        return contexts;
    }

    @Override
    public void open() throws OperatorException {
        MultipleOperatorException moe = new MultipleOperatorException("Open failed for some contexts.");
        for (Context context : contexts ) {
            try {
                context.open();
            } catch (OperatorException e) {
                moe.addException(e);
            }
        }
        if (moe.getExceptions().size() != 0) {
            throw (moe);
        }
    }

    /**
     * Attempt to close all child contexts.
     * 
     * @throws OperatorException
     */
    @Override
    public void close() throws OperatorException {
        MultipleOperatorException moe = new MultipleOperatorException("Open failed for some contexts.");
        for (Context context : contexts ) {
            try {
                context.open();
            } catch (OperatorException e) {
                moe.addException(e);
            }
        }
        if (moe.getExceptions().size() != 0) {
            throw (moe);
        }
    }

    /**
     * Attempt to close all child contexts.
     * 
     * @throws OperatorException
     */
    @Override
    public void sync() throws OperatorException {
        MultipleOperatorException moe = new MultipleOperatorException("Sync failed for some contexts.");
        for (Context context : contexts ) {
            try {
                context.sync();
            } catch (OperatorException e) {
                moe.addException(e);
            }
        }
        if (moe.getExceptions().size() != 0) {
            throw (moe);
        }
    }

    @Override
    public void initialize() throws OperatorException {
        throw (new OperatorException("Can not initialize union context."));
    }

    @Override
    public void destroy() throws OperatorException {
        throw (new OperatorException("Can not destroy union context."));
    }

    @Override
    public void perform(TripleWriter op) throws OperatorException {
        MultipleOperatorException moe = new MultipleOperatorException("TripleWriter failed to add.");
        boolean success = false;
        for (Context context : contexts ) {
            if (context instanceof TripleContext) {
                TripleWriter tw = new TripleWriter();
                tw.addAll(op.getToAdd());
                try {
                    ((TripleContext) context).perform(tw);
                    success = true;
                    break;
                } catch (OperatorException e) {
                    log.debug("Context [" + context + "] failed TripleWriter.add.", e);
                    moe.addException(e);
                }
            }
        }
        if (!success) {
            throw (moe);
        }

        moe = new MultipleOperatorException("TripleWriter failed to add.");
        success = false;
        for (Context context : contexts ) {
            if (context instanceof TripleContext) {
                TripleWriter tw = new TripleWriter();
                tw.removeAll(op.getToRemove());
                try {
                    ((TripleContext) context).perform(tw);
                    success = true;
                } catch (OperatorException e) {
                    log.debug("Context [" + context + "] failed TripleWriter.remove.", e);
                    moe.addException(e);
                }
            }
        }
        if (!success) {
            throw (moe);
        }
    }

    @Override
    public void perform(TripleIterator op) throws OperatorException {
        HashSet<Triple> result = new HashSet<Triple>();
        MultipleOperatorException moe = new MultipleOperatorException("TripleMatcher failed.");
        boolean success = false;
        for (Context context : contexts ) {
            if (context instanceof TripleContext) {
                try {
                    ((TripleContext) context).perform(op);
                    Iterators.copy(op, result);
                    success = true;
                } catch (OperatorException e) {
                    log.debug("Context [" + context + "] failed TripleIterator.", e);
                    moe.addException(e);
                }
            }
        }
        if (!success) {
            throw (moe);
        }
        op.setIterator(result);
    }

    @Override
    public void perform(TripleMatcher op) throws OperatorException {
        HashSet<Triple> result = new HashSet<Triple>();

        MultipleOperatorException moe = new MultipleOperatorException("TripleMatcher failed.");
        boolean success = false;
        for (Context context : contexts ) {
            if (context instanceof TripleContext) {
                try {
                    ((TripleContext) context).perform(op);
                    Iterators.copy(op, result);
                    success = true;
                } catch (OperatorException e) {
                    log.debug("Context [" + context + "] failed TripleMatcher.", e);
                    moe.addException(e);
                }
            }
        }
        if (!success) {
            throw (moe);
        }
        op.setIterator(result);
    }

    @Override
    public void perform(BlobIterator op) throws OperatorException {
        throw (new OperatorException("No blobs."));
    }

    @Override
    public void perform(BlobWriter op) throws OperatorException {
        throw (new OperatorException("No blobs."));
    }

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        throw (new OperatorException("No blobs."));
    }

    @Override
    public void perform(BlobRemover op) throws OperatorException {
        throw (new OperatorException("No blobs."));
    }
}
