package org.tupeloproject.kernel.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public class MemoryContext extends AbstractTripleBlobContext {
    private Map<Resource, Set<Triple>> subject;
    private Map<Resource, Set<Triple>> predicate;
    private Map<Resource, Set<Triple>> object;
    private Set<Triple>                triples;
    private Map<Resource, File>        blobs;

    public MemoryContext() {
    }

    @Override
    public void initialize() throws OperatorException {
        subject = new HashMap<Resource, Set<Triple>>();
        predicate = new HashMap<Resource, Set<Triple>>();
        object = new HashMap<Resource, Set<Triple>>();
        triples = new HashSet<Triple>();
        blobs = new HashMap<Resource, File>();
    }

    @Override
    public void destroy() throws OperatorException {
        subject = null;
        predicate = null;
        object = null;
        triples = null;
        for (File file : blobs.values() ) {
            file.delete();
        }
        blobs = null;
    }

    @Override
    public void sync() throws OperatorException {
    }

    public void open() throws OperatorException {
        if (triples == null) {
            initialize();
        }
    }

    public void close() throws OperatorException {
    }

    @Override
    public void perform(TripleIterator op) throws OperatorException {
        open();
        op.setIterator(triples.iterator());
    }

    @Override
    public void perform(TripleWriter op) throws OperatorException {
        open();
        for (Triple t : op.getToAdd() ) {
            triples.add(t);
            Set<Triple> x = subject.get(t.getSubject());
            if (x == null) {
                x = new HashSet<Triple>();
                subject.put(t.getSubject(), x);
            }
            x.add(t);
            x = predicate.get(t.getPredicate());
            if (x == null) {
                x = new HashSet<Triple>();
                predicate.put(t.getPredicate(), x);
            }
            x.add(t);
            x = object.get(t.getObject());
            if (x == null) {
                x = new HashSet<Triple>();
                object.put(t.getObject(), x);
            }
            x.add(t);
        }
        triples.removeAll(op.getToRemove());
        for (Triple t : op.getToRemove() ) {
            triples.remove(t);
            Set<Triple> x = subject.get(t.getSubject());
            if (x != null) {
                x.remove(t);
            }
            x = predicate.get(t.getPredicate());
            if (x != null) {
                x.remove(t);
            }
            x = object.get(t.getObject());
            if (x != null) {
                x.remove(t);
            }
        }
    }

    @Override
    public void perform(TripleMatcher tm) throws OperatorException {
        open();
        if ((tm.getSubject() == null) && (tm.getPredicate() == null) && (tm.getObject() == null)) {
            System.out.println(tm + " == TI");
            tm.setIterator(triples.iterator());
            return;
        }

        Set<Triple> subset = null;

        if (tm.getSubject() != null) {
            Set<Triple> x = subject.get(tm.getSubject());
            if (x == null) {
                tm.setIterator(new HashSet<Triple>());
                return;
            }
            if ((subset == null) || (x.size() < subset.size())) {
                subset = x;
            }
        }

        if (tm.getPredicate() != null) {
            Set<Triple> x = predicate.get(tm.getPredicate());
            if (x == null) {
                tm.setIterator(new HashSet<Triple>());
                return;
            }
            if ((subset == null) || (x.size() < subset.size())) {
                subset = x;
            }
        }

        if (tm.getObject() != null) {
            Set<Triple> x = object.get(tm.getObject());
            if (x == null) {
                tm.setIterator(new HashSet<Triple>());
                return;
            }
            if ((subset == null) || (x.size() < subset.size())) {
                subset = x;
            }
        }

        perform(tm, subset);
    }

    private void perform(final TripleMatcher tm, final Set<Triple> subset) throws OperatorException {
        final Iterator<Triple> iter = subset.iterator();
        tm.setIterator(new Iterator<Triple>() {
            Triple t = null;

            @Override
            public boolean hasNext() {
                while ((t == null) && iter.hasNext()) {
                    Triple x = iter.next();
                    if ((tm.getSubject() != null) && !tm.getSubject().equals(x.getSubject())) {
                        continue;
                    }
                    if ((tm.getPredicate() != null) && !tm.getPredicate().equals(x.getPredicate())) {
                        continue;
                    }
                    if ((tm.getObject() != null) && !tm.getObject().equals(x.getObject())) {
                        continue;
                    }
                    t = x;
                }
                return (t != null);
            }

            @Override
            public Triple next() {
                if ((t == null) && !hasNext()) {
                    return null;
                }
                Triple x = t;
                t = null;
                return x;
            }

            @Override
            public void remove() {
            }

        });
    }

    protected File getFile(Resource subject) {
        return blobs.get(subject);
    }

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        open();
        try {
            op.setInputStream(new FileInputStream(getFile(op.getSubject())));
        } catch (Exception e) {
            throw (new OperatorException("Could not open blob.", e));
        }
    }

    @Override
    public void perform(BlobIterator op) throws OperatorException {
        open();
        op.setIterator(blobs.keySet().iterator());
    }

    @Override
    public void perform(BlobRemover op) throws OperatorException {
        open();
        if (blobs.containsKey(op.getSubject())) {
            getFile(op.getSubject()).delete();
        }
    }

    @Override
    public void perform(BlobWriter op) throws OperatorException {
        open();
        try {
            File file = File.createTempFile("blob", ".tmp");
            file.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[10240];
            int len = 0;
            long count = 0;
            while ((len = op.getInputStream().read(buf)) > 0) {
                fos.write(buf, 0, len);
                count += len;
            }
            op.setSize(count);
            fos.close();
            blobs.put(op.getSubject(), file);
        } catch (IOException e) {
            throw (new OperatorException("Could not save blob.", e));
        }
    }
}
