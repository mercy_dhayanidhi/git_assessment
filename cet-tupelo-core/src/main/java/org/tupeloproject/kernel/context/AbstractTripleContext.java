/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.SubjectRemover;
import org.tupeloproject.kernel.operator.Transformer;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.PagingTable;
import org.tupeloproject.rdf.query.Pattern;
import org.tupeloproject.util.Iterators;
import org.tupeloproject.util.ListTable;
import org.tupeloproject.util.Tuple;
import org.tupeloproject.util.Variable;

/**
 * BaseContext provides default implementations for operators that can
 * be implemented in terms of other operators.
 * 
 * @author Rob Kooper
 */
public abstract class AbstractTripleContext implements TripleContext {

    // ----------------------------------------------------------------------
    // TRIPLES
    // ----------------------------------------------------------------------

    /**
     * Perform this TripleMatcher using a new blank TripleIterator
     * 
     * @param tm
     *            the triple matcher
     * @throws OperatorException
     */
    public void perform(final TripleMatcher tm) throws OperatorException {
        final TripleIterator ti = new TripleIterator();
        perform(ti);

        // FIXME should implement closeable
        tm.setIterator(new Iterator<Triple>() {
            Triple t = null;

            @Override
            public boolean hasNext() {
                while ((t == null) && ti.hasNext()) {
                    Triple x = ti.next();
                    if ((tm.getSubject() != null) && !tm.getSubject().equals(x.getSubject())) {
                        continue;
                    }
                    if ((tm.getPredicate() != null) && !tm.getPredicate().equals(x.getPredicate())) {
                        continue;
                    }
                    if ((tm.getObject() != null) && !tm.getObject().equals(x.getObject())) {
                        continue;
                    }
                    t = x;
                }
                return (t != null);
            }

            @Override
            public Triple next() {
                if ((t == null) && !hasNext()) {
                    return null;
                }
                Triple x = t;
                t = null;
                return x;
            }

            @Override
            public void remove() {
            }

        });
    }

    /**
     * Perform this Unifier using TripleMatcher
     * 
     * @param unifier
     *            the unifier
     * @throws OperatorException
     */
    public void perform(Unifier uf) throws OperatorException {
        ListTable<Resource> result = new ListTable<Resource>();
        result.setColumnNames(new Tuple<String>(uf.getColumnNames()));

        // create list of all open vars
        HashMap<String, Resource> vars = new HashMap<String, Resource>();
        for (org.tupeloproject.rdf.query.Pattern p : uf.getPatterns() ) {
            if (!p.get(0).isBound()) {
                vars.put(p.get(0).getName(), null);
            }
            if (!p.get(1).isBound()) {
                vars.put(p.get(1).getName(), null);
            }
            if (!p.get(2).isBound()) {
                vars.put(p.get(2).getName(), null);
            }
        }

        // call worker
        unify(uf, 0, vars, result);

        // fix paging
        if (uf.hasPaging()) {
            uf.setResult(new PagingTable(uf, result));
        } else {
            uf.setResult(result);
        }
    }

    private void unify(Unifier uf, int idx, Map<String, Resource> vars, ListTable<Resource> table) throws OperatorException {
        if (idx >= uf.getPatterns().size()) {
            Tuple<Resource> result = new Tuple<Resource>(uf.getColumnNames().size());
            for (int i = 0; i < result.size(); i++ ) {
                String name = uf.getColumnNames().get(i);
                if (vars.containsKey(name)) {
                    result.set(i, vars.get(name));
                }
            }
            table.addRow(result);
            return;
        }

        Pattern p = uf.getPatterns().get(idx);
        TripleMatcher tm = new TripleMatcher();

        // get pattern
        Variable<Resource> subj = p.get(0);
        if (subj.isBound()) {
            tm.setSubject(subj.getValue());
        } else {
            tm.setSubject(vars.get(subj.getName()));
        }
        Variable<Resource> pred = p.get(1);
        if (pred.isBound()) {
            tm.setPredicate(pred.getValue());
        } else {
            tm.setPredicate(vars.get(pred.getName()));
        }
        Variable<Resource> obj = p.get(2);
        if (obj.isBound()) {
            tm.setObject(obj.getValue());
        } else {
            tm.setObject(vars.get(obj.getName()));
        }

        // execute
        if (((tm.getSubject() == null) || tm.getSubject().isUri()) && ((tm.getPredicate() == null) || tm.getPredicate().isUri())) {
            perform(tm);
        }

        // make sure we got something
        if ((tm.getResult() != null) && tm.getResult().size() > 0) {
            // fill in vars
            for (Triple t : tm.getResult() ) {
                Map<String, Resource> clone = new HashMap<String, Resource>();
                clone.putAll(vars);
                if (!subj.isBound()) {
                    clone.put(subj.getName(), t.getSubject());
                }
                if (!pred.isBound()) {
                    clone.put(pred.getName(), t.getPredicate());
                }
                if (!obj.isBound()) {
                    clone.put(obj.getName(), t.getObject());
                }
                unify(uf, idx + 1, clone, table);
            }
        } else if (p.isOptional()) {
            unify(uf, idx + 1, vars, table);
        }
    }

    /**
     * Perform this Transformer using a Unifier
     * 
     * @param t
     *            the transformer
     * @throws OperatorException
     */
    public void perform(Transformer op) throws OperatorException {
        // determine all the variable names in the unifier patterns.
        Set<String> columnNameSet = new HashSet<String>();
        for (Pattern p : op.getInPatterns() ) {
            for (Variable<Resource> v : p ) {
                if (!v.isBound()) {
                    columnNameSet.add(v.getName());
                }
            }
        }
        List<String> columnNames = new LinkedList<String>(columnNameSet);

        // set up the unifier.
        Unifier unifier = new Unifier();
        unifier.setPatterns(op.getInPatterns());
        unifier.setColumnNames(columnNames);
        perform(unifier);

        // create output
        Resource subject, predicate, object;
        Set<Triple> result = new HashSet<Triple>();
        for (Tuple<Resource> row : unifier.getResult() ) {
            for (Pattern p : op.getOutPatterns() ) {
                if (p.get(0).isBound()) {
                    subject = p.get(0).getValue();
                } else {
                    subject = row.get(unifier.getColumnNames().indexOf(p.get(0).getName()));
                }
                if (p.get(1).isBound()) {
                    predicate = p.get(1).getValue();
                } else {
                    predicate = row.get(unifier.getColumnNames().indexOf(p.get(1).getName()));
                }
                if (p.get(2).isBound()) {
                    object = p.get(2).getValue();
                } else {
                    object = row.get(unifier.getColumnNames().indexOf(p.get(2).getName()));
                }
                if ((subject != null) && (predicate != null) && (object != null)) {
                    result.add(new Triple(subject, predicate, object));
                }
            }
        }

        op.setResult(result);
    }

    @Override
    public void perform(SubjectRemover op) throws OperatorException {
        TripleMatcher tm = new TripleMatcher();
        tm.setSubject(op.getSubject());
        perform(tm);
        TripleWriter tw = new TripleWriter();
        Iterators.copy(tm, tw.getToRemove());
        perform(tw);
    }

    // ----------------------------------------------------------------------
    // CONVENIENCE FUNCTIONS
    // ----------------------------------------------------------------------

    public void addTriple(Object s, Object p, Object o) throws OperatorException {
        addTriples(new Triple(s, p, o));
    }

    public void addTriple(Triple t) throws OperatorException {
        addTriples(t);
    }

    public void addTriples(Triple... triples) throws OperatorException {
        addTriples(Arrays.asList(triples));
    }

    public void addTriples(Collection<Triple> triples) throws OperatorException {
        TripleWriter tw = new TripleWriter();
        tw.addAll(triples);
        perform(tw);
    }

    public void removeTriple(Object s, Object p, Object o) throws OperatorException {
        removeTriples(new Triple(s, p, o));
    }

    public void removeTriple(Triple t) throws OperatorException {
        removeTriples(t);
    }

    public void removeTriples(Triple... triples) throws OperatorException {
        removeTriples(Arrays.asList(triples));
    }

    public void removeTriples(Collection<Triple> triples) throws OperatorException {
        TripleWriter tw = new TripleWriter();
        tw.removeAll(triples);
        perform(tw);
    }

    public Set<Triple> getTriples() throws OperatorException {
        TripleIterator ti = new TripleIterator();
        perform(ti);
        return Iterators.asSet(ti);
    }

    public Collection<Triple> match(Resource s, Resource p, Resource o) throws OperatorException {
        return match(new Triple(s, p, o));
    }

    public Collection<Triple> match(Triple t) throws OperatorException {
        TripleMatcher tm = new TripleMatcher();
        tm.setSubject(t.getSubject());
        tm.setPredicate(t.getPredicate());
        tm.setObject(t.getObject());
        perform(tm);
        return Iterators.asSet(tm);
    }
}
