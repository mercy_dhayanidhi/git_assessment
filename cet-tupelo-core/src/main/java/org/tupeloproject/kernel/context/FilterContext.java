/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.io.InputStream;
import java.util.Collection;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.OperatorNotSupportedException;
import org.tupeloproject.kernel.operator.BlobChecker;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.SubjectRemover;
import org.tupeloproject.kernel.operator.Transformer;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public class FilterContext implements TripleBlobContext {
    private TripleContext tripleContext;
    private BlobContext   blobContext;

    public void setContext(TripleContext tripleContext, BlobContext blobContext) {
        this.tripleContext = tripleContext;
        this.blobContext = blobContext;
    }

    public TripleContext getTripleContext() {
        return tripleContext;
    }

    public void setTripleContext(TripleContext tripleContext) {
        this.tripleContext = tripleContext;
    }

    public BlobContext getBlobContext() {
        return blobContext;
    }

    public void setBlobContext(BlobContext blobContext) {
        this.blobContext = blobContext;
    }

    // ----------------------------------------------------------------------
    // CONTEXT
    // ----------------------------------------------------------------------

    @Override
    public void open() throws OperatorException {
        if (tripleContext != null) {
            tripleContext.open();
        }
        if (blobContext != null) {
            blobContext.open();
        }
    }

    @Override
    public void close() throws OperatorException {
        if (tripleContext != null) {
            tripleContext.close();
        }
        if (blobContext != null) {
            blobContext.close();
        }
    }

    @Override
    public void initialize() throws OperatorException {
        if (tripleContext != null) {
            tripleContext.initialize();
        }
        if (blobContext != null) {
            blobContext.initialize();
        }
    }

    @Override
    public void destroy() throws OperatorException {
        if (tripleContext != null) {
            tripleContext.destroy();
        }
        if (blobContext != null) {
            blobContext.destroy();
        }
    }

    @Override
    public void sync() throws OperatorException {
        if (tripleContext != null) {
            tripleContext.sync();
        }
        if (blobContext != null) {
            blobContext.sync();
        }
    }

    // ----------------------------------------------------------------------
    // TRIPLE
    // ----------------------------------------------------------------------

    @Override
    public void perform(TripleIterator op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void perform(TripleMatcher op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void perform(TripleWriter op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void perform(Unifier op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void perform(SubjectRemover op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void perform(Transformer op) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No triple context set"));
        }
    }

    @Override
    public void addTriple(Object s, Object p, Object o) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.addTriple(s, p, o);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void addTriple(Triple t) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.addTriple(t);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void addTriples(Collection<Triple> triples) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.addTriples(triples);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void addTriples(Triple... triples) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.addTriples(triples);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public Set<Triple> getTriples() throws OperatorException {
        if (tripleContext != null) {
            return tripleContext.getTriples();
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public Collection<Triple> match(Resource s, Resource p, Resource o) throws OperatorException {
        if (tripleContext != null) {
            return tripleContext.match(s, p, o);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public Collection<Triple> match(Triple t) throws OperatorException {
        if (tripleContext != null) {
            return tripleContext.match(t);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void removeTriple(Object s, Object p, Object o) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.removeTriple(s, p, o);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void removeTriple(Triple t) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.removeTriple(t);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void removeTriples(Collection<Triple> triples) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.removeTriples(triples);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    @Override
    public void removeTriples(Triple... triples) throws OperatorException {
        if (tripleContext != null) {
            tripleContext.removeTriples(triples);
        } else {
            throw (new OperatorException("No triple context set"));
        }
    }

    // ----------------------------------------------------------------------
    // BLOB
    // ----------------------------------------------------------------------

    @Override
    public void perform(BlobChecker op) throws OperatorException {
        if (blobContext != null) {
            blobContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        if (blobContext != null) {
            blobContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public void perform(BlobIterator op) throws OperatorException {
        if (blobContext != null) {
            blobContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public void perform(BlobRemover op) throws OperatorException {
        if (blobContext != null) {
            blobContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public void perform(BlobWriter op) throws OperatorException {
        if (blobContext != null) {
            blobContext.perform(op);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public InputStream read(Resource uri) throws OperatorException {
        if (blobContext != null) {
            return blobContext.read(uri);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }

    @Override
    public void write(Resource uri, InputStream is) throws OperatorException {
        if (blobContext != null) {
            blobContext.write(uri, is);
        } else {
            throw (new OperatorNotSupportedException("No blob context set"));
        }
    }
}
