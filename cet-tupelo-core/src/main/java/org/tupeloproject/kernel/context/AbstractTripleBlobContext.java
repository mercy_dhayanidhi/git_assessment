/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.io.IOException;
import java.io.InputStream;

import org.tupeloproject.kernel.NotFoundException;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobChecker;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;

/**
 * BaseContext provides default implementations for operators that can
 * be implemented in terms of other operators.
 * 
 * @author Rob Kooper
 */
public abstract class AbstractTripleBlobContext extends AbstractTripleContext implements TripleBlobContext {

    // ----------------------------------------------------------------------
    // BLOBS
    // ----------------------------------------------------------------------

    public void perform(BlobChecker bc) throws OperatorException {
        try {
            BlobFetcher bf = new BlobFetcher(bc.getSubject());
            perform(bf);
            bc.setExists(true);
            InputStream is = bf.getInputStream();
            try {
                long size = 0, skipped = 0;
                byte buf[] = new byte[8192]; // FIXME space/time tradeoff
                while (true) {
                    skipped = is.read(buf);
                    if (skipped >= 0) {
                        size += skipped;
                    } else {
                        break;
                    }
                }
                is.close();
                bc.setSize(size);
            } catch (IOException x) {
                // unable to consume the stream to compute length; fall through.
            }
        } catch (NotFoundException x) {
            bc.setExists(false);
        }
    }

    @Override
    public InputStream read(Resource uri) throws OperatorException {
        BlobFetcher op = new BlobFetcher();
        op.setSubject(uri);
        perform(op);
        return op.getInputStream();
    }

    @Override
    public void write(Resource uri, InputStream is) throws OperatorException {
        BlobWriter op = new BlobWriter();
        op.setSubject(uri);
        op.setInputStream(is);
        perform(op);
    }
}
