/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.io.IOException;
import java.net.URL;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.OperatorNotSupportedException;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;

/**
 * This context provides a BlobFetcher that retrieves web pages from
 * the WWW.
 * 
 * @author Rob Kooper
 */
public class HttpContext extends AbstractBlobContext {
    public HttpContext() {
        super();
    }

    @Override
    public void open() throws OperatorException {
    }

    @Override
    public void close() throws OperatorException {
    }

    @Override
    public void initialize() throws OperatorException {
    }

    @Override
    public void destroy() throws OperatorException {
    }

    @Override
    public void sync() throws OperatorException {
    }

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        try {
            URL url = new URL(op.getSubject().toString());
            op.setInputStream(url.openStream());
        } catch (IOException e) {
            throw (new OperatorException("Could not fetch blob.", e));
        }
    }

    @Override
    public void perform(BlobIterator op) throws OperatorException {
        throw (new OperatorNotSupportedException("HTTPContext does not support BlobIterator."));
    }

    @Override
    public void perform(BlobRemover op) throws OperatorException {
        throw (new OperatorNotSupportedException("HTTPContext does not support BlobRemover."));
    }

    @Override
    public void perform(BlobWriter op) throws OperatorException {
        throw (new OperatorNotSupportedException("HTTPContext does not support BlobWriter."));
    }
}
