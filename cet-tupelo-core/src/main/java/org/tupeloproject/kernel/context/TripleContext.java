package org.tupeloproject.kernel.context;

import java.util.Collection;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.SubjectRemover;
import org.tupeloproject.kernel.operator.Transformer;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public interface TripleContext extends Context {
    // ----------------------------------------------------------------------
    // TRIPLE OPERATORS
    // ----------------------------------------------------------------------

    void perform(TripleIterator op) throws OperatorException;

    void perform(TripleMatcher op) throws OperatorException;

    void perform(TripleWriter op) throws OperatorException;

    void perform(Unifier op) throws OperatorException;

    void perform(SubjectRemover op) throws OperatorException;

    void perform(Transformer op) throws OperatorException;

    // ----------------------------------------------------------------------
    // CONVENIENCE FUNCTIONS
    // ----------------------------------------------------------------------

    void addTriple(Object s, Object p, Object o) throws OperatorException;

    void addTriple(Triple t) throws OperatorException;

    void addTriples(Triple... triples) throws OperatorException;

    void addTriples(Collection<Triple> triples) throws OperatorException;

    void removeTriple(Object s, Object p, Object o) throws OperatorException;

    void removeTriple(Triple t) throws OperatorException;

    void removeTriples(Triple... triples) throws OperatorException;

    void removeTriples(Collection<Triple> triples) throws OperatorException;

    Set<Triple> getTriples() throws OperatorException;

    Collection<Triple> match(Resource s, Resource p, Resource o) throws OperatorException;

    Collection<Triple> match(Triple t) throws OperatorException;
}
