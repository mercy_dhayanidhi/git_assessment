package org.tupeloproject.kernel.context;

import java.io.InputStream;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobChecker;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;

public interface BlobContext extends Context {
    // ----------------------------------------------------------------------
    // BLOB OPERATORS
    // ----------------------------------------------------------------------

    void perform(BlobChecker op) throws OperatorException;

    void perform(BlobFetcher op) throws OperatorException;

    void perform(BlobIterator op) throws OperatorException;

    void perform(BlobRemover op) throws OperatorException;

    void perform(BlobWriter op) throws OperatorException;

    // ----------------------------------------------------------------------
    // CONVENIENCE FUNCTIONS
    // ----------------------------------------------------------------------

    void write(Resource uri, InputStream is) throws OperatorException;

    InputStream read(Resource uri) throws OperatorException;

}
