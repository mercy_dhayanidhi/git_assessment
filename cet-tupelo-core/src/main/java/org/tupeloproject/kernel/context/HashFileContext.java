/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import org.tupeloproject.kernel.NotFoundException;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.util.SecureHash;

/**
 * This filesystem-based Context stores blobs in a single directory, where each
 * blob is named after a hash of its URI.
 * 
 * @author Rob Kooper
 */
public class HashFileContext extends AbstractBlobContext {
    private int    depth = 0;
    private String directory;

    public HashFileContext() {
    }

    public HashFileContext(int depth) {
        setDepth(depth);
    }

    /**
     * The depth of the subdirectory structure to create. Use larger depths to
     * reduce the
     * number of files that will end up in each directory. Depth should not
     * exceed 20.
     * 
     * @param d
     *            the depth
     */
    public void setDepth(int d) {
        if (depth > 20) {
            throw new IllegalArgumentException("max depth 20");
        }
        depth = d;
    }

    public int getDepth() {
        return depth;
    }

    /**
     * Set the base directory i.e., the prefix for the pathnames
     * 
     * @param dir
     *            the directory
     */
    public void setDirectory(String dir) {
        directory = dir;
    }

    /**
     * Set the base directory i.e., the prefix for the pathnames
     * 
     * @param dir
     *            the directory
     */
    public void setDirectory(File dir) {
        directory = dir.getAbsolutePath();
    }

    /**
     * Get the base directory
     * 
     * @return the directory
     */
    public String getDirectory() {
        return directory;
    }

    /**
     * A mapping from a URI to an absolute pathname in which the filename is a
     * hex-encoded
     * SHA-1 hash of the URI.
     */
    public String getPath(Resource uri) {
        String hash = SecureHash.digest(uri.toString());
        File path = new File(getDirectory(), hash);
        for (int i = 0; i < depth; i++ ) {
            File dir = new File(path.getParent(), hash.substring(i * 2, (i + 1) * 2));
            path = new File(dir, hash);
        }
        return path.getAbsolutePath();
    }

    public void initialize() throws OperatorException {
        if (directory != null) {
            if (!new File(directory).exists() && !new File(directory).mkdirs()) {
                throw new OperatorException("Could not create directory " + directory);
            }
        }
    }

    public void destroy() throws OperatorException {
        if (directory != null) {
            removeFolder(new File(directory));
        }
    }

    private void removeFolder(File path) throws OperatorException {
        for (File f : path.listFiles() ) {
            if (f.isDirectory()) {
                removeFolder(f);
            }
            if (!f.delete()) {
                throw (new OperatorException("Could not remove " + f.getAbsolutePath()));
            }
        }
        if (!path.delete()) {
            throw (new OperatorException("Could not remove " + path.getAbsolutePath()));
        }
    }

    @Override
    public void open() throws OperatorException {
    }

    @Override
    public void close() throws OperatorException {
    }

    @Override
    public void sync() throws OperatorException {
    }

    public void perform(BlobWriter bw) throws OperatorException {
        String path = getPath(bw.getSubject());

        try {
            // write the blob to the file
            File blob = new File(path);
            blob.getParentFile().mkdirs();
            FileOutputStream fos = new FileOutputStream(blob);
            byte[] buf = new byte[10240];
            int len = 0;
            long size = 0;
            while ((len = bw.getInputStream().read(buf)) > 0) {
                size += len;
                fos.write(buf, 0, len);
            }
            fos.close();
            bw.getInputStream().close();
            bw.setSize(size);

            // write the uri to an associated file
            PrintWriter pw = new PrintWriter(new FileOutputStream(new File(getPath(bw.getSubject()) + ".uri")));
            pw.println(bw.getSubject());
            pw.flush();
            pw.close();
        } catch (IOException e) {
            throw new OperatorException("HashFileContext failed to perform BlobWriter", e);
        }
    }

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        File file = new File(getPath(op.getSubject()));
        if (!file.exists()) {
            throw (new NotFoundException("HashFileContext could not find blob associated with " + op.getSubject()));
        }
        try {
            op.setInputStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new OperatorException("HashFileContext failed to perform BlobFetcher", e);
        }
    }

    public void perform(BlobRemover br) throws OperatorException {
        String path = getPath(br.getSubject());
        new File(path).delete();
        new File(path + ".uri").delete();
    }

    public void perform(BlobIterator bi) throws OperatorException {
        Set<Resource> subjects = new HashSet<Resource>();
        listFiles(new File(getDirectory(), "foo").getAbsoluteFile().getParentFile(), subjects);
        bi.setIterator(subjects.iterator());
    }

    private void listFiles(File dir, Set<Resource> subjects) throws OperatorException {
        for (File f : dir.listFiles() ) {
            if (f.isDirectory()) {
                listFiles(f, subjects);
            } else if (f.getName().endsWith(".uri")) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(f));
                    String line = br.readLine();
                    br.close();
                    subjects.add(Resource.uriRef(line));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
