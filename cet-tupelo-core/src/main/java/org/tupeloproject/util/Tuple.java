/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * A Tuple is a List of terms.
 * 
 * @author Joe Futrelle
 */
public class Tuple<N> extends AbstractList<N> implements Comparable<Tuple<N>>, Serializable {
    private static final long serialVersionUID = 1L;

    private N[]               terms            = null;
    private int               order;
    private int               hashCode;
    private boolean           hashed           = false;

    /**
     * Create an empty Tuple.
     */
    public Tuple() {
    }

    /**
     * Create a Tuple of the given order
     * 
     * @param order
     *            the order
     */
    public Tuple(int order) {
        setOrder(order);
    }

    void setTerms(List<N> terms) {
        int i = 0;
        setOrder(terms.size());
        for (N term : terms ) {
            set(i++, term);
        }
    }

    /**
     * Create a Tuple given a List of terms.
     * 
     * @param terms
     *            the terms
     */
    public Tuple(List<N> terms) {
        setTerms(terms);
    }

    public static <N> Tuple<N> append(List<N>... lists) {
        List<N> appended = new LinkedList<N>();
        for (List<N> list : lists ) {
            appended.addAll(list);
        }
        return new Tuple<N>(appended);
    }

    /**
     * Create a Tuple and infer the order from the number
     * of arguments.
     * 
     * @param terms
     *            the terms
     */
    public Tuple(N... terms) {
        this(Arrays.asList(terms));
    }

    /**
     * Get the order (= number of terms in the Tuple).
     * 
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * Set the order. This will resize the array backing the Tuple,
     * and copy and/or truncating existing terms as needed.
     * 
     * @param value
     *            the new order
     */
    public void setOrder(int value) {
        if (order == value) {
            return;
        }
        N[] newTerms = (N[]) new Object[value];
        for (int i = 0; i < value; i++ ) {
            if (terms != null && i < order) {
                newTerms[i] = terms[i];
            } else {
                newTerms[i] = null;
            }
        }
        terms = newTerms;
        order = value;
        hashed = false;
    }

    /**
     * Get a term.
     * 
     * @param index
     *            the index of the term (0 to order)
     * @return the value of the term
     */
    public N get(int index) {
        return terms[index];
    }

    /**
     * Set a term.
     * 
     * @param index
     *            the index of the term (0 to order)
     * @param term
     *            the value of the term
     * @return the old term
     */
    public N set(int index, N term) {
        N old = terms[index];
        terms[index] = term;
        hashed = false;
        return old;
    }

    /**
     * Same as {@link #getOrder}.
     */
    public int size() {
        //return getOrder();
        return order; // this tile is faster
    }

    /**
     * Terms are not interchangeable, so this throws
     * UnsupportedOperationException.
     * 
     * @param index
     * @throws UnsupportedOperationException
     *             always
     */
    public N remove(int index) {
        throw new UnsupportedOperationException("cannot remove term from tuple");
    }

    /**
     * Returns true if the number of terms match and all corresponding
     * terms are equal
     * 
     * @param other
     *            the other tuple
     * @return whether the Tuples are equal
     */
    public boolean equals(Tuple other) {
        if (hashCode() != other.hashCode()) {
            return false;
        }
        return equals((Object) other);
    }

    /**
     * Returns true if other is a tuple, the number of terms match and all
     * corresponding
     * terms are equal
     * 
     * @param other
     *            the other tuple
     * @return whether the Tuples are equal
     */
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        } else {
            try {
                List<N> otherTuple = (List<N>) other;
                return super.equals(otherTuple);
            } catch (ClassCastException x) {
                return false;
            }
        }
    }

    /**
     * Based on the hash codes of the terms, in order
     * 
     * @return the hashcode
     */
    public synchronized int hashCode() {
        if (!hashed) {
            int hash = 17;
            for (int i = 0; i < order; i++ ) {
                N term = terms[i];
                hash = (31 * hash) + (term == null ? 0 : term.hashCode());
            }
            hashCode = hash;
            hashed = true;
        }
        return hashCode;
    }

    /**
     * Compares two Tuples. Tuples are sorted by terms in order.
     * Terms must be comparable.
     * 
     * @param other
     *            the other tuple
     * @return order
     * @throws ClassCastException
     */
    public synchronized int compareTo(Tuple<N> other) throws ClassCastException {
        if (other == this) {
            return 0;
        } else {
            for (int i = 0; i < order; i++ ) {
                Comparable<N> term = (Comparable<N>) terms[i];
                N otherTerm = other.get(i);
                int comparedTo = term.compareTo(otherTerm);
                if (comparedTo != 0) {
                    return comparedTo;
                }
            }
            // tuples are identical.
            return 0;
        }
    }

    /**
     * Get the array backing the tuple.
     * 
     * @return the terms
     */
    public N[] getTerms() {
        return terms;
    }

    /**
     * Set the array backing the tuple. DOES NOT COPY THE ARRAY.
     * 
     * @param value
     *            the new terms
     */
    public void setTerms(N[] value) {
        terms = value;
        order = value.length;
        hashed = false;
    }
}
