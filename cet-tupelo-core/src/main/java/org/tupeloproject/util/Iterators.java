/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Provides convenience methods for dealing with Iterators.
 * 
 * @author Joe Futrelle
 */
public class Iterators {
    /**
     * Consume all elements of an iterator and put them in the
     * specified collection.
     * 
     * @param iterator
     *            the iterator to consume
     * @param collection
     *            the collection to hold the results
     * @return the collection (for chaining and convenience)
     */
    public static <T> Collection<T> copy(Iterator<T> iterator, Collection<T> collection) {
        while (iterator.hasNext()) {
            collection.add(iterator.next());
        }
        return collection;
    }

    /**
     * Convert an Iterator to a Set by consuming the iterator
     * and adding all its elements to a Set.
     * 
     * @param iterator
     *            the iterator to consume
     * @return the Set
     */
    public static <T> Set<T> asSet(Iterator<T> iterator) {
        return (Set<T>) copy(iterator, new HashSet<T>());
    }

    /**
     * Convert an Iterator to a List by consuming the iterator
     * and adding all its elements to a List.
     * 
     * @param iterator
     *            the iterator to consume
     * @return the List
     */
    public static <T> List<T> asList(Iterator<T> iterator) {
        return (List<T>) copy(iterator, new ArrayList<T>());
    }

    /**
     * Wrap an Iterator in an Iterable.
     * 
     * @param iterator
     *            the iterator to wrap
     * @return an Iterable that returns the wrapped iterator
     */
    public static <T> Iterable<T> asIterable(final Iterator<T> iterator) {
        return new Iterable<T>() {
            public Iterator<T> iterator() {
                return iterator;
            }
        };
    }
}
