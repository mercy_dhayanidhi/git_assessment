/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a convenient way to construct URI's with a given prefix.
 * The default prefix is an empty string.
 */
public class UriFactory {
    String                               prefix            = "";
    final static Map<String, UriFactory> factoryCache      = new HashMap<String, UriFactory>();
    Map<String, URI>                     primaryUriCache   = new HashMap<String, URI>();
    Map<String, URI>                     secondaryUriCache = new HashMap<String, URI>();
    final static int                     seq[]             = new int[] { 0 };
    int                                  maxCapacity       = 1000;

    /**
     * Get a VM-wide singleton factory
     * 
     * @return the singleton
     */
    public static UriFactory getFactory() {
        return getFactory("");
    }

    /**
     * Get a VM-wide singleton factory for a given prefix
     * 
     * @param prefix
     *            the prefix
     * @return the singleton
     */
    public static UriFactory getFactory(String prefix) {
        synchronized (factoryCache) {
            UriFactory cached = factoryCache.get(prefix);
            if (cached == null) {
                cached = new UriFactory(prefix);
                factoryCache.put(prefix, cached);
            }
            return cached;
        }
    }

    /**
     * Default prefix is the empty string.
     */
    public UriFactory() {
        this("");
    }

    /**
     * Constructs a new factory with the given URI (converted to a String) as
     * prefix.
     * 
     * @param prefix
     *            the URI prefix
     */
    public UriFactory(URI prefix) {
        this(prefix.toString());
    }

    /**
     * Constructs a new factory with the given prefix.
     * 
     * @param prefix
     *            the URI prefix
     */
    public UriFactory(String prefix) {
        setPrefix(prefix);
    }

    /**
     * Generate a URI consisting of the prefix followed by the given suffix.
     * 
     * @param suffix
     *            the suffix
     * @return the uri
     */
    public synchronized URI newUri(String suffix) {
        URI cached = secondaryUriCache.get(suffix);
        if (cached == null) {
            cached = primaryUriCache.get(suffix);
            if (cached == null) {
                cached = URI.create(getPrefix() + suffix);
                if (primaryUriCache.size() > maxCapacity) {
                    primaryUriCache = null;
                    primaryUriCache = new HashMap<String, URI>();
                }
                primaryUriCache.put(suffix, cached);
            } else {
                if (secondaryUriCache.size() > maxCapacity) {
                    secondaryUriCache = null;
                    secondaryUriCache = new HashMap<String, URI>();
                }
                secondaryUriCache.put(suffix, cached);
            }
        }
        return cached;
    }

    /**
     * Generate a unique URI. The scope in which the URI is unique is
     * implementation-specific. The default implementation generates
     * URI's that are unique per-runtime.
     * 
     * @return the uri
     */
    public synchronized URI newUri() {
        seq[0] = seq[0] + 1;
        return newUri("_" + seq[0]);
    }

    /**
     * Get the prefix.
     * 
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Set the prefix.
     * 
     * @param value
     *            the new prefix
     */
    public void setPrefix(String value) {
        prefix = value;
    }
}
