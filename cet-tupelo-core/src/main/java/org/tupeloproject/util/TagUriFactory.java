/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import static java.lang.String.format;

import java.net.URI;

/**
 * Mints tag URI's as defined by <a
 * href="http://www.faqs.org/rfcs/rfc4151.html">RFC 4151</a>.
 * Provides setters for the authority name, date, and an optional
 * prefix for the specific part.
 * The format of the URI's generated is:
 * <p>
 * <code>
 * tag:{authorityName},{date}:{specificPartPrefix}{specificPartSuffix}
 * </code>
 * </p>
 * This is implemented by extending UriFactory and constructing its
 * prefix property as:
 * <p>
 * <code>
 * tag:{authorityName},{date}:{specificPartPrefix}
 * </code>
 * </p>
 * Leaving the caller to specify only the specific part suffix.
 * 
 * @author Joe Futrelle
 */
public class TagUriFactory extends UriFactory {
    private String               authorityName      = "nobody@nowhere.com";
    private String               date               = "1970-01-01";
    private String               specificPartPrefix = "";
    private static TagUriFactory theFactory         = null;

    /**
     * Return a pre-seeded factory for making unique tag URI's.
     * 
     * @return a pre-seeded factory for making unique tag URI's.
     */
    public static TagUriFactory getFactory() {
        if (theFactory == null) {
            theFactory = new TagUriFactory("edu.illinois.ncsa", "2011");
        }
        return theFactory;
    }

    /** For setter injection */
    public TagUriFactory() {
    }

    /**
     * For constructor injection.
     * 
     * @param authorityName
     *            the authority name (e.g., "futrelle@uiuc.edu")
     * @param date
     *            the date (e.g., "2002")
     */
    public TagUriFactory(String authorityName, String date) {
        this(authorityName, date, "");
    }

    /**
     * For constructor injection.
     * 
     * @param authorityName
     *            the authority name (e.g., "futrelle@uiuc.edu")
     * @param date
     *            the date (e.g., "2002")
     * @param specificPartPrefix
     *            any additional prefix for the specific part
     */
    public TagUriFactory(String authorityName, String date, String specificPartPrefix) {
        setAuthorityName(authorityName);
        setDate(date);
        setSpecificPartPrefix(specificPartPrefix);
    }

    /**
     * Return a new tag URI in which the specific part is the
     * (optional) prefix followed by a recursive hash of some
     * system-dependent data. The chance of collisions is miniscule.
     */
    public URI newUri() {
        /***********************************************************************
         * synchronized(staticLock) {
         * // computing a new seed is slow, so only do it occasionally
         * if(accessionNumber++ % 1000 == 0) {
         * seed += System.currentTimeMillis() +
         * Runtime.getRuntime().freeMemory();
         * seed = SecureHash.digest(seed);
         * }
         * return newUri(seed+":"+accessionNumber);
         * }
         ***********************************************************************/
        return newUri(SecureHashMinter.getMinter().mint());
    }

    /**
     * Get the authorityName.
     * 
     * @return the authorityName
     */
    public String getAuthorityName() {
        return authorityName;
    }

    /**
     * Set the authority name and modify the prefix accordingly,
     * replacing any previous value for the prefix
     * 
     * @param value
     *            the new authorityName
     */
    public void setAuthorityName(String value) {
        authorityName = value;
        replacePrefix();
    }

    /**
     * Get the date.
     * 
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Set the date and modify the prefix accordingly, replacing any
     * previous value for the prefix
     * 
     * @param value
     *            the new date
     */
    public void setDate(String value) {
        date = value;
        replacePrefix();
    }

    /**
     * Get the specificPartPrefix.
     * 
     * @return the specificPartPrefix
     */
    public String getSpecificPartPrefix() {
        return specificPartPrefix;
    }

    /**
     * Set the specificPartPrefix and modify the prefix accordingly,
     * replacing any previous value for the prefix
     * 
     * @param value
     *            the new specificPartPrefix
     */
    public void setSpecificPartPrefix(String value) {
        specificPartPrefix = value;
        replacePrefix();
    }

    /**
     * Replace the prefix with a tag URI prefix based on the authority name,
     * date,
     * and specific part prefix
     */
    protected void replacePrefix() {
        setPrefix(format("tag:%s,%s:%s", getAuthorityName(), getDate(), getSpecificPartPrefix()));
    }
}
