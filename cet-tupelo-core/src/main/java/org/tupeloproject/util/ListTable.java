/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This implementation of {@link Table} stores its rows in a {@link List},
 * of {@link Tuple}s, which the caller can access directly or through some convenience methods.
 */
public class ListTable<T> implements Table<T> {
    Tuple<String> columnNames = null;
    List<Tuple<T>> rows = new LinkedList<Tuple<T>>();

    /** for setter injection */
    public ListTable() { }

    /**
     * Construct a new instance with data from the given table
     * @param source the existing table
     */
    public ListTable(Table<T> source) {
        setColumnNames(source.getColumnNames());
        for(Tuple<T> row : source) {
            addRow(row);
        }
    }

    /**
     * Construct a new instance with the given column names.
     * 
     * @param columnNames the column names
     */
    public ListTable(String... columnNames) {
	setColumnNames(new Tuple<String>(Arrays.asList(columnNames)));
    }

    /**
     * Get an iterator over the rows in this table.
     * @return the iterator
     */
    public Iterator<Tuple<T>> iterator() {
	return getRows().iterator();
    }

    /**
     * Get the column names for this table.
     *
     * @return the column names
     */
    public Tuple<String> getColumnNames() {
	return columnNames;
    }

    /**
     * Set the columnNames.
     *
     * @param value the new columnNames
     */
    public void setColumnNames(Tuple<String> value) {
	columnNames = value;
    }

    /**
     * Get the rows.
     *
     * @return the rows
     */
    public List<Tuple<T>> getRows() {
	return rows;
    }

    /** Add a row
     * @param row the row
     */
    public void addRow(Tuple<T> row) {
	getRows().add(row);
    }

    /**
     * Add a row
     * @param terms the terms in the row
     */
    public void addRow(T... terms) {
	getRows().add(new Tuple<T>(Arrays.asList(terms)));
    }

    /**
     * Set the rows.
     *
     * @param value the new rows
     */
    public void setRows(List<Tuple<T>> value) {
	rows = value;
    }
}
