/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

/**
 * Mint an almost probably unique String using System-dependent seed data.
 * 
 * @author Joel Plutchak
 */
public class SecureHashMinter implements Minter<String> {
    private final static Object     staticLock = new Object();
    private static String           seed       = "seed";
    private static SecureHashMinter singleton;

    /**
     * No-op constructor. Declared private to enforce singletonarianism.
     */
    private SecureHashMinter() {
    }

    /**
     * Lazily constructs a singleton.
     * 
     * @return the singleton
     */
    public static synchronized SecureHashMinter getMinter() {
        if (singleton == null) {
            singleton = new SecureHashMinter();
        }
        return singleton;
    }

    /**
     * Clone this object. Always throws an exception, to preserve the
     * singleton.
     * 
     * @return the cloned object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Mint an almost probably unique String using System-dependent seed data.
     */
    public String mint() {
        synchronized (staticLock) {
            seed += System.currentTimeMillis() + Runtime.getRuntime().freeMemory();
            seed = SecureHash.digest(seed);
            return seed;
        }
    }
}
