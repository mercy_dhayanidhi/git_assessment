/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

// for hashing

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utilities for generating secure hashes
 */
public class SecureHash {
    /** The default digest algorithm used: {@value} */
    public static String            DIGEST_ALGORITHM = "SHA-1";
    /** The default charset to use for converting characters to bytes: {@value} */
    public static String            CHARSET          = "US-ASCII";
    private static char[]           HEX_CHAR         = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    private static final SecureHash singleton        = new SecureHash();

    private String                  digestAlgorithm  = DIGEST_ALGORITHM;
    private String                  charset          = CHARSET;

    private MessageDigest           messageDigest    = null;

    /**
     * Get the digestAlgorithm.
     * 
     * @return the digestAlgorithm
     */
    public String getDigestAlgorithm() {
        return digestAlgorithm;
    }

    /**
     * Set the digestAlgorithm.
     * 
     * @param value
     *            the new digestAlgorithm
     */
    public void setDigestAlgorithm(String value) {
        digestAlgorithm = value;
        messageDigest = null;
    }

    /**
     * Get the charset.
     * 
     * @return the charset
     */
    public String getCharset() {
        return charset;
    }

    /**
     * Set the charset.
     * 
     * @param value
     *            the new charset
     */
    public void setCharset(String value) {
        charset = value;
    }

    /**
     * Hex-encode a byte array
     * 
     * @param bytes
     *            the byte array
     * @return the hex-encoded representation of the byte array
     */
    public static String hexEncode(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (byte aByte : bytes ) {
            sb.append(HEX_CHAR[(aByte & 0xf0) >>> 4]);
            sb.append(HEX_CHAR[aByte & 0x0f]);
        }
        return sb.toString();
    }

    MessageDigest getMessageDigest() {
        if (messageDigest == null) {
            try {
                messageDigest = MessageDigest.getInstance(getDigestAlgorithm());
            } catch (NoSuchAlgorithmException x) {
                System.err.println("No such algorithm: " + getDigestAlgorithm());
            }
        }
        return messageDigest;
    }

    /**
     * Return a digest of the input string using the default digest
     * algorithm and charset.
     * 
     * @param input
     *            the input string
     * @return the digest, or null if there was an error
     */
    public static byte[] digestBytes(String input) {
        return singleton.getDigestBytes(input);
    }

    /**
     * Return a hex-encoded digest of the input string using
     * the default digest algorithm and charset.
     * 
     * @param input
     *            the input string
     * @return the digest, or null if there was an error
     */
    public static String digest(String input) {
        return singleton.getDigest(input);
    }

    /**
     * Return a hex-encoded digest of the input bytestream using
     * the default digest algorithm and charset.
     * 
     * @param input
     *            the input string
     * @return the digest, or null if there was an error
     */
    public static String digest(InputStream input) throws IOException {
        return singleton.getDigest(input);
    }

    public static byte[] digestAndCopy(InputStream in, OutputStream out) throws IOException {
        // get a message digest object
        synchronized (singleton) {
            MessageDigest md = singleton.getMessageDigest();
            if (md == null) {
                return null;
            }
            byte[] buf = new byte[8192];
            int i = 0;
            synchronized (in) {
                synchronized (out) {
                    while (true) {
                        i = in.read(buf);
                        if (i == -1) {
                            break;
                        }
                        out.write(buf, 0, i);
                        md.update(buf, 0, i);
                    } //end while
                } //end synchronize on in
            }//end synchronize on out
            out.flush();
            return md.digest();
        }
    }

    public static String digestCopyAndHex(InputStream in, OutputStream out) throws IOException {
        return hexEncode(digestAndCopy(in, out));
    }

    /**
     * Return a digest of the input string
     * 
     * @param input
     *            the input string
     * @return the digest, or null if there was an error
     */
    synchronized public byte[] getDigestBytes(String input) {
        try {
            // get a message digest object
            MessageDigest md = getMessageDigest();
            if (md == null) {
                return null;
            }
            // now convert the input String into bytes, using the charset
            byte[] inputBytes = input.getBytes(getCharset());
            // now compute and return the digest
            return md.digest(inputBytes);
        } catch (UnsupportedEncodingException x) {
            System.err.println("Unsupported encoding: " + getCharset());
            return null;
        }
    }

    synchronized public byte[] getDigestBytes(InputStream input) throws IOException {
        MessageDigest md = getMessageDigest();
        if (md == null) {
            return null;
        }
        byte[] buf = new byte[8192];
        int len = -1;
        while ((len = input.read(buf, 0, 8192)) != -1) {
            md.update(buf, 0, len);
        }
        return md.digest();
    }

    public String getDigest(InputStream input) throws IOException {
        byte[] digest = getDigestBytes(input);
        if (digest == null) {
            return null;
        } else {
            return hexEncode(digest);
        }
    }

    /**
     * Return a hex-encoded digest of the input string
     * 
     * @param input
     *            the input string
     * @return the digest, or null if there was an error
     */
    public String getDigest(String input) {
        byte[] digest = getDigestBytes(input);
        if (digest == null) {
            return null;
        } else {
            return hexEncode(digest);
        }
    }
}
