/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.AbstractCollection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Represents an unordered collection which may contain duplicate
 * elements, backed by a {@link HashMap}. This is an efficient
 * implementation for collections containing many duplicate elements.
 * This implementation is not thread-safe.
 * 
 * @author Joe Futrelle
 */
public class HashBag<T> extends AbstractCollection<T> implements Bag<T> {
    private int                 size        = 0;
    private HashMap<T, Integer> cardinality = new HashMap<T, Integer>();

    /**
     * Return the number of occurrences of this element in this Bag.
     * 
     * @param element
     *            the element
     * @return the number of occurrences: zero or greater
     */
    public int getCardinality(Object element) {
        Integer c = cardinality.get(element);
        if (c == null) {
            return 0;
        }
        return c;
    }

    /**
     * Returns true if the element's cardinality is non-zero, false
     * otherwise.
     * 
     * @param element
     *            the element
     * @return true if the element's cardinality is non-zero, false
     *         otherwise.
     */
    public boolean contains(Object element) {
        return getCardinality(element) > 0;
    }

    /**
     * Increase the cardinality of the given element by 1.
     * 
     * @param element
     *            the element
     * @return true
     */
    public boolean add(T element) {
        cardinality.put(element, getCardinality(element) + 1);
        size++;
        return true;
    }

    /**
     * Decrease the cardinality of the given element by 1.
     * 
     * @param element
     *            the element
     * @return true if the cardinality was nonzero, false
     *         otherwise
     */
    public boolean remove(Object element) {
        int c = getCardinality(element);
        if (c > 0) {
            cardinality.put((T) element, c - 1);
            size--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the sum of the cardinality over all elements
     * 
     * @return the sum of the cardinality over all elements
     */
    public int size() {
        return size;
    }

    /**
     * Return an iterator over all elements with non-zero cardinality.
     * For each element with a cardinality of n, this iterator will
     * answer true to hasNext() n times and return the element n times
     * when next() is called.
     * 
     * @return an iterator over all elements with non-zero cardinality
     */
    public Iterator<T> iterator() {
        final Iterator<Map.Entry<T, Integer>> it = cardinality.entrySet().iterator();
        return new Iterator<T>() {
            public T element = null;
            int      count   = 0;

            public boolean hasNext() {
                if (count == 0) {
                    if (it.hasNext()) {
                        Map.Entry<T, Integer> entry = it.next();
                        element = entry.getKey();
                        count = entry.getValue();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }

            public T next() {
                if (count > 0) {
                    count--;
                    return element;
                } else {
                    throw new NoSuchElementException("no more elements");
                }
            }

            public void remove() {
                if (element != null) {
                    HashBag.this.remove(element);
                }
            }
        };
    }
}
