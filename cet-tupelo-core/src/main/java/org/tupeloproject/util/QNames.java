/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.net.URI;
import java.text.ParseException;

import javax.xml.namespace.QName;

/**
 * Utilities for dealing with QNames and QName->URI roundtripping.
 * 
 * @author Joe Futrelle
 */
public class QNames {
    /**
     * Parse a QName serialized in the form "{&lt;namspace uri>}&lt;local part>"
     * 
     * @param qn
     *            the qname serialization
     * @return the qname
     * @throws java.text.ParseException
     *             if the serialization is malformed
     */
    public static QName parseQName(String qn) throws ParseException {
        int closeBraceIx = -1;
        if (!qn.startsWith("{")) {
            throw new ParseException("malformed qname serialization " + qn, 0);
        } else if ((closeBraceIx = qn.indexOf('}')) == -1) {
            throw new ParseException("malformed qname serialization " + qn, 0);
        }
        try {
            String namespaceUri = qn.substring(1, closeBraceIx);
            String localPart = qn.substring(closeBraceIx + 1);
            if (localPart.length() < 1) {
                throw new ParseException("malformed local part in " + qn, closeBraceIx);
            }
            return new QName(namespaceUri, localPart);
        } catch (IndexOutOfBoundsException x) {
            throw new ParseException("malformed local part in " + qn, closeBraceIx);
        }
    }

    /**
     * Serialize a QName in the form "{&lt;namespace uri>}&lt;local part>
     * 
     * @param qn
     *            the qname
     * @return the qname serialization
     */
    public static String serializeQName(QName qn) {
        return "{" + qn.getNamespaceURI() + "}" + qn.getLocalPart();
    }

    /**
     * Convert a QName to a URI.
     * 
     * @param qn
     *            the QName
     * @return the URI
     */
    public static URI qNameToUri(QName qn) {
        return URI.create(qn.getNamespaceURI() + qn.getLocalPart());
    }

    /**
     * Attempt to guess the QName that the given URI represents.
     * This works by reading the URI backwards until a character that
     * is illegal as part of an XML Name is found.
     * There is *no guarantee* that for a given QName foo,
     * foo.equals(uriToQName(qNameToUri(foo))).
     * 
     * @param uri
     *            the uri
     * @return the qname
     * @throws ParseException
     *             if conversion fails
     */
    public static QName uriToQName(URI uri) throws ParseException {
        String uriString = uri.toString();
        String localName = uriString.replaceFirst(".*[^a-zA-Z_]([a-zA-Z_][a-zA-Z0-9_.-]*)", "$1");
        if (localName.equals("")) {
            throw new ParseException("can't convert uri to qname: " + uri, 0);
        }
        String namespaceUri = uriString.replaceFirst("(.*[^a-zA-Z_])[a-zA-Z_][a-zA-Z0-9_.-]*", "$1");
        if (namespaceUri.equals("")) {
            throw new ParseException("can't convert uri to qname: " + uri, 0);
        }
        return new QName(namespaceUri, localName);
    }
}
