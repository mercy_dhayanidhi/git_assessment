/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;

/**
 * UnicodeTranscoder encodes and decodes Unicode from ASCII according to the
 * rules specified
 * in the <a href="http://www.w3.org/TR/rdf-testcases/#ntrip_strings">RDF test
 * cases</a> document.
 * 
 * @author Joe Futrelle
 */
public class UnicodeTranscoder {
    private static String esc4(int c) {
        return "\\u" + String.format("%04x", c);
    }

    private static String esc8(int c) {
        return "\\U" + String.format("%08x", c);
    }

    /**
     * Encode a Unicode string into ASCII, escaping characters according to the
     * rules specified in the RDF test cases document.
     * 
     * @param inString
     *            the String to encode
     * @return the encoded string
     * @throws an
     *             IllegalArgumentException if the string cannot be decoded.
     */
    public static String encode(String inString) {
        StringReader in = new StringReader(inString);
        StringWriter out = new StringWriter();
        try {
            while (true) {
                int i = in.read();
                if (i == -1) {
                    return out.toString();
                } else if (i >= 0x0 && i <= 0x8) {
                    out.write(esc4(i));
                } else if (i == 0x9) { // tab
                    out.write("\\t");
                } else if (i == 0xA) { // newline
                    out.write("\\n");
                } else if (i >= 0xB && i <= 0xC) {
                    out.write(esc4(i));
                } else if (i == 0xD) { // carriage return
                    out.write("\\r");
                } else if (i >= 0xE && i <= 0x1F) {
                    out.write(esc4(i));
                } else if (i >= 0x20 && i <= 0x21) {
                    out.write(i);
                } else if (i == 0x22) { // double quote
                    out.write("\\\"");
                } else if (i >= 0x23 && i <= 0x5B) {
                    out.write(i);
                } else if (i == 0x5C) { // backslash
                    out.write("\\\\");
                } else if (i >= 0x4D && i <= 0x7E) {
                    out.write(i);
                } else if (i >= 0xD800 && i <= 0xDBFF) { // high surrogate
                    // we need to read the next UTF-16 character
                    int j = in.read();
                    // if there wasn't one, this String is malformed
                    // that won't happen.
                    if (j == -1) {
                        throw new IllegalArgumentException("bad unicode");
                    }
                    // now we need to convert the wide UTF-16 char to a real code point
                    // before escaping it
                    out.write(esc8(Character.toCodePoint((char) i, (char) j)));
                } else if (i >= 0x7F && i <= 0xFFFF) { // low unicode
                    out.write(esc4(i));
                }
            }
        } catch (IOException x) {
            // this will not happen
        }
        throw new IllegalArgumentException("cannot decode string");
    }

    private static int hexValue(int c) throws ParseException {
        if (c >= 0x30 && c <= 0x39) { // 0123456789
            return c - 0x30;
        } else if (c >= 0x41 && c <= 0x46) { // ABCDEF
            return c - 0x41 + 10;
        } else if (c >= 0x61 && c <= 0x66) { // abcdef
            return c - 0x61 + 10;
        } else {
            throw new ParseException("illegal hex digit: " + (char) c, 0);
        }
    }

    /**
     * Decode a string in the format produced by encode(String).
     * 
     * @param inString
     *            the encoded String
     * @return the Unicode String it represents
     * @throws ParseException
     *             if there is a syntax error in the encoded string
     * @throws IllegalArgumentException
     */
    public static String decode(String inString) throws ParseException {
        StringReader in = new StringReader(inString);
        StringBuffer out = new StringBuffer();
        try {
            while (true) {
                int i = in.read();
                if (i == -1) {
                    return out.toString();
                } else if (i == 0x5C) { // backslash
                    i = in.read();
                    if (i == 0x74) { // 't'
                        out.append('\t');
                    } else if (i == 0x6E) { // 'n'
                        out.append('\n');
                    } else if (i == 0x72) { // 'r'
                        out.append('\r');
                    } else if (i == 0x22) { // double quote
                        out.append('"');
                    } else if (i == 0x5C) { // backslash
                        out.append('\\');
                    } else if (i == 'u') { // low unicode
                        int u = 0x0;
                        for (int j = 0; j < 4; j++ ) {
                            u <<= 4;
                            u |= hexValue(in.read());
                        }
                        try {
                            out.appendCodePoint(u);
                        } catch (IllegalArgumentException x) {
                            throw new ParseException("illegal unicode escape", 0);
                        }
                    } else if (i == 'U') { // high unicode
                        int u = 0x0;
                        for (int j = 0; j < 8; j++ ) {
                            u *= 0x10;
                            u += hexValue(in.read());
                        }
                        try {
                            out.appendCodePoint(u);
                        } catch (IllegalArgumentException x) {
                            throw new ParseException("illegal unicode escape", 0);
                        }
                    } else {
                        throw new ParseException("unrecognized escape character: " + (char) i, 0);
                    }
                } else {
                    out.append((char) i);
                }
            }
        } catch (IOException x) {
            // this will not happen
        }
        throw new IllegalArgumentException("bad string argument");
    }
}
