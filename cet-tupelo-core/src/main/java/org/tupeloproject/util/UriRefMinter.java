package org.tupeloproject.util;

import org.tupeloproject.rdf.Resource;

public class UriRefMinter implements Minter<Resource> {
    @Override
    public Resource mint() {
        return Resource.uriRef();
    }
}
