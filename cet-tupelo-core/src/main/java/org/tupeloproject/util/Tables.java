/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Provides static utilities for dealing with Tables.
 * 
 * @author Joe Futrelle
 */
public class Tables {
    /**
     * Construct a new table based on the given column names and row iterator
     * 
     * @param columnNames
     *            the columnNames
     * @param iterator
     *            the row iterator
     * @return the table
     */
    public static <T> Table<T> newInstance(final Tuple<String> columnNames, final Iterator<Tuple<T>> iterator) {
        return new Table<T>() {
            public Tuple<String> getColumnNames() {
                return columnNames;
            }

            public Iterator<Tuple<T>> iterator() {
                return iterator;
            }
        };
    }

    /**
     * Construct a new table based on the given column names and rows
     * 
     * @param columnNames
     *            the columnNames
     * @param rows
     *            the rows
     * @return the table
     */
    public static <T> Table<T> newInstance(final Iterable<String> columnNames, final Iterable<Iterable<T>> rows) {
        final Tuple<String> c = new Tuple<String>(Iterators.asList(columnNames.iterator()));
        final Iterator<Iterable<T>> rowIterator = rows.iterator();
        Iterator<Tuple<T>> i = new Iterator<Tuple<T>>() {
            public void remove() {
                throw new UnsupportedOperationException("can't remove");
            }

            public boolean hasNext() {
                return rowIterator.hasNext();
            }

            public Tuple<T> next() {
                return new Tuple<T>(Iterators.asList(rowIterator.next().iterator()));
            }
        };
        return newInstance(c, i);
    }

    /**
     * Return the rows in a table as a List of Tuples.
     * 
     * @param table
     *            the table
     * @return the rows in a table as a List of Tuples.
     */
    public static <T> List<Tuple<T>> asList(Table<T> table) {
        return Iterators.asList(table.iterator());
    }

    /**
     * Return one of the columns in a table as a List. Useful for
     * one-column tables.
     * 
     * @param table
     *            the table
     * @param index
     *            the index of the column to return
     * @return the list of values
     */
    public static <T> List<T> getColumn(Table<T> table, int index) {
        List<T> result = new ArrayList<T>();
        for (Tuple<T> row : table ) {
            result.add(row.get(index));
        }
        return result;
    }

    /**
     * Get the index of a named column of a table
     * 
     * @param table
     *            the table
     * @param columnName
     *            the column name
     * @return the column's index
     */
    public static <T> int getColumnIndex(Table<T> table, String columnName) {
        int i = 0;
        for (String cand : table.getColumnNames() ) {
            if (cand.equals(columnName)) {
                return i;
            }
            i++;
        }
        throw new IllegalArgumentException("no such column: " + columnName);
    }

    /**
     * Return one of the columns in a table as a List.
     * 
     * @param table
     *            the table
     * @param columnName
     *            the name of the column
     * @return the column as a List
     */
    public static <T> List<T> getColumn(Table<T> table, String columnName) {
        int idx = getColumnIndex(table, columnName);
        return getColumn(table, idx);
    }

    /**
     * Get the rows of the table in order
     * 
     * @param table
     *            the table
     * @return the rows of the table, sorted
     */
    public static <T> SortedSet<Tuple<T>> sort(Table<T> table) {
        TreeSet<Tuple<T>> sorted = new TreeSet<Tuple<T>>();
        for (Tuple<T> row : table ) {
            sorted.add(row);
        }
        return sorted;
    }

    /**
     * Sort a table by a given column in its natural ordering.
     * Ordering of multiple rows for a given column value is unspecified.
     * This will fail if the cell type is not comparable.
     * 
     * @param table
     *            the table
     * @param columnIndex
     *            the column index
     * @return the rows of the table in order
     */
    public static <T extends Comparable<? super T>> SortedSet<Tuple<T>> sort(Table<T> table, int columnIndex) {
        Comparator<T> cellComparator = new Comparator<T>() {
            public int compare(T cell1, T cell2) {
                if (cell1 == cell2) {
                    return 0;
                } else {
                    int cmp = cell1.compareTo(cell2);
                    if (cmp == 0) {
                        return cell1.hashCode() < cell2.hashCode() ? -1 : 1;
                    } else {
                        return cmp;
                    }
                }
            }
        };
        return sort(table, columnIndex, cellComparator);
    }

    /**
     * Sort a table by a given column in its natural ordering.
     * Ordering of multiple rows for a given column value is unspecified.
     * This will fail if the cell type is not comparable.
     * 
     * @param table
     *            the table
     * @param columnName
     *            the column name
     * @return the rows of the table in order
     */
    public static <T extends Comparable<? super T>> SortedSet<Tuple<T>> sort(Table<T> table, String columnName) {
        return sort(table, getColumnIndex(table, columnName));
    }

    /**
     * Sort a table by a specific column, using the given comparator.
     * 
     * @param table
     *            the table to sort
     * @param columnIndex
     *            the index of the column
     * @param comparator
     *            the comparator to use
     * @return the rows of the table in order
     */
    public static <T> SortedSet<Tuple<T>> sort(Table<T> table, final int columnIndex, final Comparator<T> comparator) {
        Comparator<Tuple<T>> rowComparator = new Comparator<Tuple<T>>() {
            public int compare(Tuple<T> row1, Tuple<T> row2) {
                return comparator.compare(row1.get(columnIndex), row2.get(columnIndex));
            }
        };
        SortedSet<Tuple<T>> result = new TreeSet<Tuple<T>>(rowComparator);
        result.addAll(asList(table));
        return result;
    }

    /**
     * Sort a table by a specific column, using the given comparator.
     * 
     * @param table
     *            the table to sort
     * @param columnName
     *            the name of the column
     * @param comparator
     *            the comparator to use
     * @return the rows of the table in order
     */
    public static <T> SortedSet<Tuple<T>> sort(Table<T> table, String columnName, Comparator<T> comparator) {
        return sort(table, getColumnIndex(table, columnName), comparator);
    }
}
