/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.AbstractList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Utilities for working with DOM documents and structures.
 * 
 * @author Joe Futrelle
 */
public class Xml {
    static private ThreadLocal<DocumentBuilder> theDocumentBuilder = new ThreadLocal<DocumentBuilder>() {
                                                                       @Override
                                                                       protected DocumentBuilder initialValue() {
                                                                           return newDocumentBuilder();
                                                                       }
                                                                   };
    static private Transformer                  theTransformer     = null;

    // DOM

    /**
     * Create a namespace-aware document builder
     * 
     * @deprecated use {@link #newDocument()}
     * @return a new DocumentBuilder
     */
    @Deprecated
    public static DocumentBuilder newDocumentBuilder() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            return dbf.newDocumentBuilder();
        } catch (Exception x) {
            return null; // this will not happen.
        }
    }

    /**
     * Get the default document builder (namespace-aware)
     * 
     * @return the default document builder (namespace-aware)
     */
    protected static DocumentBuilder getDocumentBuilder() {
        return theDocumentBuilder.get();
    }

    /**
     * Create a new XML document out of thin air.
     * 
     * @return a new XML document
     */
    public static Document newDocument() {
        return getDocumentBuilder().newDocument();
    }

    /**
     * Parse XML data.
     * 
     * @param is
     *            an input stream containing XML data
     * @return the resulting Document
     * @throws SAXException
     * @throws IOException
     */
    public static Document parse(InputStream is) throws SAXException, IOException {
        return getDocumentBuilder().parse(is);
    }

    /**
     * Parse XML data.
     * 
     * @param r
     *            a reader on the XML data
     * @return the resulting Document
     * @throws SAXException
     * @throws IOException
     */
    public static Document parse(Reader r) throws SAXException, IOException {
        return getDocumentBuilder().parse(new InputSource(r));
    }

    /**
     * Write XML data
     * 
     * @param d
     *            the Document to write
     * @param os
     *            the output stream to write it on
     * @throws IOException
     */
    public static void write(Document d, OutputStream os) throws IOException {
        write(d.getDocumentElement(), os);
    }

    /**
     * Write XML data
     * 
     * @param d
     *            the Document to write
     * @param w
     *            the Writer to use to write it
     * @throws IOException
     */
    public static void write(Document d, Writer w) throws IOException {
        write(d.getDocumentElement(), w);
    }

    /**
     * Write XML data
     * 
     * @param n
     *            the XML node to write
     * @param os
     *            the output stream to write it on
     * @throws IOException
     */
    public static void write(Node n, OutputStream os) throws IOException {
        write(n, new PrintWriter(os));
    }

    static Transformer newTransformer(Source stylesheet) throws TransformerConfigurationException {
        TransformerFactory tf = TransformerFactory.newInstance();
        try {
            tf.setAttribute("indent-number", 2);
        } catch (Exception x) {
        }
        Transformer t = null;
        if (stylesheet != null) {
            t = tf.newTransformer(stylesheet);
        } else {
            t = tf.newTransformer();
        }
        try {
            t.setOutputProperty(OutputKeys.INDENT, "yes");
        } catch (Exception x) {
        }
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        try {
            t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        } catch (Exception x) {
        }
        return t;
    }

    static Transformer getTheTransformer() throws IOException {
        try {
            if (theTransformer == null) {
                theTransformer = newTransformer(null);
            }
            return theTransformer;
        } catch (TransformerException x) {
            IOException ix = new IOException("Transformer exception during XML output");
            ix.initCause(x);
            throw ix;
        }
    }

    /**
     * Write XML data
     * 
     * @param n
     *            the node to write
     * @param w
     *            the writer to write it on
     * @throws IOException
     */
    public static void write(Node n, Writer w) throws IOException {
        try {
            getTheTransformer().transform(new DOMSource(n), new StreamResult(w));
        } catch (TransformerException x) {
            IOException ix = new IOException("Transformer exception during XML output");
            ix.initCause(x);
            throw ix;
        }
    }

    /**
     * Transform XML data
     * 
     * @param is
     *            the input XML data
     * @param ss
     *            an input stylesheet
     * @param r
     *            the result
     * @throws TransformerException
     */
    public static void transform(Source is, Source ss, Result r, boolean indent) throws TransformerException, IOException {
        Transformer t = null;
        if (indent) {
            t = newTransformer(ss);
        } else {
            TransformerFactory tf = TransformerFactory.newInstance();
            t = tf.newTransformer(ss);
        }
        t.transform(is, r);
    }

    /**
     * Transform XML data
     * 
     * @param is
     *            the input XML data
     * @param ss
     *            an input stylesheet
     * @param r
     *            the result
     * @throws TransformerException
     */
    public static void transform(Source is, Source ss, Result r) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer(ss);
        t.transform(is, r);
    }

    /**
     * Transform XML data
     * 
     * @param is
     *            the input XML data
     * @param ss
     *            an input stylesheet
     * @param r
     *            the result
     * @param resolver
     *            the URI resolver to use
     * @throws TransformerException
     */
    public static void transform(Source is, Source ss, Result r, URIResolver resolver) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setURIResolver(resolver);
        Transformer t = tf.newTransformer(ss);
        t.transform(is, r);
    }

    /**
     * Transform XML data
     * 
     * @param is
     *            the input XML data
     * @param ss
     *            an input stylesheet
     * @param r
     *            the result
     * @throws IOException
     */
    public static void transform(InputStream is, InputStream ss, OutputStream r) throws IOException {
        try {
            transform(new StreamSource(is), new StreamSource(ss), new StreamResult(r));
        } catch (TransformerException e) {
            IOException ix = new IOException(e.getMessage());
            ix.initCause(e);
            throw ix;
        }
    }

    /**
     * Convert a DOM {@link NodeList} to a {@link List}<{@link Node}>.
     * 
     * @param nodeList
     *            the NodeList
     * @return a List of Nodes
     */
    public static List<Node> toList(final NodeList nodeList) {
        return new AbstractList<Node>() {
            public Node get(int i) {
                return nodeList.item(i);
            }

            public int size() {
                return nodeList.getLength();
            }
        };
    }

    /**
     * Convert a DOM {@link NodeList} to a {@link List}<{@link Element}>.
     * Does an unsafe cast from Node to Element for each list item.
     * 
     * @param nodeList
     *            the NodeList
     * @return a List of Nodes
     */
    public static List<Element> toElementList(final NodeList nodeList) {
        return new AbstractList<Element>() {
            public Element get(int i) {
                return (Element) (nodeList.item(i));
            }

            public int size() {
                return nodeList.getLength();
            }
        };
    }

    // dom and the collection api's are not friendly.

    public static Element getFirstChildElementNamed(Element elt, String name) throws IOException {
        for (Node child : toList(elt.getChildNodes()) ) {
            if (child instanceof Element && ((Element) child).getTagName().equals(name)) {
                return (Element) child;
            }
        }
        throw new IOException("unexpected XML structure: no child named " + name);
    }

    public static List<Element> getChildElementsNamed(Element elt, String name) {
        List<Element> result = new LinkedList<Element>();
        for (Node child : toList(elt.getChildNodes()) ) {
            if (child instanceof Element && ((Element) child).getTagName().equals(name)) {
                result.add((Element) child);
            }
        }
        return result;
    }

    public static int countChildElementsNamed(Element elt, String name) {
        return getChildElementsNamed(elt, name).size();
    }

    /**
     * Get the String value of the Nodes in a List of Nodes.
     * 
     * @param nodes
     *            the XML nodes from which to retrieve text
     * @return a list of extracted strings
     */
    public static List<String> getNodeValues(List<Node> nodes) {
        List<String> result = new LinkedList<String>();
        for (Node node : nodes ) {
            result.add((String) node.getNodeValue());
        }
        return result;
    }

    /**
     * Evaluate an XPath expression against a Node.
     * 
     * @param expression
     *            the expression
     * @param node
     *            the node
     * @param ns
     *            the NamespaceContext, or null if none
     * @return a list of matching nodes
     * @throws XPathExpressionException
     */
    public static List<Node> evaluateXPath(String expression, Node node, NamespaceContext ns) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        if (ns != null) {
            xPath.setNamespaceContext(ns);
        }
        return toList((NodeList) xPath.evaluate(expression, node, XPathConstants.NODESET));
    }

    /**
     * Evaluate an XPath expression against a node in a default namespace
     * context.
     * 
     * @param expression
     *            the expression
     * @param node
     *            the node
     * @return a list of matching nodes
     * @throws XPathExpressionException
     */
    public static List<Node> evaluateXPath(String expression, Node node) throws XPathExpressionException {
        return evaluateXPath(expression, node, null);
    }

    /**
     * Evaluate an XPath expression against a document.
     * 
     * @param expression
     *            the expression
     * @param doc
     *            the document
     * @param ns
     *            the NamespaceContext, or null if none
     * @return a list of matching nodes
     * @throws XPathExpressionException
     */
    public static List<Node> evaluateXPath(String expression, Document doc, NamespaceContext ns) throws XPathExpressionException {
        return evaluateXPath(expression, doc.getDocumentElement(), ns);
    }

    /**
     * Evaluate an XPath expression against a document in a default namespace
     * context.
     * 
     * @param expression
     *            the expression
     * @param doc
     *            the document
     * @return a list of matching nodes
     * @throws XPathExpressionException
     */
    public static List<Node> evaluateXPath(String expression, Document doc) throws XPathExpressionException {
        return evaluateXPath(expression, doc.getDocumentElement(), null);
    }

    /**
     * Add an empty child element (you can add children to it later)
     * 
     * @param parent
     *            the parent
     * @param name
     *            the tag name of the element
     * @return the element that was added
     */
    public static Element addChild(Node parent, String name) {
        Document ownerDocument = null;
        if (parent instanceof Document) {
            ownerDocument = (Document) parent;
        } else {
            ownerDocument = parent.getOwnerDocument();
        }
        Element child = ownerDocument.createElement(name);
        parent.appendChild(child);
        return child;
    }

    /**
     * Add a child element with a single text child.
     * 
     * @param parent
     *            the parent
     * @param name
     *            the tag name of the element
     * @param value
     *            the contents of the element
     * @return the element that was added
     */
    public static Element addChild(Node parent, String name, String value) {
        Element child = addChild(parent, name);
        child.appendChild(child.getOwnerDocument().createTextNode(value));
        return child;
    }

    /**
     * Get a {@link NamespaceContext} based on the given Map between
     * prefixes and URI's.
     * 
     * @param map
     *            a map between prefixes and URI's
     * @return the corresponding NamespaceContext
     */
    public static NamespaceContext newNamespaceContext(final Map<String, String> map) {
        return new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                return map.get(prefix);
            }

            public Iterator<String> getPrefixes(String namespaceUri) {
                Set<String> prefixes = new HashSet<String>();
                for (Map.Entry<String, String> entry : map.entrySet() ) {
                    if (entry.getValue().equals(namespaceUri)) {
                        prefixes.add(entry.getKey());
                    }
                }
                return prefixes.iterator();
            }

            public String getPrefix(String namespaceUri) {
                Iterator<String> it = getPrefixes(namespaceUri);
                if (it.hasNext()) {
                    return it.next();
                } else {
                    return null;
                }
            }
        };
    }

    /**
     * Get a {@link NamespaceContext} based on the given prefix/uri binding.
     * 
     * @param prefix
     *            the prefix
     * @param namespaceUri
     *            the uri
     * @return a NamespaceContext with the given mapping in it
     */
    public static NamespaceContext newNamespaceContext(String prefix, String namespaceUri) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(prefix, namespaceUri);
        return newNamespaceContext(map);
    }

    private static SAXParserFactory theSaxParserFactory = null;

    // SAX

    /**
     * Return a SAX parser
     * 
     * @return a SAX parser
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public static XMLReader newXmlReader() throws ParserConfigurationException, SAXException {
        if (theSaxParserFactory == null) {
            theSaxParserFactory = SAXParserFactory.newInstance();
            theSaxParserFactory.setNamespaceAware(true);
        }
        return theSaxParserFactory.newSAXParser().getXMLReader();
    }

    /**
     * escape XML entities
     */
    public static String escape(String input) {
        StringWriter sw = new StringWriter();
        StringReader sr = new StringReader(input);
        try {
            int c = 0;
            while ((c = sr.read()) != -1) {
                switch (c) {
                    case '&':
                        sw.write("&amp;");
                        break;
                    case '<':
                        sw.write("&lt;");
                        break;
                    case '>':
                        sw.write("&gt;");
                        break;
                    case '\"':
                        sw.write("&quot;");
                        break;
                    case '\'':
                        sw.write("&apos;");
                        break;
                    default:
                        sw.write(c);
                        break;
                }
            }
        } catch (IOException x) {
            // what??!?!
        }
        return sw.toString();
    }
}
