/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TupleTest {
    @Test public void testAll() throws Exception {
	Tuple<String> duple = new Tuple("foo","bar");
	assertTrue("order is wrong",duple.getOrder()==2);
	Tuple<String> copy = new Tuple("foo","bar");
	Tuple<String> different = new Tuple("bar","foo");
	assertTrue("comparing fails",duple.equals(copy));
	assertTrue("comparing fails",!duple.equals(different));
	duple.setOrder(3);
	duple.set(2,"baz");
	Tuple<String> fbb = new Tuple("foo","bar","baz");
	assertTrue("comparing fails",duple.equals(fbb));
    }

    @Test public void testList() throws Exception {
	List<String> foo = new LinkedList<String>();
	foo.add("foo");
	foo.add("bar");
	assertTrue("not equal",foo.equals(new Tuple("foo","bar")));
	assertTrue("not equal",(new Tuple("foo","bar")).equals(foo));
    }

    @Test public void testSet() {
	Set one = new HashSet<Tuple<String>>();
	Set two = new HashSet<Tuple<String>>();
	one.add(new Tuple("foo","bar"));
	one.add(new Tuple("baz", "quux"));
	two.add(new Tuple("foo","bar"));
	two.add(new Tuple("baz", "quux"));
	assertTrue("not equal",one.equals(two));
	two.add(new Tuple("snip","fnerb"));
	assertTrue("comparing fails",!one.equals(two));
    }

    @Test public void testListConstructor() {
	List<String> fooList = new LinkedList<String>();
	fooList.add("hello");
	fooList.add("world");
	Tuple<String> fooTuple = new Tuple("hello","world");
	assertTrue("failed",fooTuple.equals(new Tuple(fooList)));
    }

    @Test public void testCompareTo() {
	TreeSet list = new TreeSet<Tuple<String>>();
	Tuple<String> one   = new Tuple("abc","abc","def");
	Tuple<String> two   = new Tuple("abc","def","ghi");
	Tuple<String> three = new Tuple("foo","bar","baz");
	Tuple<String> four  = new Tuple("foo","bar","bbz");
	list.add(three);
	list.add(two);
	list.add(one);
	list.add(four);
	Iterator<Tuple<String>> it = list.iterator();
	assertTrue(it.next().equals(one));
	assertTrue(it.next().equals(two));
	assertTrue(it.next().equals(three));
	assertTrue(it.next().equals(four));
    }

    @Test
    public void testAdditional() {
        Tuple<String> t = new Tuple<String>("fish","ducks");
        Tuple<String> ta = new Tuple<String>("jelly rolls");
        Tuple<String> t2 = Tuple.append(t,ta);
        assert t2.equals(new Tuple<String>("fish","ducks","jelly rolls"));
    }
}
