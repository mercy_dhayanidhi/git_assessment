/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class SecureHashTest {
    @Test
    public void testSha1() throws Exception {
        Map<String, String> shaMap = new HashMap<String, String>();
        shaMap.put("something to hash", "72668bc961b0a78bfa1633f6141bcea69ca37468");
        shaMap.put("another thing to hash", "d4fbf104f45d2b0284db9e768e836ce933f8d494");
        shaMap.put("guess what to do with this string", "fd69d91e6fea6bdfbee0d724b7f648b1033414d2");
        for (Map.Entry<String, String> entry : shaMap.entrySet() ) {
            assertTrue("bad hash value", SecureHash.digest(entry.getKey()).equals(entry.getValue()));
        }
    }

    @Test
    public void testMd5() throws Exception {
        Map<String, String> shaMap = new HashMap<String, String>();
        shaMap.put("something to hash", "6f4815fdf1f1fd3f36ac295bf39d26b4");
        shaMap.put("another thing to hash", "a2ee7220f71cde825987e150e437a569");
        shaMap.put("guess what to do with this string", "fefd674824325d37fd00f4039b750281");
        SecureHash sh = new SecureHash();
        sh.setDigestAlgorithm("MD5");
        for (Map.Entry<String, String> entry : shaMap.entrySet() ) {
            String key = entry.getKey();
            String value = entry.getValue();
            String md5 = sh.getDigest(key);
            assertTrue(String.format("bad hash value: %s => %s != %s", key, md5, value), sh.getDigest(key).equals(value));
        }
    }

    @Test
    public void testCopy() throws Exception {
        File a = File.createTempFile("tupelo-util-", ".bin");
        File b = File.createTempFile("tupelo-util-", ".bin");
        File c = File.createTempFile("tupelo-util-", ".bin");
        try {
            String s = "This is some crazy stuff.";
            byte[] bytes = s.getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(a);
            fos.write(bytes);
            fos.flush();
            fos.close();
            FileInputStream fis = new FileInputStream(a);
            fos = new FileOutputStream(b);
            String hash1 = SecureHash.digestCopyAndHex(fis, fos);
            fis.close();
            fos.close();
            fis = new FileInputStream(b);
            fos = new FileOutputStream(c);
            String hash2 = SecureHash.digestCopyAndHex(fis, fos);
            assert hash1.equals(hash2);
        } finally {
            a.delete();
            b.delete();
            c.delete();
        }
    }

    @Test
    public void testInputStreams() throws Exception {
        String s = "Hello there.";
        InputStream is = new ByteArrayInputStream(s.getBytes("US-ASCII"));
        assert SecureHash.digest(s).equals(SecureHash.digest(is));
    }
}
