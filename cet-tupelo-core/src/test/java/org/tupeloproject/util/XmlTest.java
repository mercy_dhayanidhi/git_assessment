/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class XmlTest {
    @Test
    public void testNewDocument() throws Exception {
        Document doc = Xml.newDocument();
        assertTrue("failed to get document", doc != null);
    }

    @Test
    public void testNamespaceContext() throws Exception {
        assertTrue("nope", Xml.newNamespaceContext("foo", "http://bar").getNamespaceURI("foo").equals("http://bar"));
        assertTrue("nope", Xml.newNamespaceContext("foo", "http://bar").getPrefix("http://bar").equals("foo"));
        assertTrue("nope", Xml.newNamespaceContext("foo", "http://bar").getPrefixes("http://bar").hasNext());
    }

    @Test
    public void testParse() throws Exception {
        String xml = "<?xml version='1.0'?><foo/>";
        Document doc = Xml.parse(new StringReader(xml));
        assertTrue("failed to parse", doc != null);
        String badXml = "<?xml version='1.0'?><fo..345978r.<>><><><<>><><><";
        try {
            Xml.parse(new StringReader(badXml));
            fail("parse should have failed");
        } catch (Exception x) {
            // fall through and succeed
        }
    }

    @Test
    public void testXpath() throws Exception {
        String xml = "<?xml version='1.0'?><foo><bar><baz>quux</baz></bar><baz>fnord</baz></foo>";
        Document doc = Xml.parse(new StringReader(xml));
        List<Node> result = Xml.evaluateXPath("//baz/text()", doc);
        List<String> sResult = Xml.getNodeValues(result);
        assertTrue("bad", sResult.equals(new Tuple("quux", "fnord")));
        // now break it with namespaces
        String xml2 = "<?xml version='1.0'?>" + "<foo xmlns='http://foo.bar'>" + "  <bar baz='quux'/>" + "</foo>";
        doc = Xml.parse(new StringReader(xml2));
        NamespaceContext c = Xml.newNamespaceContext("ns", "http://foo.bar");
        result = Xml.evaluateXPath("/ns:foo/ns:bar", doc, c);
        assertTrue("no results", result.size() == 1);
        result = Xml.evaluateXPath("/ns:foo", doc, c);
        Node n = result.get(0);
        result = Xml.evaluateXPath("ns:bar/@baz", n, c);
        assertTrue("no results", result.size() == 1);
    }

    @Test
    public void testSparqlXml() throws Exception {
        Document doc = Xml.parse(getClass().getResourceAsStream("/xml/sparql-result.xml"));
        NamespaceContext ns = Xml.newNamespaceContext("s", "http://www.w3.org/2005/sparql-results#");
        List<Node> x = Xml.evaluateXPath("/s:sparql/s:head", doc, ns);
        assertTrue("no head", x.size() == 1);
        Node head = x.get(0);
        List<String> variableNames = Xml.getNodeValues(Xml.evaluateXPath("s:variable/@name", head, ns));
        assertTrue("wrong variable names", variableNames.size() == 2);
        assertTrue("wrong variable name", variableNames.get(0).equals("p"));
        assertTrue("wrong variable name", variableNames.get(1).equals("o"));
        // try a different way
        x = Xml.evaluateXPath("/s:sparql/s:head/s:variable/@name", doc, ns);
        variableNames = Xml.getNodeValues(x);
        assertTrue("wrong variable names", variableNames.size() == 2);
        assertTrue("wrong variable name", variableNames.get(0).equals("p"));
        assertTrue("wrong variable name", variableNames.get(1).equals("o"));
    }

    @Test
    public void testConcurrency() throws Exception {
        final Exception fail[] = new Exception[] { null };

        Runnable r = new Runnable() {
            public void run() {
                try {
                    for (int i = 0; i < 200; i++ ) {
                        Document doc = Xml.parse(getClass().getResourceAsStream("/xml/sparql-result.xml"));
                    }
                } catch (Exception x) {
                    fail[0] = x;
                }
            }
        };
        int n = 20;
        Thread t[] = new Thread[n];
        for (int i = 0; i < n; i++ ) {
            t[i] = new Thread(r);
            t[i].start();
        }
        for (int i = 0; i < n; i++ ) {
            t[i].join();
        }
        if (fail[0] != null) {
            throw (fail[0]);
        }
    }

    @Test
    public void testSax() throws Exception {
        //                Yielder<String> y = new Yielder<String>() {
        //                    public void run() {
        //                        try {
        //                            XMLReader reader = Xml.newXmlReader();
        //                            reader.setContentHandler(new DefaultHandler() {
        //                                public void startElement(String u, String ln, String qn, Attributes a) {
        //                                    yield(ln);
        //                                }
        //        
        //                                public void endDocument() {
        //                                    close();
        //                                }
        //                            });
        //                            reader.parse(new InputSource(getClass().getResourceAsStream("/xml/sparql-result.xml")));
        //                        } catch (Exception x) {
        //                            // do nothing
        //                            x.printStackTrace();
        //                            close();
        //                        }
        //                    }
        //                };
        //                Iterator<String> i = y.iterator();
        //                String elementNames[] = new String[] { "sparql", "head", "variable", "variable", "results", "result", "binding", "uri", "binding", "uri", "result", "binding", "uri", "binding", "uri", "result", "binding", "uri", "binding", "literal" };
        //                for (String elementName : elementNames ) {
        //                    assertTrue(i.hasNext());
        //                    String next = i.next();
        //                    assertEquals(next + "!=" + elementName, next, elementName);
        //                }
        //                assertTrue(!i.hasNext());
    }

    @Test
    public void testTransform() throws Exception {
        InputStream xml = getClass().getResourceAsStream("/xml/sparql-result.xml");
        InputStream xsl = getClass().getResourceAsStream("/xml/sparql-style.xsl");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Xml.transform(xml, xsl, baos);
        InputStream expected = getClass().getResourceAsStream("/xml/sparql-xf-output.txt");
        InputStream got = new ByteArrayInputStream(baos.toByteArray());
        while (true) {
            int c = expected.read();
            if (c == -1) {
                break;
            }
            int r = got.read();
            if (r == '\r') { // ignore Windoze line breaks
                got.read();
            } else {
                assertTrue(r == c);
            }
        }
        URIResolver resolver = new URIResolver() {
            public Source resolve(String href, String baseUri) throws TransformerException {
                return new StreamSource(getClass().getResourceAsStream("/xml/" + href));
            }
        };
        baos = new ByteArrayOutputStream();
        Source xmlSource = resolver.resolve("sparql-result.xml", null);
        Source xslSource = resolver.resolve("include-test.xsl", null);
        Xml.transform(xmlSource, xslSource, new StreamResult(baos), resolver);
        expected = getClass().getResourceAsStream("/xml/sparql-xf-output.txt");
        got = new ByteArrayInputStream(baos.toByteArray());
        // here comes a tile.
        while (true) {
            int c = expected.read();
            if (c == -1) {
                break;
            }
            int r = got.read();
            if (r == '\r') { // ignore Windoze line breaks
                got.read();
            } else {
                assertTrue(r == c);
            }
        }
        //DOMSource ds = new DOMSource(Xml.parse(getClass().getResourceAsStream("/xml/xmlliteral.xml")));
        //StreamSource ss = new StreamSource(getClass().getResourceAsStream("/xml/xmlliteral.xsl"));
        //Xml.transform(ds, ss, new StreamResult(Streams.out));
    }

    @Test
    public void testEscape() throws Exception {
        String foo = "this is some >> wacky << stuff & whatever 'you' \"say\"";
        String escaped = Xml.escape(foo);
        assertEquals("bad escape", "this is some &gt;&gt; wacky &lt;&lt; stuff &amp; whatever &apos;you&apos; &quot;say&quot;", escaped);
    }
}
