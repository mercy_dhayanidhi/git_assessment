/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import static org.tupeloproject.util.Iso8601.date2String;
import static org.tupeloproject.util.Iso8601.string2Date;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

public class Iso8601Test {
    /**
     * Testing for the contract, which is<br>
     * <br>
     * -- date to string: returns 3 digits, converts to UTC and ends with a 'Z'
     * as shorthand for GMT<br>
     * -- string to date: accepts 0 - 3 digits and time zones in the format
     * +-xxxx or +- xx:xx, including half hour<br>
     * <br>
     * -- therefore roundtripping only works partially. string to date then date
     * to string will return an equivalent
     * date in UTC. date to string then string to date will round trip exactly.
     * 
     * @throws Exception
     */
    @Test
    public void testString2Dates() throws Exception {
        // test failure modes
        try {
            // complete bad date.
            System.out.println(Iso8601.string2Date("I'm a bad date"));
            assert false;
        } catch (ParseException px) {
            assert true;
        }
        try {
            // check that bad seconds are identified.
            System.out.println(Iso8601.string2Date("1999-09-13T12:13:44.abc+0000"));
            assert false;
        } catch (ParseException px) {
            assert true;
        }
        try {
            // check that a bad time zone is identified.
            // NOTE there is no reason that weird time zones cannot be used, e.g. +02:34. These
            // are allowed by the date format.
            System.out.println(Iso8601.string2Date("1999-09-13T12:13:44.333+000q"));
            assert false;
        } catch (ParseException px) {
            assert true;
        }

        String[] dateArray = { "1999-09-13T12:13:13.374Z", // the date moonbase alpha left orbit...
                "1999-09-13T12:13:44.321993+00:00", // check parsing time zone = GMT
                "1999-09-13T12:13:44.321+0000", // ditto, no colon
                "1999-09-13T12:13:44.321+06:00", // parse tz w/ colon
                "1999-09-13T12:13:44.321+0600", // ditto, no colon
                "1999-09-13T12:13:44-04:00", // time with 0 digits ms
                "1999-09-13T12:13:44.3-04:00", // time with 1 digits ms
                "1999-09-13T12:13:44.37-04:00", // time with 2 digits ms
                "1999-09-13T12:13:44.374-04:00", // time with 3 digits ms
                "1999-09-13T12:13:44.789-04:30", // time with half hour time zone back
                "1999-09-13T12:13:44.789+04:30" // time with half hour time zone forward
        };
        // note that the above times are in one to one corresondence with the longs in the next array!
        // if you change these, you must change the corresponding long too.
        long[] dateLongs = new long[] { 937224793374L, 937224824321L, 937224824321L, 937203224321L, 937203224321L, 937239224000L, 937239224300L, 937239224370L, 937239224374L, 937241024789L, 937208624789L };
        int counter = 0;
        for (String x : dateArray ) {
            assert Iso8601.string2Date(x).getTimeInMillis() == dateLongs[counter] : "failed for date = x" + x + ", expected ms=" + dateLongs[counter];
            counter++;
        }
    }

    @Test
    public void testDate2String() throws Exception {
        String[] dateArray = { "1970-01-01T00:00:00.000Z", "1999-09-13T12:13:13.374Z", "1998-10-11T01:50:05.040Z", "2002-05-02T01:47:30.607Z", "2033-05-18T03:33:20.123Z", "1999-09-13T12:13:44.321Z", "1999-09-13T16:13:44.000Z", "1999-09-13T16:13:44.300Z", "1999-09-13T16:13:44.370Z", "1999-09-13T16:13:44.374Z", "1999-09-13T16:43:44.789Z", "1999-09-13T07:43:44.789Z" };
        // note that the above times are in one to one corresondence with the longs in the next array!
        // if you change these, you must change the corresponding long too.
        long[] dateLongs = new long[] { 0L, 937224793374L, 908070605040L, 1020304050607L, 2000000000123L, 937224824321L, 937239224000L, 937239224300L, 937239224370L, 937239224374L, 937241024789L, 937208624789L };
        Date[] dates = new Date[dateLongs.length];
        int counter = 0;
        // fill up an array of dates, checking that everything converts as it should.
        for (long x : dateLongs ) {
            Date tempDate = new Date();
            tempDate.setTime(x);
            dates[counter] = tempDate;
            assert Iso8601.date2String(tempDate).equals(dateArray[counter]) : "failed to match for date = " + dateArray[counter] + " and ms =" + x;
            counter++;
        }
        counter = 0;
        // now we round trip date -> string -> date
        for (Date date : dates ) {
            assert Iso8601.string2Date(Iso8601.date2String(date)).getTimeInMillis() == dateLongs[counter++];
        }
        counter = 0;
        // check that the dates are converted correctly for the call using milliseconds
        for (long x : dateLongs ) {
            assert Iso8601.date2String(x).equals(dateArray[counter]) : "failed to match for date = " + dateArray[counter] + " and ms =" + x;
            counter++;
        }
    }

    /**
     * This tests string -- date -- string roundtripping. Since everything ends
     * up in UTC, these don't
     * work exactly. What this test is doing is seeing that the information is
     * preserved rather than
     * using string comparisons for the formatted dates.
     * 
     * @throws Exception
     */
    @Test
    public void testString2StringRoundtrip() throws Exception {
        String[] dateArray = { "2008-04-15T12:13:13.374Z", "2007-09-13T12:13:44+01:30", "2006-09-13T12:13:44.321-0430", "2005-09-13T12:13:44.321+06:00", "1999-09-13T12:13:44.321+0630", "2000-09-13T12:13:44-04:00", "2005-09-13T12:13:44.3-04:00", "2010-09-13T12:13:44.37-04:00", "2015-09-13T12:13:44.374-04:00" };
        String[] checkDates = { "2008-04-15T12:13:13.374Z", "2007-09-13T10:43:44.000Z", "2006-09-13T16:43:44.321Z", "2005-09-13T06:13:44.321Z", "1999-09-13T05:43:44.321Z", "2000-09-13T16:13:44.000Z", "2005-09-13T16:13:44.300Z", "2010-09-13T16:13:44.370Z", "2015-09-13T16:13:44.374Z" };
        int counter = 0;
        for (String x : dateArray ) {
            assert Iso8601.date2String(Iso8601.string2Date(x)).equals(checkDates[counter]);
            counter++;

        }
    }

    // Joe's test, slightly reworked just as a double check we don't lose ground.
    @Test
    public void testAll() throws Exception {
        String dates[] = new String[] { "1987-03-14T12:13:44.374Z", "2149-03-14T12:13:44.374-05:30", "2249-03-14T12:13:44.374+04:00" };
        String expectedDates[] = new String[] { "1987-03-14T12:13:44.374Z", // so no change
                "2149-03-14T17:43:44.374Z", "2249-03-14T08:13:44.374Z" };
        int counter = 0;
        for (String date : dates ) {
            //expected = date;
            String got = date2String(string2Date(date));
            assert got.equals(expectedDates[counter]) : "got " + got + " != expected " + expectedDates[counter];
            counter++;
        }
    }
}
