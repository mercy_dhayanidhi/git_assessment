/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.util.Iterator;
import java.util.LinkedList;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ListTableTest {
    @Test public void testVarArgs() throws Exception {
	ListTable<String> table = new ListTable<String>("a","b");
	table.addRow("foo","bar");
	table.addRow("baz","quux");
	Iterator<Tuple<String>> it = table.iterator();
	assertTrue(table.getColumnNames().equals(new Tuple("a","b")));
	assertTrue(Iterators.asList(it).equals
		   (new Tuple<Tuple<String>>
		    (new Tuple("foo","bar"),
		     new Tuple("baz","quux"))));
	// that assertion works because Tuples are Lists
    }

    @Test public void testBean() throws Exception {
	ListTable<Integer> table = new ListTable<Integer>();
	table.setColumnNames(new Tuple("a","b"));
	table.setRows(new LinkedList<Tuple<Integer>>());
	table.getRows().add(new Tuple<Integer>(3,4));
	table.getRows().add(new Tuple<Integer>(-93,7));
	Iterator<Tuple<Integer>> it = table.iterator();
	assertTrue(table.getColumnNames().equals(new Tuple("a","b")));
	assertTrue(Iterators.asList(it).equals
		   (new Tuple<Tuple<Integer>>
		    (new Tuple(3,4),
		     new Tuple(-93,7))));
	// that assertion works because Tuples are Lists
    }

    @Test
    public void testConstructor() throws Exception {
        ListTable<Integer> t1 = new ListTable<Integer>("foo","bar");
        for(int i = 0; i < 3; i++) {
            t1.addRow(i,i*27);
        }
        ListTable<Integer> t2 = new ListTable<Integer>(t1);
        assert t2.getColumnNames().equals(t1.getColumnNames());
        int i = 0;
        for(Tuple<Integer> row : t2) {
            assert row.get(0)==i;
            assert row.get(1)==i*27;
            i++;
        }
    }
}
