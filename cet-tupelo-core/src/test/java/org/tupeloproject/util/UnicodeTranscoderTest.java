/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.text.ParseException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * UnicodeTranscoderTest
 */
public class UnicodeTranscoderTest {
    @Test public void testAll() throws Exception {
        int low = 0x2101; // "2/3" fraction
        int high = 0x10080; // linear B "man" ideograph
        StringBuffer origWriter = new StringBuffer();
        origWriter.append("a\rb\tc\nd\\e\"f");
        origWriter.appendCodePoint(low);
        origWriter.append("g");
        origWriter.append(Character.toChars(high));
        origWriter.append("h");
        String orig = origWriter.toString();
        String encoded = UnicodeTranscoder.encode(orig);
        assertTrue("wrong encoding", encoded.equals("a\\rb\\tc\\nd\\\\e\\\"f\\u2101g\\U00010080h"));
        String decoded = UnicodeTranscoder.decode(encoded);
        assertTrue("roundtrip failed", decoded.equals(orig));
        String badStrings[] = new String[]{
                "\\u8&Fcmn",
                "fish\\u03",
                "\\UFFFFFFFF",
                "\\zounds",
                "terminating in a \\"
        };
        for (String badString : badStrings) {
            try {
                UnicodeTranscoder.decode(badString);
                fail("bad escape accepted: " + badString);
            } catch (ParseException x) {
                // fall through
            }
        }
    }

    @Test
    public void testWorldOfHurt() throws Exception {
        String encoded = "f\\u00ee\\u00d2\\u00f3i\\u00f2\\u00ce\\u00cd\\u00e6Ec\\u00f6\\u00d1Fp\\u00d0";
        String decoded = UnicodeTranscoder.decode(encoded);
        String reencoded = UnicodeTranscoder.encode(decoded);
        assert reencoded.equals(encoded);
    }
}
