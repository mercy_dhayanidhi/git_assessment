/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.util;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TagUriFactoryTest {
    @Test public void testAll() throws Exception {
	TagUriFactory tuf = new TagUriFactory();
	tuf.setAuthorityName("schneertz.com");
	tuf.setDate("1996");
	assertTrue("mismatch",tuf.newUri("/blah").equals(URI.create("tag:schneertz.com,1996:/blah")));
    }

    @Test public void testSpp() throws Exception {
	TagUriFactory tuf = new TagUriFactory("futrelle@uiuc.edu","1997","/foo");
	assertTrue("mismatch",tuf.newUri("/bar").equals(URI.create("tag:futrelle@uiuc.edu,1997:/foo/bar")));
    }

    @Test public void testUnique() throws Exception {
	Set<URI> soFar = new HashSet<URI>();
	TagUriFactory tuf = new TagUriFactory();
	tuf.setAuthorityName("futrelle@shout.net");
	tuf.setDate("2001");
	// since it is impossible to test this in a reasonable amount
	// of wall clock time, just mint 10,000 URI's to make sure the
	// minting algorithm isn't horribly broken.
	for(int i = 0; i < 10000; i++) {
	    URI uri = tuf.newUri();
	    assertTrue("duplicate uri generated: "+uri,!soFar.contains(uri));
	    soFar.add(uri);
	}
    }
}
