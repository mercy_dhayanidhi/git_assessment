/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.junit.Test;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.rdf.Resource;

public class HttpContextTest {
    @Test
    public void testBlobs() throws Exception {
        BlobContext context = new HttpContext();
        Resource key = Resource.uriRef("http://www.ncsa.illinois.edu/");
        BlobFetcher bf = new BlobFetcher();
        bf.setSubject(key);
        context.perform(bf);
        BufferedReader br = new BufferedReader(new InputStreamReader(bf.getInputStream()));
        String line;
        boolean success = false;
        while ((line = br.readLine()) != null) {
            if (line.contains("NCSA Home")) {
                success = true;
            }
        }
        br.close();
        assertTrue("this isn't www.ncsa.illinois.edu", success);
    }
}
