/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.tupeloproject.rdf.Resource.literal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.SubjectRemover;
import org.tupeloproject.kernel.operator.Transformer;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.BlankNode;
import org.tupeloproject.rdf.Literal;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.Pattern;
import org.tupeloproject.rdf.terms.Cet;
import org.tupeloproject.rdf.terms.Dc;
import org.tupeloproject.rdf.terms.Foaf;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.rdf.terms.Xsd;
import org.tupeloproject.util.Iterators;
import org.tupeloproject.util.Table;
import org.tupeloproject.util.Tuple;

/**
 * This junit TestCase provides a set of tests for Tupelo's core
 * Triple and Blob operations. Subclasses need only implement
 * {@link #getContext()}; they should return a Context with no data in
 * it. GenericContextTest attempts to acquire all operators and run
 * tests on each one.
 */
public abstract class AbstractTripleContextTest {
    public static String TAG_PREFIX = "tag:tupeloproject.org,2004:/test/";

    protected Resource test(String suffix) {
        return Resource.uriRef(TAG_PREFIX + suffix);
    }

    /**
     * Return a Context implementation to test. Implementations MUST
     * NOT return a Context containing any data or else some tests
     * will fail.
     * 
     * @return a blank Context
     */
    protected abstract TripleContext getContext() throws Exception;

    /**
     * close a context. does nothing if c is null
     * 
     * @throws OperatorException
     */
    protected void closeContext(TripleContext c) throws OperatorException {
        if (c != null) {
            c.close();
        }
    }

    protected Set<Triple> getTriples() {
        Triple t2 = new Triple(test("baz"), test("quux"), "baz");
        Triple t4 = new Triple(test("foo"), test("bar"), "baz");
        Triple t1 = new Triple(test("foo"), test("bar"), "fish");
        Triple t3 = new Triple(test("foo"), test("buz"), "fnip");
        Triple t5 = new Triple(test("foo"), test("quux"), "frog");
        Triple t6 = new Triple(test("fnord"), test("quux"), "frog");
        Triple t7 = new Triple(test("this"), test("has"), test("uriobj"));
        Set<Triple> triples = new HashSet<Triple>();
        triples.add(t1);
        triples.add(t2);
        triples.add(t3);
        triples.add(t4);
        triples.add(t5);
        triples.add(t6);
        triples.add(t7);
        return triples;
    }

    protected Set<Triple> getTypedLiteralTriples() {
        Triple t0 = new Triple(test("badger"), test("hasValue"), "String");
        Triple t1 = new Triple(test("badger"), test("hasValue"), "String", Xsd.STRING);
        Triple t2 = new Triple(test("badger"), test("hasValue"), "1");
        Triple t3 = new Triple(test("badger"), test("hasValue"), "1", Xsd.INT);
        Triple t4 = new Triple(test("badger"), test("hasValue"), "1", Xsd.POSITIVE_INTEGER);
        Triple t5 = new Triple(test("badger"), test("hasValue"), "1", Xsd.STRING);
        Set<Triple> triples = new HashSet<Triple>();
        triples.add(t0);
        triples.add(t1);
        triples.add(t2);
        triples.add(t3);
        triples.add(t4);
        triples.add(t5);
        return triples;
    }

    protected void writeTriples(TripleContext context, Set<Triple> triples) throws Exception {
        TripleWriter tw = new TripleWriter();
        tw.addAll(triples);
        context.perform(tw);
    }

    protected void removeTriples(TripleContext context, Set<Triple> triples) throws Exception {
        TripleWriter tw = new TripleWriter();
        tw.removeAll(triples);
        context.perform(tw);
    }

    @Test
    public void testHash() throws Exception {
        TripleContext context = getContext();
        try {
            List<Triple> test = new ArrayList<Triple>();
            test.add(new Triple(test("foo"), test("bang"), Resource.literal("Aa")));
            test.add(new Triple(test("foo"), test("bang"), Resource.literal("BB")));
            context.addTriples(test);

            TripleIterator ti = new TripleIterator();
            context.perform(ti);

            List<Triple> list = Iterators.asList(ti);
            for (Triple t : test ) {
                assertTrue("did not find triple", list.contains(t));
                list.remove(t);
            }
            assertEquals("too many triples.", 0, list.size());
        } finally {
            closeContext(context);
        }
    }

    @Test
    public void testTripleWriter() throws Exception {
        TripleContext context = getContext();
        try {
            Set<Triple> triples = getTriples();
            // add a torture test for literals
            Triple tortureTest = new Triple(test("foo"), test("bang"), "\"\'`~!@#$%^&*()_+-=[]{};:,./<>?");
            triples.add(tortureTest);
            // build an index
            Map<Resource, Set<Triple>> subjectIx = new HashMap<Resource, Set<Triple>>();
            for (Triple t : triples ) {
                Resource subject = t.getSubject();
                Set<Triple> ts = subjectIx.get(subject);
                if (ts == null) {
                    ts = new HashSet<Triple>();
                    subjectIx.put(subject, ts);
                }
                ts.add(t);
            }
            // write triples
            writeTriples(context, triples);

            // verify that we added them
            for (Resource subject : subjectIx.keySet() ) {
                Set<Triple> expected = subjectIx.get(subject);
                TripleMatcher tm = new TripleMatcher();
                tm.setSubject(subject);
                context.perform(tm);
                assertTrue(tm.getResult().equals(expected));
            }

            // verify delete
            TripleWriter tw = new TripleWriter();
            tw.remove(tortureTest);
            context.perform(tw);
            TripleMatcher tm = new TripleMatcher();
            tm.setSubject(tortureTest.getSubject());
            context.perform(tm);
            assertTrue(!tm.getResult().contains(tortureTest));
        } finally {
            closeContext(context);
        }
    }

    @Test
    public void testDelete() throws Exception {
        TripleContext context = getContext();
        try {
            Resource s = Resource.uriRef();
            Triple t = new Triple(s, Rdf.TYPE, Cet.DATASET);
            TripleWriter tw = new TripleWriter();
            tw.add(t);
            context.perform(tw);
            assertTrue(context.getTriples().size() == 1);
            tw = new TripleWriter();
            tw.remove(t);
            context.perform(tw);
            assertTrue(context.getTriples().size() == 0);
        } finally {
            closeContext(context);
        }
    }

    /*
        @Test
        public void testTripleFetcher() throws Exception {
            Context context = getContext();
            try {
                writeTriples(context, getTriples());
                //context.addTriples(getTriples());
                TripleFetcher tf = new TripleFetcher();
                tf.setSubject(test("foo"));
                context.perform(tf);
                Set foos = tf.getResult();
                assertTrue(String.format("7: wrong number of triples returned %s", foos.size()), foos.size() == 4);
                tf.setSubject(test("baz"));
                context.perform(tf);
                Set bazzes = tf.getResult();
                assertTrue("8: wrong number of triples returned", bazzes.size() == 1);
                tf.setSubject(test("bar"));
                context.perform(tf);
                Set bars = tf.getResult();
                assertTrue("9: wrong number of triples returned", bars.size() == 0);
            } finally {
                closeContext(context);
            }
        }
    */
    /*
        @Test
        public void testSubjectFacade() throws Exception {
            Context context = getContext();
            try {
                writeTriples(context, getTriples());
                SubjectFacade sf = context.getSubject(test("foo"));
                Set<Triple> result = sf.getTriples();
                assertTrue(result.size() == 4;
            } catch (OperatorUnavailableException x) {
                Streams.out.println("testSubjectFacade: operation unavailable");
                // fall through and succeed
            } finally {
                closeContext(context);
            }
        }
    */
    @Test
    public void testTripleMatcher() throws Exception {
        TripleContext context = getContext();
        try {
            writeTriples(context, getTriples());
            TripleMatcher matcher = new TripleMatcher();
            // matchPredicate
            matcher.match(null, test("quux"), null);
            context.perform(matcher);
            Set<Triple> preds = matcher.getResult();
            assertTrue("0: wrong number of triples returned: " + preds.size(), preds.size() == 3);
            matcher.match(null, test("not_appearing_in_this_film"), null);
            context.perform(matcher);
            preds = matcher.getResult();
            assertTrue("1: wrong number of triples returned: " + preds.size(), preds.size() == 0);
            matcher.match(null, test("quux"), null);
            context.perform(matcher);
            preds = matcher.getResult();
            assertTrue("2: wrong number of triples returned: " + preds.size(), preds.size() == 3);
            // matchObject
            matcher.match(null, null, new Literal("frog"));
            context.perform(matcher);
            Set<Triple> objs = matcher.getResult();
            assertTrue("4: wrong number of triples returned: " + objs.size(), objs.size() == 2);
            // matchSubjectPredicate
            Resource subject = test("foo");
            Resource predicate = test("bar");
            matcher.match(subject, predicate, null);
            context.perform(matcher);
            Set<Triple> subjObjs = matcher.getResult();
            assertTrue("5: wrong number of triples returned: " + subjObjs.size(), subjObjs.size() == 2);

            /*
            Set<Resource> expectedObjects = Triple.getObjectVisitor().visitAll(matcher.getResult());
            Set<Resource> actualObjects = matcher.objects();
            assertTrue("objects returned wrong set", actualObjects.equals(expectedObjects));
            // matchPredicateObject
            predicate = test("quux");
            Resource object = new Literal("frog");
            matcher.match(null, predicate, object);
            context.perform(matcher);
            Set<Triple> predObjs = matcher.getResult();
            assertTrue("6: wrong number of triples returned", predObjs.size() == 2);
            Set<Resource> expectedSubjects = Triple.getSubjectVisitor().visitAll(matcher.getResult());
            Set<Resource> actualSubjects = matcher.subjects();
            assertTrue("subjects returned wrong set", actualSubjects.equals(expectedSubjects));
            */

            // now match a uriref object
            matcher.match(null, null, test("uriobj"));
            context.perform(matcher);
            Set<Triple> uriobj = matcher.getResult();
            assertTrue("7: wrong number of triples returned", uriobj.size() == 1);
        } finally {
            closeContext(context);
        }
    }

    @Test
    public void testSubjectRemover() throws Exception {
        TripleContext context = getContext();
        try {
            // do this first in case the context doesn't support it so we can fall through
            SubjectRemover sw = new SubjectRemover();
            // first, add several triples with the same subject and one with a different subject
            Resource subject = test("yeti");
            Triple t0 = new Triple(subject, test("breed"), test("americanEskimo"));
            Triple t1 = new Triple(subject, test("furColor"), "white");
            Triple t2 = new Triple(subject, test("cutenessPercentage"), "110");
            Triple t3 = new Triple(test("hank"), test("breed"), test("mutt"));
            HashSet<Triple> triples = new HashSet<Triple>();
            triples.add(t0);
            triples.add(t1);
            triples.add(t2);
            triples.add(t3);
            writeTriples(context, triples);
            // now delete the subject
            sw.setSubject(subject);
            context.perform(sw);
            // now confirm there are no triples with that subject
            TripleMatcher f = new TripleMatcher();
            f.setSubject(subject);
            context.perform(f);
            int count = f.getResult().size();
            assertTrue("wrong triples returned: " + count + "\n" + f.getResult(), count == 0);
            f.setSubject(test("hank"));
            context.perform(f);
            assertTrue("bathwater extension", f.getResult().size() == 1);
        } finally {
            closeContext(context);
        }
    }

    @Test
    public void testTransformer() throws Exception {
        TripleContext sourceContext = getContext();
        try {
            // create model of parents and children
            Set<Triple> source = new HashSet<Triple>();
            source.add(new Triple(test("joe"), test("hasParent"), test("carolyn")));
            source.add(new Triple(test("genevieve"), test("hasParent"), test("carolyn")));
            source.add(new Triple(test("david"), test("hasParent"), test("carolyn")));
            source.add(new Triple(test("gordon"), test("hasParent"), test("jim")));
            source.add(new Triple(test("emily"), test("hasParent"), test("jim")));
            source.add(new Triple(test("carolyn"), test("hasParent"), test("zoe")));
            source.add(new Triple(test("jim"), test("hasParent"), test("zoe")));
            source.add(new Triple(test("zoe"), test("hasParent"), test("smith")));
            //sourceContext.addTriples(source);
            writeTriples(sourceContext, source);
            // now create transformer to extract grandparent relationship
            Transformer x = new Transformer();
            // patterns to unify
            x.addInPattern(new Pattern("kid", test("hasParent"), "parent"));
            x.addInPattern(new Pattern("parent", test("hasParent"), "grandparent"));
            // pattern to project
            x.addOutPattern(new Pattern("kid", test("hasGrandparent"), "grandparent"));
            // do it
            sourceContext.perform(x);
            Set<Triple> target = x.getResult();
            // make sure all the right triples are there
            assertTrue(target.contains(new Triple(test("joe"), test("hasGrandparent"), test("zoe"))));
            assertTrue(target.contains(new Triple(test("david"), test("hasGrandparent"), test("zoe"))));
            assertTrue(target.contains(new Triple(test("genevieve"), test("hasGrandparent"), test("zoe"))));
            assertTrue(target.contains(new Triple(test("emily"), test("hasGrandparent"), test("zoe"))));
            assertTrue(target.contains(new Triple(test("gordon"), test("hasGrandparent"), test("zoe"))));
            assertTrue(target.contains(new Triple(test("jim"), test("hasGrandparent"), test("smith"))));
            assertTrue(target.contains(new Triple(test("carolyn"), test("hasGrandparent"), test("smith"))));
            assertTrue(target.size() == 7);
        } finally {
            closeContext(sourceContext);
        }
    }

    @Test
    public void testUnifier() throws Exception {
        TripleContext context = getContext();
        try {
            Set<Triple> model = new HashSet<Triple>();
            model.add(new Triple(test("joe"), test("hasDog"), test("zoey")));
            model.add(new Triple(test("bethany"), test("hasDog"), test("yeti")));
            model.add(new Triple(test("zoey"), test("hasToy"), test("squeaky")));
            model.add(new Triple(test("squeaky"), test("location"), test("underBed")));
            model.add(new Triple(test("zoey"), test("hasToy"), test("kitty")));
            model.add(new Triple(test("kitty"), test("location"), test("upstairs")));
            model.add(new Triple(test("yeti"), test("hasToy"), test("squeaky")));
            model.add(new Triple(test("this"), test("is"), test("garbage"))); // random extra crud
            //context.addTriples(model);
            writeTriples(context, model);
            // construct the unifier
            Unifier unifier = new Unifier();
            unifier.addColumnName("owner");
            unifier.addColumnName("location");
            unifier.addPattern(new Pattern("owner", test("hasDog"), "dog"));
            unifier.addPattern(new Pattern("dog", test("hasToy"), "toy"));
            unifier.addPattern(new Pattern("toy", test("location"), "location"));
            // unify
            context.perform(unifier);
            Table<Resource> table = unifier.getResult();
            // check results
            Set<Tuple<Resource>> expected = new HashSet<Tuple<Resource>>();
            expected.add(new Tuple<Resource>(test("joe"), test("upstairs")));
            expected.add(new Tuple<Resource>(test("joe"), test("underBed")));
            expected.add(new Tuple<Resource>(test("bethany"), test("underBed")));
            assertTrue("wrong results", Iterators.asSet(table.iterator()).equals(expected));
            //
            // do a mod and make sure results are updated
            model.add(new Triple(test("molly"), test("hasDog"), test("ollie")));
            model.add(new Triple(test("ollie"), test("hasToy"), test("tennisBall")));
            model.add(new Triple(test("tennisBall"), test("location"), test("yard")));
            writeTriples(context, model);
            //
            unifier = new Unifier();
            unifier.addColumnName("owner");
            unifier.addColumnName("location");
            unifier.addPattern(new Pattern("owner", test("hasDog"), "dog"));
            unifier.addPattern(new Pattern("dog", test("hasToy"), "toy"));
            unifier.addPattern(new Pattern("toy", test("location"), "location"));
            context.perform(unifier);
            table = unifier.getResult();
            expected = new HashSet<Tuple<Resource>>();
            expected.add(new Tuple<Resource>(test("joe"), test("upstairs")));
            expected.add(new Tuple<Resource>(test("joe"), test("underBed")));
            expected.add(new Tuple<Resource>(test("bethany"), test("underBed")));
            expected.add(new Tuple<Resource>(test("molly"), test("yard")));
            assertTrue("wrong results", Iterators.asSet(table.iterator()).equals(expected));
        } finally {
            closeContext(context);
        }
    }

    /**
     * Override this if your context impl doesn't support paging
     * 
     * @throws Exception
     */
    @Test
    public void testPaging() throws Exception {
        TripleContext mc = getContext();
        try {
            String names[] = new String[] { "Hansel", "Gretel", "Ted" };
            int years[] = new int[] { 2009, 1969, 1994, 1987, 2002 };
            String colors[] = new String[] { "Red", "Green" };
            int j = 0;
            for (int i = 0; i < 15; i++ ) {
                Resource s = Resource.uriRef();
                try {
                    if (j++ % 7 != 0) { // leave some blank for optional pattern below to test null sorting
                        mc.addTriple(s, Dc.DESCRIPTION, colors[i % 2]);
                    }
                    mc.addTriple(s, Foaf.NAME, names[i % 3]);
                    mc.addTriple(s, Dc.DATE, years[i % 5]);
                } catch (OperatorException x) {
                    // unable to write, so we can't do the rest of the test
                    return;
                }
            }
            Unifier u = new Unifier();
            u.setColumnNames("color", "name", "date");
            u.addPattern("s", Foaf.NAME, "name");
            u.addPattern("s", Dc.DATE, "date");
            u.addPattern("s", Dc.DESCRIPTION, "color", true);
            u.addOrderBy("color");
            u.addOrderBy("name");
            u.addOrderByDesc("date");
            u.setOffset(2);
            u.setLimit(11);
            mc.perform(u);
            String results[] = new String[] {
                    // Tupelo <=2.5 ordering
                    /*
                    "[Green, Hansel, 2002]",
                    "[Green, Hansel, 1987]",
                    "[Green, Ted, 2009]",
                    "[Green, Ted, 1969]",
                    "[Red, Gretel, 2009]",
                    "[Red, Gretel, 2002]",
                    "[Red, Hansel, 1994]",
                    "[Red, Hansel, 1969]",
                    "[Red, Ted, 1994]",
                    "[Red, Ted, 1987]",
                    "[null, Gretel, 1994]"
                    */
                    // SPARQL ordering
                    "[null, Ted, 2002]", "[Green, Gretel, 1987]", "[Green, Gretel, 1969]", "[Green, Hansel, 2002]", "[Green, Hansel, 1987]", "[Green, Ted, 2009]", "[Green, Ted, 1969]", "[Red, Gretel, 2009]", "[Red, Gretel, 2002]", "[Red, Hansel, 1994]", "[Red, Hansel, 1969]" };
            int i = 0;
            for (Tuple<Resource> row : u.getResult() ) {
                assertTrue(row.toString().equals(results[i++]));
            }
            assertEquals("wronger number of triples.", results.length, i);
        } finally {
            closeContext(mc);
        }
    }

    @Test
    public void testBlankNodeInsertion() throws Exception {
        TripleContext context = getContext();
        try {
            Set<Triple> triples = new HashSet<Triple>();
            // add a blank node test that should work
            BlankNode b = new BlankNode();
            triples.add(new Triple(test("subject"), test("to"), b));
            triples.add(new Triple(b, test("from"), test("subject")));
            // write triples
            writeTriples(context, triples);
            // verify that we added them
            Set<Triple> got = context.getTriples();
            assertEquals("wronger number of triples.", 2, got.size());
            BlankNode b2 = null;
            for (Triple t : got ) {
                if (t.getSubject().isBlank()) {
                    if (b2 == null) {
                        b2 = (BlankNode) t.getSubject();
                    }
                    assertTrue(t.getPredicate().equals(test("from")));
                    assertTrue(t.getObject().equals(test("subject")));
                } else if (t.getSubject().equals(test("subject"))) {
                    assertTrue(t.getPredicate().equals(test("to")));
                    if (b2 != null) {
                        assertTrue(t.getObject().equals(b2));
                    } else {
                        b2 = (BlankNode) t.getObject();
                    }
                }
            }
        } finally {
            closeContext(context);
        }
    }

    /*
    @Test
    public void testSubjectReplacer() throws Exception {
        Context c = getContext();
        try {
            writeTriples(c, getTriples());
            Set<Triple> newFoo = new HashSet<Triple>(0);
            newFoo.add(new Triple(test("foo"), test("blorb"), test("snub")));
            SubjectReplacer sr = new SubjectReplacer();
            sr.setSubject(test("foo"));
            sr.setTriples(newFoo);
            c.perform(sr);
            TripleMatcher tf = new TripleMatcher();
            tf.setSubject(test("foo"));
            c.perform(tf);
            assertTrue(tf.getResult().size() == 1;
            for (Triple t : tf.getResult() ) {
                assertTrue(t.equals(new Triple(test("foo"), test("blorb"), test("snub")));
            }
        } finally {
            closeContext(c);
        }
    }
    */

    /*
    @Test
    public void testAsk() throws Exception {
        try {
            Resource foo = Resource.uriRef("urn:foo");
            Resource bar = Resource.uriRef("urn:bar");
            Resource baz = Resource.uriRef("urn:baz");
            Triple t = new Triple(foo,bar,baz);
            Pattern p = new Pattern(foo,bar,baz);
            Context c = getContext();
            TripleWriter tw = new TripleWriter();
            tw.add(t);
            c.perform(tw);
            Transformer tr = new Transformer();
            tr.addOutPattern(p);
            tr.addInPattern(p);
            c.perform(tr);
            assertTrue(tr.getResult().size() == 1;
            tw = new TripleWriter();
            tw.remove(t);
            c.perform(tw);
            tr = new Transformer();
            tr.addOutPattern(p);
            tr.addInPattern(p);
            c.perform(tr);
            assertTrue(tr.getResult().size() == 0;
        } catch(OperatorUnavailableException x) {
            // fall through
        }
    }
    */

    public void testOptionalPatterns() throws Exception {
        TripleContext c = getContext();
        try {
            c.addTriples(new Triple(test("foo"), test("link"), test("bar")), new Triple(test("foo"), Rdf.TYPE, test("Person")), new Triple(test("foo"), test("name"), "fred"), new Triple(test("bar"), test("link"), test("baz")), new Triple(test("bar"), Rdf.TYPE, test("Person")), new Triple(test("baz"), test("link"), test("foo")), new Triple(test("baz"), test("name"), "bubba"));
            Unifier u = new Unifier();
            u.setColumnNames("s", "s2", "name");
            u.addPattern("s", test("link"), "s2");
            u.addPattern("s", Rdf.TYPE, test("Person"));
            u.addPattern("s", test("name"), "name", true);
            c.perform(u);
            int count = 0;
            for (Tuple<Resource> row : u.getResult() ) {
                Resource s = row.get(0);
                Resource s2 = row.get(1);
                Resource name = row.get(2);
                if (s.equals(test("foo"))) {
                    assertTrue(s2.equals(test("bar")));
                    assertTrue(name.equals(literal("fred")));
                } else if (s.equals(test("bar"))) {
                    assertTrue(s2.equals(test("baz")));
                    assertTrue(name == null);
                } else {
                    assertTrue(false);
                }
                count++;
            }
            assertTrue(count == 2);
            //
            c.addTriples(new Triple(test("bar"), test("link"), test("snord")));
            u = new Unifier();
            u.setColumnNames("s", "sName", "s2", "s2Name", "s3", "s3Name");
            u.addPattern("s", test("name"), "sName");
            u.addPattern("s", test("link"), "s2", true);
            u.addPattern("s2", test("name"), "s2Name", true);
            u.addPattern("s2", test("link"), "s3", true);
            u.addPattern("s3", test("name"), "s3Name", true);
            c.perform(u);
            count = 0;
            for (Tuple<Resource> row : u.getResult() ) {
                Resource s = row.get(0);
                Resource sName = row.get(1);
                Resource s2 = row.get(2);
                Resource s2Name = row.get(3);
                Resource s3 = row.get(4);
                Resource s3Name = row.get(5);
                if (s.equals(test("foo"))) {
                    assertTrue(sName.equals(literal("fred")));
                    assertTrue(s2.equals(test("bar")));
                    assertTrue(s2Name == null);
                    assertTrue((s3.equals(test("baz")) && s3Name.equals(literal("bubba"))) || (s3.equals(test("snord")) && s3Name == null));
                }
                count++;
            }
            assertTrue(count == 3);
            Transformer t = new Transformer();
            t.addInPattern("s", test("link"), "s2");
            t.addInPattern("s", test("name"), "name", true);
            t.addOutPattern("s", test("xlink"), "s2");
            t.addOutPattern("s", test("xname"), "name");
            c.perform(t);
            Set<Triple> result = t.getResult();
            assertTrue(result.size() == 6);
            assertTrue(result.contains(new Triple(test("foo"), test("xlink"), test("bar"))));
            assertTrue(result.contains(new Triple(test("foo"), test("xname"), literal("fred"))));
            assertTrue(result.contains(new Triple(test("bar"), test("xlink"), test("baz"))));
            assertTrue(result.contains(new Triple(test("bar"), test("xlink"), test("snord"))));
            assertTrue(result.contains(new Triple(test("baz"), test("xlink"), test("foo"))));
            assertTrue(result.contains(new Triple(test("baz"), test("xname"), literal("bubba"))));

            // regression for two-hop case
            Unifier twoHop = new Unifier();
            twoHop.addPattern(test("foo"), "p1", "so");
            twoHop.addPattern("so", "p2", "o2", true);
            twoHop.setColumnNames("p1", "so", "p2", "o2");
            c.perform(twoHop);
            for (Tuple<Resource> row : twoHop.getResult() ) {
                //Streams.out.println(">>> " + row); // FIXME debug
            }
        } finally {
            closeContext(c);
        }
    }

    @Test
    public void testAskStyleQuery() throws Exception {
        TripleContext c = getContext();
        try {
            // try it as a transformer
            Triple theTriple = new Triple(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            Transformer ask = new Transformer();
            ask.addInPattern(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            ask.addOutPattern(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            c.perform(ask);
            Set<Triple> ts = ask.getResult();
            assertTrue(ts.size() == 0);
            // try it as a triplematcher
            TripleMatcher tm = new TripleMatcher();
            tm.match(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            c.perform(tm);
            ts = tm.getResult();
            assertTrue(ts.size() == 0);
            // now add the triple
            c.addTriples(theTriple);
            ask = new Transformer();
            ask.addInPattern(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            ask.addOutPattern(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            c.perform(ask);
            ts = ask.getResult();
            assertTrue(ts.size() == 1);
            assertTrue(ts.contains(theTriple));
            // try it as a triplematcher
            tm = new TripleMatcher();
            tm.match(Rdf.TYPE, Rdf.TYPE, Rdf.TYPE);
            c.perform(tm);
            ts = tm.getResult();
            assertTrue(ts.size() == 1);
            assertTrue(ts.contains(theTriple));
            // now remove the triple
            c.removeTriples(theTriple);
        } finally {
            closeContext(c);
        }
    }
    /*
    protected static final String SUBJ_NAME = "subj";
    protected static final String PRED_NAME = "pred";
    protected static final String OBJ_NAME  = "obj";

    @Test
    public void testSparqlSelect() throws Exception {
        Context c = getContext();
        try {
            Set<Triple> written = getTriples();
            writeTriples(c, written);

            String query = "SELECT ?" + SUBJ_NAME + " ?" + PRED_NAME + " ?" + OBJ_NAME + " WHERE { ?" + SUBJ_NAME + " ?" + PRED_NAME + " ?" + OBJ_NAME + " . }";
            SparqlSelectOperator so = new SparqlSelectOperator(query);
            c.perform(so);

            Table<Resource> table = so.getResult();

            // convert the tuple table into a set of triples to check equivalence
            Set<Triple> output = new HashSet<Triple>();
            for (Tuple<Resource> t : table ) {
                output.add(new Triple(t.get(Tables.getColumnIndex(table, SUBJ_NAME)), t.get(Tables.getColumnIndex(table, PRED_NAME)), t.get(Tables.getColumnIndex(table, OBJ_NAME))));
            }
            assertTrue("Written != read", output.equals(written));
        } finally {
            closeContext(c);
        }
    }

    @Test
    public void testSparqlAsk() throws Exception {
        Context c = getContext();
        try {
            Set<Triple> triples = new HashSet<Triple>();
            triples.add(new Triple(URI.create("http://foo"), URI.create("http://bar"), "baz"));
            writeTriples(c, triples);

            String query = "ASK WHERE { ?subj ?pred ?obj . }";
            SparqlAskOperator so = new SparqlAskOperator(query);
            c.perform(so);
            boolean b = so.getResult();
            assertTrue("Non-empty set test failed.", b);

            query = "ASK WHERE { ?subj ?obj \"frankzappa\" . }";
            so = new SparqlAskOperator(query);
            c.perform(so);
            b = so.getResult();
            assertFalse("Nonexistent triple test failed.", b);

            query = "ASK WHERE { ?subj ?pred \"baz\" . }";
            so = new SparqlAskOperator(query);
            c.perform(so);
            b = so.getResult();
            assertTrue("Existing triple test failed.", b);
        } finally {
            closeContext(c);
        }
    }

    public static final java.lang.String VCARD = "http://www.w3.org/2001/vcard-rdf/3.0#";

    public static String vcard(String suffix) {
        return VCARD + suffix;
    }

    @Test
    public void testSparqlConstruct() throws Exception {
        Context c = getContext();
        try {
            // test example from <http://www.w3.org/TR/2008/REC-rdf-sparql-query-20080115/#construct>
            Set<Triple> written = new HashSet<Triple>();
            BlankNode bn = new BlankNode();
            written.add(new Triple(bn, NAME, "Alice"));
            written.add(new Triple(bn, MBOX, "mailto:alice@example.org"));
            writeTriples(c, written);

            // should match this:
            Set<Triple> expected = new HashSet<Triple>();
            expected.add(new Triple(URI.create("http://example.org/person#Alice"), URI.create(vcard("FN")), "Alice"));

            String query = "PREFIX foaf:    <http://xmlns.com/foaf/0.1/>\n";
            query += "PREFIX vcard:   <http://www.w3.org/2001/vcard-rdf/3.0#>\n";
            query += "CONSTRUCT   { <http://example.org/person#Alice> vcard:FN ?name }\n";
            query += "WHERE       { ?x foaf:name ?name }";
            SparqlConstructOperator so = null;
            so = new SparqlConstructOperator(query);
            c.perform(so);
            c.close();
            Set<Triple> result = so.getResult();

            assertTrue("expected != result:\n  expected: " + expected + "\n  result: " + result, expected.equals(result));
        } finally {
            closeContext(c);
        }
    }

    @Test
    public void testSparqlDescribe() throws Exception {
        Context c = getContext();
        try {
            // test simple example that should return *something*; depends on the contents
            // of getTriples() not changing substantially.
            Set<Triple> written = getTriples();
            writeTriples(c, written);

            String query = "DESCRIBE " + test("foo").toNTriples();
            SparqlDescribeOperator so = new SparqlDescribeOperator(query);
            c.perform(so);
            c.close();
            Set<Triple> result = so.getResult();

            Streams.out.println(query + ": " + result);
            assertTrue("Nothing returned!", !result.isEmpty());
        } finally {
            closeContext(c);
        }
    }
    */
}
