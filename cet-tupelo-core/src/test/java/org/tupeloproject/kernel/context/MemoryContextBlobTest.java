package org.tupeloproject.kernel.context;

import static org.junit.Assert.assertFalse;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.junit.Test;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;

public class MemoryContextBlobTest extends AbstractBlobContextTest {

    @Override
    protected BlobContext getContext() throws Exception {
        return new MemoryContext();
    }

    @Test
    public void testFileDelete() throws Exception {
        BlobContext c = getContext();

        Resource key = test("foo");

        BlobWriter bw = new BlobWriter(key);
        bw.setInputStream(new ByteArrayInputStream("foobar".getBytes("US-ASCII")));
        c.perform(bw);

        File f = ((MemoryContext) c).getFile(key);

        BlobRemover br = new BlobRemover(key);
        c.perform(br);

        assertFalse("File exists after remove.", f.exists());
    }
}
