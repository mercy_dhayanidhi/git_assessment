package org.tupeloproject.kernel.context;

import java.io.File;
import java.io.IOException;

import org.tupeloproject.kernel.OperatorException;

public class HashFileContextBlobTest extends AbstractBlobContextTest {
    public static class TestHashFileContext extends HashFileContext {
        @Override
        public void close() throws OperatorException {
            destroy();
        }
    }

    @Override
    protected BlobContext getContext() throws Exception {
        File dir = new File("testhashfile");
        if (dir.exists()) {
            throw (new IOException("Folder is not empty."));
        }
        HashFileContext hfc = new TestHashFileContext();
        hfc.setDirectory(dir);
        hfc.initialize();
        return hfc;
    }

}
