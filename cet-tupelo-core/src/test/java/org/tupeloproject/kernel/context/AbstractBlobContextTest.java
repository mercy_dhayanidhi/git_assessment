/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.NotFoundException;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.operator.BlobChecker;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.util.Iterators;

/**
 * This junit TestCase provides a set of tests for Tupelo's core
 * Triple and Blob operations. Subclasses need only implement
 * {@link #getContext()}; they should return a Context with no data in
 * it. GenericContextTest attempts to acquire all operators and run
 * tests on each one.
 */
public abstract class AbstractBlobContextTest {
    public static String TAG_PREFIX = "tag:tupeloproject.org,2004:/test/";

    protected Resource test(String suffix) {
        return Resource.uriRef(TAG_PREFIX + suffix);
    }

    /**
     * Return a Context implementation to test. Implementations MUST
     * NOT return a Context containing any data or else some tests
     * will fail.
     * 
     * @return a blank Context
     */
    protected abstract BlobContext getContext() throws Exception;

    /**
     * close a context. does nothing if c is null
     * 
     * @throws OperatorException
     */
    protected void closeContext(BlobContext c) throws OperatorException {
        if (c != null) {
            c.close();
        }
    }

    @Test
    public void testBlobs() throws Exception {
        BlobContext context = getContext();
        try {
            byte[] blob = new byte[9000];
            for (int i = 0; i < 9000; i++ ) {
                blob[i] = (byte) (i % 128);
            }
            Resource key = test("/some/file/foo");

            // write blob
            BlobWriter bw = new BlobWriter();
            bw.setSubject(key);
            bw.setInputStream(new ByteArrayInputStream(blob));
            context.perform(bw);
            assertTrue("blob size not reported", bw.getSize() == blob.length);

            // fetch blob, and check for right data
            BlobFetcher bf = new BlobFetcher();
            bf.setSubject(key);
            context.perform(bf);
            InputStream is = bf.getInputStream();
            int i = 0;
            for (byte b : blob ) {
                assertTrue("blobs don't match at byte " + i, is.read() == b);
                i++;
            }
            is.close();

            // check blob is there
            BlobChecker bc = new BlobChecker(key);
            context.perform(bc);
            assertTrue("blob does not exist.", bc.exists());
            assertTrue("blob does not report right size.", bc.getSize() == BlobChecker.SIZE_UNKNOWN || bc.getSize() == 9000);

            // remove blob
            BlobRemover br = new BlobRemover();
            br.setSubject(key);
            context.perform(br);

            // make sure blob is really gone
            try {
                bf = new BlobFetcher();
                bf.setSubject(key);
                context.perform(bf);
                bf.getInputStream().close();
                fail("failed to remove blob");
            } catch (NotFoundException x) {
                //
            } catch (OperatorException x) {
                //
            }
        } finally {
            closeContext(context);
        }
    }

    public void testBlobIterator() throws Exception {
        BlobContext c = getContext();
        try {
            BlobWriter bw = new BlobWriter(Resource.uriRef("urn:blaza"));
            bw.setInputStream(new ByteArrayInputStream("foobar".getBytes("US-ASCII")));
            c.perform(bw);

            bw = new BlobWriter(Resource.uriRef("urn:snaza"));
            bw.setInputStream(new ByteArrayInputStream("foobaz".getBytes("US-ASCII")));
            c.perform(bw);

            BlobIterator bi = new BlobIterator();
            c.perform(bi);
            Set<Resource> bUris = Iterators.asSet(bi);
            assertEquals("not right number of blobs", 2, bUris.size());
            assertTrue("missing blob urn:blaza", bUris.contains(Resource.uriRef("urn:blaza")));
            assertTrue("missing blob urn:snaza", bUris.contains(Resource.uriRef("urn:snaza")));
        } finally {
            closeContext(c);
        }
    }

    /*
    public void testBlobCounter() throws Exception {
        BlobContext c = getContext();
        try {
            BlobWriter bw = new BlobWriter(Resource.uriRef("urn:blaza"));
            bw.setInputStream(new ByteArrayInputStream("foobar".getBytes("US-ASCII")));
            c.perform(bw);
            bw = new BlobWriter(Resource.uriRef("urn:snaza"));
            bw.setInputStream(new ByteArrayInputStream("foobaz".getBytes("US-ASCII")));
            c.perform(bw);
            BlobCounter bc = new BlobCounter();
            c.perform(bc);
            assert bc.getLowerBound() >= 2;
        } finally {
            closeContext(c);
        }
    }
    */
}
