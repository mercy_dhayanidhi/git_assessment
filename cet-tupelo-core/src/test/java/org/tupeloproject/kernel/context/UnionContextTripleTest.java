/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Dc;
import org.tupeloproject.rdf.terms.Foaf;
import org.tupeloproject.util.Iterators;
import org.tupeloproject.util.Tuple;

public class UnionContextTripleTest extends AbstractTripleContextTest {
    public TripleContext getContext() throws Exception {
        UnionContext context = new UnionContext();
        context.addContext(new MemoryContext());
        context.addContext(new MemoryContext());
        return context;
    }

    int countTriples(TripleContext c) throws Exception {
        return c.getTriples().size();
    }

    @Test
    public void testMerge() throws Exception {
        UnionContext context = new UnionContext();
        for (int i = 0; i < 25; i++ ) {
            MemoryContext child = new MemoryContext();
            TripleWriter tw = new TripleWriter();
            tw.add(new Triple(test("joe"), test("age"), test(36 + i + "")));
            child.perform(tw);
            context.addContext(child);
        }
        int sum = 0;
        for (Context child : context.getContexts() ) {
            if (child instanceof TripleContext) {
                sum += countTriples((TripleContext) child);
            }
        }
        assertTrue("union triple count unexpected", countTriples(context) == sum);
    }

    @Test
    public void testComplexMerge() throws Exception {
        UnionContext context = new UnionContext();
        for (int i = 0; i < 25; i++ ) {
            MemoryContext child = new MemoryContext();
            TripleWriter tw = new TripleWriter();
            tw.add(new Triple(test("joe" + i), Foaf.KNOWS, test("joe" + (i + 1))));
            child.perform(tw);
            context.addContext(child);
        }
        Unifier u = new Unifier();
        u.setColumnNames("x", "y");
        u.addPattern("x", Foaf.KNOWS, "y");
        context.perform(u);
        int len = (test("joe") + "").length();
        for (Tuple<Resource> row : u.getResult() ) {
            int x = Integer.parseInt(row.get(0).toString().substring(len));
            int y = Integer.parseInt(row.get(1).toString().substring(len));
            assertEquals("Invalid merge", y, x + 1);
        }
    }

    @Test
    public void testUnifier() throws Exception {
        MemoryContext c1 = new MemoryContext();
        MemoryContext c2 = new MemoryContext();
        Resource r1 = Resource.uriRef();
        Resource r2 = Resource.uriRef();
        Resource p = Resource.uriRef();
        c1.addTriples(new Triple(r1, Dc.CREATOR, p), new Triple(p, Foaf.NAME, "Joe"));
        c2.addTriples(new Triple(r2, Dc.CREATOR, p), new Triple(p, Foaf.NAME, "Zoey"));
        UnionContext uc = new UnionContext();
        uc.addContext(c1);
        uc.addContext(c2);
        //
        Unifier u = new Unifier();
        u.setColumnNames("r", "name");
        u.addPattern("r", Dc.CREATOR, "c");
        u.addPattern("c", Foaf.NAME, "name");
        //
        uc.perform(u);
        assertEquals("wrong column names", u.getColumnNames(), u.getResult().getColumnNames());

        Set<Tuple<Resource>> expected = new HashSet<Tuple<Resource>>();
        expected.add(new Tuple<Resource>(r1, Resource.literal("Joe")));
        expected.add(new Tuple<Resource>(r1, Resource.literal("Zoey")));
        expected.add(new Tuple<Resource>(r2, Resource.literal("Joe")));
        expected.add(new Tuple<Resource>(r2, Resource.literal("Zoey")));
        assertTrue("wrong results", Iterators.asSet(u.getResult().iterator()).equals(expected));
    }
}
