package org.tupeloproject.kernel.context;

public class MemoryContextTripleTest extends AbstractTripleContextTest {

    @Override
    protected TripleContext getContext() throws Exception {
        return new MemoryContext();
    }
}
