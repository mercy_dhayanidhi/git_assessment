package org.tupeloproject.kernel.beans;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import edu.illinois.ncsa.sematic.annotation.RdfNameSpace;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@RdfNameSpace(ns = { "dc=http://purl.org/dc/elements/1.1/", "foaf=http://xmlns.com/foaf/0.1/" })
@RdfType(type = "foaf:Person")
public class PersonBean {
    @RdfPredicate(predicate = "dc:identifier", subject = true)
    private String                  id;

    @RdfPredicate(predicate = "foaf:name")
    private String                  name;

    @RdfPredicate(predicate = "foaf:mbox")
    private String                  email;

    @RdfPredicate(predicate = "foaf:knows", value = PersonBean.class)
    private Collection<PersonBean>  friends;

    @RdfPredicate(predicate = "foaf:iou", key = PersonBean.class, value = Double.class)
    private Map<PersonBean, Double> ious;

    public PersonBean() {
        friends = new HashSet<PersonBean>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<PersonBean> getFriends() {
        return friends;
    }

    public void setFriends(Collection<PersonBean> friends) {
        this.friends = friends;
    }

    public void addFriend(PersonBean friend) {
        this.friends.add(friend);
    }

    public void setIOUs(Map<PersonBean, Double> ious) {
        this.ious = ious;
    }

    public Map<PersonBean, Double> getIOUs() {
        return ious;
    }
}
