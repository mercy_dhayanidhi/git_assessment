package org.tupeloproject.kernel.beans;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.illinois.ncsa.sematic.annotation.RdfNameSpace;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@RdfNameSpace(ns = { "dc=http://purl.org/dc/elements/1.1/", "cet=http://cet.ncsa.uiuc.edu/2007/" })
@RdfType(type = "cet:ComplexBean")
public class ComplexBean {
    @RdfPredicate(predicate = "dc:identifier", subject = true)
    private String               subject;

    @RdfPredicate(predicate = "cet:stringValue")
    private String               stringValue;

    @RdfPredicate(predicate = "cet:stringValues")
    private String[]             stringValues;

    @RdfPredicate(predicate = "cet:stringNull")
    private String               stringNull;

    @RdfPredicate(predicate = "cet:dateValue")
    private Date                 dateValue;

    @RdfPredicate(predicate = "cet:dateNull")
    private Date                 dateNull;

    @RdfPredicate(predicate = "cet:integerValue")
    private Integer              integerValue;

    @RdfPredicate(predicate = "cet:longValue")
    private Long                 longValue;

    @RdfPredicate(predicate = "cet:doubleValue")
    private Double               doubleValue;

    @RdfPredicate(predicate = "cet:doubleInf")
    private Double               doubleInf;

    @RdfPredicate(predicate = "cet:doubleNaN")
    private Double               doubleNaN;

    @RdfPredicate(predicate = "cet:bean")
    private ComplexBean          bean;

    @RdfPredicate(predicate = "cet:me")
    private ComplexBean          me;

    @RdfPredicate(predicate = "cet:mapValue")
    private Map<String, Integer> mapValue;

    @RdfPredicate(predicate = "cet:mapNull")
    private Map<String, String>  mapNull;

    @RdfPredicate(predicate = "cet:setValue")
    private Set<Integer>         setValue;

    @RdfPredicate(predicate = "cet:setNull")
    private Set<Integer>         setNull;

    @RdfPredicate(predicate = "cet:listValue")
    private List<Integer>        listValue;

    @RdfPredicate(predicate = "cet:listNull")
    private List<Integer>        listNull;

    @RdfPredicate(predicate = "cet:transientValue")
    private transient Integer    transientValue;

    public ComplexBean() {
    }

    public static ComplexBean createBean() {
        ComplexBean bean = new ComplexBean();
        bean.subject = null;
        bean.stringValue = "Hello World";
        bean.stringValues = new String[] { "foo", "bar" };
        bean.stringNull = null;
        bean.dateValue = new Date(9999);
        bean.dateNull = null;
        bean.integerValue = Integer.MAX_VALUE;
        bean.longValue = Long.MAX_VALUE;
        bean.doubleValue = -Double.MAX_VALUE;
        bean.doubleInf = Double.POSITIVE_INFINITY;
        bean.doubleNaN = Double.NaN;
        bean.bean = new ComplexBean();
        bean.me = bean;
        bean.mapValue = new HashMap<String, Integer>();
        bean.mapValue.put("1", 1);
        bean.mapValue.put("2", 2);
        bean.mapValue.put("3", 3);
        bean.mapNull = null;
        bean.setValue = new HashSet<Integer>();
        bean.setValue.add(7);
        bean.setValue.add(99);
        bean.setValue.add(-200);
        bean.setNull = null;
        bean.listValue = new ArrayList<Integer>();
        bean.listValue.add(1);
        bean.listValue.add(2);
        bean.listValue.add(3);
        bean.listValue.add(4);
        bean.listValue.add(5);
        bean.listNull = null;
        bean.transientValue = 7;
        return bean;
    }

    public static void checkBean(ComplexBean bean) throws Exception {
        ComplexBean orig = ComplexBean.createBean();

        assertNotNull("subject", bean.subject);
        assertEquals("stringValue.", orig.stringValue, bean.stringValue);
        assertArrayEquals("stringValues", orig.stringValues, bean.stringValues);
        assertNull("stringNull", bean.stringNull);
        assertEquals("dateValue.", orig.dateValue, bean.dateValue);
        assertNull("dateNull", bean.dateNull);
        assertEquals("integerValue.", orig.integerValue, bean.integerValue);
        assertEquals("longValue.", orig.longValue, bean.longValue);
        assertEquals("doubleValue.", orig.doubleValue, bean.doubleValue);
        assertTrue("doubleInf.", bean.doubleInf.isInfinite());
        assertTrue("doubleNaN.", bean.doubleNaN.isNaN());
        assertTrue("bean", (bean.bean instanceof ComplexBean));
        assertNotSame("bean", bean, bean.bean);
        assertTrue("me", (bean.me instanceof ComplexBean));
        assertSame("me", bean, bean.me);
        assertEquals("mapValue", orig.mapValue, bean.mapValue);
        assertNull("mapNull", bean.mapNull);
        assertEquals("setValue", orig.setValue, bean.setValue);
        assertNull("setNull", bean.setNull);
        assertEquals("listValue", orig.listValue, bean.listValue);
        assertNull("listNull", bean.listNull);
        assertFalse("transienValue", orig.transientValue.equals(bean.transientValue));
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String[] getStringValues() {
        return stringValues;
    }

    public void setStringValues(String[] stringValues) {
        this.stringValues = stringValues;
    }

    public String getStringNull() {
        return stringNull;
    }

    public void setStringNull(String stringNull) {
        this.stringNull = stringNull;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Date getDateNull() {
        return dateNull;
    }

    public void setDateNull(Date dateNull) {
        this.dateNull = dateNull;
    }

    public Integer getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(Integer integerValue) {
        this.integerValue = integerValue;
    }

    public Long getLongValue() {
        return longValue;
    }

    public void setLongValue(Long longValue) {
        this.longValue = longValue;
    }

    public Double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public Double getDoubleInf() {
        return doubleInf;
    }

    public void setDoubleInf(Double doubleInf) {
        this.doubleInf = doubleInf;
    }

    public Double getDoubleNaN() {
        return doubleNaN;
    }

    public void setDoubleNaN(Double doubleNaN) {
        this.doubleNaN = doubleNaN;
    }

    public ComplexBean getBean() {
        return bean;
    }

    public void setBean(ComplexBean bean) {
        this.bean = bean;
    }

    public ComplexBean getMe() {
        return me;
    }

    public void setMe(ComplexBean me) {
        this.me = me;
    }

    public Map<String, Integer> getMapValue() {
        return mapValue;
    }

    public void setMapValue(Map<String, Integer> mapValue) {
        this.mapValue = mapValue;
    }

    public Map<String, String> getMapNull() {
        return mapNull;
    }

    public void setMapNull(Map<String, String> mapNull) {
        this.mapNull = mapNull;
    }

    public Set<Integer> getSetValue() {
        return setValue;
    }

    public void setSetValue(Set<Integer> setValue) {
        this.setValue = setValue;
    }

    public List<Integer> getListValue() {
        return listValue;
    }

    public void setListValue(List<Integer> listValue) {
        this.listValue = listValue;
    }

    public Integer getTransientValue() {
        return transientValue;
    }

    public void setTransientValue(Integer transientValue) {
        this.transientValue = transientValue;
    }
}
