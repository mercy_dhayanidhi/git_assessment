package org.tupeloproject.kernel.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Foaf;
import org.tupeloproject.rdf.terms.Rdf;

public class SessionTest {
    public static String TAG_PREFIX = "tag:tupeloproject.org,2004:/test/";

    protected Resource test(String suffix) {
        return Resource.uriRef(TAG_PREFIX + suffix);
    }

    protected TripleContext getContext() {
        return new MemoryContext();
    }

    protected Resource addPersonBeanTriples(TripleContext c) throws OperatorException {
        TripleWriter tw = new TripleWriter();

        Resource joe = test("joe");
        tw.add(joe, Rdf.TYPE, Foaf.PERSON);
        tw.add(joe, Foaf.NAME, "Joe Futrelle");
        tw.add(joe, Foaf.MBOX, "futrelle@ncsa");

        Resource rob = test("rob");
        tw.add(rob, Rdf.TYPE, Foaf.PERSON);
        tw.add(rob, Foaf.NAME, "Rob Kooper");
        tw.add(rob, Foaf.MBOX, "kooper@ncsa");
        tw.add(rob, Foaf.KNOWS, joe);

        c.perform(tw);
        return rob;
    }

    private void checkPersonBean(PersonBean rob) {
        assertNotNull("bean not found", rob);
        assertEquals("invalid subject", test("rob").getString(), rob.getId());
        assertEquals("invalid name", "Rob Kooper", rob.getName());
        assertEquals("invalid email", "kooper@ncsa", rob.getEmail());
        assertEquals("no friends", 1, rob.getFriends().size());

        PersonBean friend = rob.getFriends().iterator().next();
        assertNotNull("bean not found", friend);
        assertEquals("invalid subject", test("joe").getString(), friend.getId());
        assertEquals("invalid name", "Joe Futrelle", friend.getName());
        assertEquals("invalid email", "futrelle@ncsa", friend.getEmail());
        assertEquals("no friends", 0, friend.getFriends().size());
    }

    @Test
    public void testFetchRawBean() throws OperatorException {
        TripleContext c = getContext();
        Resource robid = addPersonBeanTriples(c);

        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        PersonBean rob = session.load(PersonBean.class, robid);
        checkPersonBean(rob);
    }

    @Test
    public void testWriteRead() throws OperatorException {
        TripleContext c = getContext();

        PersonBean rob = new PersonBean();
        rob.setId(test("rob").getString());
        rob.setName("Rob Kooper");
        rob.setEmail("kooper@ncsa");
        PersonBean joe = new PersonBean();
        joe.setId(test("joe").getString());
        joe.setName("Joe Futrelle");
        joe.setEmail("futrelle@ncsa");
        rob.addFriend(joe);

        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        Resource id = session.save(rob, test("rob"));
        assertEquals("not right id", test("rob"), id);
        session.commit();

        PersonBean check = bf.openSession().load(PersonBean.class, test("rob"));

        checkPersonBean(check);
    }

    @Test
    public void testComplexBean() throws Exception {
        TripleContext c = getContext();

        ComplexBean bean = ComplexBean.createBean();

        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        Resource id = session.save(bean);
        session.commit();

        session = bf.openSession();
        session.setCheckNullCollection(true);
        ComplexBean check = session.load(ComplexBean.class, id);
        assertNotNull(check);
        assertEquals("subject not set correctly.", id.getString(), check.getSubject());
        ComplexBean.checkBean(check);
    }

    @Test
    public void testWriteReadId() throws OperatorException {
        TripleContext c = getContext();

        PersonBean rob = new PersonBean();
        rob.setName("Rob Kooper");
        rob.setEmail("kooper@ncsa");
        PersonBean joe = new PersonBean();
        joe.setId(test("joe").getString());
        joe.setName("Joe Futrelle");
        joe.setEmail("futrelle@ncsa");
        rob.addFriend(joe);

        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        session.save(rob, test("rob"));
        session.commit();

        PersonBean check = bf.openSession().load(PersonBean.class, test("rob"));

        checkPersonBean(check);
    }

    @Test
    public void testBeanFactoryCache() throws OperatorException {
        TripleContext c = getContext();
        Resource robid = addPersonBeanTriples(c);

        MapCache cache = new MapCache();
        BeanFactory bf = new BeanFactory(c, cache);

        // load object in session and commit sesion to global cache
        Session session = bf.openSession();
        session.beginTransaction();
        PersonBean rob = session.load(PersonBean.class, robid);
        checkPersonBean(rob);
        session.commit();
        checkPersonBean(rob);

        // load object in new session and check result
        session = bf.openSession();
        session.beginTransaction();
        rob = session.load(PersonBean.class, robid);
        checkPersonBean(rob);

        // check global cache
        assertTrue("rob not in cache.", cache.containsKey(robid));

        // check triples in cache
        Set<Triple> t = c.getTriples();
        for (Triple x : cache.get(robid).triples ) {
            assertTrue("did not contain " + x, t.contains(x));
            t.remove(x);
        }
        // only joe is left
        assertEquals("more triples left " + t, 4, t.size());
    }

    @Test
    public void testComplexNetwork() throws OperatorException {
        int people = 1000;

        PersonBean pb0 = new PersonBean();
        pb0.setName("bean 0");
        pb0.setId(test("bean0").getString());

        PersonBean last = pb0;
        for (int i = 1; i < people; i++ ) {
            PersonBean pb = new PersonBean();
            pb.setName("bean " + i);
            pb.setId(test("bean" + i).getString());
            last.addFriend(pb);
            last = pb;
        }
        last.addFriend(pb0);

        TripleContext c = getContext();
        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        session.save(pb0);
        session.commit();

        pb0 = bf.openSession().load(PersonBean.class, test("bean0"));
        last = pb0;
        for (int i = 0; i < people; i++ ) {
            assertNotNull("null bean", last);
            assertEquals("Mismatch id", test("bean" + i).getString(), last.getId());
            assertEquals("Mismatch name", "bean " + i, last.getName());
            assertEquals("Mismatch friends", 1, last.getFriends().size());
            last = last.getFriends().iterator().next();
        }

        assertNotNull("null bean", last);
        assertEquals("Mismatch friends", pb0, last);
    }

    @Test
    public void testEnum() throws Exception {
        MoreComplexBean bean = MoreComplexBean.createBean();
        bean.setSubject(test("bean").getString());

        TripleContext c = getContext();
        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        session.save(bean);
        session.commit();

        for (Triple t : c.getTriples() ) {
            System.out.println(t);
        }

        bean = bf.openSession().load(MoreComplexBean.class, test("bean"));

        MoreComplexBean.checkBean(bean);
    }
}
