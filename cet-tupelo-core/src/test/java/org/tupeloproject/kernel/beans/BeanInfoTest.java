package org.tupeloproject.kernel.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.tupeloproject.kernel.beans.BeanInfo.FieldInfo;
import org.tupeloproject.rdf.terms.Foaf;

public class BeanInfoTest {

    @Test
    public void testBeanInfo() throws Exception {
        BeanInfo bi = BeanInfo.get(PersonBean.class);

        assertEquals("Class does not match", PersonBean.class, bi.getBeanClass());
        assertEquals("Type/Namespace was not correct", Foaf.PERSON, bi.getType());
        assertEquals("wrong subject", PersonBean.class.getDeclaredField("id"), bi.getSubject());
        assertEquals("Wrong number of fields", 5, bi.getFields().size());

        FieldInfo mbox = null;
        FieldInfo friends = null;
        FieldInfo ious = null;
        for (FieldInfo fi : bi.getFields() ) {
            if ("email".equals(fi.getField().getName())) {
                if (mbox != null) {
                    throw (new Exception("2 fields with name email."));
                }
                mbox = fi;
            }
            if ("friends".equals(fi.getField().getName())) {
                if (friends != null) {
                    throw (new Exception("2 fields with name friends."));
                }
                friends = fi;
            }
            if ("ious".equals(fi.getField().getName())) {
                if (ious != null) {
                    throw (new Exception("2 fields with name ious."));
                }
                ious = fi;
            }
        }

        assertNotNull("email field not found.", mbox);
        assertEquals("Invalid predicate", Foaf.MBOX, mbox.getPredicate());

        assertNotNull("friends field not found.", friends);
        assertEquals("Invalid predicate", Foaf.KNOWS, friends.getPredicate());
        assertEquals("friends wrong key", null, friends.getKey());
        assertEquals("friends wrong value", PersonBean.class, friends.getValue());

        assertNotNull("ious field not found.", ious);
        assertEquals("ious wrong key", PersonBean.class, ious.getKey());
        assertEquals("ious wrong value", Double.class, ious.getValue());
    }
}
