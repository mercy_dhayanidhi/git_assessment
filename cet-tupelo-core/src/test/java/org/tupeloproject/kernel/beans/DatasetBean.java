package org.tupeloproject.kernel.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class DatasetBean {
    private String                 id;
    private List<String>           title;
    private String                 type;
    private PersonBean             creator;
    private Collection<PersonBean> contributors;

    public DatasetBean() {
        title = new ArrayList<String>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public void addTitle(String title) {
        this.title.add(title);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PersonBean getCreator() {
        return creator;
    }

    public void setCreator(PersonBean creator) {
        this.creator = creator;
    }

    public Collection<PersonBean> getContributors() {
        if (contributors == null) {
            contributors = new HashSet<PersonBean>();
        }
        return contributors;
    }

    public void setContributors(Collection<PersonBean> contributors) {
        this.contributors = contributors;
    }

    public void addContributor(PersonBean contributor) {
        getContributors().add(contributor);
    }

}
