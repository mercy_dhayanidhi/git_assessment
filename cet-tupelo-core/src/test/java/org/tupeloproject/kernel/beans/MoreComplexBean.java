package org.tupeloproject.kernel.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import edu.illinois.ncsa.sematic.annotation.RdfNameSpace;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@RdfNameSpace(ns = { "dc=http://purl.org/dc/elements/1.1/", "cet=http://cet.ncsa.uiuc.edu/2007/" })
@RdfType(type = "cet:MoreComplexBean")
public class MoreComplexBean {
    public static enum Options {
        ONE, TWO
    }

    @RdfPredicate(predicate = "dc:identifier", subject = true)
    private String  subject;

    @RdfPredicate(predicate = "cet:enum")
    private Options enumValue;

    public MoreComplexBean() {
    }

    public static MoreComplexBean createBean() {
        MoreComplexBean bean = new MoreComplexBean();

        bean.subject = null;
        bean.enumValue = Options.TWO;

        return bean;
    }

    public static void checkBean(MoreComplexBean bean) throws Exception {
        MoreComplexBean orig = MoreComplexBean.createBean();

        assertNotNull("subject", bean.subject);
        assertEquals("stringValue.", orig.enumValue, bean.enumValue);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Options getEnumValue() {
        return enumValue;
    }

    public void setName(Options enumValue) {
        this.enumValue = enumValue;
    }
}
