package org.tupeloproject.kernel;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.context.HashFileContext;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

public class ContextsTest {
    public static String TAG_PREFIX = "tag:tupeloproject.org,2004:/test/";

    protected Resource test(String suffix) {
        return Resource.uriRef(TAG_PREFIX + suffix);
    }

    @Test
    public void testCopyTriples() throws OperatorException {
        Set<Triple> triples = new HashSet<Triple>();
        triples.add(new Triple(test("baz"), test("quux"), "baz"));
        triples.add(new Triple(test("foo"), test("bar"), "baz"));
        triples.add(new Triple(test("foo"), test("bar"), "fish"));
        triples.add(new Triple(test("foo"), test("buz"), "fnip"));
        triples.add(new Triple(test("foo"), test("quux"), "frog"));
        triples.add(new Triple(test("fnord"), test("quux"), "frog"));
        triples.add(new Triple(test("this"), test("has"), test("uriobj")));

        MemoryContext mc1 = new MemoryContext();
        mc1.addTriples(triples);

        MemoryContext mc2 = new MemoryContext();
        Contexts.copyContext(mc1, mc2);

        assertEquals("Missing triples.", mc2.getTriples(), triples);
    }

    @Test
    public void testCopyBlobs() throws OperatorException, IOException {
        HashFileContext hf1 = null;
        try {
            byte[] buf = new byte[256];
            for (int i = 0; i < buf.length; i++ ) {
                buf[i] = (byte) (i & 0xff);
            }

            hf1 = new HashFileContext();
            hf1.setDirectory("tmp");
            BlobWriter bw = new BlobWriter();
            bw.setSubject(test("foo"));
            bw.setInputStream(new ByteArrayInputStream(buf));
            hf1.perform(bw);

            MemoryContext mc2 = new MemoryContext();
            Contexts.copyContext(hf1, mc2);

            BlobFetcher bf = new BlobFetcher();
            bf.setSubject(test("foo"));
            mc2.perform(bf);
            int i = 0;
            int x;
            while ((x = bf.getInputStream().read()) >= 0) {
                assertEquals("wrong byte at " + i, buf[i++], (byte) x);
            }
            assertEquals("wrong number of bytes.", buf.length, i);
        } finally {
            if (hf1 != null) {
                hf1.destroy();
            }
        }
    }
}
