/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import static org.junit.Assert.assertTrue;

import java.util.TreeSet;

import org.junit.Test;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.OperatorUnavailableException;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.context.UnionContext;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.Pattern;
import org.tupeloproject.util.Tuple;

public class UnifierTest {
    Resource ns(String suffix) {
        return Resource.uriRef("http://ns#" + suffix);
    }

    MemoryContext getModel() throws Exception {
        MemoryContext c = new MemoryContext();
        TripleWriter tw = new TripleWriter();
        tw.add(new Triple(ns("person1"), ns("hasDog"), ns("dog1")));
        tw.add(new Triple(ns("person2"), ns("hasDog"), ns("dog2")));
        tw.add(new Triple(ns("person3"), ns("hasDog"), ns("dog3")));
        tw.add(new Triple(ns("person4"), ns("hasDog"), ns("dog3")));
        tw.add(new Triple(ns("person5"), ns("hasDog"), ns("dog4")));
        tw.add(new Triple(ns("person1"), ns("hasPhoneNumber"), "123-4567"));
        tw.add(new Triple(ns("person2"), ns("hasPhoneNumber"), "234-5678"));
        tw.add(new Triple(ns("person3"), ns("hasPhoneNumber"), "345-6789"));
        tw.add(new Triple(ns("person4"), ns("hasPhoneNumber"), "456-7890"));
        tw.add(new Triple(ns("person5"), ns("hasPhoneNumber"), "567-8901"));
        tw.add(new Triple(ns("dog1"), ns("hasName"), "fido"));
        tw.add(new Triple(ns("dog2"), ns("hasName"), "rover"));
        tw.add(new Triple(ns("dog3"), ns("hasName"), "spot"));
        tw.add(new Triple(ns("dog4"), ns("hasName"), "spot"));
        c.perform(tw);
        return c;
    }

    Unifier getQuery() {
        Unifier u = new Unifier();
        u.setColumnNames("person", "phoneNo");
        u.addPattern("person", ns("hasDog"), "dog");
        u.addPattern("dog", ns("hasName"), Resource.literal("spot"));
        Pattern p = new Pattern("person", ns("hasPhoneNumber"), "phoneNo");
        u.addPattern((Pattern) p.clone()); // test pattern cloning while we're at it
        return u;
    }

    @Test
    public void testAll() throws Exception {
        MemoryContext c = getModel();
        Unifier u = getQuery();
        TreeSet<Tuple<Resource>> st = new TreeSet<Tuple<Resource>>();
        c.perform(u);
        for (Tuple<Resource> t : u.getResult() ) {
            st.add(t);
        }
        String expected[] = new String[] { "[http://ns#person3, 345-6789]", "[http://ns#person4, 456-7890]", "[http://ns#person5, 567-8901]" };
        int i = 0;
        for (Tuple<Resource> t : st ) {
            assertTrue(t.toString().equals(expected[i++]));
        }
    }

    // FIXME aggregating context now does not extend BaseContext
    // merge will have to be implemented otherwise
    public void _testMerge() throws Exception {
        for (int step = 2; step < 6; step++ ) {
            int i;
            UnionContext c = new UnionContext();
            TripleContext cs[] = new TripleContext[step];
            for (i = 0; i < step; i++ ) {
                cs[i] = new MemoryContext() {
                    public void perform(Unifier u) throws OperatorException {
                        throw new OperatorUnavailableException("unifier disabled");
                    }
                };
                c.addContext(cs[i]);
            }
            i = 0;
            for (Triple t : getModel().getTriples() ) {
                cs[i++ % step].addTriples(t);
            }
            Unifier u = getQuery();
            TreeSet<Tuple<Resource>> st = new TreeSet<Tuple<Resource>>();
            c.perform(u);
            for (Tuple<Resource> t : u.getResult() ) {
                st.add(t);
            }
            String expected[] = new String[] { "[http://ns#person3, 345-6789]", "[http://ns#person4, 456-7890]", "[http://ns#person5, 567-8901]" };
            i = 0;
            for (Tuple<Resource> t : st ) {
                assertTrue(t.toString().equals(expected[i++]));
            }
        }
    }
}
