package org.tupeloproject.kernel.operator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;

import org.junit.Test;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Dc;
import org.tupeloproject.rdf.terms.Foaf;
import org.tupeloproject.rdf.terms.Rdf;

public class SerializableTest {

    private void serialize(Object o) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object ser = ois.readObject();
        ois.close();

        // check same class
        assertSame("class", o.getClass(), ser.getClass());

        // check fields
        for (Class<?> c = o.getClass(); c != null; c = c.getSuperclass() ) {
            // predicates used
            for (Field f : c.getDeclaredFields() ) {
                if (!Modifier.isTransient(f.getModifiers())) {
                    continue;
                }
                f.setAccessible(true);
                assertEquals(f.toString(), f.get(o), f.get(ser));
            }
        }
    }

    @Test
    public void testResource() throws Exception {
        serialize(Resource.blankNode("foo"));
        serialize(Resource.uriRef());
        serialize(Resource.literal("Hello World"));
        serialize(Resource.literal(77));
        serialize(Resource.literal(new Date()));
    }

    @Test
    public void testBlobChecker() throws Exception {
        BlobChecker op = new BlobChecker();
        op.setSubject(Resource.uriRef());
        op.setExists(true);
        op.setSize(9911);
        op.setCreated(new Date(9988));
        op.setModified(new Date(8899));

        serialize(op);
    }

    @Test
    public void testBlobFetcher() throws Exception {
        BlobFetcher op = new BlobFetcher();
        op.setSubject(Resource.uriRef());

        serialize(op);
    }

    @Test
    public void testBlobIterator() throws Exception {
        BlobIterator op = new BlobIterator();

        serialize(op);
    }

    @Test
    public void testBlobRemover() throws Exception {
        BlobRemover op = new BlobRemover();
        op.setSubject(Resource.uriRef());

        serialize(op);
    }

    @Test
    public void testBlobWriter() throws Exception {
        BlobWriter op = new BlobWriter();
        op.setSubject(Resource.uriRef());
        op.setSize(999);

        serialize(op);
    }

    @Test
    public void testSubjectRemover() throws Exception {
        SubjectRemover op = new SubjectRemover();
        op.setSubject(Resource.uriRef());

        serialize(op);
    }

    @Test
    public void testTransformer() throws Exception {
        Transformer op = new Transformer();
        op.addInPattern("x", Rdf.TYPE, Foaf.PERSON);
        op.addOutPattern("x", Rdf.TYPE, Foaf.AGENT);

        serialize(op);
    }

    @Test
    public void testTripleIterator() throws Exception {
        TripleIterator op = new TripleIterator();

        serialize(op);
    }

    @Test
    public void testTripleMatcher() throws Exception {
        TripleMatcher op = new TripleMatcher();
        op.setSubject(Dc.CREATOR);
        op.setPredicate(Resource.literal("x"));
        op.setObject(Foaf.AGENT);

        serialize(op);
    }

    @Test
    public void testTripleWriter() throws Exception {
        TripleWriter op = new TripleWriter();
        op.add(new Triple(Resource.uriRef(), Rdf.TYPE, "1"));
        op.remove(new Triple(Resource.uriRef(), Rdf.TYPE, "3"));

        serialize(op);
    }

    @Test
    public void testUnifier() throws Exception {
        Unifier op = new Unifier();
        op.addPattern("x", Rdf.TYPE, Foaf.PERSON, true);
        op.setColumnNames("x", "y");
        op.addOrderBy("x");
        op.setLimit(100);
        op.setOffset(9);

        serialize(op);
    }

}
