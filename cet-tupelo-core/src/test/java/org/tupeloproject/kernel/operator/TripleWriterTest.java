/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel.operator;

import java.util.Set;

import org.junit.Test;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;

/**
 * TripleWriterTest
 */
public class TripleWriterTest {
    void containsExactly(Set<Triple> s, Triple... ts) {
        assert s.size() == ts.length;
        for (Triple t : ts ) {
            assert s.contains(t);
        }
    }

    @Test
    public void testAddRemove() throws Exception {
        Resource s1 = Resource.uriRef();
        Resource s2 = Resource.uriRef();
        Resource s3 = Resource.uriRef();
        Resource p = Resource.uriRef();
        Resource o = Resource.uriRef();
        Triple t1 = new Triple(s1, p, o);
        Triple t2 = new Triple(s2, p, o);
        Triple t3 = new Triple(s3, p, o);
        TripleWriter tw = new TripleWriter();
        tw.add(t1);
        containsExactly(tw.getToAdd(), t1);
        containsExactly(tw.getToRemove());
        tw.add(s2, p, o); // t2
        containsExactly(tw.getToAdd(), t1, t2);
        containsExactly(tw.getToRemove());
        tw.remove(t3);
        containsExactly(tw.getToAdd(), t1, t2);
        containsExactly(tw.getToRemove(), t3);
        tw.remove(s1, p, o); // t1
        containsExactly(tw.getToAdd(), t2);
        containsExactly(tw.getToRemove(), t1, t3);
        tw.remove(t2);
        containsExactly(tw.getToAdd());
        containsExactly(tw.getToRemove(), t1, t2, t3);
    }
}
