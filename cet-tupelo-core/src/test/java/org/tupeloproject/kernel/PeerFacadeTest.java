/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.kernel;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.tupeloproject.kernel.context.Context;
import org.tupeloproject.kernel.context.HashFileContext;
import org.tupeloproject.kernel.context.HttpContext;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.UnionContext;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.terms.Rdfs;
import org.tupeloproject.rdf.xml.RdfXml;

/**
 * PeerFacadeTest
 */
public class PeerFacadeTest {
    @Test
    public void testAll() throws Exception {
        // create a union context of 1) a memory context 2) a simple file context
        UnionContext mama = new UnionContext();
        mama.addContext(new MemoryContext());
        HashFileContext fc = new HashFileContext();
        fc.setDirectory("/foo/bar");
        mama.addContext(fc);
        // create an http context
        HttpContext http = new HttpContext();
        //
        // create a peer facade backed by a memory context
        MemoryContext store = new MemoryContext();
        PeerFacade pf = new PeerFacade();
        pf.setContext(store);
        // save the union context at pid with an extra triple on pid2
        // save the http context at pid2
        Resource pid = Resource.uriRef("http://my.context/thisIsMyContext#foobar");
        Resource pid2 = Resource.uriRef();
        String inscrutable = "This is a very long string that no other tests are using -- honest! " + "especially because it contains *(@#&$%(*#$&958&#$(75";
        pf.savePeer(http, pid2);
        Triple extra = new Triple(pid2, Rdfs.LABEL, inscrutable);
        pf.savePeer(mama, pid, extra);
        //
        // attempt to load the union context from the facade
        UnionContext uc = (UnionContext) pf.loadPeer(pid);
        // some assertions
        List<Context> kids = uc.getContexts();
        assertEquals("wrong number of kids", 2, kids.size());
        assertEquals("wrong class context 0", MemoryContext.class, kids.get(0).getClass());
        assertEquals("wrong class context 1", HashFileContext.class, kids.get(1).getClass());
        HashFileContext sfc2 = (HashFileContext) kids.get(1);
        assertEquals("wrong folder", "/foo/bar", sfc2.getDirectory());

        // same with the http context
        HttpContext hc = (HttpContext) pf.loadPeer(pid2);

        //
        TripleMatcher tm = new TripleMatcher();
        tm.setObject(Resource.literal(inscrutable));
        pf.getContext().perform(tm);
        assertEquals("Wrong number of extra triples.", 1, tm.getResult().size());
        for (Triple t : tm.getResult() ) {
            assertEquals("extra triple wrong", extra, t);
        }
        //
        MemoryContext mc2 = new MemoryContext();
        PeerFacade pf2 = new PeerFacade();
        pf2.setContext(mc2);
        pf2.saveDefaultPeer(mama);
        RdfXml.write(mc2.getTriples(), System.out);
    }
}
