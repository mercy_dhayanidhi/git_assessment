/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf;

import static org.junit.Assert.assertTrue;
import static org.tupeloproject.rdf.Resource.resource;

import java.util.Iterator;
import java.util.TreeSet;

import org.junit.Test;
import org.tupeloproject.rdf.terms.Xsd;

public class ResourceTest {
    @Test
    public void testAll() throws Exception {
        // uri refs
        assertTrue(resource("http://foo.bar/baz#blah") instanceof UriRef);
        assertTrue(resource("tag:joe@blah,1923:yez").isUri());
        // blank nodes
        assertTrue(resource("foo") instanceof BlankNode);
        assertTrue(resource("blaz").getLocalId().equals("blaz"));
        assertTrue(resource("fuzzzz").isBlank());
        //assertTrue(!resource("chump").isScoped());
        Resource blank = Resource.blankNode("nab", "urn:f");
        Resource nonBlank = Resource.uriRef("urn:fnib");
        int ct1 = blank.compareTo(nonBlank);
        int ct2 = nonBlank.compareTo(blank);
        switch (ct1) {
            case -1:
                assertTrue(ct2 == 1);
                break;
            case 0:
                assertTrue(ct2 == 0);
                break;
            case 1:
                assertTrue(ct2 == -1);
                break;
            default:
                assertTrue(false);
                break;
        }
    }

    /*
     * RDF Term	Reason
    Unbound results sort earliest.
    _:z	Blank nodes follow unbound.
    _:a	There is no relative ordering of blank nodes.
    <http://script.example/Latin>	IRIs follow blank nodes.
    removed 2 lines
    "http://script.example/Latin"	Simple literals follow IRIs.
    "http://script.example/Latin"^^xsd:string
     */
    @Test
    public void testOrder() throws Exception {
        TreeSet<Resource> ordered = new TreeSet<Resource>();
        ordered.add(Resource.literal("http://script.example/Latin", Xsd.STRING));
        ordered.add(Resource.literal("http://script.example/Latin"));
        ordered.add(Resource.uriRef("http://script.example/Latin"));
        ordered.add(Resource.blankNode("a"));
        ordered.add(Resource.blankNode("b"));
        Iterator<Resource> it = ordered.iterator();
        assert it.next().isBlank();
        assert it.next().isBlank();
        assert it.next().equals(Resource.uriRef("http://script.example/Latin"));
        assert it.next().equals(Resource.literal("http://script.example/Latin"));
        assert it.next().equals(Resource.literal("http://script.example/Latin", Xsd.STRING));
        assert !it.hasNext();
        for (Resource r : ordered ) {
            assert r.compareTo(null) == 1;
        }
    }
}
