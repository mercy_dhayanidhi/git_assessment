/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *  
 * Copyright (c) 2010, NCSA.  All rights reserved.
 *  
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *  
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *  
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package org.tupeloproject.rdf;

import static java.lang.Math.abs;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.tupeloproject.rdf.ObjectResourceMapping.object;
import static org.tupeloproject.rdf.ObjectResourceMapping.resource;

import java.net.URI;
import java.util.Date;

import org.junit.Test;
import org.tupeloproject.rdf.terms.Xsd;

public class ObjectResourceMappingTest {
    @Test
    public void testString() throws Exception {
        String value = "foo";
        assertTrue("string mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testUri() throws Exception {
        URI value = URI.create("http://foo.bar/");
        assertTrue("URI mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testInt() throws Exception {
        Integer value = -74;
        assertTrue("int mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testLong() throws Exception {
        Long value = 958274638L;
        assertTrue("long mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testFloat() throws Exception {
        Float value = (float) -49.25;
        assertTrue("float mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testDouble() throws Exception {
        Double value = 4027.5948;
        assertTrue("double mapping failed", value.equals(object(resource(value))));
    }

    @Test
    public void testDate() throws Exception {
        Date value = new Date();
        Date echo = (Date) object(resource(value));
        // as long as we're within 1s, we're good
        assertTrue("dateTime mapping failed", abs(value.getTime() - echo.getTime()) < 1000.0);
    }

    @Test
    public void testBoolean() throws Exception {
        boolean bs[] = new boolean[] { true, false };
        for (Boolean b : bs ) {
            assertTrue("boolean mapping failed", b.equals(object(resource(b))));
        }
    }

    @Test
    public void testUnsupported() throws Exception {
        try {
            resource(new Object());
            fail("visiting unsupported object type should have failed");
        } catch (IllegalArgumentException x) {
            // this is the correct failure mode.
        }
    }

    @Test
    public void testCompareInt() throws Exception {
        String[] values = new String[] { "-2", "-01", "03", "7", "8", "17" };
        for (int i = 0; i < values.length; i++ ) {
            Comparable a = (Comparable) Resource.literal(values[i], Xsd.INT).asObject();
            for (int j = 0; j < values.length; j++ ) {
                Comparable b = (Comparable) Resource.literal(values[j], Xsd.INT).asObject();
                a.compareTo(b);
            }
        }
    }
}
