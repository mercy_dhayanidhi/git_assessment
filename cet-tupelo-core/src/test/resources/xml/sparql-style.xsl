<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:s="http://www.w3.org/2005/sparql-results#">

  <xsl:output method="text" encoding="us-ascii"/>

  <!-- suppress output -->
  <xsl:template match="text()|@*"/>

  <xsl:template match="/s:sparql/s:results/s:result/s:binding">
    <xsl:value-of select="@name"/>
    <xsl:text> = </xsl:text>
    <xsl:value-of select="*/text()"/>
    <xsl:text>
</xsl:text>
  </xsl:template>

</xsl:stylesheet>