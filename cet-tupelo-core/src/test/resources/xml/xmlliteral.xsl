<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">

    <!-- copy input -->
    <xsl:template
        match="*|@*|comment()|processing-instruction()|text()">
        <xsl:copy>
            <xsl:apply-templates
                select="*|@*|comment()|processing-instruction()|text()"/>
        </xsl:copy>
    </xsl:template>

    <!-- except for xmlliterals -->
    <xsl:template match="*[@rdf:datatype='http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral']/text()">
      <xsl:value-of disable-output-escaping="yes" select="."/>
    </xsl:template>

</xsl:stylesheet>