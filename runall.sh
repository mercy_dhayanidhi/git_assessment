#!/bin/bash

RUNS=50
DATASETS=1000

mvn package -Dmaven.test.skip=true

#echo "drop database jena; create database jena" | /usr/local/mysql/bin/mysql -u jena -pjena

#java -Xmx4G -Xms4G -jar mmdbperf/target/mmdbperf-1.1.0-SNAPSHOT.jar --configuration jena --backend memory --datasets ${DATASETS} --runs ${RUNS}     | tee jena.memory.txt
#java -Xmx4G -Xms4G -jar mmdbperf/target/mmdbperf-1.1.0-SNAPSHOT.jar --configuration jena --backend mysql --datasets ${DATASETS} --runs ${RUNS}      | tee jena.mysql.txt

#java -Xmx4G -Xms4G -jar mmdbperf/target/mmdbperf-1.1.0-SNAPSHOT.jar --configuration tupelo --backend memory --datasets ${DATASETS} --runs ${RUNS}   | tee tupelo.memory.txt
#java -Xmx4G -Xms4G -jar mmdbperf/target/mmdbperf-1.1.0-SNAPSHOT.jar --configuration tupelo --backend newmysql --datasets ${DATASETS} --runs ${RUNS} | tee tupelo.mysql.txt

java -Xmx4G -Xms4G -jar mmdbperf/target/mmdbperf-1.1.0-SNAPSHOT.jar --configuration jpa --backend mysql --datasets ${DATASETS} --runs ${RUNS}       | tee jpa.mysql.txt
