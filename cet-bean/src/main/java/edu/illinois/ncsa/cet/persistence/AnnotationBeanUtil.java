package edu.illinois.ncsa.cet.persistence;

import edu.illinois.ncsa.cet.bean.AnnotationBean;

public interface AnnotationBeanUtil extends BeanUtil<AnnotationBean> {
}
