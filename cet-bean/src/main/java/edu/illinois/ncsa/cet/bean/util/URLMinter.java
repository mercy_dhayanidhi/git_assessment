package edu.illinois.ncsa.cet.bean.util;

import java.io.IOException;
import java.util.UUID;

public class URLMinter implements Minter {
    private String BASE = "QWERTYUIOPASDFGHJKLZXCVBNM1234567890abcdefghijklmnopqrstuvwxyz";

    private String url;

    public URLMinter(String url) {
        this.url = url;
        if (!url.endsWith("/") && !url.endsWith("#")) {
            this.url += "/";
        }
    }

    @Override
    public String createId(Object bean) {
        UUID uuid = UUID.randomUUID();
        try {
            if (!fromShortUUID(toShortUUID(uuid)).equals(uuid)) {
                System.out.println("BAD");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url + toShortUUID(uuid);
    }

    public static String toShortUUID(UUID uuid) {
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        byte[] b = new byte[16];

        for (int i = 0; i < 8; i++ ) {
            b[i] = (byte) (msb >>> 8 * (7 - i));
        }
        for (int i = 8; i < 16; i++ ) {
            b[i] = (byte) (lsb >>> 8 * (7 - i));
        }
        String s = new sun.misc.BASE64Encoder().encode(b).split("=")[0];
        s = s.replace("/", "_").replace("+", "-");
        return s;
    }

    public static UUID fromShortUUID(String uuid) throws IOException {
        byte[] b = new sun.misc.BASE64Decoder().decodeBuffer(uuid.replace("_", "/").replace("-", "+"));
        long msb = 0;
        long lsb = 0;
        for (int i = 0; i < 8; i++ )
            msb = (msb << 8) | (b[i] & 0xff);
        for (int i = 8; i < 16; i++ )
            lsb = (lsb << 8) | (b[i] & 0xff);
        return new UUID(msb, lsb);
    }

    static public void main(String[] args) {
        for (int i = 0; i < 20; i++ ) {
            System.out.println(new URLMinter("http://ncsa.edu/").createId(null));
        }
    }
}
