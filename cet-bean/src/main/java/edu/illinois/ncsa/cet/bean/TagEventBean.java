/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *
 * Copyright (c) 2010 , NCSA.  All rights reserved.
 *
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package edu.illinois.ncsa.cet.bean;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@Entity
@DiscriminatorValue("tagEvent")
@RdfType(type = "tag:Tagging")
@SuppressWarnings("serial")
public class TagEventBean extends AbstractBean {
    /** creator of the tag event */
    @OneToOne
    @RdfPredicate(predicate = "tag:taggedBy")
    private PersonBean   creator   = null;

    /** Time at which event was created */
    @RdfPredicate(predicate = "tag:taggedOn")
    private Date         eventDate = new Date();

    /** Tags associated with this event */
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @JoinTable(name = "TagEventTags")
    @RdfPredicate(predicate = "tag:associatedTag")
    private Set<TagBean> tags      = null;

    public TagEventBean() {
    }

    public PersonBean getCreator() {
        return creator;
    }

    public void setCreator(PersonBean creator) {
        this.creator = creator;
    }

    public Date getTagEventDate() {
        return eventDate;
    }

    public void setTagEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    /**
     * Returns the set of annotations associated with this object.
     * 
     * @return the set of annotations associated with this object.
     */
    public Set<TagBean> getTags() {
        if (tags == null) {
            tags = new HashSet<TagBean>();
        }
        return tags;
    }

    /**
     * Sets the set of annotations associated with this object.
     * 
     * @param annotations
     *            the set of annotations of the object.
     */
    public void setTags(Collection<TagBean> tags) {
        getTags().clear();
        if (tags != null) {
            getTags().addAll(tags);
        }
    }

    /**
     * Add the annotation to the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be added.
     */
    public void addTag(TagBean tag) {
        if (tag != null) {
            getTags().add(tag);
        }
    }

    /**
     * Remove the annotation from the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be removed.
     */
    public void removeTag(TagBean tag) {
        getTags().remove(tag);
    }

}
