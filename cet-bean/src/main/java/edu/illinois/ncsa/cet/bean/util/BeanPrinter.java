package edu.illinois.ncsa.cet.bean.util;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.illinois.ncsa.cet.bean.AbstractBean;

public class BeanPrinter {
    /**
     * Print all information that is known about the bean. This will go through
     * the bean, find all getters and prints the values for the getter. If the
     * getter returns another AbstractBean it will print the values for this
     * bean as
     * well. This will print the bean name and print the rest of the bean with
     * two spaces as prefix. The bean will be printed to stdout.
     * 
     * @param bean
     *            the bean to be printed.
     */
    static public void debug(AbstractBean bean) {
        debug(new PrintWriter(System.out, true), bean);
    }

    /**
     * Print all information that is known about the bean. This will go through
     * the bean, find all getters and prints the values for the getter. If the
     * getter returns another AbstractBean it will print the values for this
     * bean as
     * well. This will print the bean name and print the rest of the bean with
     * two spaces as prefix. The bean will be printed to the printstream.
     * 
     * @param pw
     *            the printstream the data should be printed to.
     * @param bean
     *            the bean to be printed.
     */
    public static void debug(PrintWriter pw, AbstractBean bean) {
        if (bean == null) {
            return;
        }
        pw.println(String.format("%-15s : %s", "bean", bean.getClass().getName())); //$NON-NLS-1$ //$NON-NLS-2$
        debug(pw, bean, "  "); //$NON-NLS-1$
    }

    /**
     * Print all information that is known about the bean. This will go through
     * the bean, find all getters and prints the values for the getter. If the
     * getter returns another AbstractBean it will print the values for this
     * bean as
     * well the prefix will be increased by two spaces.
     * 
     * @param bean
     *            the bean to be printed.
     * @param prefix
     *            the number of spaces before the bean information is printed.
     */
    static public void debug(AbstractBean bean, String prefix) {
        debug(new PrintWriter(System.out, true), bean, prefix);
    }

    /**
     * Print all information that is known about the bean. This will go through
     * the bean, find all getters and prints the values for the getter. If the
     * getter returns another AbstractBean it will print the values for this
     * bean as
     * well the prefix will be increased by two spaces.
     * 
     * @param pw
     *            the printstream the data should be printed to.
     * @param bean
     *            the bean to be printed.
     * @param prefix
     *            the number of spaces before the bean information is printed.
     */
    static public void debug(PrintWriter pw, AbstractBean bean, String prefix) {
        if (bean == null) {
            return;
        }

        List<Method> later = new ArrayList<Method>();
        List<Method> methods = new ArrayList<Method>(Arrays.asList(bean.getClass().getMethods()));
        Collections.sort(methods, new Comparator<Method>() {
            public int compare(Method o1, Method o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (Method m : methods ) {
            if (m.getName().startsWith("get") && (m.getParameterTypes().length == 0)) { //$NON-NLS-1$
                String name = m.getName().substring(3);
                try {
                    Object o = m.invoke(bean);
                    if (debug(pw, o, name, prefix, false)) {
                        later.add(m);
                    }
                } catch (Exception e) {
                    pw.println(String.format("%s%-15s : %s", prefix, name, e.getMessage())); //$NON-NLS-1$
                }
            }
        }
        for (Method m : later ) {
            String name = m.getName().substring(3);
            try {
                Object o = m.invoke(bean);
                debug(pw, o, name, prefix, true);
            } catch (Exception e) {
                pw.println(String.format("%s%-15s : %s", prefix, name, e.getMessage())); //$NON-NLS-1$
            }
        }

        //        List<Field> later = new ArrayList<Field>();
        //        List<Field> fields = new ArrayList<Field>(Arrays.asList(bean.getClass().getDeclaredFields()));
        //        Collections.sort(fields, new Comparator<Field>() {
        //            public int compare(Field o1, Field o2) {
        //                return o1.getName().compareTo(o2.getName());
        //            }
        //        });
        //        for (Field f : fields ) {
        //            f.setAccessible(true);
        //            try {
        //                Object o = f.get(bean);
        //                if (debug(pw, o, f.getName(), prefix, false)) {
        //                    later.add(f);
        //                }
        //            } catch (Exception e) {
        //                e.printStackTrace();
        //                pw.println(String.format("%s%-15s : %s", prefix, f.getName(), e.getMessage())); //$NON-NLS-1$
        //            }
        //        }
        //        for (Field f : later ) {
        //            f.setAccessible(true);
        //            try {
        //                Object o = f.get(bean);
        //                debug(pw, o, f.getName(), prefix, true);
        //            } catch (Exception e) {
        //                e.printStackTrace();
        //                pw.println(String.format("%s%-15s : %s", prefix, f.getName(), e.getMessage())); //$NON-NLS-1$
        //            }
        //        }
    }

    /**
     * Helper function to print the value, or loop through the collection/array
     * as needed. It will call debug if the object is a bean.
     * 
     * @param obj
     *            the object to be printed.
     * @param name
     *            the name of the object.
     * @param prefix
     *            number of spaces in front of the name.
     * @param goDeep
     *            if this is true it can go deep into the object.
     * @return if the object can be expanded this function returns true if
     *         goDeep is false.
     */
    private static boolean debug(PrintWriter pw, Object obj, String name, String prefix, boolean goDeep) {
        if (obj == null) {
            pw.println(String.format("%s%-15s : %s", prefix, name, "null")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            return false;
        } else if (obj instanceof AbstractBean) {
            if (goDeep) {
                pw.println(String.format("%s%-15s : %s", prefix, name, obj.getClass().getName())); //$NON-NLS-1$
                debug((AbstractBean) obj, prefix + "  "); //$NON-NLS-1$
                return false;
            } else {
                return true;
            }
        } else if (obj instanceof Collection<?>) {
            if (goDeep) {
                for (Object o : (Collection<?>) obj ) {
                    debug(pw, o, name, prefix, true);
                }
                return false;
            } else {
                return true;
            }
        } else if (obj.getClass().isArray()) {
            if (goDeep) {
                for (Object o : Arrays.asList(obj) ) {
                    debug(pw, o, name, prefix, true);
                }
                return false;
            } else {
                return true;
            }
        } else {
            pw.println(String.format("%s%-15s : %s", prefix, name, obj.toString().replaceAll("\n", " "))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            return false;
        }
    }

}
