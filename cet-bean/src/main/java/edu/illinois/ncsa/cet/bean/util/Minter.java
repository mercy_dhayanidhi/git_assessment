package edu.illinois.ncsa.cet.bean.util;

public interface Minter {
    String createId(Object bean);
}
