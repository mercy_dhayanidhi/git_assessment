package edu.illinois.ncsa.cet.persistence;

import edu.illinois.ncsa.cet.bean.TagEventBean;

public interface TagEventBeanUtil extends BeanUtil<TagEventBean> {
}
