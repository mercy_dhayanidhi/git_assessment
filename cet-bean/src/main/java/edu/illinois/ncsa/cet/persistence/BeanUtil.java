package edu.illinois.ncsa.cet.persistence;

import java.util.Collection;

public interface BeanUtil<T> {
    Class<T> getBeanClass() throws PersistenceException;

    T create() throws PersistenceException;

    int count() throws PersistenceException;

    Collection<String> getAllIds() throws PersistenceException;

    Collection<T> loadAll() throws PersistenceException;

    Collection<T> loadAll(boolean includeDeleted) throws PersistenceException;

    T load(String id) throws PersistenceException;

    T save(T object) throws PersistenceException;
}
