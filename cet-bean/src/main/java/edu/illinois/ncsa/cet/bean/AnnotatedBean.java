package edu.illinois.ncsa.cet.bean;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import edu.illinois.ncsa.sematic.annotation.RdfPredicate;

@Entity
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@SuppressWarnings("serial")
public abstract class AnnotatedBean extends AbstractBean {
    /** Annotations */
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @JoinTable(name = "Annotations")
    @RdfPredicate(predicate = "cet:annotation/hasAnnotation")
    private Set<AnnotationBean> annotations = null;

    /** Tags */
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @JoinTable(name = "TagEvents")
    @RdfPredicate(predicate = "tag:tag")
    private Set<TagEventBean>   tagEvents   = null;

    public AnnotatedBean() {
    }

    /**
     * Returns the set of annotations associated with this object.
     * 
     * @return the set of annotations associated with this object.
     */
    public Set<AnnotationBean> getAnnotations() {
        if (annotations == null) {
            annotations = new HashSet<AnnotationBean>();
        }
        return annotations;
    }

    /**
     * Sets the set of annotations associated with this object.
     * 
     * @param annotations
     *            the set of annotations of the object.
     */
    public void setAnnotations(Collection<AnnotationBean> annotations) {
        getAnnotations().clear();
        if (annotations != null) {
            getAnnotations().addAll(annotations);
        }
    }

    /**
     * Add the annotation to the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be added.
     */
    public void addAnnotation(AnnotationBean annotation) {
        if (annotation != null) {
            getAnnotations().add(annotation);
        }
    }

    /**
     * Remove the annotation from the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be removed.
     */
    public void removeAnnotation(AnnotationBean annotation) {
        getAnnotations().remove(annotation);
    }

    /**
     * Returns the set of annotations associated with this object.
     * 
     * @return the set of annotations associated with this object.
     */
    public Set<TagEventBean> getTagEvents() {
        if (tagEvents == null) {
            tagEvents = new HashSet<TagEventBean>();
        }
        return tagEvents;
    }

    /**
     * Sets the set of annotations associated with this object.
     * 
     * @param annotations
     *            the set of annotations of the object.
     */
    public void setTagEvents(Collection<TagEventBean> tagEvents) {
        getAnnotations().clear();
        if (tagEvents != null) {
            getTagEvents().addAll(tagEvents);
        }
    }

    /**
     * Add the annotation to the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be added.
     */
    public void addTagEvent(TagEventBean tagEvent) {
        if (tagEvent != null) {
            getTagEvents().add(tagEvent);
        }
    }

    /**
     * Remove the annotation from the set of annotations.
     * 
     * @param annotation
     *            the AnnotationBean to be removed.
     */
    public void removeTagEvent(TagEventBean tagEvent) {
        getTagEvents().remove(tagEvent);
    }

}
