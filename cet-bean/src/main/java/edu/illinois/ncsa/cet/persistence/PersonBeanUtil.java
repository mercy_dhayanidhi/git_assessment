package edu.illinois.ncsa.cet.persistence;

import edu.illinois.ncsa.cet.bean.PersonBean;

public interface PersonBeanUtil extends BeanUtil<PersonBean> {
}
