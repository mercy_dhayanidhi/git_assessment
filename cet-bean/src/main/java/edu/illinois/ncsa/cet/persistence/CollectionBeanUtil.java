package edu.illinois.ncsa.cet.persistence;

import edu.illinois.ncsa.cet.bean.CollectionBean;

public interface CollectionBeanUtil extends BeanUtil<CollectionBean> {
}
