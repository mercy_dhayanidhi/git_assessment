package edu.illinois.ncsa.cet.bean.util;

import java.util.UUID;

public class CETMinter implements Minter {

    @Override
    public String createId(Object bean) {
        String name = bean.getClass().getSimpleName();
        int idx = name.lastIndexOf("Bean"); //$NON-NLS-1$
        if (idx != -1) {
            name = name.substring(0, idx);
        }
        return "tag:cet.ncsa.uiuc.edu,2008:/bean/" + name + "/" + UUID.randomUUID().toString(); //$NON-NLS-1$ //$NON-NLS-2$
    }
}
