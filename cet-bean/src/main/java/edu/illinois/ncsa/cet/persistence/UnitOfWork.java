package edu.illinois.ncsa.cet.persistence;

public interface UnitOfWork {
    void begin() throws PersistenceException;

    void end() throws PersistenceException;

    //void abort() throws PersistenceException;
}
