package edu.illinois.ncsa.cet.persistence;

import java.util.Collection;

import edu.illinois.ncsa.cet.bean.AbstractBean;
import edu.illinois.ncsa.cet.bean.AnnotatedBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;

public interface TagBeanUtil extends BeanUtil<TagBean> {
    Collection<AbstractBean> getTagged(String word) throws PersistenceException;

    Collection<String> getTags() throws PersistenceException;

    void addTags(AnnotatedBean toBeTagged, PersonBean creator, String... tags) throws PersistenceException;
}
