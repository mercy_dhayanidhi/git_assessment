package edu.illinois.ncsa.cet.bean.rbac;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import edu.illinois.ncsa.cet.bean.AnnotatedBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;

//@Entity
//@DiscriminatorValue("Group")
//@RdfType(type = "foaf:Group")
//@RdfNameSpace(ns = "foaf=http://xmlns.com/foaf/0.1/")
//@SuppressWarnings("serial")
public class Group extends AnnotatedBean {
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @RdfPredicate(predicate = "foaf:member")
    private Set<PersonBean> members;

    /**
     * Return the set of PersonBeans that represents those that are members
     * of the group.
     * 
     * @return set of PersonBeans that represents all the users that are
     *         members of the group.
     */
    public Set<PersonBean> getMembers() {
        if (members == null) {
            members = new HashSet<PersonBean>();
        }
        return members;
    }

    /**
     * Set the set of PersonBeans that represents those that contributed to the
     * artifact.
     * 
     * @param contributors
     *            the set of contributors to the artifact.
     */
    public void setMembers(Collection<PersonBean> members) {
        getMembers().clear();
        if (members != null) {
            getMembers().addAll(members);
        }
    }

    /**
     * Add the contributor to the set of contributors to the artifact.
     * 
     * @param contributor
     *            the PersonBean of the contributor to be added.
     */
    public void addMmeber(PersonBean member) {
        if (member != null) {
            getMembers().add(member);
        }
    }

    /**
     * Remove the contributor from the set of contributors to the worflow tool.
     * 
     * @param contributor
     *            the PersonBean of the contributor to be removed.
     */
    public void removeMember(PersonBean member) {
        getMembers().remove(member);
    }

}
