/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *
 * Copyright (c) 2010 , NCSA.  All rights reserved.
 *
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package edu.illinois.ncsa.cet.bean;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@Entity
@DiscriminatorValue("Annotation")
@RdfType(type = "cet:Annotation")
@SuppressWarnings("serial")
public class AnnotationBean extends AnnotatedBean {
    /** Title of the annotation */
    @RdfPredicate(predicate = "dc:title")
    private String     title       = "";        //$NON-NLS-1$

    /** creator of the annotation */
    @OneToOne
    @RdfPredicate(predicate = "dc:creator")
    private PersonBean creator     = null;

    /** Date the artifact is created */
    @RdfPredicate(predicate = "dc:date")
    private Date       date        = new Date();

    /** Description of the annotation */
    @RdfPredicate(predicate = "dc:description")
    @Lob
    private String     description = "";        //$NON-NLS-1$

    /**
     * Create a new instance of the annotation.
     */
    public AnnotationBean() {
    }

    /**
     * Return the title of the artifact.
     * 
     * @return title of the artifact
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the artifact
     * 
     * @param title
     *            sets the title of the artifact.
     * 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return the description of the artifact.
     * 
     * @return description of the artifact
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the artifact
     * 
     * @param description
     *            sets the description of the artifact
     * 
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return the date when the artifact was created.
     * 
     * @return date the artifact was created.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date when the artifact was created.
     * 
     * @param date
     *            sets the date when the artifact was created.
     * 
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Return the PersonBean that is the creator of the artifact
     * 
     * @return PersonBean that represents the creator
     */
    public PersonBean getCreator() {
        return creator;
    }

    /**
     * Sets the PersonBean that represents the creator of the artifact.
     * 
     * @param creator
     *            sets the PersonBeans that represents the creator of the
     *            artifact.
     * 
     */
    public void setCreator(PersonBean creator) {
        this.creator = creator;
    }
}
