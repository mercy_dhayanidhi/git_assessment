package edu.illinois.ncsa.cet.persistence;

public class PersistenceException extends Exception {
    private static final long serialVersionUID = 1L;

    public PersistenceException() {
        super();
    }

    public PersistenceException(String msg) {
        super(msg);
    }

    public PersistenceException(Throwable thr) {
        super(thr);
    }

    public PersistenceException(String msg, Throwable thr) {
        super(msg, thr);
    }
}
