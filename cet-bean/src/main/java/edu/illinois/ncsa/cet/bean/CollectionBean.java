/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *
 * Copyright (c) 2010 , NCSA.  All rights reserved.
 *
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package edu.illinois.ncsa.cet.bean;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfType;

@Entity
@DiscriminatorValue("Collection")
@RdfType(type = "cet:Collection")
@SuppressWarnings("serial")
public class CollectionBean extends AnnotatedBean {
    @RdfPredicate(predicate = "dc:description")
    private String           description;

    @OneToOne
    @RdfPredicate(predicate = "dc:creator")
    private PersonBean       creator;

    @RdfPredicate(predicate = "dcterms:created")
    private Date             creationDate;

    @RdfPredicate(predicate = "dcterms:modified")
    private Date             lastModifiedDate;

    @RdfPredicate(predicate = "dc:title")
    private String           title;

    /** List of datasets in collection. */
    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinTable(name = "CollectionMembers")
    @Column(nullable = true)
    @RdfPredicate(predicate = "dcterms:hasPart")
    private Set<DatasetBean> members = null;

    public CollectionBean() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PersonBean getCreator() {
        return creator;
    }

    public void setCreator(PersonBean creator) {
        this.creator = creator;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof CollectionBean)) {
            return false;
        }
        CollectionBean cb = (CollectionBean) object;
        return checkEquals(cb.getTitle(), getTitle()) && checkEquals(cb.getCreator(), getCreator()) && checkEquals(cb.getCreationDate(), getCreationDate()) && checkEquals(cb.getLastModifiedDate(), getLastModifiedDate()) && checkEquals(cb.getDescription(), getDescription()) && checkEquals(cb.getId(), getId());
    }

    boolean checkEquals(Object o1, Object o2) {
        if (o1 == null) {
            if (o2 == null) {
                return true;
            } else {
                return false;
            }
        } else {
            if (o2 == null) {
                return false;
            } else {
                return o1.equals(o2);
            }
        }

    }

    /**
     * Return the set of PersonBeans that represents those that are contributors
     * to the artifact.
     * 
     * @return set of PersonBeans that represents all the users that have
     *         contributes to the artifact.
     */
    public Set<DatasetBean> getMembers() {
        if (members == null) {
            members = new HashSet<DatasetBean>();
        }
        return members;
    }

    /**
     * Set the set of PersonBeans that represents those that contributed to the
     * artifact.
     * 
     * @param contributors
     *            the set of contributors to the artifact.
     */
    public void setMembers(Collection<DatasetBean> members) {
        getMembers().clear();
        if (members != null) {
            getMembers().addAll(members);
        }
    }

    /**
     * Add the contributor to the set of contributors to the artifact.
     * 
     * @param contributor
     *            the PersonBean of the contributor to be added.
     */
    public void addMember(DatasetBean member) {
        if (member != null) {
            getMembers().add(member);
        }
    }

    /**
     * Remove the contributor from the set of contributors to the worflow tool.
     * 
     * @param contributor
     *            the PersonBean of the contributor to be removed.
     */
    public void removeMember(DatasetBean member) {
        getMembers().remove(member);
    }
}
