package edu.illinois.ncsa.cet.bean;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import edu.illinois.ncsa.cet.bean.util.CETMinter;
import edu.illinois.ncsa.cet.bean.util.Minter;
import edu.illinois.ncsa.sematic.annotation.RdfNameSpace;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;

//@MappedSuperclass
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@SuppressWarnings("serial")
@RdfNameSpace(ns = { "dc=http://purl.org/dc/elements/1.1/", "dcterms=http://purl.org/dc/terms/", "cet=http://cet.ncsa.uiuc.edu/2007/", "tag=http://www.holygoat.co.uk/owl/redwood/0.1/tags/" })
public abstract class AbstractBean implements Serializable {
    public static Minter minter  = new CETMinter();

    /** Unique identifier for this bean */
    @Id
    @RdfPredicate(predicate = "dc:identifier", subject = true)
    private String       id      = null;

    /** Whether bean is marked as deleted */
    @RdfPredicate(predicate = "cet:isDeleted")
    private boolean      deleted = false;

    public AbstractBean() {
        id = minter.createId(this);
    }

    public AbstractBean(String id) {
        this.id = id;
    }

    /**
     * @deprecated use getId()
     */
    @Deprecated
    public String getUri() {
        return getId();
    }

    /**
     * Return the id of the bean.
     * 
     * @return id of the bean
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id of the bean
     * 
     * @param id
     *            sets the id of the bean.
     * 
     */
    public void setId(String id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
