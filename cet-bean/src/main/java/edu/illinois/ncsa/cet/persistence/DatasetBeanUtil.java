package edu.illinois.ncsa.cet.persistence;

import java.util.List;
import java.util.Set;

import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;

public interface DatasetBeanUtil extends BeanUtil<DatasetBean> {
    List<DatasetBean> getOrderedList(String orderBy, boolean desc, int offset, int limit) throws PersistenceException;

    Set<DatasetBean> getByCreator(PersonBean person) throws PersistenceException;
}
