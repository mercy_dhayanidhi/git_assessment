package org.tupeloproject.mysql.context;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.context.AbstractTripleContextTest;
import org.tupeloproject.kernel.context.TripleContext;


public class MysqlContextTripleTest extends AbstractTripleContextTest {

	@Override
	protected TripleContext getContext() throws Exception {
		NewMysqlContext mysql = new NewMysqlContext();
		
		try {
			mysql.destroy();
		} catch (OperatorException e) {
			e.printStackTrace();
		}
		
		mysql.initialize();
		
		return mysql;
	}

}
