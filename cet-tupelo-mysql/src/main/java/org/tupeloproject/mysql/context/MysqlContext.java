package org.tupeloproject.mysql.context;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.NotFoundException;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.context.AbstractTripleBlobContext;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Literal;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.UriRef;
import org.tupeloproject.rdf.query.OrderBy;
import org.tupeloproject.rdf.query.Pattern;
import org.tupeloproject.util.ByteCountingInputStream;
import org.tupeloproject.util.ListTable;
import org.tupeloproject.util.Tuple;
import org.tupeloproject.util.Variable;

/**
 * 
 * @author Rob Kooper
 * 
 */
public class MysqlContext extends AbstractTripleBlobContext {
    private static Log            log        = LogFactory.getLog(MysqlContext.class);
    private static final String[] tupCols    = new String[] { "sub", "pre", "obj" };

    private String                schema     = "tupelo";                             //$NON-NLS-1$
    private String                user       = "tupelo";                             //$NON-NLS-1$
    private String                password   = "tupelo";                             //$NON-NLS-1$
    private String                host       = "localhost";                          //$NON-NLS-1$
    private int                   port       = 3306;
    private BasicDataSource       datasource = null;
    private String                tableType  = "InnoDB";                             //$NON-NLS-1$

    public MysqlContext() {
    }

    /**
     * Database host. Default "localhost"
     * 
     * @return host
     */
    public String getHost() {
        return host;
    }

    /**
     * Database host
     * 
     * @param host
     *            hostname or IP address
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Port. Default depends on database implementation
     * 
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * Port.
     * 
     * @param port
     *            the port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Schema or database ID (optional) for databases that support
     * multiple schemas or sub-databases.
     * 
     * @return schema or database ID
     */
    public String getSchema() {
        return schema;
    }

    /**
     * Schema or database ID
     * 
     * @param schema
     *            schema or database ID
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * Username used to access database
     * 
     * @return username
     */
    public String getUser() {
        return user;
    }

    /**
     * Username used to access database
     * 
     * @param user
     *            username
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Password used to access database
     * 
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password used to access database
     * 
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public void setTableType(String tt) {
        if ("InnoDB".equals(tt) || "MyISAM".equals(tt)) {
            tableType = tt;
        } else {
            throw new IllegalArgumentException("illegal table type " + tt + ": legal ones are InnoDB, MyISAM");
        }
    }

    public String getTableType() {
        return tableType;
    }

    // the names of the symbol, blob, and tuple tables
    protected String nodeTable() {
        return "sym";
    }

    protected String tripleTable() {
        return "tup";
    }

    protected String blobTable() {
        return "blb";
    }

    // ----------------------------------------------------------------------
    // CONTEXT
    // ----------------------------------------------------------------------

    @Override
    public void open() throws OperatorException {
        if (datasource == null) {
            datasource = new BasicDataSource();
            datasource.setDriverClassName("com.mysql.jdbc.Driver"); //$NON-NLS-1$
            datasource.setUsername(user);
            datasource.setPassword(password);
            datasource.setRemoveAbandoned(true);
            datasource.setRemoveAbandonedTimeout(30); // 30 seconds
            datasource.setUrl(String.format("jdbc:mysql://%s:%d/?characterEncoding=utf8", host, port)); //$NON-NLS-1$
        }
    }

    @Override
    public void close() throws OperatorException {
        try {
            datasource.close();
            datasource = null;
        } catch (SQLException e) {
            log.debug("Could not close JDBC connection.", e);
            datasource = null;
            throw (new OperatorException("Could not close JDBC connection.", e));
        }
    }

    @Override
    public void sync() throws OperatorException {
    }

    private Connection getConnection() throws OperatorException {
        open();
        try {
            return datasource.getConnection();
        } catch (SQLException e) {
            log.debug("Could not get connection.", e);
            throw (new OperatorException("Could not get connection.", e));
        }
    }

    @Override
    public void initialize() throws OperatorException {
        Connection c = null;
        Statement s = null;

        try {
            c = getConnection();
            s = c.createStatement();

            s.executeUpdate("create database if not exists " + schema);

            s.executeUpdate("create table " + schema + "." + nodeTable() + " (" + "uid bigint not null auto_increment primary key," + "hsh varchar(32)," + "sym text character set utf8," + "unique index(hsh)" + ") type=" + getTableType());
            s.executeUpdate("create table " + schema + "." + tripleTable() + " (" + "sub bigint not null," + "pre bigint not null," + "obj bigint not null," + "typ bigint," + "unique(sub,pre,obj)," + "index(sub)," + "index(pre)," + "index(obj)," + "index(sub,pre)," + "index(sub,obj)," + "index(pre,obj)" + ") type=" + getTableType());
            s.executeUpdate("create table " + schema + "." + blobTable() + " (" + "bid bigint not null primary key," + "bda longblob" + ") type=" + getTableType());

        } catch (SQLException e) {
            throw (new OperatorException("Could not create schema", e));
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.warn("Could not close statement.", e);
                }
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.warn("Could not close connection.", e);
                }
            }
        }
    }

    @Override
    public void destroy() throws OperatorException {
        Connection c = null;
        Statement s = null;

        try {
            c = getConnection();
            s = c.createStatement();
            s.executeUpdate("drop table if exists " + schema + "." + nodeTable());
            s.executeUpdate("drop table if exists " + schema + "." + tripleTable());
            s.executeUpdate("drop table if exists " + schema + "." + blobTable());

        } catch (SQLException e) {
            throw (new OperatorException("Could not drop schema", e));
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.warn("Could not close statement.", e);
                }
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.warn("Could not close connection.", e);
                }
            }
        }
    }

    // ----------------------------------------------------------------------
    // TRIPLE OPERATIONS
    // ----------------------------------------------------------------------

    public void perform(TripleWriter tw) throws OperatorException {
        while (true) { // retry loop
            Connection c = getConnection();
            try {
                PreparedStatement intern = c.prepareStatement("insert ignore into " + schema + "." + nodeTable() + " (hsh,sym) values (md5(?),?)");
                PreparedStatement insertTyped = c.prepareStatement("insert ignore into " + schema + "." + tripleTable() + " select " + "" + getSymSubquery() + " as sub, " + "" + getSymSubquery() + " as pre, " + "" + getSymSubquery() + " as obj, " + "" + getSymSubquery() + " as typ");
                PreparedStatement insertPlain = c.prepareStatement("insert ignore into " + schema + "." + tripleTable() + " select " + "" + getSymSubquery() + " as sub, " + "" + getSymSubquery() + " as pre, " + "" + getSymSubquery() + " as obj, " + "null");
                c.setAutoCommit(false);
                for (Triple t : tw.getToAdd() ) {
                    String s = toSqlString(t.getSubject());
                    intern.setString(1, s);
                    intern.setString(2, s);
                    intern.executeUpdate();

                    s = toSqlString(t.getPredicate());
                    intern.setString(1, s);
                    intern.setString(2, s);
                    intern.executeUpdate();

                    s = toSqlString(t.getObject());
                    intern.setString(1, s);
                    intern.setString(2, s);
                    intern.executeUpdate();

                    Resource o = t.getObject();
                    if (o.isTypedLiteral()) {
                        String dt = toSqlString(o.getDatatype());
                        intern.setString(1, dt);
                        intern.setString(2, dt);
                        intern.executeUpdate();
                    }
                }
                intern.close();
                // DEBUG ok. now we make sure that all these symbols exist
                /*
                PreparedStatement check = c.prepareStatement(getSymSubquery());
                for(Triple t : tw.getToAdd()) {
                    for(Resource r : t) {
                        check.setString(1, toSqlString(r));
                        ResultSet rs = check.executeQuery();
                        if(!rs.next()) {
                            System.out.println("MysqlContext FAILED to intern "+toSqlString(r));
                        }
                        rs.close();
                    }
                }
                check.close();
                */
                // END DEBUG
                for (Triple t : tw.getToAdd() ) {
                    Resource s = t.getSubject();
                    Resource p = t.getPredicate();
                    Resource o = t.getObject();
                    PreparedStatement insert = null;
                    if (o.isTypedLiteral()) {
                        insert = insertTyped;
                        String dt = toSqlString(o.getDatatype());
                        insert.setString(4, dt);
                    } else {
                        insert = insertPlain;
                    }
                    insert.setString(1, toSqlString(s));
                    insert.setString(2, toSqlString(p));
                    insert.setString(3, toSqlString(o));
                    insert.executeUpdate();
                }
                insertTyped.close();
                insertPlain.close();
                PreparedStatement toast = c.prepareStatement("delete from " + schema + "." + tripleTable() + " " + "where sub=" + getSymSubquery() + " " + "and pre=" + getSymSubquery() + " " + "and obj=" + getSymSubquery() + "");
                for (Triple t : tw.getToRemove() ) {
                    toast.setString(1, toSqlString(t.getSubject()));
                    toast.setString(2, toSqlString(t.getPredicate()));
                    toast.setString(3, toSqlString(t.getObject()));
                    toast.executeUpdate();
                }
                toast.close();
                commit(c);
                c.close();
                return;
            } catch (SQLException x) {
                if (x.getErrorCode() != 1213) { // 1213 is 'deadlock; retry'
                    try {
                        c.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    throw new OperatorException("sql failed with code " + x.getErrorCode(), x);
                } else {
                    try {
                        // roll back the transaction so we can retry
                        c.rollback();
                    } catch (SQLException sx) {
                        // rollback failed, we're in deep doo-doo
                        System.err.println("rollback failed, attempting to release connection anyway");
                        sx.printStackTrace();
                    }
                    try {
                        c.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void perform(Unifier u) throws OperatorException {
        // a unifier query is built to have one self-join on the tup
        // table per pattern. the table aliases are p{n} where n is
        // the ordinal pattern number.
        //
        // first we need to know which variables are bound and which are
        // unbound. for each bound variable we need to construct a clause
        // on the correct SQL column to test for a match. for each unbound
        // that appears more than once we need to create a join condition
        int nPats = u.getPatterns().size();
        // there is at most one clause and binding for each term in each pattern
        String[][] clauses = new String[nPats][3];
        Resource[][] bindings = new Resource[nPats][3];
        // maps variable to column names
        Map<String, String> joinColumns = new HashMap<String, String>();
        // maps variable to column names of "datatype" columns, for literals
        Map<String, String> joinTypeColumns = new HashMap<String, String>();
        // pseudo-columns representing the vars
        Map<String, String> pseudoColumns = new HashMap<String, String>();
        for (int p = 0; p < nPats; p++ ) {
            Pattern pattern = u.getPatterns().get(p);
            for (int v = 0; v < 3; v++ ) {
                Variable<Resource> var = pattern.get(v);
                String sqlColumn = "p" + p + "." + tupCols[v];
                if (var.isBound()) {
                    // this is the query that looks up the uid of the node referred to in a bound term
                    // (e.g., "p3.s=(select uid from sym where sym=?)")
                    clauses[p][v] = sqlColumn + "=" + getSymSubquery();
                    bindings[p][v] = var.getValue();
                } else {
                    String joinColumn = joinColumns.get(var.getName());
                    // the first mention of the column does not generate a join clause
                    if (joinColumn == null) {
                        joinColumns.put(var.getName(), sqlColumn);
                    } else {
                        // >1st mention joins back to the first mention
                        clauses[p][v] = sqlColumn + "=" + joinColumn;
                    }
                    // if an object, we have a type column
                    if (v == 2) {
                        joinTypeColumns.put(var.getName(), "p" + p + ".typ");
                    }
                }
            }
        }
        // now that we've generated clauses, we need to format them into a query
        // now construct the entire query
        String query = "select distinct \n";
        // construct the unifier column subselects
        // (e.g., "(select sym from sym where uid=p0.pre) as c2")
        // this is what associates unifier columns (c{n}) with unbound pattern variables (p{n}.[spo])
        String symTextColumn = nodeTable();
        int nCols = u.getColumnNames().size();
        for (int c = 0; c < nCols; c++ ) {
            String variableName = u.getColumnNames().get(c);
            if (c > 0) {
                query += ", \n";
            }
            String sqlColumnName = joinColumns.get(variableName);
            query += "(select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=" + sqlColumnName + ") as c" + c;
            String sqlTypeColumnName = joinTypeColumns.get(variableName);
            if (sqlTypeColumnName != null) {
                query += ", (select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=" + sqlTypeColumnName + ") as c" + c + "_typ";
            }
            // record the association between the variable and the pseudo-column name c{n}
            pseudoColumns.put(variableName, "c" + c);
        }
        if (nCols == 0) {
            query += "'match' as dummy ";
        }
        // we have to keep track of what order we add the in parameters to the query
        // so we don't set them in the wrong order
        List<Resource> inParameters = new LinkedList<Resource>();
        query += " \nfrom " + schema + "." + tripleTable() + " p0\n";
        // now the from clauses
        for (int p = 1; p < nPats; p++ ) {
            if (u.getPatterns().get(p).isOptional()) {
                query += "left outer join ";
            } else {
                query += "join ";
            }
            query += schema + "." + tripleTable() + " p" + p + " on ";
            List<String> conditions = new LinkedList<String>();
            for (int v = 0; v < 3; v++ ) {
                if (clauses[p][v] != null) {
                    conditions.add(clauses[p][v]);
                    // make a note that we need to set this in parameter
                    if (bindings[p][v] != null) {
                        inParameters.add(bindings[p][v]);
                    }
                }
            }
            query += andify(conditions) + " \n";
        }
        // any conditions on the first pattern need to be added as a trailing where clause
        List<String> whereClauses = new LinkedList<String>();
        for (int v = 0; v < 3; v++ ) {
            if (clauses[0][v] != null) {
                whereClauses.add(clauses[0][v]);
                if (bindings[0][v] != null) {
                    inParameters.add(bindings[0][v]);
                }
            }
        }
        query += "where " + andify(whereClauses);
        // order by
        List<OrderBy> obs = u.getOrderBy();
        if (obs != null && obs.size() > 0) {
            query += " order by ";
            boolean first = true;
            for (OrderBy ob : u.getOrderBy() ) {
                if (!first) {
                    query += ", ";
                }
                first = false;
                // we want to order on pseduo-column c{n} representing the given var
                query += orderByExpression(pseudoColumns.get(ob.getName()));
                if (ob.isAscending()) {
                    query += " asc";
                } else {
                    query += " desc";
                }
            }
        }
        // limit and offset
        if (!u.hasNoLimit() || u.getOffset() != 0) {
            query += " " + getLimitClause(u);
        }
        // now construct the query
        Connection c = getConnection();
        try {
            PreparedStatement statement = c.prepareStatement(query);
            // prepare the query
            // set the values
            int pi = 1;
            for (Resource value : inParameters ) {
                statement.setString(pi++, toSqlString(value));
            }
            // execute the query
            ResultSet rs = statement.executeQuery();
            // convert the results to a table
            ListTable<Resource> result = new ListTable<Resource>();
            result.setColumnNames(new Tuple<String>(u.getColumnNames()));
            while (rs.next()) {
                Tuple<Resource> row = new Tuple<Resource>(nCols);
                for (int col = 0; col < nCols; col++ ) {
                    String res = rs.getString("c" + col);
                    String resType = null;
                    if (joinTypeColumns.get(u.getColumnNames().get(col)) != null) {
                        resType = rs.getString("c" + col + "_typ");
                    }
                    row.set(col, fromSqlString(res, resType));
                }
                result.addRow(row);
            }
            rs.close();
            statement.close();
            u.setResult(result);
            // we're done!
        } catch (SQLException x) {
            throw new OperatorException("sql exception executing " + query, x);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    protected String andify(List<String> clauses) {
        return andify(clauses, "and");
    }

    protected String andify(List<String> clauses, String and) {
        String query = "";
        boolean needsAnd = false;
        for (String clause : clauses ) {
            if (needsAnd) {
                query += " " + and + " ";
            }
            query += clause;
            needsAnd = true;
        }
        return query;
    }

    protected String getLimitClause(Unifier u) throws OperatorException {
        long offset = u.getOffset();
        if (offset > 0 && u.hasNoLimit()) {
            throw new OperatorException("if query has nonzero offset, must have limit"); // FIXME postprocess
        }
        return "limit " + offset + "," + u.getLimit();
    }

    protected String orderByExpression(String columnName) {
        return columnName;
    }

    public void perform(TripleMatcher tm) throws OperatorException {
        Connection c = getConnection();
        try {
            Resource s = tm.getSubject();
            Resource p = tm.getPredicate();
            Resource o = tm.getObject();
            String symTextColumn = nodeTable();
            PreparedStatement matchQuery = c.prepareStatement("select " + "(select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=sub) as sub, " + "(select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=pre) as pre, " + "(select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=obj) as obj, " + "(select " + symTextColumn + " from " + schema + "." + nodeTable() + " where uid=typ) as typ " + "from " + schema + "." + tripleTable() + " where " + (s == null ? "1=1 " : "sub=" + getSymSubquery() + " ") + "and " + (p == null ? "1=1 " : "pre=" + getSymSubquery() + " ") + "and " + (o == null ? "1=1" : "obj=" + getSymSubquery() + ""));
            int pi = 1;
            if (s != null) {
                matchQuery.setString(pi++, toSqlString(s));
            }
            if (p != null) {
                matchQuery.setString(pi++, toSqlString(p));
            }
            if (o != null) {
                matchQuery.setString(pi++, toSqlString(o));
            }
            ResultSet resultSet = matchQuery.executeQuery();
            Set<Triple> matchingTriples = new HashSet<Triple>();
            while (resultSet.next()) {
                String ms = resultSet.getString(1);
                String mp = resultSet.getString(2);
                String mo = resultSet.getString(3);
                String mt = resultSet.getString(4);
                Resource rs = fromSqlString(ms);
                Resource rp = fromSqlString(mp);
                Resource ro = fromSqlString(mo, mt);
                Triple matchingTriple = new Triple(rs, rp, ro);
                matchingTriples.add(matchingTriple);
            }
            resultSet.close();
            matchQuery.close();
            tm.setIterator(matchingTriples.iterator());
        } catch (SQLException x) {
            throw new OperatorException("sql exception", x);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void perform(TripleIterator ti) throws OperatorException {
        TripleMatcher tm = new TripleMatcher();
        perform(tm);
        ti.setIterator(tm.getIterator());
    }

    // ----------------------------------------------------------------------
    // BLOB OPERATIONS
    // ----------------------------------------------------------------------

    public void perform(BlobWriter bw) throws OperatorException {
        while (true) { // retry loop
            Connection c = getConnection();
            InputStream bwIs = bw.getInputStream();
            if (bwIs.markSupported()) {
                bwIs.mark(Integer.MAX_VALUE);
            }
            try {
                PreparedStatement intern = c.prepareStatement("insert ignore into " + schema + "." + nodeTable() + " (hsh,sym) values (md5(?),?)");
                PreparedStatement delete = c.prepareStatement("delete from " + schema + "." + blobTable() + " where bid=" + getSymSubquery() + "");
                PreparedStatement insert = c.prepareStatement("insert into " + schema + "." + blobTable() + " select " + getSymSubquery() + " as bid, ? as bda");
                c.setAutoCommit(false);
                String blobUri = toSqlString(bw.getSubject());
                intern.setString(1, blobUri);
                intern.setString(2, blobUri);
                delete.setString(1, blobUri);
                insert.setString(1, blobUri);
                ByteCountingInputStream is = new ByteCountingInputStream(bwIs);
                insert.setBinaryStream(2, is, -1);
                intern.executeUpdate();
                intern.close();
                delete.executeUpdate();
                delete.close();
                insert.executeUpdate();
                insert.close();
                commit(c);
                bw.setSize(is.getBytesRead());
                c.close();
                return;
            } catch (SQLException e) {
                if (e.getErrorCode() != 1213 && e.getErrorCode() != 1062) { // 1213 is 'deadlock; retry', 1062 is duplicate key
                    try {
                        c.close();
                    } catch (SQLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    throw new OperatorException("sql failed during blob write with code " + e.getErrorCode(), e);
                } else {
                    try {
                        // roll back the transaction so we can retry
                        c.rollback();
                        // reset the stream for retry
                        if (bwIs.markSupported()) {
                            try {
                                bwIs.reset();
                            } catch (IOException x) {
                                throw new OperatorException("concurrent access, non-markable stream: retry", e);
                            }
                        } else {
                            throw new OperatorException("concurrent access, non-markable stream: retry", e);
                        }
                    } catch (SQLException sx) {
                        sx.printStackTrace();
                    } finally {
                        try {
                            c.close();
                        } catch (SQLException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void perform(BlobFetcher bf) throws OperatorException {
        final Connection c = getConnection();
        try {
            final PreparedStatement retrieve = c.prepareStatement("select bda from " + schema + "." + blobTable() + " where bid=" + getSymSubquery() + "");
            retrieve.setString(1, toSqlString(bf.getSubject()));
            retrieve.executeQuery();
            final ResultSet rs = retrieve.getResultSet();
            if (!rs.next()) {
                try {
                    rs.close();
                    retrieve.close();
                    throw new NotFoundException("blob not found");
                } finally {
                    c.close();
                }
            }
            bf.setInputStream(new FilterInputStream(rs.getBinaryStream(1)) {
                public void close() throws IOException {
                    super.close();
                    try {
                        rs.close();
                        retrieve.close();
                    } catch (SQLException x) {
                        IOException ix = new IOException("sql error");
                        ix.initCause(x);
                        throw ix;
                    } finally {
                        try {
                            c.close();
                        } catch (SQLException x) {
                            IOException iox = new IOException("sql error: " + x.getMessage());
                            iox.initCause(x);
                            throw iox;
                        }
                    }
                }
            });
        } catch (SQLException e) {
            try {
                c.close();
            } catch (SQLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            throw new OperatorException("sql failed reading blob " + bf.getSubject(), e);
        }
        // don't release the connection--that happens when the caller closes the blob input stream
    }

    @Override
    public void perform(BlobIterator bi) throws OperatorException {
        Connection c = getConnection();
        try {
            PreparedStatement blobs = c.prepareStatement("select " + nodeTable() + " from " + schema + "." + nodeTable() + ", " + schema + "." + blobTable() + " where uid=bid");
            List<Resource> result = new LinkedList<Resource>();
            ResultSet rs = blobs.executeQuery();
            while (rs.next()) {
                result.add(fromSqlString(rs.getString(1)));
            }
            rs.close();
            blobs.close();
            bi.setIterator(result.iterator());
        } catch (SQLException x) {
            throw new OperatorException("sql failed listing blobs");
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void perform(BlobRemover br) throws OperatorException {
        Connection c = getConnection();
        try {
            PreparedStatement delete = c.prepareStatement("delete from " + schema + "." + blobTable() + " where bid=" + getSymSubquery() + "");
            delete.setString(1, toSqlString(br.getSubject()));
            c.setAutoCommit(false);
            delete.executeUpdate();
            commit(c);
            delete.close();
        } catch (SQLException e) {
            throw new OperatorException("sql failed removing blob " + br.getSubject(), e);
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    // ----------------------------------------------------------------------
    // HELPER FUNCTIONS
    // ----------------------------------------------------------------------

    protected void commit(Connection c) throws SQLException {
        c.commit();
        c.setAutoCommit(true);
    }

    public String getSymSubquery() {
        return "(select uid from " + schema + "." + nodeTable() + " where hsh=md5(?))";
    }

    protected String toSqlString(Resource r) throws OperatorException {
        if (r.isLiteral()) {
            return "l" + r.getString();
        } else if (r.isUri()) {
            return "u" + r.getString();
        } else if (r.isBlank()) {
            return "b" + r.getLocalId();
        } else {
            throw new OperatorException("can't convert to sql: " + r);
        }
    }

    protected Resource fromSqlString(String s) throws OperatorException {
        if (s == null) {
            return null;
        }
        String pfx = s.substring(0, 1);
        String sfx = s.substring(1);
        if (pfx.equals("l")) {
            return Resource.literal(sfx);
        } else if (pfx.equals("u")) {
            return Resource.uriRef(sfx);
        } else if (pfx.equals("b")) {
            return Resource.blankNode(sfx);
        } else {
            throw new OperatorException("can't convert from sql: " + s);
        }
    }

    protected Resource fromSqlString(String s, String t) throws OperatorException {
        Resource r = fromSqlString(s);
        if (t != null) {
            ((Literal) r).setDatatype((UriRef) fromSqlString(t));
        }
        return r;
    }
}
