package org.tupeloproject.mysql.context;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.OperatorNotSupportedException;
import org.tupeloproject.kernel.context.AbstractTripleBlobContext;
import org.tupeloproject.kernel.operator.BlobFetcher;
import org.tupeloproject.kernel.operator.BlobIterator;
import org.tupeloproject.kernel.operator.BlobRemover;
import org.tupeloproject.kernel.operator.BlobWriter;
import org.tupeloproject.kernel.operator.TripleIterator;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.TripleWriter;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.Triple;
import org.tupeloproject.rdf.query.OrderBy;
import org.tupeloproject.util.Table;
import org.tupeloproject.util.Tuple;
import org.tupeloproject.util.UnicodeTranscoder;

/**
 * 
 * @author Rob Kooper
 * 
 */
public class NewMysqlContext extends AbstractTripleBlobContext {
    private static Log      log           = LogFactory.getLog(NewMysqlContext.class);
    private static String[] columns       = { "subject", "predicate", "object" };    //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    private String          database      = "tupelo";                                //$NON-NLS-1$
    private String          username      = "tupelo";                                //$NON-NLS-1$
    private String          password      = "tupelo";                                //$NON-NLS-1$
    private String          hostname      = "localhost";                             //$NON-NLS-1$
    private int             port          = 3306;
    private int             indexsize     = 100;
    private int             maxPacketSize = 1000000;
    private BasicDataSource datasource    = null;

    public NewMysqlContext() {
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getIndexsize() {
        return indexsize;
    }

    public void setIndexsize(int indexsize) {
        this.indexsize = indexsize;
    }

    // ----------------------------------------------------------------------
    // CONTEXT
    // ----------------------------------------------------------------------

    @Override
    public void open() throws OperatorException {
        if (datasource == null) {
            datasource = new BasicDataSource();
            datasource.setDriverClassName("com.mysql.jdbc.Driver"); //$NON-NLS-1$
            datasource.setUsername(username);
            datasource.setPassword(password);
            datasource.setRemoveAbandoned(true);
            datasource.setRemoveAbandonedTimeout(30); // 30 seconds
            datasource.setUrl(String.format("jdbc:mysql://%s:%d/?characterEncoding=utf8", hostname, port)); //$NON-NLS-1$
        }
    }

    @Override
    public void close() throws OperatorException {
        try {
            datasource.close();
            datasource = null;
        } catch (SQLException e) {
            log.debug("Could not close JDBC connection.", e);
            datasource = null;
            throw (new OperatorException("Could not close JDBC connection.", e));
        }
    }

    @Override
    public void sync() throws OperatorException {
    }

    private Connection getConnection() throws OperatorException {
        open();
        try {
            if (datasource.getNumActive() > datasource.getMaxActive() - 2) {
                log.info(String.format("Connection stats : A=%d I=%d MA=%d", datasource.getNumActive(), datasource.getNumIdle(), datasource.getMaxActive()));
            }
            return datasource.getConnection();
        } catch (SQLException e) {
            log.debug("Could not get connection.", e);
            throw (new OperatorException("Could not get connection.", e));
        }
    }

    @Override
    public void initialize() throws OperatorException {
        Connection c = null;
        Statement s = null;

        try {
            c = getConnection();
            s = c.createStatement();

            s.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database); //$NON-NLS-1$

            StringBuilder sb = new StringBuilder();

            sb.append("CREATE  TABLE IF NOT EXISTS `").append(database).append("`.`triples` (\n"); //$NON-NLS-1$ //$NON-NLS-2$
            sb.append("`subject` TEXT NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("`subject-hash` INTEGER NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("`predicate` TEXT NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("`predicate-hash` INTEGER NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("`object` TEXT NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("`object-hash` INTEGER NOT NULL ,\n"); //$NON-NLS-1$
            sb.append("UNIQUE INDEX `spo` USING BTREE (subject(").append(indexsize).append(") ASC, predicate(").append(indexsize).append(") ASC, object(").append(indexsize).append(") ASC, `subject-hash`, `predicate-hash`, `object-hash`) ,\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            sb.append("INDEX `pos` USING BTREE (predicate(").append(indexsize).append(") ASC, object(").append(indexsize).append(") ASC, subject(").append(indexsize).append(") ASC) ,\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            sb.append("INDEX `osp` USING BTREE (object(").append(indexsize).append(") ASC, subject(").append(indexsize).append(") ASC, predicate(").append(indexsize).append(") ASC) )\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            sb.append("ENGINE = MyISAM"); //$NON-NLS-1$

            s.executeUpdate(sb.toString());
        } catch (SQLException e) {
            throw (new OperatorException("Could not create schema", e));
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.warn("Could not close statement.", e);
                }
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.warn("Could not close connection.", e);
                }
            }
        }
    }

    @Override
    public void destroy() throws OperatorException {
        Connection c = null;
        Statement s = null;

        try {
            c = getConnection();
            s = c.createStatement();
            s.executeUpdate("DROP DATABASE IF EXISTS " + database); //$NON-NLS-1$
        } catch (SQLException e) {
            throw (new OperatorException("Could not drop schema", e));
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.warn("Could not close statement.", e);
                }
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.warn("Could not close connection.", e);
                }
            }
        }
    }

    // ----------------------------------------------------------------------
    // TRIPLE OPERATIONS
    // ----------------------------------------------------------------------       

    public void perform(TripleWriter tw) throws OperatorException {
        Connection c = null;
        Statement s = null;

        try {
            c = getConnection();
            s = c.createStatement();

            int inserts = 0;
            if (tw.getToAdd().size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (Triple t : tw.getToAdd() ) {
                    if (sb.length() == 0) {
                        sb.append("INSERT IGNORE INTO ").append(database).append(".`triples` VALUES"); //$NON-NLS-1$ //$NON-NLS-2$
                    } else {
                        sb.append(", \n"); //$NON-NLS-1$
                    }
                    inserts++;

                    String str = toSQL(t.getSubject());
                    sb.append("('").append(str).append("',").append(str.hashCode()); //$NON-NLS-1$ //$NON-NLS-2$

                    str = toSQL(t.getPredicate());
                    sb.append(",'").append(str).append("',").append(str.hashCode()); //$NON-NLS-1$ //$NON-NLS-2$

                    str = toSQL(t.getObject());
                    sb.append(",'").append(str).append("',").append(str.hashCode()).append(")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

                    if (sb.length() > maxPacketSize) {
                        s.addBatch(sb.toString());
                        sb.setLength(0);
                    }
                }

                if (sb.length() > 0) {
                    s.addBatch(sb.toString());
                }
            }

            for (Triple t : tw.getToRemove() ) {
                StringBuilder sb = new StringBuilder();

                sb.append("DELETE IGNORE FROM ").append(database).append(".`triples` WHERE"); //$NON-NLS-1$ //$NON-NLS-2$

                String str = toSQL(t.getSubject());
                sb.append(" subject = '").append(str).append("'"); //$NON-NLS-1$ //$NON-NLS-2$

                str = toSQL(t.getPredicate());
                sb.append(" AND predicate = '").append(str).append("'"); //$NON-NLS-1$ //$NON-NLS-2$

                str = toSQL(t.getObject());
                sb.append(" AND object = '").append(str).append("'"); //$NON-NLS-1$ //$NON-NLS-2$

                s.addBatch(sb.toString());
            }

            int[] result = s.executeBatch();
            //            int idx = 0;
            //            if (inserts > 0) {
            //                if (result[idx] != inserts) {
            //                    System.out.println("INSERTS MISSING, DUPLICATES?");
            //                }
            //                idx++;
            //            }
            //            while (idx < result.length) {
            //                if (result[idx] != 1) {
            //                    System.out.println("DELETE MISSING, DID NOT EXIST?");
            //                }
            //                idx++;
            //            }

        } catch (Exception e) {
            throw (new OperatorException("TripleWriter failed.", e));
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.warn("Could not close statement.", e);
                }
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.warn("Could not close connection.", e);
                }
            }
        }
    }

    public void perform(Unifier uf) throws OperatorException {
        StringBuilder wh = new StringBuilder();
        StringBuilder jn = new StringBuilder();
        HashMap<String, String> vars = new HashMap<String, String>();

        //        # [?o, http://www.w3.org/1999/02/22-rdf-syntax-ns#type, ?oType] = optonal

        //        # [tag:cet.ncsa.uiuc.edu,2008:/bean/Dataset/23, ?p, ?o]         = required
        //        # [?o, http://www.w3.org/1999/02/22-rdf-syntax-ns#type, ?oType] = optonal
        //        SELECT STRAIGHT_JOIN p000.predicate AS `p`, p000.object AS `o`, p001.object AS `ot` FROM tupelo.triples AS p000
        //        LEFT OUTER JOIN tupelo.triples AS p001 ON p001.subject = p000.object AND p001.predicate = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
        //        WHERE p000.subject = '<http://org.tupeloproject/bean#org.tupeloproject.kernel.beans.PersonBean>'

        //        # [tag:cet.ncsa.uiuc.edu,2008:/bean/Dataset/23, ?p, ?o]         = required
        //        # [?o, http://www.w3.org/1999/02/22-rdf-syntax-ns#type, ?oType] = required
        //        SELECT STRAIGHT_JOIN p000.predicate AS `p`, p000.object AS `o`, p001.object AS `ot` FROM tupelo.triples AS p000
        //        JOIN tupelo.triples AS p001 ON p001.subject = p000.object AND p001.predicate = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
        //        WHERE p000.subject = '<http://org.tupeloproject/bean#org.tupeloproject.kernel.beans.PersonBean>'

        // create crazy select
        // - first pattern == where and from
        // - all others are joins
        // -- optional is left outer join
        // -- required is join

        // first pattern
        org.tupeloproject.rdf.query.Pattern p = uf.getPatterns().get(0);
        for (int i = 0; i < 3; i++ ) {
            if (!p.get(i).isBound()) {
                if (!vars.containsKey(p.get(i).getName())) {
                    vars.put(p.get(i).getName(), "p000." + columns[i]); //$NON-NLS-1$
                } else {
                    wh.append(" AND p000.").append(columns[i]).append(" = ").append(vars.get(p.get(i).getName())); //$NON-NLS-1$ //$NON-NLS-2$                  
                }
            } else {
                wh.append(" AND p000.").append(columns[i]).append(" = '").append(toSQL(p.get(i).getValue())).append("'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$                    
            }
        }

        // all other patterns
        for (int j = 1; j < uf.getPatterns().size(); j++ ) {
            p = uf.getPatterns().get(j);
            String sx = String.format("p%03d", j); //$NON-NLS-1$
            boolean join = false;
            for (int i = 0; i < 3; i++ ) {
                if (!p.get(i).isBound()) {
                    if (!vars.containsKey(p.get(i).getName())) {
                        vars.put(p.get(i).getName(), sx + "." + columns[i]); //$NON-NLS-1$
                    } else {
                        if (!join) {
                            if (p.isOptional()) {
                                jn.append(" LEFT OUTER JOIN ").append(database).append(".triples AS ").append(sx); //$NON-NLS-1$ //$NON-NLS-2$
                            } else {
                                jn.append(" JOIN ").append(database).append(".triples AS ").append(sx); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            jn.append(" ON "); //$NON-NLS-1$
                            join = true;
                        } else {
                            jn.append(" AND "); //$NON-NLS-1$
                        }
                        jn.append(sx).append(".").append(columns[i]).append(" = ").append(vars.get(p.get(i).getName())); //$NON-NLS-1$ //$NON-NLS-2$                            
                    }
                } else {
                    if (!join) {
                        if (p.isOptional()) {
                            jn.append(" LEFT OUTER JOIN ").append(database).append(".triples AS ").append(sx); //$NON-NLS-1$ //$NON-NLS-2$
                        } else {
                            jn.append(" JOIN ").append(database).append(".triples AS ").append(sx); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        jn.append(" ON "); //$NON-NLS-1$
                        join = true;
                    } else {
                        jn.append(" AND "); //$NON-NLS-1$
                    }
                    jn.append(sx).append(".").append(columns[i]).append(" = '").append(toSQL(p.get(i).getValue())).append("'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
            }
            if (join) {
                jn.append("\n"); //$NON-NLS-1$
            }
        }

        // build query
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT STRAIGHT_JOIN "); //$NON-NLS-1$
        if (vars.size() == 0) {
            sb.append("1"); //$NON-NLS-1$
        } else {
            boolean comma = false;
            for (Entry<String, String> entry : vars.entrySet() ) {
                if (comma) {
                    sb.append(", "); //$NON-NLS-1$
                }
                comma = true;
                sb.append(entry.getValue()).append(" AS `").append(entry.getKey()).append("`"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        sb.append(" FROM ").append(database).append(".triples AS p000\n"); //$NON-NLS-1$ //$NON-NLS-2$
        if (jn.length() > 0) {
            sb.append(jn);
        }
        if (wh.length() > 0) {
            sb.append(" WHERE ").append(wh.substring(5)); //$NON-NLS-1$
        }

        // add any paging to the query
        if (uf.hasPaging()) {
            if (uf.getOrderBy().size() > 0) {
                sb.append(" ORDER BY"); //$NON-NLS-1$
                boolean comma = false;
                for (OrderBy o : uf.getOrderBy() ) {
                    if (comma) {
                        sb.append(","); //$NON-NLS-1$
                    }
                    comma = true;
                    if (o.isAscending()) {
                        sb.append(" `").append(o.getName()).append("`"); //$NON-NLS-1$ //$NON-NLS-2$
                    } else {
                        sb.append(" `").append(o.getName()).append("` DESC"); //$NON-NLS-1$//$NON-NLS-2$
                    }
                }
                sb.append("\n"); //$NON-NLS-1$
            }
            if ((uf.getLimit() > 0) || (uf.getOffset() > 0)) {
                sb.append(" LIMIT "); //$NON-NLS-1$
                if (uf.getOffset() > 0) {
                    sb.append(uf.getOffset()).append(", "); //$NON-NLS-1$
                }
                if (uf.getLimit() > 0) {
                    sb.append(uf.getLimit());
                }
            }
        }

        uf.setResult(new MyTableterator(sb.toString(), uf.getColumnNames()));
    }

    public void perform(TripleMatcher tm) throws OperatorException {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT subject, predicate, object FROM " + database + ".`triples`"); //$NON-NLS-1$ //$NON-NLS-2$

        boolean haswhere = false;
        if (tm.getSubject() != null) {
            haswhere = true;
            sb.append(" WHERE"); //$NON-NLS-1$
            sb.append(" subject = '").append(toSQL(tm.getSubject())).append("'"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (tm.getPredicate() != null) {
            if (!haswhere) {
                haswhere = true;
                sb.append(" WHERE"); //$NON-NLS-1$
            } else {
                sb.append(" AND"); //$NON-NLS-1$
            }
            sb.append(" predicate = '").append(toSQL(tm.getPredicate())).append("'"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (tm.getObject() != null) {
            if (!haswhere) {
                haswhere = true;
                sb.append(" WHERE"); //$NON-NLS-1$
            } else {
                sb.append(" AND"); //$NON-NLS-1$
            }
            sb.append(" object = '").append(toSQL(tm.getObject())).append("'"); //$NON-NLS-1$ //$NON-NLS-2$
        }

        tm.setIterator(new MyTripleIterator(sb.toString()));
    }

    public void perform(TripleIterator ti) throws OperatorException {
        ti.setIterator(new MyTripleIterator("SELECT subject, predicate, object FROM " + database + ".triples")); //$NON-NLS-1$ //$NON-NLS-2$
    }

    // ----------------------------------------------------------------------
    // BLOB OPERATIONS
    // ----------------------------------------------------------------------       

    @Override
    public void perform(BlobFetcher op) throws OperatorException {
        throw (new OperatorNotSupportedException("BlobFetcher is not supported."));
    }

    @Override
    public void perform(BlobIterator op) throws OperatorException {
        throw (new OperatorNotSupportedException("BlobIterator is not supported."));
    }

    @Override
    public void perform(BlobRemover op) throws OperatorException {
        throw (new OperatorNotSupportedException("BlobRemover is not supported."));
    }

    @Override
    public void perform(BlobWriter op) throws OperatorException {
        throw (new OperatorNotSupportedException("BlobRemover is not supported."));
    }

    // ----------------------------------------------------------------------
    // RESULT ITERATOR
    // ----------------------------------------------------------------------

    class MyTripleIterator implements Iterator<Triple>, Closeable {
        private MyTableterator iter;

        public MyTripleIterator(String sql) {
            iter = new MyTableterator(sql, columns);
        }

        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }

        @Override
        public Triple next() {
            Tuple<Resource> row = iter.next();
            if (row != null) {
                return new Triple(row.get(0), row.get(1), row.get(2));
            }
            return null;
        }

        @Override
        public void remove() {
        }

        @Override
        public void close() throws IOException {
            iter.close();
        }
    }

    class MyTableterator implements Table<Resource>, Iterator<Tuple<Resource>>, Iterable<Tuple<Resource>>, Closeable {
        private Connection      c  = null;
        private Statement       s  = null;
        private ResultSet       rs = null;
        private Tuple<String>   names;
        private Tuple<Resource> row;

        public MyTableterator(String sql, String... names) {
            this(sql, Arrays.asList(names));
        }

        public MyTableterator(String sql, List<String> names) {
            this.names = new Tuple<String>(names);
            try {
                c = getConnection();
                s = c.createStatement();
                rs = s.executeQuery(sql);
            } catch (Exception e) {
                log.error("Could not execute SQL statement.", e);
                try {
                    close();
                } catch (IOException e1) {
                    log.error("Could not get close connection.", e1);
                }
            }
        }

        @Override
        public Tuple<String> getColumnNames() {
            return names;
        }

        @Override
        public Iterator<Tuple<Resource>> iterator() {
            return this;
        }

        @Override
        public boolean hasNext() {
            if (rs == null) {
                return false;
            }
            try {
                if (row == null) {
                    if (rs.next()) {
                        row = new Tuple<Resource>(names.size());
                        for (int i = 0; i < names.size(); i++ ) {
                            String x = rs.getString(names.get(i));
                            if (x != null) {
                                row.set(i, fromSQL(x));
                            }
                        }
                    } else {
                        close();
                    }
                }
            } catch (Exception e) {
                log.error("Could not get row.", e);
                try {
                    close();
                } catch (IOException e1) {
                    log.error("Could not get close connection.", e1);
                }
            }
            return (row != null);
        }

        @Override
        public Tuple<Resource> next() {
            if ((row != null) || hasNext()) {
                Tuple<Resource> x = row;
                row = null;
                return x;
            }
            return null;
        }

        @Override
        public void remove() {
        }

        public void close() throws IOException {
            IOException ioe = null;
            row = null;
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    log.debug("Could not close resultset.", e);
                    ioe = new IOException("Could not close resultset", e);
                }
                rs = null;
            }
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    log.debug("Could not close statement.", e);
                    ioe = new IOException("Could not close statement", e);
                }
                s = null;
            }
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    log.debug("Could not close connection.", e);
                    ioe = new IOException("Could not close connection", e);
                }
                c = null;
            }

            if (ioe != null) {
                throw (ioe);
            }
        }
    }

    // ----------------------------------------------------------------------
    // HELPER FUNCTIONS
    // ----------------------------------------------------------------------       

    private String toSQL(Resource x) throws OperatorException {
        return x.toNTriples().replace("'", "\\'"); //$NON-NLS-1$ //$NON-NLS-2$
    }

    private Resource fromSQL(String x) throws OperatorException {
        try {
            x = x.replace("\\'", "'"); //$NON-NLS-1$ //$NON-NLS-2$
            if (x.startsWith("<")) { //$NON-NLS-1$
                return Resource.uriRef(x.substring(1, x.length() - 1));
            } else if (x.startsWith("_:")) { //$NON-NLS-1$
                return Resource.blankNode(x.substring(2));
            } else {
                Matcher m = Pattern.compile("\"(.*)\"\\^\\^\\<(.*)\\>").matcher(x); //$NON-NLS-1$
                if (m.find()) {
                    return Resource.literal(UnicodeTranscoder.decode(m.group(1)), m.group(2));
                }

                m = Pattern.compile("\"(.*)\"\\@(.*)").matcher(x); //$NON-NLS-1$
                if (m.find()) {
                    return Resource.plainLiteral(UnicodeTranscoder.decode(m.group(1)), m.group(2));
                }
            }
            return Resource.literal(UnicodeTranscoder.decode(x.substring(1, x.length() - 1)));
        } catch (ParseException exc) {
            throw (new OperatorException("Error parsing ntriple.", exc));
        }
    }
}
