package org.tupeloproject.mysql;

import org.tupeloproject.kernel.Context;

/**
 * @deprecated use org.tupeloproject.kernel.context.NewMysqlContext
 */
public class NewMysqlContext extends org.tupeloproject.mysql.context.NewMysqlContext implements Context {
}
