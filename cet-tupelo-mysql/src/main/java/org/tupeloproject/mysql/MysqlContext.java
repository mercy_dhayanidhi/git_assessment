package org.tupeloproject.mysql;

import org.tupeloproject.kernel.Context;

/**
 * @deprecated use org.tupeloproject.kernel.context.MysqlContext
 */
public class MysqlContext extends org.tupeloproject.mysql.context.MysqlContext implements Context {
}
