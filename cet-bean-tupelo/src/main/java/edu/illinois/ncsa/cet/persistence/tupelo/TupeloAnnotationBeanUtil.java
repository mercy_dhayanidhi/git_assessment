package edu.illinois.ncsa.cet.persistence.tupelo;

import org.tupeloproject.kernel.beans.Session;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.AnnotationBean;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;

public class TupeloAnnotationBeanUtil extends TupeloBeanUtil<AnnotationBean> implements AnnotationBeanUtil {

    @Inject
    protected TupeloAnnotationBeanUtil(Session session, Provider<AnnotationBean> creator) {
        super(session, creator);
    }
}
