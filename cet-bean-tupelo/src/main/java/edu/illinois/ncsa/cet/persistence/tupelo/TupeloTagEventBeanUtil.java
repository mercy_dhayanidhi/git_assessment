package edu.illinois.ncsa.cet.persistence.tupelo;

import org.tupeloproject.kernel.beans.Session;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class TupeloTagEventBeanUtil extends TupeloBeanUtil<TagEventBean> implements TagEventBeanUtil {

    @Inject
    protected TupeloTagEventBeanUtil(Session session, Provider<TagEventBean> creator) {
        super(session, creator);
    }
}
