package edu.illinois.ncsa.cet.persistence.tupelo;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.BeanException;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.terms.Tags;
import org.tupeloproject.util.Tuple;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.AbstractBean;
import edu.illinois.ncsa.cet.bean.AnnotatedBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;
import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;

public class TupeloTagBeanUtil extends TupeloBeanUtil<TagBean> implements TagBeanUtil {
    @Inject
    private Provider<TagEventBeanUtil> tagEventProvider;

    @Inject
    protected TupeloTagBeanUtil(Session session, Provider<TagBean> creator) {
        super(session, creator);
    }

    @Override
    public Collection<AbstractBean> getTagged(String word) throws PersistenceException {
        Set<AbstractBean> result = new HashSet<AbstractBean>();

        Unifier uf = new Unifier();
        uf.addPattern("tb", Tags.HAS_TAG_TITLE, Resource.literal(word));
        uf.addPattern("tb", Tags.TAGGED_WITH_TAG, "object");
        uf.setColumnNames("tb", "object");

        try {
            session.getBeanFactory().getContext().perform(uf);
        } catch (OperatorException e) {
            new PersistenceException("Could not get tags.", e);
        }

        for (Tuple<Resource> row : uf.getResult() ) {
            try {
                result.add((AbstractBean) session.load(null, row.get(1)));
            } catch (BeanException e) {
                throw (new PersistenceException("Could not load tagged object.", e));
            }
        }
        return result;
    }

    @Override
    public Collection<String> getTags() throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addTags(AnnotatedBean toBeTagged, PersonBean creator, String... tags) throws PersistenceException {
        TagEventBeanUtil tebu = tagEventProvider.get();

        TagEventBean teb = tebu.create();
        teb.setCreator(creator);
        teb.setTagEventDate(new Date());

        for (String tag : tags ) {
            TagBean t = create();
            t.setTag(tag);
            teb.addTag(save(t));
        }

        toBeTagged.getTagEvents().add(tebu.save(teb));
    }
}
