package edu.illinois.ncsa.cet.persistence.tupelo;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.PeerFacade;
import org.tupeloproject.kernel.beans.BeanFactory;
import org.tupeloproject.kernel.beans.BeanInfo;
import org.tupeloproject.kernel.beans.BeanUtil;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.context.Context;
import org.tupeloproject.kernel.context.TripleContext;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import edu.illinois.ncsa.cet.bean.AnnotationBean;
import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.TagBean;
import edu.illinois.ncsa.cet.bean.TagEventBean;
import edu.illinois.ncsa.cet.persistence.AnnotationBeanUtil;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagEventBeanUtil;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;

public class TupeloBeanModule extends AbstractModule {
    private static Log           log             = LogFactory.getLog(TupeloBeanModule.class);

    private String               persistenceName = null;

    private ThreadLocal<Session> session         = new ThreadLocal<Session>();

    public TupeloBeanModule(String persistenceName) {
        this.persistenceName = persistenceName;
    }

    @Override
    protected void configure() {
        bind(DatasetBeanUtil.class).to(TupeloDatasetBeanUtil.class);
        try {
            BeanInfo.add(DatasetBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }
        bind(CollectionBeanUtil.class).to(TupeloCollectionBeanUtil.class);
        try {
            BeanInfo.add(CollectionBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }
        bind(PersonBeanUtil.class).to(TupeloPersonBeanUtil.class);
        try {
            BeanInfo.add(PersonBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }
        bind(TagEventBeanUtil.class).to(TupeloTagEventBeanUtil.class);
        try {
            BeanInfo.add(TagEventBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }
        bind(TagBeanUtil.class).to(TupeloTagBeanUtil.class);
        try {
            BeanInfo.add(TagBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }
        bind(AnnotationBeanUtil.class).to(TupeloAnnotationBeanUtil.class);
        try {
            BeanInfo.add(AnnotationBean.class);
        } catch (Exception e) {
            log.error("Could not register bean.");
        }

        bind(UnitOfWork.class).to(TupeloUnitOfWork.class);
    }

    @Provides
    BeanUtil getBeanUtil(BeanFactory factory) {
        return new BeanUtil(factory);
    }

    @Provides
    Session getSession(BeanFactory factory) {
        if ((session.get() == null) || session.get().wasComitted()) {
            session.set(factory.openSession());
        }
        return session.get();
    }

    @Provides
    @Singleton
    BeanFactory getBeanFactory() {
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> resources = cl.getResources("META-INF/" + persistenceName + ".xml");
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                InputStream is = url.openStream();
                try {
                    Context c = PeerFacade.fromXml(is);
                    if (c instanceof TripleContext) {
                        //                        try {
                        //                            c.destroy();
                        //                        } catch (OperatorException e) {
                        //                            e.printStackTrace();
                        //                        }
                        //c.initialize();
                        return new BeanFactory((TripleContext) c);
                    }
                } finally {
                    is.close();
                }
            }
            log.error("Could not find conext with name " + persistenceName);
        } catch (Exception e) {
            log.error("Could not load conext from xmlfile " + persistenceName, e);
        }
        return null;
    }
}
