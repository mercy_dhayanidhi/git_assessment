package edu.illinois.ncsa.cet.persistence.tupelo;

import org.tupeloproject.kernel.beans.Session;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;

public class TupeloPersonBeanUtil extends TupeloBeanUtil<PersonBean> implements PersonBeanUtil {

    @Inject
    protected TupeloPersonBeanUtil(Session session, Provider<PersonBean> creator) {
        super(session, creator);
    }

    //    public PersonBean getAnonymous() {
    //        if (anonymous == null) {
    //            anonymous = find(PersonBean.class, ANONYMOUS);
    //            if (anonymous == null) {
    //                anonymous = new PersonBean();
    //                anonymous.setId(ANONYMOUS);
    //                anonymous.setName("Anonymous");
    //            }
    //        }
    //        return anonymous;
    //    }

}
