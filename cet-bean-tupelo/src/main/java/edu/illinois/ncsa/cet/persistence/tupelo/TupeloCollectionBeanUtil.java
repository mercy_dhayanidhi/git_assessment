package edu.illinois.ncsa.cet.persistence.tupelo;

import org.tupeloproject.kernel.beans.Session;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;

public class TupeloCollectionBeanUtil extends TupeloBeanUtil<CollectionBean> implements CollectionBeanUtil {

    @Inject
    protected TupeloCollectionBeanUtil(Session session, Provider<CollectionBean> creator) {
        super(session, creator);
    }
}
