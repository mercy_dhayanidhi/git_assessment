package edu.illinois.ncsa.cet.persistence.tupelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.terms.Cet;
import org.tupeloproject.rdf.terms.Dc;
import org.tupeloproject.rdf.terms.Rdf;
import org.tupeloproject.util.Tuple;

import com.google.inject.Inject;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public class TupeloDatasetBeanUtil extends TupeloBeanUtil<DatasetBean> implements DatasetBeanUtil {

    @Inject
    protected TupeloDatasetBeanUtil(Session session, Provider<DatasetBean> creator) {
        super(session, creator);
    }

    @Override
    public List<DatasetBean> getOrderedList(String orderBy, boolean desc, int offset, int limit) throws PersistenceException {
        Unifier uf = new Unifier();
        uf.addPattern("ds", Rdf.TYPE, Cet.DATASET);
        uf.addColumnName("ds");
        uf.setOffset(offset);
        uf.setLimit(limit);

        if ("title".equals(orderBy)) {
            uf.addPattern("ds", Dc.TITLE, "title");
            uf.addColumnName("title");
            uf.addOrderBy("title", !desc);
        } else {
            throw (new PersistenceException("Don't know how to order by " + orderBy));
        }

        try {
            session.getBeanFactory().getContext().perform(uf);
        } catch (OperatorException e) {
            throw (new PersistenceException("Could not get list.", e));
        }

        List<DatasetBean> result = new ArrayList<DatasetBean>();
        for (Tuple<Resource> row : uf.getResult() ) {
            result.add(load(row.get(0)));
        }

        return result;
    }

    @Override
    public Set<DatasetBean> getByCreator(PersonBean person) throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }
}
