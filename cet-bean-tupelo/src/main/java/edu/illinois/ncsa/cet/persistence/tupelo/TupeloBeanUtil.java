package edu.illinois.ncsa.cet.persistence.tupelo;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.BeanException;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.terms.Cet;
import org.tupeloproject.rdf.terms.Rdf;

import com.google.inject.Provider;

import edu.illinois.ncsa.cet.persistence.BeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;

public abstract class TupeloBeanUtil<T> implements BeanUtil<T> {
    private static Log          log = LogFactory.getLog(TupeloBeanUtil.class);

    protected final Provider<T> creator;

    protected final Session     session;

    protected TupeloBeanUtil(Session session, Provider<T> creator) {
        this.session = session;
        this.creator = creator;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<T> getBeanClass() {
        return (Class<T>) create().getClass();
    }

    @Override
    public T create() {
        return creator.get();
    }

    @Override
    public int count() throws PersistenceException {
        try {
            return session.getBeanFactory().getContext().match(null, Rdf.TYPE, Cet.DATASET).size();
        } catch (OperatorException e) {
            log.debug("Could not get bean count.", e);
            throw (new PersistenceException("Could not get bean", e));
        }
    }

    @Override
    public Collection<String> getAllIds() throws PersistenceException {
        org.tupeloproject.kernel.beans.BeanUtil bu = new org.tupeloproject.kernel.beans.BeanUtil(session.getBeanFactory());
        try {
            return bu.getAllIds(getBeanClass());
        } catch (Exception e) {
            log.debug("Could not get beans.", e);
            throw (new PersistenceException("Could not get beans", e));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<T> loadAll() throws PersistenceException {
        org.tupeloproject.kernel.beans.BeanUtil bu = new org.tupeloproject.kernel.beans.BeanUtil(session.getBeanFactory());
        try {
            return (Collection<T>) bu.getAll(getBeanClass());
        } catch (Exception e) {
            log.debug("Could not get beans.", e);
            throw (new PersistenceException("Could not get beans", e));
        }
    }

    @Override
    public Collection<T> loadAll(boolean includeDeleted) throws PersistenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public T load(String id) throws PersistenceException {
        return load(Resource.uriRef(id));
    }

    protected T load(Resource id) throws PersistenceException {
        try {
            return session.load(getBeanClass(), id, true);
        } catch (BeanException e) {
            log.debug("Could not get bean.", e);
            throw (new PersistenceException("Could not get bean", e));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T save(T object) throws PersistenceException {
        if (!session.hasTransaction()) {
            throw (new PersistenceException("No unit of work started."));
        }
        try {
            return (T) session.merge(object);
        } catch (BeanException e) {
            log.debug("Could not save bean.", e);
            throw (new PersistenceException("Could not save bean", e));
        }
    }
}
