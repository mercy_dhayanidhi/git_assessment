package edu.illinois.ncsa.sematic.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target({ FIELD })
public @interface RdfPredicate {
    /**
     * Predicate to be used when saving
     */
    String predicate();

    /**
     * Mark the property as read only, the contents will not be written out.
     */
    boolean readOnly() default false;

    /**
     * Use the subject as the object
     */
    boolean inverse() default false;

    /**
     * Set to true if this should be used as the subject
     */
    boolean subject() default false;

    /**
     * Key when type is a map (e.g. Map<Key, Value>)
     */
    Class<?> key() default Undefined.class;

    /**
     * Value when type is either map or list (e.g. List<Value>)
     */
    public Class<?> value() default Undefined.class;

    class Undefined {
    }
}