package org.tupeloproject.kernel.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashSet;

import org.junit.Test;
import org.tupeloproject.kernel.BeanSession;
import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.BeanInfo.FieldInfo;
import org.tupeloproject.kernel.beans.reflect.BeanMapping;
import org.tupeloproject.kernel.beans.reflect.BeanMappingCache;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.terms.Cet;
import org.tupeloproject.rdf.terms.Dc;
import org.tupeloproject.rdf.terms.Rdf;

public class BeanMappingTest {
    protected TripleContext getContext() {
        return new MemoryContext();
    }
    
    private BeanMapping getComplextBeanMapping() throws OperatorException {
        BeanMapping map = new BeanMapping();
        map.setJavaClass(ComplexBean.class);
        map.setSubjectPropertyName("subject");
        map.setRdfType(Cet.uriRef("ComplexBean"));

        map.addProperty(Dc.IDENTIFIER, "subject", String.class);
        map.addProperty(Cet.uriRef("stringValue"), "stringValue", String.class);
        map.addProperty(Cet.uriRef("stringValues"), "stringValues", String.class);
        map.addProperty(Cet.uriRef("stringNull"), "stringNull", String.class);
        map.addProperty(Cet.uriRef("dateValue"), "dateValue", Date.class);
        map.addProperty(Cet.uriRef("dateNull"), "dateNull", Date.class);
        map.addProperty(Cet.uriRef("integerValue"), "integerValue", Integer.class);
        map.addProperty(Cet.uriRef("longValue"), "longValue", Long.class);
        map.addProperty(Cet.uriRef("doubleValue"), "doubleValue", Double.class);
        map.addProperty(Cet.uriRef("doubleInf"), "doubleInf", Double.class);
        map.addProperty(Cet.uriRef("doubleNaN"), "doubleNaN", Double.class);
        map.addProperty(Cet.uriRef("bean"), "bean", ComplexBean.class);
        map.addProperty(Cet.uriRef("me"), "me", ComplexBean.class);
        map.addMapProperty(Cet.uriRef("mapValue"), "mapValue", String.class, Integer.class);
        map.addMapProperty(Cet.uriRef("mapNull"), "mapNull", String.class, Integer.class);
        map.addProperty(Cet.uriRef("setValue"), "setValue", Integer.class);
        map.addProperty(Cet.uriRef("setNull"), "setNull", Integer.class);
        map.addListProperty(Cet.uriRef("listValue"), "listValue", Integer.class);
        map.addListProperty(Cet.uriRef("listNull"), "listNull", Integer.class);
        map.addProperty(Cet.uriRef("transientValue"), "transientValue", Integer.class);

        return map;
    }


    @Test
    public void testBeanMapping() throws Exception {
        BeanMapping map = new BeanMapping();
        map.setJavaClass(DatasetBean.class);
        map.setRdfType(Cet.DATASET);
        map.setSubjectPropertyName("id");
        map.addProperty(Dc.IDENTIFIER, "id", String.class);
        map.addListProperty(Dc.TITLE, "title", String.class);
        map.addProperty(Dc.FORMAT, "type", String.class);
        map.addProperty(Dc.CREATOR, "creator", PersonBean.class);
        map.addCollectionProperty(Dc.CONTRIBUTOR, "contributors", HashSet.class, PersonBean.class);

        try {
            map.addProperty(Rdf.TYPE, "foobar", String.class);
            assertTrue("Should not be able to use Rdf.TYPE as predicate.", false);
        } catch (IllegalArgumentException e) {
            // all is good
        }
        assertNull("foobar should not exist as property.", map.getProperty("foobar"));

        BeanMappingCache cache = new BeanMappingCache();
        cache.put(map);

        BeanInfo bi = BeanInfo.get(DatasetBean.class);
        assertEquals("Class does not match", DatasetBean.class, bi.getBeanClass());
        assertEquals("Type was not correct", Cet.DATASET, bi.getType());
        assertEquals("wrong subject", DatasetBean.class.getDeclaredField("id"), bi.getSubject());
        assertEquals("Wrong number of fields", 5, bi.getFields().size());

        FieldInfo id = null;
        FieldInfo contributors = null;
        for (FieldInfo fi : bi.getFields() ) {
            assertNotSame("foobar should not exist.", "foobar", fi.getField().getName());
            if ("id".equals(fi.getField().getName())) {
                assertNull("2 or more fields with name id.", id);
                id = fi;
            }
            if ("contributors".equals(fi.getField().getName())) {
                assertNull("2 or more fields with name contributors.", contributors);
                contributors = fi;
            }
        }

        assertNotNull("id field not found.", id);
        assertEquals("Invalid predicate", Dc.IDENTIFIER, id.getPredicate());

        assertNotNull("contributors field not found.", contributors);
        assertEquals("Invalid predicate", Dc.CONTRIBUTOR, contributors.getPredicate());
        assertEquals("friends wrong key", null, contributors.getKey());
        assertEquals("friends wrong value", PersonBean.class, contributors.getValue());

        BeanMapping mapds = cache.getFirstByRDFType(Cet.DATASET);
        assertEquals("Class does not match", DatasetBean.class, mapds.getJavaClass());
        assertEquals("ClassName does not match", DatasetBean.class.getName(), mapds.getJavaClassName());
        assertEquals("Type was not correct", Cet.DATASET, mapds.getRdfType());
        assertEquals("wrong subject", "id", mapds.getSubjectPropertyName());
        assertEquals("Wrong number of fields", 5, mapds.getProperties().size());

        assertNotNull("id field not found.", mapds.getProperty("id"));
        assertEquals("Invalid predicate", Dc.IDENTIFIER, mapds.getProperty("id").getPredicate());

        assertNotNull("contributors field not found.", mapds.getProperty("contributors"));
        assertEquals("Invalid predicate", Dc.CONTRIBUTOR, mapds.getProperty("contributors").getPredicate());
        assertEquals("contributors wrong value", PersonBean.class, mapds.getProperty("contributors").getValue());
    }

    @Test
    public void testComplexBean() throws Exception {
        TripleContext c = getContext();

        ComplexBean bean = ComplexBean.createBean();

        BeanMappingCache cache = new BeanMappingCache();
        cache.put(getComplextBeanMapping());
        BeanSession bs = new BeanSession(c);
        bs.setBeanMappingCache(cache);
        bs.save(bean);
        Resource id = bs.getSubject(bean);

        BeanInfo.remove(ComplexBean.class);
        BeanFactory bf = new BeanFactory(c);
        Session session = bf.openSession();
        session.setCheckNullCollection(true);
        ComplexBean loaded = session.load(ComplexBean.class, id);
        ComplexBean.checkBean(loaded);
    }
}
