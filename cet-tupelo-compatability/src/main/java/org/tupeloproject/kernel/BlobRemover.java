package org.tupeloproject.kernel;

import org.tupeloproject.rdf.Resource;

/**
 * @deprecated use org.tupeloproject.kernel.operator.BlobRemover
 */
public class BlobRemover extends org.tupeloproject.kernel.operator.BlobRemover {
    public BlobRemover() {
        super();
    }

    public BlobRemover(Resource s) {
        super(s);
    }
}
