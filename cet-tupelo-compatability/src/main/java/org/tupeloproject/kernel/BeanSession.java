package org.tupeloproject.kernel;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.tupeloproject.kernel.beans.BeanException;
import org.tupeloproject.kernel.beans.BeanFactory;
import org.tupeloproject.kernel.beans.BeanUtil;
import org.tupeloproject.kernel.beans.Cache;
import org.tupeloproject.kernel.beans.Cache.TripleObject;
import org.tupeloproject.kernel.beans.MapCache;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.beans.reflect.BeanMapping;
import org.tupeloproject.kernel.beans.reflect.BeanMappingCache;
import org.tupeloproject.kernel.context.FilterContext;
import org.tupeloproject.kernel.context.TripleBlobContext;
import org.tupeloproject.kernel.context.TripleContext;
import org.tupeloproject.kernel.operator.TripleMatcher;
import org.tupeloproject.kernel.operator.Unifier;
import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.UriRef;
import org.tupeloproject.rdf.terms.Rdf;

public class BeanSession {
    private TripleBlobContext context;
    private BeanMappingCache  cache;
    private BeanFactory       bf;
    private BeanUtil          beanUtil;
    private Cache             beanCache;

    public BeanSession() {
        this(null);
    }

    public BeanSession(TripleContext context) {
        FilterContext fc = new FilterContext();
        fc.setTripleContext(context);
        setContext(fc);
    }

    public BeanSession(TripleBlobContext context) {
        setContext(context);
    }

    public void close() throws OperatorException {
        this.context.close();
        this.context = null;
        this.cache = null;
        this.beanCache = null;
        this.beanUtil = null;
        this.bf = null;
    }

    public Context getContext() {
        return (Context) context;
    }

    public void setContext(TripleBlobContext context) {
        if (context != null) {
            try {
                context.close();
            } catch (OperatorException e) {
                e.printStackTrace();
            }
        }
        this.context = context;

        bf = new BeanFactory(context, new MapCache());
        beanUtil = new BeanUtil(bf);
        beanCache = new MapCache();
    }

    public void setBeanMappingCache(BeanMappingCache cache) {
        this.cache = cache;
    }

    public BeanMappingCache getBeanMappingCache() {
        return cache;
    }

    public Resource getSubject(Object o) throws BeanException {
        for (Entry<Resource, TripleObject> entry : bf.getCache().entrySet() ) {
            if (entry.getValue().object == o) {
                return entry.getKey();
            }
        }
        return new BeanUtil(bf).getId(o);
    }

    public void register(Object o) throws BeanException {
        register(null, o);
    }

    public void register(Resource s, Object o) throws BeanException {
        if (s == null) {
            s = beanUtil.getId(o);
        }
        beanCache.put(s, o);
    }

    public void deregister(Resource s) throws BeanException {
        beanCache.remove(s);
        bf.evict(s);
    }

    public void save() throws BeanException {
        Session session = bf.openSession();
        session.beginTransaction();
        for (Entry<Resource, TripleObject> entry : beanCache.entrySet() ) {
            session.save(entry.getValue().object, entry.getKey());
        }
        session.commit();
    }

    public void registerAndSave(Object o) throws OperatorException {
        save(o);
    }

    public void save(Object o) throws BeanException {
        Session s = bf.openSession();
        Resource id = s.save(o);
        s.commit();
        beanCache.put(id, o);
    }

    public void update(Object o) throws BeanException {
        update(o, false);
    }

    public void update(Object o, boolean deep) throws BeanException {
        throw (new BeanException("Not implemented."));
    }

    public boolean isRegistered(Resource s) {
        return beanCache.containsKey(s) || bf.getCache().containsKey(s);
    }

    public boolean isRegistered(Object o) throws OperatorException {
        return (beanCache.firstKey(o) != null) || (bf.getCache().firstKey(o) != null);
    }

    public Object fetchBean(Resource id) throws OperatorException {
        return fetchBean(id, (Class<?>) null);
    }

    public Object fetchBean(Resource id, BeanMapping mapping) throws OperatorException {
        Class<?> clz = null;
        if (mapping != null) {
            try {
                clz = getClass().getClassLoader().loadClass(mapping.getJavaClassName());
            } catch (ClassNotFoundException e) {
                throw (new OperatorException("Could not load class for " + mapping.getJavaClassName(), e));
            }
        }
        return fetchBean(id, clz);
    }

    public Object fetchBean(Resource id, Class<?> clz) throws OperatorException {
        Object o = bf.openSession().load(clz, id);
        beanCache.put(id, o);
        return o;
    }

    public Collection<Resource> getRDFTypes(Resource subject) throws OperatorException {
        TripleMatcher tm = new TripleMatcher();
        tm.setSubject(subject);
        tm.setPredicate(Rdf.TYPE);
        bf.getContext().perform(tm);
        return tm.objects();
    }

    public InputStream fetchBlob(Resource s) throws OperatorException {
        // TODO Auto-generated method stub
        throw (new OperatorException("Not implemented."));
    }

    public void writeBlob(Resource s, InputStream is) throws OperatorException {
        // TODO Auto-generated method stub
        throw (new OperatorException("Not implemented."));
    }

    public List<Object> fetchBeans(Unifier unifier, String string) throws OperatorException {
        // TODO Auto-generated method stub
        throw (new OperatorException("Not implemented."));
    }

    public boolean hasPendingChanges(Resource s) throws OperatorException {
        // TODO Auto-generated method stub
        throw (new OperatorException("Not implemented."));
    }

    public void removeBlob(Resource s) throws OperatorException {
        // TODO Auto-generated method stub
        throw (new OperatorException("Not implemented."));
    }

    public Resource getActiveRDFType(UriRef uriRef) {
        // TODO Auto-generated method stub
        return null;
    }

    public Object refetch(Resource uri) {
        // TODO Auto-generated method stub
        return null;
    }

    public ThingSession getThingSession() {
        // TODO Auto-generated method stub
        return null;
    }
}
