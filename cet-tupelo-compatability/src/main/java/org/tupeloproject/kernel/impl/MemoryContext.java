package org.tupeloproject.kernel.impl;

import org.tupeloproject.kernel.Context;

/**
 * @deprecated use org.tupeloproject.kernel.context.MemoryContext
 */
public class MemoryContext extends org.tupeloproject.kernel.context.MemoryContext implements Context {
}
