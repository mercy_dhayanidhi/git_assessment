package org.tupeloproject.kernel.beans.reflect;

import org.tupeloproject.rdf.Resource;
import org.tupeloproject.rdf.terms.Rdf;

public class Property {
    private Resource predicate;
    private String   variable;
    private Class<?> key;
    private Class<?> value;

    public Property(Resource predicate, String variable, Class<?> key, Class<?> value) throws IllegalArgumentException {
        if (Rdf.TYPE.equals(predicate)) {
            throw (new IllegalArgumentException("predicate can not be RDF.Type"));
        }
        this.predicate = predicate;
        this.variable = variable;
        this.key = key;
        this.value = value;
    }

    public Property() {
    }

    public Resource getPredicate() {
        return predicate;
    }

    public void setPredicate(Resource predicate) {
        this.predicate = predicate;
    }

    public String getName() {
        return getVariable();
    }

    public void setName(String name) {
        setVariable(name);
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String var) {
        this.variable = var;
    }

    public Class<?> getKey() {
        return key;
    }

    public void setKey(Class<?> key) {
        this.key = key;
    }

    public Class<?> getValue() {
        return value;
    }

    public void setValue(Class<?> value) {
        this.value = value;
    }
}
