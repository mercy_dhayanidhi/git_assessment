package org.tupeloproject.kernel.beans.reflect;

import java.util.HashSet;
import java.util.Set;

import org.tupeloproject.rdf.Resource;

public class BeanMapping {
    private String        javaClassName;
    private Resource      rdfType;
    private Resource      subject;
    private String        subjectPropertyName;
    private Set<Property> properties = new HashSet<Property>();

    public void setJavaClass(Class<?> clazz) {
        setJavaClassName(clazz.getName());
    }

    public Class<?> getJavaClass() throws ClassNotFoundException {
        return getClass().getClassLoader().loadClass(javaClassName);
    }

    public void setJavaClassName(String name) {
        this.javaClassName = name;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setRdfType(Resource type) throws IllegalArgumentException {
        if (!type.isUri()) {
            throw (new IllegalArgumentException("type should be a URI"));
        }
        this.rdfType = type;
    }

    public Resource getRdfType() {
        return rdfType;
    }

    public void setSubject(Resource mappingSubject) {
        this.subject = mappingSubject;
    }

    public Resource getSubject() {
        return subject;
    }

    public void setSubjectPropertyName(String string) {
        this.subjectPropertyName = string;
    }

    public String getSubjectPropertyName() {
        return subjectPropertyName;
    }

    public void addProperty(Resource predicate, String variable, Class<?> type) throws IllegalArgumentException {
        properties.add(new Property(predicate, variable, null, null));
    }

    public void addCollectionProperty(Resource predicate, String variable, Class<?> type) throws IllegalArgumentException {
        properties.add(new Property(predicate, variable, null, type));
    }

    public void addListProperty(Resource predicate, String variable, Class<?> type) throws IllegalArgumentException {
        properties.add(new Property(predicate, variable, null, type));
    }

    public void addCollectionProperty(Resource predicate, String variable, Class<?> collectionType, Class<?> type) throws IllegalArgumentException {
        properties.add(new Property(predicate, variable, null, type));
    }

    public void addMapProperty(Resource predicate, String variable, Class<?> key, Class<?> value) throws IllegalArgumentException {
        properties.add(new Property(predicate, variable, key, value));
    }

    public Set<Property> getProperties() {
        return properties;
    }

    public Property getProperty(String variable) {
        for (Property p : properties ) {
            if (p.getVariable().equals(variable)) {
                return p;
            }
        }
        return null;
    }

    public void setBeanMappingCache(BeanMappingCache beanMappingCache) {
        // TODO Auto-generated method stub

    }
}
