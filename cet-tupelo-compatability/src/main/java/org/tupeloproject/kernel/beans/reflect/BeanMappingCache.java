package org.tupeloproject.kernel.beans.reflect;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tupeloproject.kernel.OperatorException;
import org.tupeloproject.kernel.beans.BeanException;
import org.tupeloproject.kernel.beans.BeanInfo;
import org.tupeloproject.kernel.beans.BeanInfo.FieldInfo;
import org.tupeloproject.rdf.Resource;

public class BeanMappingCache {
    public void put(BeanMapping map) throws OperatorException {
        BeanInfo.add(fromBeanMapping(map));
    }

    public BeanMapping getFirstByRDFType(Resource type) throws OperatorException {
        BeanInfo bi = BeanInfo.get(type);
        if (bi == null) {
            throw (new BeanException("Could not find beaninfo for type " + type));
        }
        return toBeanMapping(bi);
    }

    private BeanInfo fromBeanMapping(BeanMapping map) throws OperatorException {
        Class<?> beanClass = null;
        Resource type = map.getRdfType();
        Field subject = null;
        Set<FieldInfo> fields = new HashSet<FieldInfo>();

        try {
            beanClass = getClass().getClassLoader().loadClass(map.getJavaClassName());

            // get all fields
            fields = new HashSet<FieldInfo>();
            for (Class<?> c = beanClass; c != null; c = c.getSuperclass() ) {
                // predicates used
                for (Field f : c.getDeclaredFields() ) {
                    Property p = map.getProperty(f.getName());
                    if (p != null) {
                        try {
                            f.setAccessible(true);
                            fields.add(new FieldInfo(f, p.getPredicate(), p.getKey(), p.getValue()));
                            if (f.getName().equals(map.getSubjectPropertyName())) {
                                subject = f;
                            }
                        } catch (SecurityException e) {
                            throw (new BeanException(String.format("Could not get access to field %s.", f), e));
                        }
                    }
                }
            }

            return new BeanInfo(beanClass, type, fields, subject);
        } catch (Exception e) {
            throw (new OperatorException("Could not convert beanmapping to beaninfo.", e));
        }
    }

    private BeanMapping toBeanMapping(BeanInfo bi) throws OperatorException {
        BeanMapping map = new BeanMapping();

        map.setJavaClass(bi.getBeanClass());
        map.setRdfType(bi.getType());

        if (bi.getSubject() != null) {
            map.setSubjectPropertyName(bi.getSubject().getName());
        }

        for (FieldInfo fi : bi.getFields() ) {
            if (Map.class.isAssignableFrom(fi.getField().getType())) {
                map.addMapProperty(fi.getPredicate(), fi.getField().getName(), fi.getKey(), fi.getValue());

            } else if (List.class.isAssignableFrom(fi.getField().getType())) {
                map.addListProperty(fi.getPredicate(), fi.getField().getName(), fi.getValue());

            } else if (Collection.class.isAssignableFrom(fi.getField().getType())) {
                map.addCollectionProperty(fi.getPredicate(), fi.getField().getName(), fi.getValue());

            } else {
                map.addProperty(fi.getPredicate(), fi.getField().getName(), fi.getField().getType());
            }
        }

        return map;
    }
}
