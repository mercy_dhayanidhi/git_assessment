package edu.illinois.ncsa.cet.mmdbperf;

import org.tupeloproject.kernel.beans.BeanFactory;
import org.tupeloproject.kernel.beans.Session;
import org.tupeloproject.kernel.context.MemoryContext;
import org.tupeloproject.rdf.Triple;

import com.google.inject.Guice;
import com.google.inject.Injector;

import edu.illinois.ncsa.cet.bean.AbstractBean;
import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.bean.util.URLMinter;
import edu.illinois.ncsa.cet.jpa.persistence.JpaBeanModule;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;

public class LinkedDataExample {
    public static void main(String[] args) throws Exception {
        AbstractBean.minter = new URLMinter("http://cet.ncsa.illinois.edu/test");

        Injector injector = Guice.createInjector(new JpaBeanModule("mysql"));
        PersonBeanUtil pbu = injector.getInstance(PersonBeanUtil.class);
        PersonBean pb = pbu.create();

        DatasetBeanUtil dbu = injector.getInstance(DatasetBeanUtil.class);
        DatasetBean db = dbu.create();
        db.setCreator(pb);
        db.setTitle("FOOBAR");
        db = dbu.save(db);

        // serialize to RDF
        MemoryContext mc = new MemoryContext();
        BeanFactory bf = new BeanFactory(mc);
        Session session = bf.openSession();
        session.merge(db);
        session.commit();
        for (Triple t : mc.getTriples() ) {
            System.out.println(t.toString());
        }
    }
}
