package edu.illinois.ncsa.cet.mmdbperf;

import org.kohsuke.args4j.Option;

public class CmdLineOptions {
    @Option(name = "--words", usage = "number of words that can be used as tags")
    public int    words         = 100;

    @Option(name = "--datasets", usage = "number of datasets created per run")
    public int    datasets      = 1000;

    @Option(name = "--ordered", usage = "number of datasets to return in ordered list")
    public int    ordered       = 30;

    @Option(name = "--runs", usage = "number or runs")
    public int    runs          = 50;

    @Option(name = "--tags", usage = "maximum number of tags created per dataset")
    public int    tags          = 5;

    @Option(name = "--collection", usage = "maximum number of datasets per collection")
    public int    collection    = 10;

    @Option(name = "--configuration", usage = "configuration type (tupelo, jpa, jena)")
    public String configuration = "jpa";

    @Option(name = "--backend", usage = "backend type (memory, mysql, newmysql)")
    public String backend       = "mysql";
}
