package edu.illinois.ncsa.cet.mmdbperf;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import edu.illinois.ncsa.cet.bean.CollectionBean;
import edu.illinois.ncsa.cet.bean.DatasetBean;
import edu.illinois.ncsa.cet.bean.PersonBean;
import edu.illinois.ncsa.cet.jpa.persistence.JpaBeanModule;
import edu.illinois.ncsa.cet.jpa.persistence.JpaDatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.CollectionBeanUtil;
import edu.illinois.ncsa.cet.persistence.DatasetBeanUtil;
import edu.illinois.ncsa.cet.persistence.PersistenceException;
import edu.illinois.ncsa.cet.persistence.PersonBeanUtil;
import edu.illinois.ncsa.cet.persistence.TagBeanUtil;
import edu.illinois.ncsa.cet.persistence.UnitOfWork;
import edu.illinois.ncsa.cet.persistence.jena.JenaBeanModule;
import edu.illinois.ncsa.cet.persistence.tupelo.TupeloBeanModule;

public class Performance {
    private static CmdLineOptions        options         = new CmdLineOptions();

    private static List<String>          words           = new ArrayList<String>();

    private static int                   TAG_FIND        = 100;
    private static int                   DATASET_FIND    = 100;
    private static int                   COLLECTION_FIND = 100;

    private Random                       random          = new Random();

    private int                          datasetCount    = 0;
    private int                          collectionCount = 0;
    private int                          tagCount        = 0;

    private List<PersonBean>             people          = new ArrayList<PersonBean>();

    private final SimpleDateFormat       sdf             = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); //$NON-NLS-1$

    @Inject
    private Provider<DatasetBeanUtil>    datasetProvider;

    @Inject
    private Provider<CollectionBeanUtil> collectionProvider;

    @Inject
    private Provider<TagBeanUtil>        tagProvider;

    @Inject
    private Provider<PersonBeanUtil>     personProvider;

    @Inject
    private Provider<UnitOfWork>         workProvider;

    public static void main(String[] args) throws Exception {

        CmdLineParser parser = new CmdLineParser(options);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException exc) {
            System.err.println("syntax error: " + exc.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        System.out.print("configuration=" + options.configuration);
        System.out.print("\tbackend=" + options.backend);
        System.out.print("\tdatasets=" + options.datasets);
        System.out.print("\truns=" + options.runs);
        System.out.println();

        // initialize
        InputStream is = Performance.class.getResourceAsStream("/words.txt"); //$NON-NLS-1$
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String word;
        while ((word = br.readLine()) != null) {
            words.add(word);
        }
        br.close();

        // create instance
        Injector injector = null;
        if ("jpa".equalsIgnoreCase(options.configuration)) {
            injector = Guice.createInjector(new JpaBeanModule(options.backend));
        } else if ("jena".equalsIgnoreCase(options.configuration)) {
            injector = Guice.createInjector(new JenaBeanModule(options.backend));
        } else if ("tupelo".equalsIgnoreCase(options.configuration)) {
            injector = Guice.createInjector(new TupeloBeanModule(options.backend));
        } else {
            throw (new PersistenceException("Can not create injector based on " + options.configuration));
        }
        Performance perf = injector.getInstance(Performance.class);

        // garbage run
        //        perf.stats(true);
        //        System.gc();
        //        perf.datasetCount = 0;
        //        perf.collectionCount = 0;
        //        perf.tagCount = 0;

        // realrun
        perf.stats(false);
    }

    public void stats(boolean warmup) throws Exception {
        int createds;
        if (warmup) {
            createds = 100;
        } else {
            System.out.print("#datasets\t#collections\t#tags\tmemory (MB)\t");
            System.out.print("insert datasets\tavg insert dataset\tcount datasets\tavg find datasets\t");
            System.out.print("avg find datasets in collection\tavg find datasets with tag\t");
            System.out.print("find subset\ttimestamp");
            System.out.println();
            createds = options.datasets;
        }

        for (int i = 0; i < options.runs; i++ ) {
            Date startTime = new Date();
            // datasets
            long now = System.currentTimeMillis();
            insertDatasetsBean(createds);
            long insert = System.currentTimeMillis() - now;

            now = System.currentTimeMillis();
            int x = countDatasets();
            if (x != datasetCount) {
                throw (new Exception(String.format("Counted %d datasets instead of %d datasets.", x, datasetCount)));
            }
            long count = System.currentTimeMillis() - now;

            now = System.currentTimeMillis();
            findDatasets();
            long find = System.currentTimeMillis() - now;

            // collection
            now = System.currentTimeMillis();
            countDatasetsCollection();
            long collfind = System.currentTimeMillis() - now;

            // tags
            now = System.currentTimeMillis();
            findDatasetsTag();
            long tagfind = System.currentTimeMillis() - now;

            // order
            now = System.currentTimeMillis();
            findOrderedDatasets();
            long orderfind = System.currentTimeMillis() - now;

            // memory
            if (!warmup) {
                if ("jpa".equalsIgnoreCase(options.configuration)) {
                    ((JpaDatasetBeanUtil) datasetProvider.get()).clear();
                }
                System.gc();
                System.gc();
                System.gc();
                long mem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

                System.out.print(String.format("%d\t%d\t%d\t%5.2f\t", datasetCount, collectionCount, tagCount, mem / 1048576.0));
                System.out.print(String.format("%d\t%5.3f\t%d\t%5.3f\t", insert, (double) insert / createds, count, (double) find / DATASET_FIND));
                System.out.print(String.format("%5.3f\t%5.3f\t", (double) collfind / COLLECTION_FIND, (double) tagfind / TAG_FIND));
                System.out.print(String.format("%d\t%s", orderfind, sdf.format(startTime)));
                System.out.println();
                System.out.flush(); // for more accurate monitoring
            } else {
                break;
            }
        }

        if (!warmup) {
            System.out.println("End timestamp: " + sdf.format(new Date()));
        }
    }

    // ----------------------------------------------------------------------
    // CREATE DATASETS
    // ----------------------------------------------------------------------

    private void insertDatasetsBean(int count) throws PersistenceException {
        int collection = 0;
        CollectionBean cb = null;

        DatasetBeanUtil dbu = datasetProvider.get();
        CollectionBeanUtil cbu = collectionProvider.get();
        TagBeanUtil tbu = tagProvider.get();
        UnitOfWork uow = workProvider.get();

        uow.begin();

        int beans = 0;
        for (int i = 0; i < count; i++ ) {
            // dataset
            DatasetBean db = dbu.save(fillDataset(dbu.create()));
            beans++;

            // tags
            String tags[] = new String[getTagCount()];
            for (int j = 0; j < tags.length; j++ ) {
                tags[j] = getWord(options.words);
            }
            tbu.addTags(db, getPerson(), tags);
            tagCount += tags.length;

            // collection
            if (collection == 0) {
                collection = random.nextInt(options.collection) + 1;
                cb = cbu.save(fillCollection(cbu.create()));
            }

            cb.addMember(db);
            collection--;
        }

        uow.end();
    }

    private int getTagCount() {
        double x = random.nextDouble();
        double y = 0.5;
        int count = 0;
        while ((count <= options.tags) && (x < y)) {
            count++;
            y = y / 2.0;
        }
        return count;
    }

    public DatasetBean fillDataset(DatasetBean db) throws PersistenceException {
        db.setId("tag:cet.ncsa.uiuc.edu,2008:/bean/Dataset/" + datasetCount); //$NON-NLS-1$
        datasetCount++;
        db.setCreator(getPerson());
        db.setDate(new Date());
        db.setTitle(getWord(-1));
        db.setMimeType("text/plain"); //$NON-NLS-1$
        return db;
    }

    public CollectionBean fillCollection(CollectionBean cb) throws PersistenceException {
        cb.setId("tag:cet.ncsa.uiuc.edu,2008:/bean/Collection/" + collectionCount); //$NON-NLS-1$
        collectionCount++;
        cb.setCreationDate(new Date());
        cb.setCreator(getPerson());
        cb.setTitle(getWord(-1));
        return cb;
    }

    public PersonBean getPerson() throws PersistenceException {
        if (people.size() == 0) {
            people.add(personProvider.get().create());
        }
        return people.get(random.nextInt(people.size()));
    }

    public String getWord(int max) {
        if ((max < 0) || (max > words.size())) {
            return words.get(random.nextInt(words.size()));
        } else {
            return words.get(random.nextInt(max));
        }
    }

    // ----------------------------------------------------------------------
    // COUNT DATASETS
    // ----------------------------------------------------------------------

    public int countDatasets() throws PersistenceException {
        return datasetProvider.get().count();
    }

    // ----------------------------------------------------------------------
    // FIND DATASETS
    // ----------------------------------------------------------------------

    public void findDatasets() throws PersistenceException {
        DatasetBeanUtil dbu = datasetProvider.get();
        for (int i = 0; i < DATASET_FIND; i++ ) {
            String uri = "tag:cet.ncsa.uiuc.edu,2008:/bean/Dataset/" + random.nextInt(datasetCount); //$NON-NLS-1$
            dbu.load(uri);
        }
    }

    // ----------------------------------------------------------------------
    // COUNT DATASETS IN COLLECTION
    // ----------------------------------------------------------------------

    public void countDatasetsCollection() throws PersistenceException {
        CollectionBeanUtil cbu = collectionProvider.get();
        for (int i = 0; i < COLLECTION_FIND; i++ ) {
            String uri = "tag:cet.ncsa.uiuc.edu,2008:/bean/Collection/" + random.nextInt(collectionCount); //$NON-NLS-1$
            cbu.load(uri).getMembers().size();
        }
    }

    // ----------------------------------------------------------------------
    // FIND DATASET WITH TAG
    // ----------------------------------------------------------------------

    public void findDatasetsTag() throws PersistenceException {
        TagBeanUtil tbu = tagProvider.get();
        for (int i = 0; i < TAG_FIND; i++ ) {
            tbu.getTagged(getWord(options.words)).size();
        }
    }

    // ----------------------------------------------------------------------
    // FIND DATASET ORDERED BY DATE
    // ----------------------------------------------------------------------

    public void findOrderedDatasets() throws PersistenceException {
        int limit = Math.min(options.ordered, datasetCount / 2);
        int start = random.nextInt(datasetCount - limit);

        DatasetBeanUtil dbu = datasetProvider.get();
        int found = dbu.getOrderedList("title", false, start, limit).size();
        if (limit != found) {
            System.err.println("findOrderedDatasets : Not right number of datasets returned [" + start + " " + limit + " != " + found + "].");
        }
    }
}
