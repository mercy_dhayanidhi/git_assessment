package edu.illinois.ncsa.cet.jena.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.RDF;

public class BeanUtil {
    private static Logger      log = LoggerFactory.getLogger(BeanUtil.class);

    public static final String NS  = "tag:edu.illinois.ncsa,2011:/1.0/";

    private BeanFactory        beanFactory;

    public BeanUtil() {
        this(null);
    }

    public BeanUtil(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    // ----------------------------------------------------------------------
    // GET CLASS/ID
    // ----------------------------------------------------------------------

    public Class<?> getClass(Resource id) throws BeanException {
        // find all rdf types
        NodeIterator iter = beanFactory.getModel().listObjectsOfProperty(id, RDF.type);

        // find the first (random) class
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if (cl == null) {
            cl = BeanUtil.class.getClassLoader();
        }
        String autoclass = BeanUtil.resource("beans/class/").toString();
        try {
            while (iter.hasNext()) {
                RDFNode o = iter.next();
                if (o instanceof Resource) {
                    try {
                        return BeanInfo.get((Resource) o).getBeanClass();
                    } catch (BeanException e) {
                        log.debug(e.getMessage(), e);
                    }
                    if (o.toString().startsWith(autoclass)) {
                        try {
                            Class<?> clz = cl.loadClass(o.toString().substring(autoclass.length()));
                            return BeanInfo.get(clz).getBeanClass();
                        } catch (Exception e) {
                            log.debug(e.getMessage(), e);
                        }
                    }
                }
            }
        } finally {
            iter.close();
        }

        throw (new BeanException(String.format("Could not find information for id %s", id)));
    }

    public Resource getId(Object object) throws BeanException {
        BeanInfo bi = BeanInfo.get(object.getClass());
        try {
            if ((bi.getSubject() != null) && (bi.getSubject().get(object) != null)) {
                return ResourceFactory.createResource(bi.getSubject().get(object).toString());
            } else {
                return ResourceFactory.createResource();
            }
        } catch (IllegalAccessException e) {
            throw (new BeanException(String.format("Could not access subject field %s.", bi.getSubject()), e));
        }
    }

    // ----------------------------------------------------------------------
    // GET ALL
    // ----------------------------------------------------------------------

    /**
     * Based on a collection of ids this will return the collection of Beans
     * with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<? extends Object> get(Class<?> clz, Collection<String> ids) throws BeanException {
        return get(clz, new ArrayList<String>(ids), false);
    }

    /**
     * Based on a collection of ids this will return the collection of Beans
     * with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> get(Class<?> clz, Collection<String> ids, boolean getDeleted) throws BeanException {
        return get(clz, new ArrayList<String>(ids), getDeleted);
    }

    /**
     * Based on a list of ids this will return the ordered beans with those ids.
     * This will only return a list of items that are not deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> get(Class<?> clz, List<String> ids) throws BeanException {
        return get(clz, ids, false);
    }

    /**
     * Based on a list of ids this will return the ordered beans with those ids.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param ids
     *            list of ids of whom to return the Bean
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of Beans
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> get(Class<?> clz, List<String> ids, boolean getDeleted) throws BeanException {
        List<Object> result = new ArrayList<Object>();
        Session sess = getBeanFactory().openSession();
        sess.beginTransaction();
        for (String id : ids ) {
            Object obj = sess.load(clz, ResourceFactory.createResource(id), getDeleted);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Return a collection of all the beans found for the given class. This
     * could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> getAll(Class<?> clz) throws Exception {
        return getAll(clz, false, 0, -1);
    }

    /**
     * Return a collection of all the beans found of this specific type. This
     * could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all Beans of this type.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public Collection<?> getAll(Class<?> clz, boolean getDeleted) throws BeanException {
        return getAll(clz, getDeleted, 0, -1);
    }

    /**
     * Returns a list of beans based on the given class. This will only return
     * beans marked as not deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param offset
     *            the starting point in the list of beans.
     * @param limit
     *            the maximum number of beans to return.
     * @return list of all beans of the given class.
     * @throws BeanException
     *             throws a BeanException if one of the beans could not be
     *             loaded.
     */
    public List<?> getAll(Class<?> clz, long offset, long limit) throws BeanException {
        return getAll(clz, false, offset, limit);
    }

    /**
     * Fetch all beans of a given rdf type with paging parameters.
     * 
     * @param getDeleted
     * @param offset
     * @param limit
     * @return
     * @throws Exception
     */
    public List<?> getAll(Class<?> clz, boolean getDeleted, long offset, long limit) throws BeanException {
        return get(clz, getAllIds(clz, getDeleted, offset, limit), getDeleted);
    }

    /**
     * Return a collection of all ids of the beans found of this specific type.
     * This could be a large list. This will only return the ids of those that
     * are not marked as deleted.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @return list of all ids of Beans of this type.
     * @throws BeanException
     *             throws a BeanException if the list could not be retrieved.
     */
    public List<String> getAllIds(Class<?> clz) throws BeanException {
        return getAllIds(clz, false, 0, -1);
    }

    /**
     * Return a collection of all ids of the beans found of this specific type.
     * This could be a large list.
     * 
     * @param clz
     *            the class of the bean type to retrieve.
     * @param getDeleted
     *            if true, returns beans marked as deleted too.
     * @return list of all ids of Beans of this type.
     * @throws BeanException
     *             throws a BeanException if the list could not be retrieved.
     */
    public List<String> getAllIds(Class<?> clz, boolean getDeleted) throws BeanException {
        return getAllIds(clz, getDeleted, 0, -1);
    }

    /**
     * Fetch all beans of a given rdf type with paging parameters.
     * 
     * @param getDeleted
     * @param offset
     * @param limit
     * @return
     * @throws Exception
     */
    public List<String> getAllIds(Class<?> clz, boolean getDeleted, long offset, long limit) throws BeanException {
        // FIXME error will return a list that contains deleted items, resulting in a smaller list returned.
        // SPARQL
        String q = "";
        q += "PREFIX rdf: <" + RDF.getURI() + ">\n";
        q += "SELECT ?s WHERE {\n";
        q += "  ?s " + RDF.type + " " + BeanInfo.get(clz).getType() + " .\n";
        q += "}\n";
        q += "ORDER BY ASC(?s)\n";
        q += "OFFSET " + offset + "\n";
        if (limit > 0) {
            q += "LIMIT " + limit + "\n";
        }

        Query query = QueryFactory.create(q);
        QueryExecution qe = QueryExecutionFactory.create(query, beanFactory.getModel());
        ResultSet results = qe.execSelect();

        List<String> ids = new ArrayList<String>();
        while (results.hasNext()) {
            QuerySolution qs = results.nextSolution();
            ids.add(qs.get("?s").toString());
        }

        qe.close();

        return ids;
    }

    public static Resource resource(String suffix) {
        return ResourceFactory.createResource(NS + suffix);
    }

    public static Property property(String suffix) {
        return ResourceFactory.createProperty(NS + suffix);
    }
}
