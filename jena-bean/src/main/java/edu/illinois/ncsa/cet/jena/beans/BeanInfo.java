package edu.illinois.ncsa.cet.jena.beans;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.RDF;

import edu.illinois.ncsa.sematic.annotation.RdfIgnore;
import edu.illinois.ncsa.sematic.annotation.RdfNameSpace;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate;
import edu.illinois.ncsa.sematic.annotation.RdfPredicate.Undefined;
import edu.illinois.ncsa.sematic.annotation.RdfType;

public class BeanInfo {
    private static Logger                        log          = LoggerFactory.getLogger(BeanInfo.class);

    private static final Map<Resource, BeanInfo> typeMapping  = new HashMap<Resource, BeanInfo>();
    private static final Map<Class<?>, BeanInfo> classMapping = new HashMap<Class<?>, BeanInfo>();

    private Field                                subject;
    private Resource                             type;
    private Set<FieldInfo>                       fields;
    private Map<String, String>                  namespace;
    private Class<?>                             beanClass;

    public BeanInfo(Class<?> clz, Resource type, Set<FieldInfo> fields, Field subject) throws BeanException {
        this.beanClass = clz;
        this.subject = subject;
        this.type = type;
        this.fields = fields;
    }

    public BeanInfo(Class<?> clz) throws BeanException {
        this.beanClass = clz;
        this.subject = null;
        this.type = null;

        // load field information and predicates
        mapBean();

        // get rdf type
        RdfType t = clz.getAnnotation(RdfType.class);
        if (t == null) {
            type = BeanUtil.property("beans/class/" + beanClass.getName());
        } else {
            type = getResource(t.type());
        }
    }

    private void mapBean() throws BeanException {
        // get the namespaces for this class
        namespace = new HashMap<String, String>();

        // get namepsace
        for (Class<?> c = beanClass; c != null; c = c.getSuperclass() ) {
            // get namespace
            RdfNameSpace n = c.getAnnotation(RdfNameSpace.class);
            if (n != null) {
                for (String x : n.ns() ) {
                    String[] p = x.split("=", 2); //$NON-NLS-1$
                    String s = namespace.get(p[0]);
                    if (s == null) {
                        namespace.put(p[0], p[1]);
                    } else if (!s.equals(p[1])) {
                        log.info(String.format("Already have namespace '%s' defined with value '%s' ignoring value '%s'", p[0], s, p[1]));
                    }
                }
            }

            Package pkg = c.getPackage();
            n = pkg.getAnnotation(RdfNameSpace.class);
            if (n != null) {
                for (String x : n.ns() ) {
                    String[] p = x.split("=", 2); //$NON-NLS-1$
                    String s = namespace.get(p[0]);
                    if (s == null) {
                        namespace.put(p[0], p[1]);
                    } else if (!s.equals(p[1])) {
                        log.info(String.format("Already have namespace '%s' defined with value '%s' ignoring value '%s'", p[0], s, p[1]));
                    }
                }
            }
        }

        // get all fields
        boolean hasAnnotation = false;
        Set<Property> predicates = new HashSet<Property>();
        fields = new HashSet<FieldInfo>();
        for (Class<?> c = beanClass; c != null; c = c.getSuperclass() ) {
            // get fields
            for (Field f : c.getDeclaredFields() ) {
                // make sure we can access field
                try {
                    f.setAccessible(true);
                } catch (SecurityException e) {
                    throw (new BeanException(String.format("Could not get access to field %s.", f), e));
                }

                // if marked with RdfIgnore ignore it
                if (f.getAnnotation(RdfIgnore.class) != null) {
                    continue;
                }

                // get annotation (if any)
                RdfPredicate p = f.getAnnotation(RdfPredicate.class);

                // check for transient fields (not stored)
                if (Modifier.isTransient(f.getModifiers())) {
                    if (p != null) {
                        log.info(String.format("Transient field '%s' has predicate '%s', will not be saved.", f.getName(), p.predicate()));
                    }
                    continue;
                }

                // check for static fields (not stored)
                if (Modifier.isStatic(f.getModifiers())) {
                    if (p != null) {
                        log.info(String.format("Static field '%s' has predicate '%s', will not be saved.", f.getName(), p.predicate()));
                    }
                    continue;
                }

                // use annotation if any
                if (p != null) {
                    hasAnnotation = true;
                    Property predicate = getProperty(p.predicate());
                    if (predicates.contains(predicate)) {
                        throw (new BeanException("Duplicate predicate detected " + predicate));
                    }
                    predicates.add(predicate);
                    fields.add(new FieldInfo(f, predicate, p));

                    if ((subject == null) && p.subject()) {
                        subject = f;
                    }

                } else {
                    Property predicate = BeanUtil.property("beans/field/" + f.getName());
                    log.debug("Mapped " + f.getName() + " to " + predicate.toString());
                    if (predicates.contains(predicate)) {
                        throw (new BeanException("Duplicate predicate detected " + predicate));
                    }
                    predicates.add(predicate);
                    fields.add(new FieldInfo(f, predicate, null, null));
                }
            }
        }

        if (hasAnnotation) {
            if (subject == null) {
                log.info("Class has no field marked to store subject.");
            }
        } else {
            log.info("Creating classmapping for : " + beanClass);
        }
    }

    public Field getSubject() {
        return subject;
    }

    public Resource getType() {
        return type;
    }

    public Collection<FieldInfo> getFields() {
        return fields;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    private Resource getResource(String r) throws BeanException {
        if (r == null) {
            throw (new BeanException("Can not convert null to a resource."));
        }

        String[] split = r.split(":", 2); //$NON-NLS-1$
        if (split.length == 2) {
            if (namespace.get(split[0]) != null) {
                return ResourceFactory.createResource(namespace.get(split[0]) + split[1]);
            }
        }

        return ResourceFactory.createResource(r);
    }

    private Property getProperty(String r) throws BeanException {
        if (r == null) {
            throw (new BeanException("Can not convert null to a property."));
        }

        String[] split = r.split(":", 2); //$NON-NLS-1$
        if (split.length == 2) {
            if (namespace.get(split[0]) != null) {
                return ResourceFactory.createProperty(namespace.get(split[0]) + split[1]);
            }
        }

        return ResourceFactory.createProperty(r);
    }

    public static class FieldInfo {
        private final Field    field;
        private Class<?>       key;
        private Class<?>       value;
        private final boolean  readOnly;
        private final Property predicate;
        private final boolean  inverse;

        public FieldInfo(Field f, Property p, Class<?> key, Class<?> value) throws BeanException {
            if ((p == null) || DC.type.equals(p)) {
                throw (new BeanException("Invalid predicate"));
            }
            if (f == null) {
                throw (new BeanException("Invalid field"));
            }
            field = f;
            predicate = p;
            readOnly = false;
            inverse = false;
            this.key = key;
            this.value = value;
        }

        public FieldInfo(Field f, Property p, RdfPredicate rdfp) throws BeanException {
            if ((p == null) || DC.type.equals(p)) {
                throw (new BeanException("Predicate is null " + f));
            }
            if (RDF.type.equals(p)) {
                throw (new BeanException("Predicate can not be Rdf.Type " + f));
            }
            if (f == null) {
                throw (new BeanException("Invalid field " + p));
            }
            field = f;
            predicate = p;
            readOnly = rdfp.readOnly();
            inverse = rdfp.inverse();
            if (!rdfp.key().isAssignableFrom(Undefined.class)) {
                key = rdfp.key();
            }
            if (!rdfp.value().isAssignableFrom(Undefined.class)) {
                value = rdfp.value();
            }
        }

        public Class<?> getKey() {
            return key;
        }

        public void setKey(Class<?> key) {
            this.key = key;
        }

        public Class<?> getValue() {
            return value;
        }

        public void setValue(Class<?> value) {
            this.value = value;
        }

        public Field getField() {
            return field;
        }

        public boolean isReadOnly() {
            return readOnly;
        }

        public boolean isInverse() {
            return inverse;
        }

        public Property getPredicate() {
            return predicate;
        }
    }

    // ----------------------------------------------------------------------
    // CLASS/TYPE TO BEANINFO MAPPING
    // ----------------------------------------------------------------------

    public static void remove(Class<?> clz) {
        if (classMapping.containsKey(clz)) {
            BeanInfo bi = classMapping.remove(clz);
            typeMapping.remove(bi.getType());
        }
    }

    public static void remove(BeanInfo bi) {
        remove(bi.getBeanClass());
    }

    public static void remove(Resource type) {
        if (typeMapping.containsKey(type)) {
            BeanInfo bi = typeMapping.remove(type);
            classMapping.remove(bi.getBeanClass());
        }
    }

    /**
     * Tell the beanfactory how to map a type to a class, this is only needed
     * when reading a bean with no given class.
     * 
     * @param clz
     *            the class needing to be mapped.
     */
    public static void add(Class<?> clz) throws BeanException {
        add(new BeanInfo(clz));
    }

    public static void add(BeanInfo bi) {
        if (!classMapping.containsKey(bi.getBeanClass())) {
            classMapping.put(bi.getBeanClass(), bi);
            typeMapping.put(bi.getType(), bi);
        }
    }

    public static BeanInfo get(Class<?> clz) throws BeanException {
        if (!classMapping.containsKey(clz)) {
            add(clz);
        }
        return classMapping.get(clz);
    }

    public static BeanInfo get(Resource type) throws BeanException {
        if (!typeMapping.containsKey(type)) {
            throw (new BeanException(String.format("Could not find information for type %s", type)));
        }
        return typeMapping.get(type);
    }

    public static boolean isBean(Class<?> clz) {
        if (clz.isArray()) {
            clz = clz.getComponentType();
        }
        if (clz.isPrimitive()) {
            return false;
        }
        if (Boolean.class.isAssignableFrom(clz)) {
            return false;
        }
        if (Character.class.isAssignableFrom(clz)) {
            return false;
        }
        if (String.class.isAssignableFrom(clz)) {
            return false;
        }
        if (Number.class.isAssignableFrom(clz)) {
            return false;
        }
        if (Date.class.isAssignableFrom(clz)) {
            return false;
        }
        return true;
    }
}