package edu.illinois.ncsa.cet.jena.beans;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

import edu.illinois.ncsa.cet.jena.beans.Cache.StatmentObject;

public class MapCache extends HashMap<Resource, StatmentObject> implements Cache {
    private static final long serialVersionUID = 1L;

    @Override
    public StatmentObject put(Resource key, Object object) {
        return put(key, new StatmentObject(object));
    }

    @Override
    public StatmentObject put(Resource key, Object object, Set<Statement> statements) {
        return put(key, new StatmentObject(object, statements));
    }

    @Override
    public Resource firstKey(Object object) {
        for (Entry<Resource, StatmentObject> entry : entrySet() ) {
            if (entry.getValue().object == object) {
                return entry.getKey();
            }
        }
        return null;
    }
}