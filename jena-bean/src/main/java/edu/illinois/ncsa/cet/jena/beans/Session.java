package edu.illinois.ncsa.cet.jena.beans;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.RDF;

import edu.illinois.ncsa.cet.jena.beans.BeanInfo.FieldInfo;
import edu.illinois.ncsa.cet.jena.beans.Cache.StatmentObject;

/**
 * The main runtime interface between a Java application and a Context. This is
 * the central API class abstracting the notion of a persistence service. This
 * is loosely modeled after the Session interface in Hibernate.
 * 
 * The lifecycle of a Session is bounded by the beginning and end of a logical
 * transaction.
 * 
 * The main function of the Session is to offer create, read and delete
 * operations for instances of mapped entity classes. Instances may exist in one
 * of three states:
 * 
 * transient: never persistent, not associated with the Session<br>
 * persistent: associated with a the Session<br>
 * detached: previously persistent, not associated with the Session<br>
 * 
 * Transient instances may be made persistent by calling save(). Persistent
 * instances may be made transient by calling delete(). Any instance returned by
 * a load() method is persistent. The state of a transient or detached instance
 * may also be made persistent as a new persistent instance by calling merge().
 * 
 * It is not intended that implementors be threadsafe. Instead each
 * thread/transaction should obtain its own instance from a BeanFactory.
 * 
 * A typical transaction should use the following idiom:
 * 
 * <pre>
 * Session sess = factory.getSession();
 * try {
 *     sess.beginTransaction();
 *     //do some work
 *     sess.commit();
 * } catch (Exception e) {
 *     sess.rollback();
 *     throw e;
 * }
 * </pre>
 * 
 * If the Session throws an exception, the transaction must be rolled back and
 * the session discarded. The internal state of the Session might not be
 * consistent with the database after the exception occurs.
 * 
 * @author Rob Kooper
 * @see BeanFactory
 * 
 */
public class Session {
    private static final Resource STORAGE_TYPE_MAP_ENTRY = BeanUtil.resource("beans/storageTypeMapEntry"); //$NON-NLS-1$
    private static final Property STORAGE_TYPE_MAP_KEY   = BeanUtil.property("beans/storageTypeMapKey");  //$NON-NLS-1$
    private static final Property STORAGE_TYPE_MAP_VALUE = BeanUtil.property("beans/storageTypeMapValue"); //$NON-NLS-1$

    private static Logger         log                    = LoggerFactory.getLogger(Session.class);
    private static final String   RDF_IDX                = RDF.getURI() + "_";                            //$NON-NLS-1$
    private static int            MAXPATTERN             = 50;
    private static Method         isInitializedMethod    = null;

    private Cache                 sessionCache;
    private Set<Resource>         deletes;
    private final BeanFactory     beanFactory;
    private final BeanUtil        beanUtil;
    private TripleWriter          tw;
    private boolean               comitted;
    private boolean               rollback;

    /**
     * do a quick check to see if there is any data when reading
     * set/list/collection/map, instead of always using a proxy.
     */
    private boolean               checkNullCollection    = false;

    static {
        try {
            isInitializedMethod = UninitializedProxy.class.getMethod("isInitialized");
        } catch (SecurityException e) {
            log.warn("Could not find isInitialized method for UninitializedProxy", e);
        } catch (NoSuchMethodException e) {
            log.warn("Could not find isInitialized method for UninitializedProxy", e);
        }
    }

    public Session(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
        this.beanUtil = new BeanUtil(beanFactory);
        this.sessionCache = null;
        this.deletes = null;
        this.comitted = false;
        this.rollback = false;
    }

    /**
     * Should a check be done and set/list/collection/map be set to null if no
     * data is found. This requires a check of the context, which could be
     * expensive.
     * 
     * @return true if a check for no data needs to be done.
     */
    public boolean isCheckNullCollection() {
        return checkNullCollection;
    }

    /**
     * Should a check be done and set/list/collection/map be set to null if no
     * data is found. This requires a check of the context, which could be
     * expensive.
     * 
     * @param checkNullCollection
     *            true if a check for no data needs to be done.
     */
    public void setCheckNullCollection(boolean checkNullCollection) {
        this.checkNullCollection = checkNullCollection;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    // ----------------------------------------------------------------------
    // TRANSACTION
    // ----------------------------------------------------------------------

    /**
     * @deprecated session should be used only once.
     */
    public void reuseSession() {
        if ((sessionCache != null) && !comitted) {
            log.info("Session had transaction but not comitted.");
        }
        rollback = false;
        comitted = false;
        deletes = new HashSet<Resource>();
        tw = new TripleWriter();
    }

    public boolean hasTransaction() {
        return (deletes != null);
    }

    /**
     * Begin a unit of work.
     */
    public void beginTransaction() throws BeanException {
        if (hasTransaction()) {
            throw (new BeanException("Can only start a transaction once."));
        }
        if (sessionCache == null) {
            sessionCache = new MapCache();
        }
        deletes = new HashSet<Resource>();
        tw = new TripleWriter();
    }

    /**
     * This method will commit the underlying transaction and end the unit of
     * work.
     * 
     * @throws BeanException
     */
    public void commit() throws BeanException {
        if (!hasTransaction()) {
            log.info("Trying to commit a transaction that was not started.");
            beginTransaction();
        }
        if (comitted) {
            throw (new BeanException("Session is already comitted."));
        }
        comitted = true;

        if (beanFactory.getModel().supportsTransactions()) {
            beanFactory.getModel().begin();
        }
        writeTriples();
        if (beanFactory.getModel().supportsTransactions()) {
            beanFactory.getModel().commit();
        }
    }

    /**
     * Check if this transaction was successfully committed.
     * 
     * @return True if the transaction was (unequivocally) committed; false
     *         otherwise.
     */
    public boolean wasComitted() {
        return comitted;
    }

    /**
     * Force the underlying transaction to roll back.
     * 
     * @throws BeanException
     */
    public void rollBack() throws BeanException {
        if (!comitted) {
            throw (new BeanException("Session is not yet comitted."));
        }
        if (rollback) {
            throw (new BeanException("Session is already rolled back."));
        }

        tw.execute(beanFactory.getModel());
        rollback = true;
    }

    /**
     * Was this transaction rolled back.
     * 
     * @return True if the transaction was rolled back; false otherwise.
     */
    public boolean wasRolledBack() {
        return rollback;
    }

    /**
     * Remove this instance from the session cache. Changes to the instance will
     * not be synchronized with the context.
     * 
     * @param bean
     * @throws BeanException
     */
    public void evict(Object bean) throws BeanException {
        // FIXME implement
        throw (new BeanException("NOT YET IMPLEMENTED."));
    }

    public void close() throws BeanException {
        if (hasTransaction() && !comitted) {
            log.info("Session had transaction but not comitted.", new Exception());
        }
        sessionCache = null;
        deletes = null;
        tw = null;
    }

    // ----------------------------------------------------------------------
    // DELETE BEAN
    // ----------------------------------------------------------------------
    /**
     * Remove a persistent instance from the context. The argument may be an
     * instance associated with the receiving Session or a transient instance
     * with an identifier associated with existing persistent state. This
     * operation will NOT cascades to associated instances.
     * 
     * @param object
     *            the instance to be removed
     * @throws BeanException
     */
    public void delete(Object object) throws BeanException {
        if (!hasTransaction()) {
            beginTransaction();
        }
        deletes.add(beanUtil.getId(object));
    }

    // ----------------------------------------------------------------------
    // SAVE/MERGE/UPDATE BEAN
    // ----------------------------------------------------------------------

    /**
     * Persist the given transient instance, first assigning a generated
     * identifier. (Or using the current value of the identifier property if the
     * assigned generator is used.) This operation cascades to associated
     * instances.
     * 
     * @param object
     *            a transient instance of a persistent class
     * @return the generated identifier
     * @throws BeanException
     *             a BeanException is thrown if the object already exists in the
     *             session.
     */
    public Resource save(Object object) throws BeanException {
        return save(object, null);
    }

    /**
     * Persist the given transient instance, using the give id. This operation
     * cascades to associated instances.
     * 
     * @param object
     *            a transient instance of a persistent class
     * @param id
     *            the id of the object to save
     * @return the generated identifier
     * @throws BeanException
     *             a BeanException is thrown if the object already exists in the
     *             session.
     */
    @SuppressWarnings("unchecked")
    public Resource save(Object object, Resource id) throws BeanException {
        if (object == null) {
            return null;
        }
        if (!hasTransaction()) {
            beginTransaction();
        }

        // check if saved already
        Resource sessionid = sessionCache.firstKey(object);
        if (sessionid != null) {
            return sessionid;
        }

        // get bean info for object, throws exception if non found (such as for String)
        BeanInfo bi = BeanInfo.get(object.getClass());

        // find generate id for bean
        if (id == null) {
            id = beanUtil.getId(object);
        }

        // save bean
        sessionCache.put(id, object);
        log.trace("SAVING : " + id);

        // now save all associated instances
        for (FieldInfo fi : bi.getFields() ) {
            Object o;
            try {
                o = fi.getField().get(object);
            } catch (IllegalArgumentException e) {
                throw (new BeanException("Could not read field " + fi.getField(), e));
            } catch (IllegalAccessException e) {
                throw (new BeanException("Could not read field " + fi.getField(), e));
            }

            // null means?
            if (o == null) {
                continue;
            }

            // check for UnitializedProxy and ignore anything that was not fetched yet
            if (o instanceof UninitializedProxy) {
                if (!((UninitializedProxy) o).isInitialized()) {
                    continue;
                }
            }

            // check for array
            if (fi.getField().getType().isArray()) {
                for (Object x : ((Object[]) o) ) {
                    if (BeanInfo.isBean(x.getClass())) {
                        save(x, null);
                    }
                }
                continue;
            }

            // check for map
            if (Map.class.isAssignableFrom(fi.getField().getType())) {
                for (Entry<Object, Object> entry : ((Map<Object, Object>) o).entrySet() ) {
                    if (BeanInfo.isBean(entry.getKey().getClass())) {
                        save(entry.getKey(), null);
                    }
                    if (BeanInfo.isBean(entry.getValue().getClass())) {
                        save(entry.getValue(), null);
                    }
                }
                continue;
            }

            // check for collection
            if (Collection.class.isAssignableFrom(fi.getField().getType())) {
                for (Object x : ((Collection<Object>) o) ) {
                    if (BeanInfo.isBean(x.getClass())) {
                        save(x, null);
                    }
                }
                continue;
            }

            // check for enum
            if (fi.getField().getType().isEnum()) {
                continue;
            }

            // check for bean
            if (BeanInfo.isBean(o.getClass())) {
                save(o, null);
            }
        }

        return id;
    }

    /**
     * Copy the state of the given object onto the persistent object with the
     * same identifier. If there is no persistent instance currently associated
     * with the session, it will be loaded. Return the persistent instance. The
     * given instance does not become associated with the session. This
     * operation cascades to associated instances.
     * 
     * @param object
     *            a detached instance with state to be copied
     * @return an updated persistent instance
     * @throws BeanException
     */
    public Object merge(Object bean) throws BeanException {
        return load(bean.getClass(), save(bean));
    }

    /**
     * Update the persistent instance with the identifier of the given detached
     * instance. If there is a persistent instance with the same identifier, an
     * exception is thrown. This operation cascades to associated instances if
     * the association is mapped with cascade="save-update". Parameters: object
     * - a detached instance containing updated state Throws: HibernateException
     * 
     * @param object
     * @throws BeanException
     */
    public void update(Object object) throws BeanException {
        // FIXME implement
        throw (new BeanException("NOT YET IMPLEMENTED."));
    }

    // ----------------------------------------------------------------------
    // (RE)LOAD BEAN
    // ----------------------------------------------------------------------

    /**
     * Re-read the state of the given instance from the underlying context. It
     * is inadvisable to use this to implement long-running sessions that span
     * many business tasks. This method is, however, useful in certain special
     * circumstances. For example where a event is fired indicating an external
     * change to the object. This will refresh all instances associated with the
     * object as well.
     * 
     * @param object
     *            a persistent or detached instance
     * @throws BeanException
     */
    public void refresh(Object object) throws BeanException {
        // get the id of the object
        Resource id = beanUtil.getId(object);

        // make sure it is cached
        if ((sessionCache != null) && sessionCache.containsKey(id)) {
            load(object.getClass(), id, false, true, new HashMap<Resource, Object>());
            return;
        }
        if ((beanFactory.getCache() != null) && beanFactory.getCache().containsKey(id)) {
            load(object.getClass(), id, false, true, new HashMap<Resource, Object>());
            return;
        }
        throw (new BeanException("Object is not cached, no need to refresh."));
    }

    /**
     * Return the persistent instance of the given entity class with the given
     * identifier. (If the instance is already associated with the session,
     * return that instance.)
     */
    public <T> T load(Class<? extends T> clz, Resource id) throws BeanException {
        return load(clz, id, false);
    }

    /**
     * Return the persistent instance of the given entity class with the given
     * identifier. (If the instance is already associated with the session,
     * return that instance.) This will not return an instance that has been
     * marked as deleted.
     */
    @SuppressWarnings("unchecked")
    public <T> T load(Class<? extends T> clz, Resource id, boolean getDeleted) throws BeanException {
        Object o = load(clz, id, getDeleted, false, new HashMap<Resource, Object>());
        if (clz == null) {
            return (T) o;
        } else if (o == null) {
            return (T) o;
        } else if (clz.isInstance(o)) {
            return (T) o;
        } else {
            throw (new BeanException("Can not convert object, which is type " + o.getClass() + " to " + clz));
        }
    }

    // ----------------------------------------------------------------------
    // CREATE OBJECT FROM CONTEXT
    // ----------------------------------------------------------------------
    /**
     * Actual code to load the bean and all sub-beans from the context.
     */
    protected Object load(Class<?> clz, Resource id, boolean getDeleted, boolean refresh, Map<Resource, Object> done) throws BeanException {
        log.trace(String.format("[%s] : Reading bean.", id));

        // make sure we have a session cache, no need for transaction yet.
        if (sessionCache == null) {
            sessionCache = new MapCache();
        }

        // return previous fetched
        if (done.containsKey(id)) {
            return done.get(id);
        }

        // check cache for object
        Model model = beanFactory.getModel();
        if (!refresh && sessionCache.containsKey(id)) {
            log.trace("Found " + id + " in session cache.");
            Object o = sessionCache.get(id).object;
            done.put(id, o);
            return o;
        }
        if (!refresh && (beanFactory.getCache() != null) && beanFactory.getCache().containsKey(id)) {
            log.trace("Found " + id + " in secondary cache.");
            model = ModelFactory.createDefaultModel();
            model.add(new ArrayList<Statement>(beanFactory.getCache().get(id).statements));
        }

        // find class if not associated
        if ((clz == null) || Modifier.isAbstract(clz.getModifiers())) {
            clz = beanUtil.getClass(id);
        }

        // bean is not in cache now create the bean
        BeanInfo bi = BeanInfo.get(clz);
        Object result;
        try {
            result = bi.getBeanClass().newInstance();
        } catch (InstantiationException e) {
            throw (new BeanException(String.format("Could not create bean for %s.", id), e));
        } catch (IllegalAccessException e) {
            throw (new BeanException(String.format("Could not create bean for %s.", id), e));
        }
        done.put(id, result);
        Set<Statement> statements = new HashSet<Statement>();

        // Create the unifier
        Map<String, FieldInfo> fields = new HashMap<String, FieldInfo>();
        SparqlSelectCreator ssc = new SparqlSelectCreator();

        // limit to beans of correct type
        ssc.addPattern(id, RDF.type, bi.getType());

        // check for deleted beans
        if (!getDeleted) {
            ssc.addOptional(id, DCTerms.isReplacedBy, "?replacedBy"); //$NON-NLS-1$
        }
        ssc.addVariable("?replacedBy");//$NON-NLS-1$

        // recursively process all the fields.
        int idx = 0;
        for (FieldInfo fi : bi.getFields() ) {
            // check for array
            if (fi.getField().getType().isArray()) {
                try {
                    List<Object> list = readList(fi, id, getDeleted, refresh, done, statements, model);
                    if (list != null) {
                        Object o = Array.newInstance(fi.getField().getType().getComponentType(), list.size());
                        for (int i = 0; i < list.size(); i++ ) {
                            Array.set(o, i, list.get(i));
                        }
                        fi.getField().set(result, o);
                    } else {
                        fi.getField().set(result, null);
                    }
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                }
                continue;
            }

            // check for map
            if (Map.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readMap(fi, id, getDeleted, refresh, done, statements, model));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy map.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy map.", fi.getField()), e));
                }
                continue;
            }

            // check for list
            if (List.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readList(fi, id, getDeleted, refresh, done, statements, model));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy list.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy list.", fi.getField()), e));
                }
                continue;
            }

            // check for set
            if (Set.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readSet(fi, id, getDeleted, refresh, done, statements, model));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy set.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy set.", fi.getField()), e));
                }
                continue;
            }

            // check for collection
            if (Collection.class.isAssignableFrom(fi.getField().getType())) {
                try {
                    fi.getField().set(result, readCollection(fi, id, getDeleted, refresh, done, statements, model));
                } catch (IllegalArgumentException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                } catch (IllegalAccessException e) {
                    throw (new BeanException(String.format("Could not set '%s' to a proxy collection.", fi.getField()), e));
                }
                continue;
            }

            // fetch the rest
            String name = "?" + Integer.toHexString(idx++);
            if (fi.isInverse()) {
                ssc.addOptional(name, fi.getPredicate(), id);
            } else {
                ssc.addOptional(id, fi.getPredicate(), name);
            }
            ssc.addVariable(name);
            fields.put(name, fi);
        }

        // run the query
        Query query = ssc.getQuery();
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet results = qe.execSelect();

        // fill in the bean
        try {
            List<String> cols = ssc.getVariables();
            Map<String, Set<RDFNode>> seen = new HashMap<String, Set<RDFNode>>();
            while (results.hasNext()) {
                QuerySolution qs = results.next();

                // check deleted
                if (!getDeleted && RDF.nil.equals(qs.get("?replacedBy"))) {
                    return null;
                }

                // set fields
                for (String name : cols ) {
                    RDFNode r = qs.get(name);
                    if (r != null) {
                        FieldInfo fi = fields.get(name);
                        Set<RDFNode> rs = seen.get(name);
                        if (rs == null) {
                            rs = new HashSet<RDFNode>();
                            seen.put(name, rs);
                        } else {
                            if (rs.contains(r)) {
                                // ignore
                                continue;
                            } else if (!Map.class.isAssignableFrom(fi.getField().getType())) {
                                log.info(String.format("[%s] : Found additional values for %s.", id, fields.get(name).getField()));
                            }
                        }
                        rs.add(r);

                        // store cached value
                        if (fi.isInverse()) {
                            statements.add(ResourceFactory.createStatement(qs.getResource(name), fi.getPredicate(), id));
                        } else {
                            statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), r));
                        }

                        // set field to the new value
                        try {
                            readField(id, result, fi, r, getDeleted, refresh, done, statements, model);
                        } catch (IllegalAccessException e) {
                            log.warn(String.format("[%s] : Could not set property for name %s.", id, fields.get(name)), e);
                        }

                    }
                }
            }
        } finally {
            qe.close();
        }

        // set the subject, overwriting anything that was there.
        if (bi.getSubject() != null) {
            Field subject = bi.getSubject();
            try {
                if (subject.getType().isAssignableFrom(String.class)) {
                    subject.set(result, id.toString());
                } else if (subject.getType().isAssignableFrom(URI.class)) {
                    subject.set(result, new URI(id.toString()));
                } else if (subject.getType().isAssignableFrom(Resource.class)) {
                    subject.set(result, id);
                } else {
                    throw (new BeanException("Could not set subject field."));
                }
            } catch (IllegalArgumentException e) {
                throw (new BeanException("Could not set subject field.", e));
            } catch (IllegalAccessException e) {
                throw (new BeanException("Could not set subject field.", e));
            } catch (URISyntaxException e) {
                throw (new BeanException("Could not set subject field.", e));
            }
        }

        // store object in cache with all triples
        sessionCache.put(id, result, statements);

        return result;
    }

    /**
     * Based on the value read from the context set the value for the
     * corresponding field. This could result in getting more data being read
     * from the context in case of another bean, map, list or collection.
     * 
     * @param id
     *            the id of bean that is being populated.
     * @param obj
     *            the bean that is being populated.
     * @param field
     *            the field whose variable is to be set.
     * @param val
     *            the value for the field as retrieved from the context.
     * @param getDeleted
     *            used when getting other beans.
     * @param cache
     *            used when getting other beans.
     * @throws OperatorException
     *             throws OperatorException if the item could not be read from
     *             the context.
     * @throws OperatorException
     *             throws IllegalAccessException if the field could not be
     *             modified.
     */
    private void readField(Resource id, Object obj, FieldInfo fi, RDFNode val, boolean getDeleted, boolean refresh, Map<Resource, Object> done, Set<Statement> triples, Model model) throws BeanException, IllegalAccessException {
        // check for map
        if (Map.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING MAP FOR " + fi.getField()));
        }

        // check for list
        if (List.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING LIST FOR " + fi.getField()));
        }

        // check for collection
        if (Collection.class.isAssignableFrom(fi.getField().getType())) {
            throw (new BeanException("CAN NOT HAPPEN FETCHING COLLECTION FOR " + fi.getField()));
        }

        // check for enum
        if (fi.getField().getType().isEnum()) {
            loadEnum(fi, obj, val);
            return;
        }

        // check for bean
        Object result = loadObject(fi.getField().getType(), val, getDeleted, refresh, done);
        fi.getField().set(obj, result);
    }

    /**
     * Converts the resource to the appropriate enum value.
     * 
     * @param fi
     *            the field whose value is to be read.
     * @param obj
     *            the object whose field is to be read.
     * @param v
     *            the resource to be converted.
     * @throws IllegalAccessException
     *             throws IllegalAccessException if the map could not be read
     *             from the field.
     */
    private void loadEnum(FieldInfo fi, Object obj, RDFNode v) throws IllegalAccessException {
        String s = v.toString();

        for (Object o : fi.getField().getType().getEnumConstants() ) {
            if (o.toString().equals(s)) {
                fi.getField().set(obj, o);
                return;
            }
        }
    }

    /**
     * Return the java object mapped from the given resource. This will try and
     * load a bean if the id is a URI and clz is not Resource.
     * 
     * @param id
     *            the resource to be mapped.
     * @param clz
     *            the desired class for the object, can be null.
     * @param getDeleted
     *            should deleted beans be considered?
     * @param cache
     *            cached objects
     * @return the object either as a bean or just by converting the resource to
     *         a java object.
     * @throws BeanException
     */
    protected Object loadObject(Class<?> clz, RDFNode id, boolean getDeleted, boolean refresh, Map<Resource, Object> done) throws BeanException {
        if (id.isResource()) {
            if ((clz != null) && (Resource.class.isAssignableFrom(clz))) {
                return id;
            } else {
                return load(clz, (Resource) id, getDeleted, refresh, done);
            }
        } else if (id.isLiteral()) {
            return ((Literal) id).getValue();
        } else {
            throw (new BeanException("id is not a resource nor a literal [" + id + "]"));
        }
    }

    // ----------------------------------------------------------------------
    // SAVE SESSION
    // ----------------------------------------------------------------------
    private void writeTriples() throws BeanException {
        // delete beans
        for (Resource x : deletes ) {
            tw.add(x, DCTerms.isReplacedBy, RDF.nil);
        }

        // delete old triples
        SparqlSelectCreator ssc = new SparqlSelectCreator();
        Map<String, FieldResource> names = new HashMap<String, FieldResource>();
        for (Resource id : sessionCache.keySet() ) {
            StatmentObject to = sessionCache.get(id);
            BeanInfo bi = BeanInfo.get(to.object.getClass());

            // remove all known triples
            if (to.statements.isEmpty()) {
                for (FieldInfo fi : bi.getFields() ) {
                    ssc = updateUnifier(id, fi, ssc, names, tw);
                }

                // add to unifier
            } else {
                tw.removeAll(to.statements);
            }

            // write new triples
            to.statements.clear();
            to.statements.add(ResourceFactory.createStatement(id, RDF.type, bi.getType()));
            for (FieldInfo fi : bi.getFields() ) {
                try {
                    writeField(id, fi, to);
                } catch (IllegalAccessException e) {
                    log.warn(String.format("[%s] : Could not write property for name %s.", id, fi.getField()), e);
                }
            }
            tw.addAll(to.statements);
        }

        // run unifier to get missing values
        List<String> cols = ssc.getVariables();
        Query query = ssc.getQuery();
        QueryExecution qe = QueryExecutionFactory.create(query, beanFactory.getModel());
        try {
            ResultSet results = qe.execSelect();
            while (results.hasNext()) {
                QuerySolution qs = results.next();
                for (String name : cols ) {
                    if (qs.contains(name) && (qs.get(name) != null)) {
                        FieldResource fr = names.get(name);
                        if (fr.fi.isInverse()) {
                            tw.remove(qs.getResource(name), fr.fi.getPredicate(), fr.subj);
                        } else {
                            tw.remove(fr.subj, fr.fi.getPredicate(), qs.get(name));
                        }

                    }
                }
            }
        } finally {
            qe.close();
        }

        // write all new values and remove old values
        tw.execute(beanFactory.getModel());

        // add new values to secondary cache
        if (beanFactory.getCache() != null) {
            for (Resource key : sessionCache.keySet() ) {
                beanFactory.getCache().put(key, sessionCache.get(key));
            }
        }
    }

    private SparqlSelectCreator updateUnifier(Resource id, FieldInfo fi, SparqlSelectCreator ssc, Map<String, FieldResource> names, TripleWriter tw) throws BeanException {
        if (ssc.getPatternCount() > MAXPATTERN) {
            List<String> cols = ssc.getVariables();
            Query query = ssc.getQuery();
            QueryExecution qe = QueryExecutionFactory.create(query, beanFactory.getModel());
            try {
                ResultSet results = qe.execSelect();
                while (results.hasNext()) {
                    QuerySolution qs = results.next();
                    for (String name : cols ) {
                        if (qs.contains(name) && (qs.get(name) != null)) {
                            FieldResource fr = names.get(name);
                            if (fr.fi.isInverse()) {
                                tw.remove(qs.getResource(name), fr.fi.getPredicate(), fr.subj);
                            } else {
                                tw.remove(fr.subj, fr.fi.getPredicate(), qs.get(name));
                            }
                        }
                    }
                }
            } finally {
                qe.close();
            }

            names.clear();
            ssc = new SparqlSelectCreator();
        }

        String name = "?" + Integer.toHexString(names.size());
        if (fi.isInverse()) {
            ssc.addOptional(name, fi.getPredicate(), id);
        } else {
            ssc.addOptional(id, fi.getPredicate(), name);
        }
        ssc.addVariable(name);
        names.put(name, new FieldResource(fi, id));
        return ssc;
    }

    private void writeField(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        // don't write this field
        if (fi.isReadOnly()) {
            return;
        }

        // no data means no triples
        Object o = fi.getField().get(to.object);
        if (o == null) {
            return;
        }

        // check for UnitializedProxy and ignore anything that was not fetched yet
        if (o instanceof UninitializedProxy) {
            if (!((UninitializedProxy) o).isInitialized()) {
                return;
            }
        }

        // check for array
        if (fi.getField().getType().isArray()) {
            writeArray(id, fi, to);
            return;
        }

        // check for map
        if (Map.class.isAssignableFrom(fi.getField().getType())) {
            writeMap(id, fi, to);
            return;
        }

        // check for list
        if (List.class.isAssignableFrom(fi.getField().getType())) {
            writeList(id, fi, to);
            return;
        }

        // check for set
        if (Set.class.isAssignableFrom(fi.getField().getType())) {
            writeSet(id, fi, to);
            return;
        }

        // check for collection
        if (Collection.class.isAssignableFrom(fi.getField().getType())) {
            writeCollection(id, fi, to);
            return;
        }

        // check for enum
        if (fi.getField().getType().isEnum()) {
            writeEnum(id, fi, to);
            return;
        }

        // try writing it as a bean or literal
        writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.statements);
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeMap(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalArgumentException, IllegalAccessException {
        Map<Object, Object> objects = (Map<Object, Object>) fi.getField().get(to.object);
        for (Entry<Object, Object> entry : objects.entrySet() ) {
            Resource mapid = beanFactory.getMinter().mint();

            to.statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), mapid));
            to.statements.add(ResourceFactory.createStatement(mapid, RDF.type, STORAGE_TYPE_MAP_ENTRY));

            writeObject(mapid, STORAGE_TYPE_MAP_KEY, entry.getKey(), fi.isInverse(), to.statements);
            writeObject(mapid, STORAGE_TYPE_MAP_VALUE, entry.getValue(), fi.isInverse(), to.statements);

        }
    }

    // TODO JavaDoc
    private void writeArray(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        // create the list
        Resource lstitem = beanFactory.getMinter().mint();
        to.statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), lstitem));
        to.statements.add(ResourceFactory.createStatement(lstitem, RDF.type, RDF.Seq));

        // add all items to the list
        int idx = 1;
        Object[] objects = (Object[]) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(lstitem, ResourceFactory.createProperty(RDF_IDX + idx), o, fi.isInverse(), to.statements);
            idx++;
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeList(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        // create the list
        Resource lstitem = beanFactory.getMinter().mint();
        to.statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), lstitem));
        to.statements.add(ResourceFactory.createStatement(lstitem, RDF.type, RDF.Seq));

        // add all items to the list
        int idx = 1;
        List<Object> objects = (List<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(lstitem, ResourceFactory.createProperty(RDF_IDX + idx), o, fi.isInverse(), to.statements);
            idx++;
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeSet(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        Collection<Object> objects = (Collection<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.statements);
        }
    }

    // TODO JavaDoc
    @SuppressWarnings("unchecked")
    private void writeCollection(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        Collection<Object> objects = (Collection<Object>) fi.getField().get(to.object);
        for (Object o : objects ) {
            writeObject(id, fi.getPredicate(), o, fi.isInverse(), to.statements);
        }
    }

    // TODO JavaDoc
    private void writeEnum(Resource id, FieldInfo fi, StatmentObject to) throws BeanException, IllegalAccessException {
        String value = fi.getField().get(to.object).toString();
        writeObject(id, fi.getPredicate(), value, fi.isInverse(), to.statements);
    }

    /**
     * Either object is a bean in which case the ID is associated, or the object
     * is written as a literal. If the object is a bean it is assumed the bean
     * is written by this session or already exists.
     * 
     * @throws BeanException
     */
    private void writeObject(Resource id, Property pred, Object o, boolean inverse, Set<Statement> statements) throws BeanException {
        if (o == null) {
            return;
        }
        if (BeanInfo.isBean(o.getClass())) {
            Resource oid = sessionCache.firstKey(o);
            if (oid == null) {
                oid = beanUtil.getId(o);
            }
            if (inverse) {
                statements.add(ResourceFactory.createStatement(oid, pred, id));
            } else {
                statements.add(ResourceFactory.createStatement(id, pred, oid));
            }
        } else {
            statements.add(ResourceFactory.createStatement(id, pred, ResourceFactory.createTypedLiteral(o)));
        }
    }

    // ----------------------------------------------------------------------
    // PROXY CLASSES
    // ----------------------------------------------------------------------

    @SuppressWarnings("unchecked")
    private List<Object> readList(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Statement> statements, final Model model) {
        if (checkNullCollection) {
            String q = "";
            q += "SELECT ?x ?p ?v WHERE {\n";
            q += "  <" + id.toString() + "> <" + fi.getPredicate().toString() + "> ?x .\n";
            q += "  ?x <" + RDF.type.toString() + "> <" + RDF.Seq.toString() + "> .\n";
            q += "  ?x ?p ?v .\n";
            q += "}\n";
            q += "LIMIT 1\n";

            Query query = QueryFactory.create(q);
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            ResultSet results = qe.execSelect();
            boolean result = results.hasNext();
            qe.close();

            if (!result) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private List<Object> list = null;

            private void fetchData() {
                if (list != null) {
                    return;
                }
                list = new ArrayList<Object>();

                String q = "";
                q += "SELECT ?x ?p ?v WHERE {\n";
                q += "  <" + id.toString() + "> <" + fi.getPredicate().toString() + "> ?x .\n";
                q += "  ?x <" + RDF.type.toString() + "> <" + RDF.Seq.toString() + "> .\n";
                q += "  ?x ?p ?v .\n";
                q += "}\n";

                Query query = QueryFactory.create(q);
                QueryExecution qe = QueryExecutionFactory.create(query, model);
                ResultSet results = qe.execSelect();

                while (results.hasNext()) {
                    QuerySolution qs = results.next();

                    if ((qs.get("?v") != null) && qs.get("?v").isResource()) {
                        statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), qs.getResource("?x")));
                        statements.add(ResourceFactory.createStatement(qs.getResource("?x"), RDF.type, RDF.Seq));
                        Property p = ResourceFactory.createProperty(qs.get("?p").toString());
                        statements.add(ResourceFactory.createStatement(qs.getResource("?x"), p, qs.get("?v")));

                        if (qs.get("?v").toString().startsWith(RDF_IDX)) {
                            // make sure there is enough space in the array
                            int idx = Integer.parseInt(qs.get("?p").toString().substring(RDF_IDX.length())) - 1;
                            while (list.size() <= idx) {
                                list.add(null);
                            }

                            // get the object, either mapped from a resource or the bean
                            try {
                                Object o = loadObject(fi.getValue(), qs.getResource("?v"), getDeleted, refresh, done);
                                if (o != null) {
                                    // set the index in the array to the new value
                                    if (list.get(idx) != null) {
                                        log.info(String.format("[%s] : Replacing %d value in array for field %s.", id, idx, fi.getField()));
                                    }
                                    list.set(idx, o);
                                } else {
                                    log.warn("loadObject returned null " + qs.getResource("?v"));
                                }
                            } catch (BeanException e) {
                                log.warn("Could not fetch data for item " + qs.getResource("?v"), e);
                            }
                        }
                    }
                }

                qe.close();
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (list != null);
                }
                fetchData();
                return method.invoke(list, args);
            }
        };

        return (List<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { List.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Collection<Object> readCollection(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Statement> statements, final Model model) {
        if (checkNullCollection) {
            NodeIterator iter = model.listObjectsOfProperty(id, fi.getPredicate());
            boolean result = iter.hasNext();
            iter.close();
            if (!result) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private List<Object> list = null;

            private void fetchData() {
                if (list != null) {
                    return;
                }
                list = new ArrayList<Object>();

                NodeIterator iter = model.listObjectsOfProperty(id, fi.getPredicate());
                while (iter.hasNext()) {
                    RDFNode node = iter.next();
                    if (node instanceof Resource) {
                        statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), node));

                        try {
                            Object o = loadObject(fi.getValue(), (Resource) node, getDeleted, refresh, done);
                            if (o != null) {
                                list.add(o);
                            } else {
                                log.warn("loadObject returned null " + node.toString());
                            }
                        } catch (BeanException e) {
                            log.warn("Could not fetch data for item " + node.toString(), e);
                        }
                    }
                }
                iter.close();
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (list != null);
                }
                fetchData();
                return method.invoke(list, args);
            }
        };

        return (Collection<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Collection.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Set<Object> readSet(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Statement> statements, final Model model) {
        if (checkNullCollection) {
            NodeIterator iter = model.listObjectsOfProperty(id, fi.getPredicate());
            boolean result = iter.hasNext();
            iter.close();
            if (!result) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private Set<Object> set = null;

            private void fetchData() {
                if (set != null) {
                    return;
                }
                set = new HashSet<Object>();

                NodeIterator iter = model.listObjectsOfProperty(id, fi.getPredicate());
                while (iter.hasNext()) {
                    RDFNode node = iter.next();
                    if (node instanceof Resource) {
                        statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), node));

                        // get the object, either mapped from a resource or the bean
                        try {
                            Object o = loadObject(fi.getValue(), (Resource) node, getDeleted, refresh, done);
                            if (o != null) {
                                set.add(o);
                            } else {
                                log.warn("loadObject returned null " + node.toString());
                            }
                        } catch (BeanException e) {
                            log.warn("Could not fetch data for item " + node.toString(), e);
                        }
                    }
                }
                iter.close();
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (set != null);
                }
                fetchData();
                return method.invoke(set, args);
            }
        };

        return (Set<Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Set.class, UninitializedProxy.class }, handler);
    }

    @SuppressWarnings("unchecked")
    private Map<Object, Object> readMap(final FieldInfo fi, final Resource id, final boolean getDeleted, final boolean refresh, final Map<Resource, Object> done, final Set<Statement> statements, final Model model) {
        if (checkNullCollection) {
            String q = "";
            q += "SELECT ?x ?key ?value WHERE {\n";
            q += "  <" + id.toString() + "> <" + fi.getPredicate().toString() + "> ?x .\n";
            q += "  ?x <" + RDF.type.toString() + " " + STORAGE_TYPE_MAP_ENTRY.toString() + "> .\n";
            q += "  ?x <" + STORAGE_TYPE_MAP_KEY.toString() + "> ?key .\n";
            q += "  ?x <" + STORAGE_TYPE_MAP_VALUE.toString() + "> ?value .\n";
            q += "}\n";
            q += "LIMIT 1\n";

            Query query = QueryFactory.create(q);
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            ResultSet results = qe.execSelect();
            boolean result = results.hasNext();
            qe.close();

            if (!result) {
                return null;
            }
        }

        InvocationHandler handler = new InvocationHandler() {
            private Map<Object, Object> map = null;

            private void fetchData() {
                if (map != null) {
                    return;
                }
                map = new HashMap<Object, Object>();

                String q = "";
                q += "SELECT ?x ?key ?value WHERE {\n";
                q += "  <" + id.toString() + "> <" + fi.getPredicate().toString() + "> ?x .\n";
                q += "  ?x <" + RDF.type.toString() + " " + STORAGE_TYPE_MAP_ENTRY.toString() + "> .\n";
                q += "  ?x <" + STORAGE_TYPE_MAP_KEY.toString() + "> ?key .\n";
                q += "  ?x <" + STORAGE_TYPE_MAP_VALUE.toString() + "> ?value .\n";
                q += "}\n";

                Query query = QueryFactory.create(q);
                QueryExecution qe = QueryExecutionFactory.create(query, model);
                ResultSet results = qe.execSelect();

                while (results.hasNext()) {
                    QuerySolution qs = results.next();

                    if (qs.get("?key").isResource() && qs.get("?value").isResource()) {
                        statements.add(ResourceFactory.createStatement(id, fi.getPredicate(), qs.get("?x")));
                        statements.add(ResourceFactory.createStatement(qs.getResource("?x"), RDF.type, STORAGE_TYPE_MAP_ENTRY));
                        statements.add(ResourceFactory.createStatement(qs.getResource("?x"), STORAGE_TYPE_MAP_KEY, qs.get("?key")));
                        statements.add(ResourceFactory.createStatement(qs.getResource("?x"), STORAGE_TYPE_MAP_VALUE, qs.get("?value")));

                        try {
                            Object key = loadObject(fi.getKey(), qs.getResource("?key"), getDeleted, refresh, done);
                            Object value = loadObject(fi.getValue(), qs.getResource("?value"), getDeleted, refresh, done);
                            if (key == null) {
                                log.warn("loadObject returned null " + qs.getResource("?key"));
                            } else if (value == null) {
                                log.warn("loadObject returned null " + qs.getResource("?value"));
                            } else {
                                map.put(key, value);
                            }
                        } catch (BeanException e) {
                            log.warn("Could not fetch data for item " + qs.getResource("?key") + " or " + qs.getResource("?value"), e);
                        }
                    }
                }
                qe.close();
            }

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.equals(isInitializedMethod)) {
                    return (map != null);
                }
                fetchData();
                return method.invoke(map, args);
            }
        };

        return (Map<Object, Object>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Map.class, UninitializedProxy.class }, handler);
    }

    // ----------------------------------------------------------------------
    // HELPER CLASSES
    // ----------------------------------------------------------------------
    class FieldResource {
        public FieldInfo fi;
        public Resource  subj;

        public FieldResource(FieldInfo fi, Resource subj) {
            this.fi = fi;
            this.subj = subj;
        }
    }

    class SparqlSelectCreator {
        private List<String> variables    = new ArrayList<String>();
        private String       patterns     = "";
        private String       optional     = "";
        private String       orderby      = "";
        private int          offset       = -1;
        private int          limit        = -1;
        private int          patterncount = 0;

        public SparqlSelectCreator() {
        }

        public void addVariable(String name) {
            if (!variables.contains(name)) {
                variables.add(name);
            }
        }

        public List<String> getVariables() {
            return variables;
        }

        public void addPattern(Object s, Object p, Object o) {
            if (s instanceof RDFNode) {
                patterns += "<" + s.toString() + "> ";
            } else {
                patterns += s.toString() + " ";
            }
            if (p instanceof RDFNode) {
                patterns += "<" + p.toString() + "> ";
            } else {
                patterns += p.toString() + " ";
            }
            if (o instanceof RDFNode) {
                patterns += "<" + o.toString() + "> .\n";
            } else {
                patterns += o.toString() + " .\n";
            }
            patterncount++;
        }

        public void addOptional(Object s, Object p, Object o) {
            if (s instanceof RDFNode) {
                patterns += "<" + s.toString() + "> ";
            } else {
                patterns += s.toString() + " ";
            }
            if (p instanceof RDFNode) {
                patterns += "<" + p.toString() + "> ";
            } else {
                patterns += p.toString() + " ";
            }
            if (o instanceof RDFNode) {
                patterns += "<" + o.toString() + "> .\n";
            } else {
                patterns += o.toString() + " .\n";
            }
            patterncount++;
        }

        public int getPatternCount() {
            return patterncount;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public Query getQuery() {
            return QueryFactory.create(toString());
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT ");
            for (String v : variables ) {
                sb.append(v);
                sb.append(" ");
            }
            sb.append("WHERE {\n");
            if (!patterns.isEmpty()) {
                sb.append(patterns);
            }
            if (!optional.isEmpty()) {
                sb.append("OPTIONAL {\n");
                sb.append(patterns);
                sb.append("}\n");
            }
            sb.append("}\n");
            if (!orderby.isEmpty()) {

            }
            if (offset > 0) {
                sb.append("OFFSET ");
                sb.append(offset);
                sb.append("\n");
            }
            if (limit > 0) {
                sb.append("LIMIT ");
                sb.append(limit);
                sb.append("\n");
            }
            return sb.toString();
        }
    }

    class TripleWriter {
        Set<Statement> added   = new HashSet<Statement>();
        Set<Statement> removed = new HashSet<Statement>();

        public void add(Resource s, Property p, RDFNode o) {
            add(ResourceFactory.createStatement(s, p, o));
        }

        public void add(Statement s) {
            removed.remove(s);
            added.add(s);
        }

        public void addAll(Collection<Statement> statements) {
            removed.removeAll(statements);
            added.addAll(statements);
        }

        public void remove(Resource s, Property p, RDFNode o) {
            remove(ResourceFactory.createStatement(s, p, o));
        }

        public void remove(Statement s) {
            added.remove(s);
            removed.add(s);
        }

        public void removeAll(Collection<Statement> statements) {
            added.removeAll(statements);
            removed.addAll(statements);
        }

        public void execute(Model model) {
            model.remove(new ArrayList<Statement>(removed));
            model.add(new ArrayList<Statement>(added));
        }
    }

    public interface UninitializedProxy {
        /**
         * Simple check to see if a proxy has been initialized (i.e. fetched the
         * data from the backend). When saving the system can ignore this field
         * when trying to save the bean.
         * 
         * @return true if the proxy has been initialized, false otherwise.
         */
        boolean isInitialized();
    }
}
