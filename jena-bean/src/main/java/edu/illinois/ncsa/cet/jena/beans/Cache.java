package edu.illinois.ncsa.cet.jena.beans;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

import edu.illinois.ncsa.cet.jena.beans.Cache.StatmentObject;

public interface Cache extends Map<Resource, StatmentObject> {
    StatmentObject put(Resource key, StatmentObject object);

    StatmentObject put(Resource key, Object object);

    StatmentObject put(Resource key, Object object, Set<Statement> statements);

    Resource firstKey(Object object);

    static public class StatmentObject {
        public Set<Statement> statements;
        public Object         object;

        public StatmentObject(Object object) {
            this(object, new HashSet<Statement>());
        }

        public StatmentObject(Object object, Set<Statement> statements) {
            this.object = object;
            this.statements = statements;
        }
    }
}