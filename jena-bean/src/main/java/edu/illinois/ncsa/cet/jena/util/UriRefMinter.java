package edu.illinois.ncsa.cet.jena.util;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

public class UriRefMinter implements Minter<Resource> {
    @Override
    public Resource mint() {
        return ResourceFactory.createResource();
    }
}
