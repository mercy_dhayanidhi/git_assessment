package edu.illinois.ncsa.cet.jena.beans;


public class BeanException extends Exception {
    private static final long serialVersionUID = 1L;

    public BeanException() {
    }

    public BeanException(String msg) {
        super(msg);
    }

    public BeanException(String msg, Throwable thr) {
        super(msg, thr);
    }

    public BeanException(Throwable thr) {
        super(thr);
    }
}