package edu.illinois.ncsa.cet.jena.beans;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

import edu.illinois.ncsa.cet.jena.util.Minter;
import edu.illinois.ncsa.cet.jena.util.UriRefMinter;

public class BeanFactory {
    private Model            model;
    private Cache            cache;
    private Minter<Resource> minter;

    public BeanFactory() {
        this(null, null, null);
    }

    public BeanFactory(Model model) {
        this(model, null, null);
    }

    public BeanFactory(Model model, Cache cache) {
        this(model, cache, null);
    }

    public BeanFactory(Model model, Minter<Resource> minter) {
        this(model, null, minter);
    }

    public BeanFactory(Model model, Cache cache, Minter<Resource> minter) {
        this.model = model;
        this.cache = cache;
        if (minter != null) {
            this.minter = minter;
        } else {
            this.minter = new UriRefMinter();
        }
    }

    public Session openSession() {
        return new Session(this);
    }

    public Minter<Resource> getMinter() {
        return minter;
    }

    public void setMinter(Minter<Resource> minter) {
        this.minter = minter;
    }

    public Model getModel() {
        return model;
    }

    public void setContext(Model model) {
        this.model = model;
    }

    // ----------------------------------------------------------------------
    // CACHE OPERATIONS
    // ----------------------------------------------------------------------

    public Cache getCache() {
        return cache;
    }

    public void setCache(Cache cache) {
        this.cache = cache;
    }

    public void evict(Resource subject) throws BeanException {
        // TODO implement
        throw (new BeanException("Not implemented."));
    }

    public void evict(Object object) throws BeanException {
        // TODO implement
        throw (new BeanException("Not implemented."));
    }
}