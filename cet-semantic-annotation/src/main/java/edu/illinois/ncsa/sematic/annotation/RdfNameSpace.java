package edu.illinois.ncsa.sematic.annotation;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target({ PACKAGE, TYPE })
public @interface RdfNameSpace {
    /**
     * List of namespaces to use. Each namespace is defined as prefix=ns.
     */
    String[] ns();
}